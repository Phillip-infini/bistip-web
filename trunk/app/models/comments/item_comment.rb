# sub class of comment that represent Comment for Item
class ItemComment < Comment

  # create notification and send email if needed
  def broadcast
    item = Item.find(self.item_id)

    # always notify item owner
    if !item.owner?(self.user)
      Notifier.delay.email_item_comment(item.id, self.user, self)
      CommentNotification.create!(:receiver => item.user, :sender => self.user, :data_id => self.id)
    end
    
    # if it's a reply then notify the original comment owner
    if self.reply?
      parent = self.thread_starter
      notified = Array.new

      # let parent comment owner knows
      if !item.owner?(parent.user) and !(parent.user == self.user)
        Notifier.delay.email_item_comment_reply(self, parent.user)
        CommentReplyNotification.create!(:receiver => parent.user, :sender => self.user, :data_id => self.id)
        notified << parent.user
      end

      # reply all the previous contributor to this comment thread, except new commentator
      replies = Comment.find_comments_in_thread_and_except(parent.id, self.id)
      replies.each do |reply|
        user = reply.user

        # sned if not item owner, not commentator and has not been notified
        if (!item.owner?(user)) and (user != self.user) and (!notified.include?(user))
          Notifier.delay.email_item_comment_reply(self, user) if user.user_configuration.email_comment_reply
          CommentReplyNotification.create!(:receiver => user, :sender => self.user, :data_id => self.id)
          notified << user
        end
      end
    end
  end
  
  # find which page this comment belongs to
  def find_page
    item = Item.find(self.item_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    index = item.comments.find_all_by_reply_to_id(nil).index(comment_to_use) + 1
    ((index * 1.0)/Comment::PER_PAGE).ceil
  end

  # generate path to the comment
  def get_path
    item = Item.find(self.item_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    path_helper.item_path(item, :page => self.find_page, :anchor => comment_to_use.internal_id)
  end

  # generate path to the comment
  def get_url
    item = Item.find(self.item_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    path_helper.item_url(item, :page => self.find_page, :anchor => comment_to_use.internal_id)
  end

  # this method generate a link_to to this comment
  def get_path_link
    link_to(I18n.t('notification.field.item'), get_path)
  end

end
