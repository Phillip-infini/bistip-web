class SuggestedItemVote < ActiveRecord::Base

  scope :without_disabled_user, lambda { |suggested_item_id| {:conditions => ["users.disabled = false and suggested_item_id = ?", suggested_item_id], :include => [:user]} }
  
end
