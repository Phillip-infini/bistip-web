# sub class of Item that represent item for Seek
class SeekItem < Item

  # contants
  MAXIMUM = 8
  SUMMARY_COUNT = 2

  belongs_to :seek
  belongs_to :tip_unit
  belongs_to :price_unit

  # events
  before_validation :clean_unused_data

  validates :tip, :numericality => {:only_integer => true, :allow_nil => true, :less_than => 2000000001}
  validates :price, :numericality => {:allow_nil => true}

  # other
  attr_accessor :offer_method

  # clean tip unit if no tip given
  def clean_unused_data
    if self.offer_method == ItemOfferMethod::SELL_PRICE
      self.tip_unit = nil
      self.tip = nil
    elsif
      self.offer_method == ItemOfferMethod::TIP
      self.price_unit = nil
      self.price = nil
    else
      self.tip_unit = nil
      self.tip = nil
      self.price_unit = nil
      self.price = nil
    end
  end

  # tip display with unit append to it
  def tip_with_unit
    unless self.tip.blank?
      if self.tip_unit.before_number?
        number_to_currency(self.tip.to_s, :unit => (self.tip_unit.symbol + ' '), :precision => 0)
      else
        self.tip.to_s + ' ' + self.tip_unit.symbol
      end
    end
  end

  # price display with unit append to it
  def price_with_unit
    unless self.price.blank?
      number_to_currency(self.price.to_s, :unit => (self.price_unit.symbol + ' '), :precision => 0)
    end
  end

  # do soft delete on item
  def destroy
    update_attribute(:deleted, true)
  end

  def user
    self.seek.user
  end

  def edit_path
    if !self.seek_id.blank?
      path_helper.edit_seek_path(self.seek)
    end
  end

  # friendly URL for SEO
  def to_param
    if self.offer_method == ItemOfferMethod::TIP
      I18n.t('item.show.tip_url', :id => id, :flag => TrackLink::BS_V1, :item => name, :tip => tip_with_unit).parameterize
    else
      I18n.t('item.show.price_url', :id => id, :flag => TrackLink::BS_V1, :item => name, :price => price_with_unit).parameterize
    end
  end

  def desc_string
    if self.offer_method == ItemOfferMethod::TIP
      I18n.t('item.show.tip_desc', :item => name, :tip => tip_with_unit)
    else
      I18n.t('item.show.price_desc', :item => name, :price => price_with_unit)
    end
  end

  # override offer method accessor
  def offer_method
    if @offer_method.blank?
      if !self.tip.blank? and !self.tip_unit.blank?
        ItemOfferMethod::TIP
      elsif !self.price.blank? and !self.price_unit.blank?
        ItemOfferMethod::SELL_PRICE
      end
    else
      @offer_method
    end
  end

  def parent
    self.seek
  end

end