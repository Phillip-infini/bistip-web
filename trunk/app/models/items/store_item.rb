class StoreItem < Item
  include Location

  # scope
  scope :active_only, lambda { {:conditions => ["deleted = ?", false]} }
  scope :name_match_and_not_id_and_not_origin_city_id, lambda { |name, id, origin_city_id, limit| {:conditions => ["match(name) against(?) and id <> ? and origin_city_id <> ?", name, id, origin_city_id], :limit => limit} }
  scope :name_contains, lambda { |keyword| {:conditions => ["match(suggested_items.name) against(?)", keyword]} }
  
  # restriction
  attr_readonly :name
  attr_readonly :origin_city

  # constant
  NAME_MAX_LENGTH = 50
  NAME_MIN_LENGTH = 3
  REASON_MAX_LENGTH = 400
  REASON_MIN_LENGTH = 10
  NOTES_MIN_LENGTH = 100
  NOTES_MAX_LENGTH = 600
  PICTURES_MAXIMUM = 3
  TOTAL_SHOW_ON_SUMMARY = 10
  SEARCH_PER_PAGE = 10
  SHOW_PER_PAGE = 10
  COMP_START = DateTime.new(2012, 11, 15)
  COMP_END = DateTime.new(2012, 12, 20)
  VOTING_PER_PAGE = 100
  PRICE_MIN_LENGTH = 10000
  SHOW_PATH = "store_items/show"

  # validation
  validates :notes, :length => {:minimum => NOTES_MIN_LENGTH, :maximum => NOTES_MAX_LENGTH}
  validates :user, :presence => true
  validates :origin_city, :origin_city => true
  validates :price, :presence => true, :numericality => {:allow_nil => true, :greater_than_or_equal_to => PRICE_MIN_LENGTH}
  validates :pictures, :presence => true

  # associations
  belongs_to :user
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'

  # events
  before_create :set_approved_and_price_unit

  # friendly URL for SEO
  def to_param
    city = origin_city
    label_city = city.all_cities? ? city.country.name : city.name

    I18n.t('store_item.show.url', :id => id, :flag => TrackLink::BS_V1, :item => name, :location => label_city).parameterize
  end

  # check if item suggestion has a picture
  def has_picture?
    self.pictures.count > 0
  end

  # check if the suggested has been approved or not
  def active?
    !self.deleted? and self.approved?
  end

  # get name for display that will shorten if it's too long
  def name_short
    self.name.length > 20 ? self.name[0..20] + '...' : self.name
  end

  # get name for display that will shorten if it's too long
  def name_mini
    self.name.length > 12 ? self.name[0..12] + '...' : self.name
  end

  def is_origin_europe?
    if self.origin_city.country.continent.code == 'eu'
      true
    else
      false
    end
  end

  # do soft delete on item
  def destroy
    update_attribute(:deleted, true)
  end

  # return the renderer to use for this item
  def show_path
    SHOW_PATH
  end

  # set approved and price unit
  def set_approved_and_price_unit
    self.approved = false
    self.price_unit_id = PriceUnit.find_by_symbol('IDR').id
  end

  # price display with unit append to it
  def price_with_unit
    unless self.price.blank?
      number_to_currency(self.price.to_s, :unit => (self.price_unit.symbol + ' '), :precision => 0)
    end
  end

end
