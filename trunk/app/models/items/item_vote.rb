class ItemVote < ActiveRecord::Base

  # associations
  belongs_to :user
  belongs_to :item

  scope :without_disabled_user, lambda { |item_id| {:conditions => ["users.disabled = false and item_id = ?", item_id], :include => [:user]} }
  
end
