require 'digest'

# Affiliate class
class Affiliate < ActiveRecord::Base

  AFFILIATE_CODE_KEY = 'bistip_affcode'

  attr_accessor :password

  has_many :programs, :class_name => "AffiliateProgram", :foreign_key => 'affiliate_id'
  has_many :payments, :class_name => "AffiliatePayment", :foreign_key => 'affiliate_id'

  validates :email,
    :uniqueness => true,
    :length => { :within => 5..100 },
    :format => { :with => /^[^@][\w.-]+@[\w.-]+[.][a-z]{2,4}$/i}

  validates :password,
    :presence => true, :if => :password_required?,
    :length => { :within => 4..25 }

  validates :name, :presence => true, :length => { :maximum => 100 }

  # events
  before_create :generate_code, :generate_salt
  before_save :encrypt_new_password
  
  # method to populate affiliate code for new account
  def generate_code
    self.code = ActiveSupport::SecureRandom.hex(4)
  end

  # method to populate salt for new account
  def generate_salt
    self.salt = ActiveSupport::SecureRandom.base64(8)
  end

  # static method to handle authentication
  def self.authenticate(email, password)
    affiliate = find_by_email(email)
    return affiliate if affiliate && affiliate.authenticated?(password)
  end

  # authenticate user with salt
  def self.authenticate_with_salt(affiliate_id, salt)
    affiliate = Affiliate.find(affiliate_id)
    if affiliate and affiliate.salt == salt
      affiliate
    else
      nil
    end
  end

  # check method to see whether a password is a match
  def authenticated?(password)
    self.hashed_password == encrypt(password)
  end

  protected
    
    # method to encrypt password before store it to database
    def encrypt_new_password
      return if password.blank?
      self.hashed_password = encrypt(password)
    end

    # check method to help trigger password validation
    def password_required?
      hashed_password.blank? || password.present?
    end

    # helper to encrypt password
    def encrypt(string)
      Digest::SHA1.hexdigest(string)
    end

end
