class SeekVote < ActiveRecord::Base

  # associations
  belongs_to :user
  belongs_to :seek

  scope :without_disabled_user, lambda { |seek_id| {:conditions => ["users.disabled = false and seek_id = ?", seek_id], :include => [:user]} }

end
