class UserRole < ActiveRecord::Base
  
  ADMIN = 'Admin'

  # equality for user
  def ==(other)
    return self.id == other.id
  end

  def self.admin_role
    UserRole.find_by_name(ADMIN)
  end
end
