class SellerOutOfStockStoreOrderNotification < Notification

  # override to_link method to perform specified action of NewStoreOrderNotification
  def to_link(current_user)
    store_order = StoreOrder.find(data_id)
    link = I18n.t("notification.title.seller_out_of_stock",
      :seller => store_order.buyer.username,
      :item => store_order.order_items.first.item.name,
      :link => link_to(I18n.t("general.click_here"), path_helper.market_path))
    return link
  end
end
