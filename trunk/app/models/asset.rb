# model that represents message attachments
class Asset < ActiveRecord::Base

  LINK_AGE = 3600

  belongs_to :message
  has_attached_file :attachment, :styles => lambda{ |a| ['image/gif', 'image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg', 'application/octet-stream', 'application/pdf'].include?( a.content_type ) ? { :original => "600x600>", :thumb => "48x48>" } : {}},
                             :storage => :s3,
                             :s3_credentials => "#{Rails.root}/config/s3.yml",
                             :s3_permissions => :private,
                             :path => "/:attachment/:id/:style/:filename"

  validates_attachment_content_type :attachment, :content_type => ['image/gif', 'image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg', 'application/octet-stream', 'application/pdf'],
                                             :message => 'must be image'

  validates_attachment_size :attachment, :less_than => 2.megabytes

  scope :find_all_by_ids, lambda {|ids| {:conditions => ["message_id IN (?)", ids]}}
end
