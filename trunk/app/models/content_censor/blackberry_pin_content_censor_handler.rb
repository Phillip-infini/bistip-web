# handler for Email Content Censor
class BlackberryPinContentCensorHandler
  include ContentCensorChainable

  # set next in chain
  def initialize(link = nil)
    next_in_chain(link)
  end

  # try to handle the request
  def handle(content)
    content.gsub!(/[1-9]{1}[0-9a-zA-Z]{7}/, I18n.t("message.label.censored.bb_pin"))
    if @next
      @next.handle(content)
    end
  end
 
end