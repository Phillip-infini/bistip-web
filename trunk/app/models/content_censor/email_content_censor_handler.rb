# handler for Email Content Censor
class EmailContentCensorHandler
  include ContentCensorChainable

  # set next in chain
  def initialize(link = nil)
    next_in_chain(link)
  end

  # try to handle the request
  def handle(content)
    content.gsub!(/[\w.-]+(@|at|\(at\))[\w.-]+[.][a-z]{2,4}/, I18n.t("message.label.censored.email"))
    if @next
      @next.handle(content)
    end
  end
 
end