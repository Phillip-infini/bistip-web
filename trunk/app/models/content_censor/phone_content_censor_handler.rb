# handler for Email Content Censor
require 'oniguruma'

class PhoneContentCensorHandler
  include ContentCensorChainable

  # set next in chain
  def initialize(link = nil)
    next_in_chain(link)
  end

  # try to handle the request
  def handle(content)
    content.gsub!(/\+[0-9\s]{6,17}/, I18n.t("message.label.censored.phone"))
    content.gsub!(/0[0-9\s]{6,17}/, I18n.t("message.label.censored.phone"))
    content.gsub!(/[0-9\s]{5,14}[1-9][1-9][1-9]/, I18n.t("message.label.censored.phone"))
    if @next
      @next.handle(content)
    end
  end
 
end
