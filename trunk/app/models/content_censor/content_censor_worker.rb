# handler for Email Content Censor
class ContentCensorWorker

  CENSOR_CHAIN_HANDLER = EmailContentCensorHandler.new(PhoneContentCensorHandler.new(BlackberryPinContentCensorHandler.new))

  AUTO_LINK_RE = %r{
            (?: ((?:ed2k|ftp|http|https|irc|mailto|news|gopher|nntp|telnet|webcal|xmpp|callto|feed|svn|urn|aim|rsync|tag|ssh|sftp|rtsp|afs):)// | www\. )
            [^\s<]+
          }x

  MARKER = "B1STLM8"

  def self.process(content)
    #url_maps = Hash.new
    #take_out_url(content, url_maps)
    #CENSOR_CHAIN_HANDLER.handle(content)
    #repopulate_url(content, url_maps)
    content
  end

  def self.take_out_url(content, url_maps)
    index = 1
    content.gsub!(AUTO_LINK_RE) do
      index = index + 1
      key = "##{MARKER}#{index}"
      url_maps[key] = $&
      key
    end
  end

  def self.repopulate_url(content, url_maps)
    if !url_maps.blank?
      url_maps.each do |key, value|
        content.gsub!(Regexp.new(key.to_s), value)
      end
    end
    content
  end

end