class SellerReceivedPayoutLog < EscrowLog

  def broadcast
    if !self.escrow.for_store?
      super
    else
      EscrowLogNotification.create!(:data_id => self.id, :sender => escrow.seller, :receiver => escrow.buyer)
      Notifier.delay.email_store_escrow_log(escrow.buyer, self)
    end
  end

  def to_notification_link(current_user)
    if !self.escrow.for_store?
      super
    else
      log_state = Escrow.get_log_state(self)
      escrow = self.escrow
      store_order = escrow.store_order

      path_to_go = path_helper.dashboard_my_shopping_path(:anchor => store_order.internal_id)

      # generate path to the current user message. Current user must be the recipient
      transaction = link_to(store_order.internal_id, path_to_go)
      body = I18n.t("notification.title.escrow.#{log_state}",
        :seller => link_to(escrow.seller.username, path_helper.profile_path(escrow.seller.username)),
        :buyer => link_to(escrow.buyer.username, path_helper.profile_path(escrow.buyer.username)))

      whole = I18n.t("notification.title.transaction",
        :transaction => transaction,
        :body => body)

      return whole
    end
  end
  
end
