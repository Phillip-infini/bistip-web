class AddForStoryLog < PointLog

  # do operation on point given
  def do_point(amount)
    amount + self.amount
  end

  # generate title label
  def title_label
    story = object
    title = story.title.blank? ? I18n.t('stories.general.no_title') : story.title
    I18n.t('dashboard.points.label.story_title', :title => link_to(title, path_helper.story_path(story)))
  end

  # generate point label
  def point_label
    I18n.t('general.points_plus', :points => self.amount)
  end

  def object
    Story.find(self.data_id)
  end

end
