# class that wrap an activity in Bistip
class StoryActivityWrap < ActivityWrap
  CNAME = 'story_activity_wrap'

  def cname
    CNAME
  end
end
