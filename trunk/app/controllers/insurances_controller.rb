class InsurancesController < ApplicationController
  before_filter :authenticate, :only => [:new, :create]

  def new
    message_id = params[:message_id]
    seller_id = params[:seller_id]
    session[:insurance_params] ||= {}

    if message_id
      # build new insurance object
      message = Message.find(message_id)

      # check if message already has a active insurance
      unless message.insurance.blank?
        redirect_to :back
      else
        session[:insurance_params] = {}
        session[:insurance_step] = nil
        @insurance = Insurance.new
        @insurance.buyer = current_user
        @insurance.seller = User.find(seller_id)
        @insurance.current_step = session[:insurance_step]
        @insurance.message = message
        @insurance.currency = Currency.find_by_name('IDR');
      end
    elsif session[:insurance_params]
      # in the middle of wizard, build insurance from parameter
      @insurance = Insurance.new(session[:insurance_params])
      @insurance.current_step = session[:insurance_step]
    else
      # not valid, redirect back then
      redirect_to :back
    end
  end

  def create

    # build the insurance data
    session[:insurance_params].deep_merge!(params[:insurance]) if params[:insurance]
    @insurance = Insurance.new(session[:insurance_params])
    @insurance.current_step = session[:insurance_step]

    # is insurance valid for the current step?
    if @insurance.valid?
      if params[:back_button]
        @insurance.previous_step
      elsif @insurance.last_step?
        # insurance is valid, save the insurance and create the initiate log
        if @insurance.all_valid?
          @insurance.save
          BuyerAppliedInsuranceLog.create!(:insurance_id => @insurance.id)
        end
      else
        @insurance.next_step
      end
      session[:insurance_step] = @insurance.current_step
    else
      put_model_errors_to_flash(@insurance.errors)
    end

    # if insurance still have not been saved yet, that means we still need to render new form
    if @insurance.new_record?
      render "new"
    else
      # insurance initiate done, redirect to paypal
      session[:insurance_step] = session[:insurance_params] = nil
      redirect_to message_path(@insurance.message)
    end
  end

end
