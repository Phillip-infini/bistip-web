class StoreOrdersController < ApplicationController

  # must be logged in
  before_filter :authenticate, :only => [:new, :create, :quote_delivery, :create_quote_delivery]

  # display a idea item new form
  def new
    item_id = params[:item_id]
    seller_username = params[:seller]
    session[:store_order_params] ||= {}

    if item_id and seller_username
      
      buyer = current_user
      seller = User.find_by_username(seller_username)
      item = Item.find(item_id)

      # check if all parameter for store order is valid
      if item.blank? or buyer.blank? or seller.blank? or buyer == seller or item.user != seller
        redirect_to market_path
      else
        session[:store_order_params] = {}
        session[:store_order_step] = nil
        @store_order = StoreOrder.new
        @store_order.buyer = buyer
        @store_order.seller = seller

        store_order_item = StoreOrderItem.new(:item_id => item.id, :quantity => StoreOrderItem::DEFAULT_QUANTITY, :price => item.price)
        @store_order.order_items << store_order_item
      end
    elsif session[:store_order_params]
      # in the middle of wizard, build escrow from parameter
      @store_order = StoreOrder.new(session[:store_order_params])
      @store_order.current_step = session[:store_order_step]
    else
      # not valid, redirect back then
      redirect_to :back
    end
  end

  # create store order, handle wizard form submission of store order
  def create

    # build the store order data
    session[:store_order_params].deep_merge!(params[:store_order]) if params[:store_order]
    @store_order = StoreOrder.new(session[:store_order_params])
    @store_order.current_step = session[:store_order_step]

    # is store_order valid for the current step?
    if @store_order.valid?
      if params[:back_button]
        @store_order.previous_step
      elsif @store_order.last_step?
        # store_order is valid, save the store_order and create the initiate log
        if @store_order.all_valid?
          @store_order.save
          NewStoreOrderNotification.create(:data_id => @store_order.id, :sender => @store_order.buyer, :receiver => @store_order.seller)
        end
      else
        @store_order.next_step
      end
      session[:store_order_step] = @store_order.current_step
    else
      put_model_errors_to_flash(@store_order.errors)
    end

    # if store_order still have not been saved yet, that means we still need to render new form
    if @store_order.new_record?
      render "new"
    else
      # store_order initiate done
      flash[:longer_notice] = t('store_order.create.message.success')
      session[:store_order_step] = session[:store_order_params] = nil
      redirect_to item_path(@store_order.order_items.first.item)
    end
  end

  def quote_delivery
    @store_order = StoreOrder.find_by_id_and_seller_id(params[:id], current_user.id)
    if @store_order.blank?
      redirect_to dashboard_store_orders_path
    elsif !@store_order.delivery_fee.blank? or (!@store_order.delivery_fee.blank? and @store_order.delivery_fee > 0)
      flash[:longer_notice] = t('store_order.quote_delivery.message.already_exist')
      redirect_to dashboard_store_orders_path(:anchor => @store_order.internal_id)
    end
  end

  def create_quote_delivery
    id = params[:id]
    delivery_fee = params[:delivery_fee]

    @store_order = StoreOrder.find(id)
    @store_order.delivery_fee = delivery_fee

    if delivery_fee.blank?
      flash[:longer_notice] = t('store_order.quote_delivery.message.delivery_fee_blank')
      render :action => "quote_delivery"
    elsif @store_order.save
      Notifier.delay.email_total_store_order(@store_order)
      DeliveryQuotedStoreOrderNotification.create(:data_id => @store_order.id, :sender => @store_order.seller, :receiver => @store_order.buyer)
      flash[:longer_notice] = t('store_order.quote_delivery.message.success')
      redirect_to dashboard_store_orders_path(:anchor => @store_order.internal_id)
    else
      put_model_errors_to_flash(@store_order.errors)
      render :action => "quote_delivery"
    end
  end

  def seller_out_of_stock
    id = params[:id]

    @store_order = StoreOrder.find(id)

    if @store_order.update_attribute(:seller_out_of_stock, true)
      Notifier.delay.email_seller_out_of_stock(@store_order)
      SellerOutOfStockStoreOrderNotification.create(:data_id => @store_order.id, :sender => @store_order.seller, :receiver => @store_order.buyer)
      flash[:longer_notice] = t('store_order.seller_out_of_stock.message.success')
      redirect_to dashboard_store_orders_path(:anchor => @store_order.internal_id)
    else
      put_model_errors_to_flash(@store_order.errors)
      redirect_to :back
    end
  end

  def view
    id = params[:id]
    store_order = StoreOrder.find(id)
    if current_user == store_order.buyer
      redirect_to dashboard_my_shopping_path(:anchor => store_order.internal_id)
    elsif current_user == store_order.seller
      redirect_to dashboard_store_orders_path(:anchor => store_order.internal_id)
    else
      redirect_to dashboard_path
    end
  end
  
end
