class SeekVotesController < ApplicationController

  before_filter :authenticate, :only => [:new]

  def new
    seek = Seek.find(params[:seek_id])

    # owner cannot vote for himself
    if current_user == seek.user
      flash[:notice] = t('seek.new.not_self')
    # check if vote already exist
    elsif SeekVote.where(:user_id => current_user.id, :seek_id => seek.id).count < 1
      vote = SeekVote.new
      vote.seek = seek
      vote.user = current_user
      if vote.save
        Notifier.delay.email_seek_vote(vote)
        flash[:notice] = t('seek_vote.new.success', :item => seek.name)
      else
        flash[:notice] = t('seek_vote.new.failure')
      end
    else
      flash[:notice] = t('seek_vote.new.already_exist', :item => seek.name)
    end

    if params[:comp]
      redirect_to event_seek_voting_path
    else
      redirect_to seek_path(seek)
    end
  end

end
