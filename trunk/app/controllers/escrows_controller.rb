# class to handle operations for escrow account
class EscrowsController < ApplicationController
  
  ssl_required :new,
    :create,
    :redirect_paypal,
    :redirect_indonesia_bank_transfer,
    :seller_got_item,
    :buyer_received_item,
    :seller_cancelled,
    :seller_requested_payout,
    :seller_send_item,
    :buyer_resumed,
    :buyer_requested_payout,
    :seller_received_payout,
    :buyer_received_payout
  ssl_allowed :calculate_amount_after_cost, :history
  before_filter :authenticate, :only => [:new, 
    :create,
    :redirect_paypal,
    :redirect_indonesia_bank_transfer,
    :seller_got_item,
    :buyer_received_item,
    :seller_cancelled,
    :seller_requested_payout,
    :seller_send_item,
    :buyer_resumed,
    :buyer_requested_payout,
    :seller_received_payout,
    :buyer_received_payout]
  before_filter :load_escrow, :except => [:new, :create, :calculate_amount_after_cost]
  after_filter :create_notice, :except => [:new, :create, :calculate_amount_after_cost, :history]
  
  # new escrow form
  def new
    # escrow must have seller and message reference
    seller_id = params[:seller_id]
    message_id = params[:message_id]
    session[:escrow_params] ||= {}

    if seller_id and message_id
      # build new escrow object
      message = Message.find(message_id)

      # check if message already has a active escrow
      unless message.escrow.blank?
        redirect_to :back
      else
        session[:escrow_params] = {}
        session[:escrow_step] = nil
        @escrow = Escrow.new
        @escrow.buyer = current_user
        @escrow.seller = User.find(seller_id)
        @escrow.current_step = session[:escrow_step]
        @escrow.message = message
      end
    elsif session[:escrow_params]
      # in the middle of wizard, build escrow from parameter
      @escrow = Escrow.new(session[:escrow_params])
      @escrow.current_step = session[:escrow_step]
    else
      # not valid, redirect back then
      redirect_to :back
    end
  end

  # create escrow, handle wizard form submission of escrow
  def create

    # build the escrow data
    session[:escrow_params].deep_merge!(params[:escrow]) if params[:escrow]
    @escrow = Escrow.new(session[:escrow_params])
    @escrow.current_step = session[:escrow_step]

    # is escrow valid for the current step?
    if @escrow.valid?
      if params[:back_button]
        @escrow.previous_step
      elsif @escrow.last_step?
        # escrow is valid, save the escrow and create the initiate log
        @escrow.save if @escrow.all_valid?
        BuyerInitiatedLog.create!(:escrow_id => @escrow.id)
      else
        @escrow.next_step
      end
      session[:escrow_step] = @escrow.current_step
    else
      put_model_errors_to_flash(@escrow.errors)
    end

    # if escrow still have not been saved yet, that means we still need to render new form
    if @escrow.new_record?
      render "new"
    else
      # escrow initiate done, redirect to paypal
      session[:escrow_step] = session[:escrow_params] = nil
      redirect_to @escrow.payment_method.get_redirect_path(@escrow)
    end
  end

  # redirect to paypal payment page
  def redirect_paypal
    unless @escrow.is_buyer_or_seller?(current_user)
      redirect_to root_path
    end
  end

  # redirect to indonesia bank transfer page
  def redirect_indonesia_bank_transfer
    unless @escrow.is_buyer_or_seller?(current_user)
      redirect_to root_path
    end
  end

  # return the amount after cost
  def calculate_amount_after_cost
    amount = params[:amount]
    @payment_method = PaymentMethod.find(params[:payment_method_id])

    unless amount.blank? and @payment_method.blank?
      @amount_after_cost = @payment_method.calculate_amount_after_cost(amount)
    else
      @amount_after_cost = 0
    end
  end
  
  # create seller_got_item log when escrow's current state is paypal_notify
  def seller_got_item
    if current_user == @escrow.seller and 
        (@escrow.current_state.eql?(Escrow::STATE_PAYPAL_NOTIFIED) or 
          @escrow.current_state.eql?(Escrow::STATE_BUYER_RESUMED) or
          @escrow.current_state.eql?(Escrow::STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER))
      SellerGotItemLog.create!(:escrow_id => params[:escrow_id]) 
      redirect_to message_path(@escrow.message.id)
    else
      redirect_to root_path
    end
  end

  # create seller_send_item log when escrow's current state is paypal_notify
  def seller_send_item
    if current_user == @escrow.seller and
       !@escrow.store_order.blank?
        (@escrow.current_state.eql?(Escrow::STATE_PAYPAL_NOTIFIED) or
          @escrow.current_state.eql?(Escrow::STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER))
      SellerSendItemLog.create!(:escrow_id => params[:escrow_id])
      redirect_to dashboard_store_orders_path(:anchor => @escrow.store_order.internal_id)
    else
      redirect_to root_path
    end
  end
  
  #create buyer_received_item log when escrow's current state is seller_got_item
  def buyer_received_item
    if current_user == @escrow.buyer and
        (@escrow.current_state.eql?(Escrow::STATE_PAYPAL_NOTIFIED) or
          @escrow.current_state.eql?(Escrow::STATE_BUYER_RESUMED) or
          @escrow.current_state.eql?(Escrow::STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER) or
          @escrow.current_state.eql?(Escrow::STATE_SELLER_GOT_ITEM) or
          @escrow.current_state.eql?(Escrow::STATE_SELLER_SEND_ITEM))
      # this action must come with a review
      review = get_review

      if review and review.save
        # link review to escrow
        @escrow.update_attribute(:buyer_review_id, review.id)
        BuyerReceivedItemLog.create!(:escrow_id => params[:escrow_id])
      else
        session[:review] = review
        put_model_errors_to_flash(review.errors, 'redirect')
      end

      if @escrow.store_order.blank?
        redirect_to message_path(@escrow.message.id)
      else
        redirect_to dashboard_my_shopping_path(:anchor => @escrow.store_order.internal_id)
      end
    else
      redirect_to root_path
    end
  end
  
  #create seller_cancel log when escrow's current state is paypal_notify
  def seller_cancelled
    if current_user == @escrow.seller and
        (@escrow.current_state.eql?(Escrow::STATE_PAYPAL_NOTIFIED) or
         @escrow.current_state.eql?(Escrow::STATE_BUYER_RESUMED) or
         @escrow.current_state.eql?(Escrow::STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER))
      SellerCancelledLog.create!(:escrow_id => params[:escrow_id])
      redirect_to message_path(@escrow.message.id)
    else
      redirect_to root_path
    end
  end
  
  #create seller_request_payout log when escrow's current state is buyer_received_item
  def seller_requested_payout
    if current_user == @escrow.seller and @escrow.current_state.eql?(Escrow::STATE_BUYER_RECEIVED_ITEM)
      # this action must come with a review
      review = get_review

      if review and review.save
        # link review to escrow
        @escrow.update_attribute(:seller_review_id, review.id)
        SellerRequestedPayoutLog.create!(:escrow_id => params[:escrow_id])
      else
        session[:review] = review
        put_model_errors_to_flash(review.errors, 'redirect')
      end
      
      if !current_user.has_payout_method?
        redirect_to payout_methods_path
      else
        if @escrow.store_order.blank?
          redirect_to message_path(@escrow.message.id)
        else
          redirect_to dashboard_my_store_path(:anchor => @escrow.store_order.internal_id)
        end
      end
    else
      redirect_to root_path
    end
  end

  #create seller_received_payout log when escrow's current state is bistip_released_seller_payout
  def seller_received_payout
    if current_user == @escrow.seller and @escrow.current_state.eql?(Escrow::STATE_BISTIP_RELEASED_SELLER_PAYOUT)
      SellerReceivedPayoutLog.create!(:escrow_id => params[:escrow_id])
      if @escrow.store_order.blank?
        redirect_to message_path(@escrow.message.id)
      else
        redirect_to dashboard_my_store_path(:anchor => @escrow.store_order.internal_id)
      end
    else
      redirect_to root_path
    end
  end
  
  #create buyer_resume log when escrow's current state is seller_cancel
  def buyer_resumed
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_SELLER_CANCELLED
      BuyerResumedLog.create!(:escrow_id => params[:escrow_id])
      redirect_to message_path(@escrow.message.id)
    else
      redirect_to root_path
    end
  end
  
  #create buyer_request_payout log when escrow's current state is seller_cancel
  def buyer_requested_payout
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_SELLER_CANCELLED
      BuyerRequestedPayoutLog.create!(:escrow_id => params[:escrow_id])
      if !current_user.has_payout_method?
        redirect_to payout_methods_path
      else
        if @escrow.store_order.blank?
          redirect_to message_path(@escrow.message.id)
        else
          redirect_to dashboard_my_shopping_path(:anchor => @escrow.store_order.internal_id)
        end
      end
    else
      redirect_to root_path
    end
  end

  #create buyer_received_payment log when escrow's current state is bistip_released_buyer_payout
  def buyer_received_payout
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_BISTIP_RELEASED_BUYER_PAYOUT
      BuyerReceivedPayoutLog.create!(:escrow_id => params[:escrow_id])
      if @escrow.store_order.blank?
        redirect_to message_path(@escrow.message.id)
      else
        redirect_to dashboard_my_shopping_path(:anchor => @escrow.store_order.internal_id)
      end
    else
      redirect_to root_path
    end
  end

  def history
    redirect_to root_path unless current_user == @escrow.buyer or current_user == @escrow.seller

    @upcoming = nil
    last_log_class = @escrow.last_log.class
    if Escrow::SUCCESS_FLOW.flatten.include? last_log_class
      index = find_index_in_flow(Escrow::SUCCESS_FLOW, last_log_class)
      @upcoming = Escrow::SUCCESS_FLOW.drop(index + 1)
    elsif Escrow::CANCEL_FLOW.flatten.include? last_log_class
      index = find_index_in_flow(Escrow::CANCEL_FLOW, last_log_class)
      @upcoming = Escrow::CANCEL_FLOW.drop(index + 1)
    end

    render :layout => false unless mobile_version?
  end

  private

    def find_index_in_flow(flow, log_class)
      result = nil
      flow.each_with_index do |log, index|
        if log.is_a? Array and log.include?(log_class)
          result = index
        elsif log == log_class
          result = index
        end
      end
      result
    end
  
    def load_escrow
      @escrow = Escrow.find(params[:escrow_id])
    end

    def create_notice
      if !@escrow.blank? and flash.now[:model_alert].blank? and flash[:model_alert].blank?
        flash[:escrow] = t("escrow.message.success.#{@escrow.current_state}", :buyer => @escrow.buyer.username, :seller => @escrow.seller.username)
      end
    end

    # build a review from param
    def get_review
      type = params[:review_type].blank? ? PositiveReview : params[:review_type].constantize
      review = type.new
      review.body = params[:review_body]
      review.giver = current_user
      review.receiver = @escrow.not_this_user(current_user)
      review.verified = true
      review
    end
end
