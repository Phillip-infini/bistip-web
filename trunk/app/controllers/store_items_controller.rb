class StoreItemsController < ApplicationController
  
  # must be logged in
  before_filter :authenticate, :only => [:new, :create, :edit, :update]
  before_filter :check_rest_password, :only => [:approve]

  # display a idea item new form
  def new
    @store_item = StoreItem.new
    StoreItem::PICTURES_MAXIMUM.times { @store_item.pictures.build }
  end

  # accept form submission to create idea item
  def create
    # associate suggested item with the current user
    @store_item = StoreItem.new(params[:store_item])
    @store_item.user = current_user
    
    if @store_item.save
      flash[:notice] = t('store_item.create.message.success')
      ContactUsNotifier.delay.email_store_item_approval(@store_item)
      redirect_to item_path(@store_item)
    else
      # if there is form error, upload picture will be gone so lets clean the assets and rebuild
      flash.now[:alert] = t('general.reupload_picture') unless @store_item.pictures.blank?
      @store_item.pictures.clear
      IdeaItem::PICTURES_MAXIMUM.times { @store_item.pictures.build }
      puts @store_item.errors
      put_model_errors_to_flash(@store_item.errors)
      render :action => "new"
    end
  end

  # display a idea item edit form
  def edit
    @store_item = current_user.store_items.find(params[:id])
  end

  # update suggested item
  def update
    @store_item = current_user.store_items.find(params[:id])

    if @store_item.update_attributes(params[:store_item])
      flash[:notice] = t('store_item.update.message.success')
      redirect_to item_path(@store_item)
    else
      put_model_errors_to_flash(@store_item.errors)
      render :action => "edit"
    end
  end

  # DELETE 
  def destroy
    # only the owner of the comment can do the deletion
    @store_item = current_user.store_items.active_only.find(params[:id])
    # apply soft delete, set deleted attribute to true
    @store_item.update_attribute(:deleted, true)
    flash[:notice] = t('store_item.destroy.message.success')

    redirect_to :back
  end

  # DELETE
  def undestroy
    # only the owner of the comment can do the deletion
    @store_item = current_user.store_items.find(params[:id])
    # apply soft delete, set deleted attribute to true
    @store_item.update_attribute(:deleted, false)
    flash[:notice] = t('store_item.undestroy.message.success')

    redirect_to :back
  end

  
end
