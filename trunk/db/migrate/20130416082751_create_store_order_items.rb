class CreateStoreOrderItems < ActiveRecord::Migration
  def self.up
    create_table :store_order_items do |t|
      t.column :store_order_id, :integer, :null => false, :references => :store_orders
      t.column :item_id, :integer, :null => false, :references => :items
      t.column :quantity, :integer, :null => false
      t.column :price, :decimal, :precision => 20, :scale => 2, :null => false
      t.column :price_unit_id, :integer, :null => false, :references => :price_units
      t.timestamps
    end
  end

  def self.down
    drop_table :store_order_items
  end
end
