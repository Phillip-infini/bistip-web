# Default value on MySQL
#   string :limit => 255
#   text, :limit => 65536
class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :username
      t.string :fullname
      t.string :email
      t.string :email_validation_key
      t.string :hashed_password
      t.string :reset_password_key
      t.string :location
      t.string :contact_number
      t.string :extra_contact_number
      t.string :web
      t.string :facebook
      t.string :twitter
      t.text :bio
      t.string :avatar_file_name
      t.string :avatar_content_type
      t.integer :avatar_file_size
      t.datetime :avatar_updated_at

      t.timestamps
    end
    add_index(:users, :id, :unique => true)
  end

  def self.down
    drop_table :users
  end
end
