class AddSuggestedItemFieldsToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :origin_city_id, :integer, :references => :cities
    add_column :items, :user_id, :integer, :references => :users
    add_column :items, :reason, :text
    add_column :items, :notes, :text
  end

  def self.down
    remove_column :items, :origin_city_id
    remove_column :items, :user_id
    remove_column :items, :reason
    remove_column :items, :notes
  end
end
