class PopulateTypeForItems < ActiveRecord::Migration
  def self.up
    Item.all.each do |item|
      if !item.trip_id.blank?
        item.update_attribute(:type, 'TripItem')
        puts "Set type TripItem for item id #{item.id}"
      elsif !item.seek_id.blank?
        item.update_attribute(:type, 'SeekItem')
        puts "Set type SeekItem for item id #{item.id}"
      end
    end
  end

  def self.down
    Item.all.each do |item|
      item.update_attribute(:type, nil)
    end
  end
end
