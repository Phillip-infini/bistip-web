class AddPositionToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :position, :integer
    add_index(:items, :position)

    scope = Item.scoped({})
    scope = scope.market_type
    scope = scope.active_only
    scope = scope.has_picture

    scope.each do |item|
      item.refresh_position
    end
  end

  def self.down
    remove_column :items, :position
  end
end
