class AddInternalToEscrows < ActiveRecord::Migration
  def self.up
    add_column :escrows, :internal, :boolean, :default => false
  end

  def self.down
    remove_column :escrows, :internal
  end
end
