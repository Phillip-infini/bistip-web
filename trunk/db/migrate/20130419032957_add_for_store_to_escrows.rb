class AddForStoreToEscrows < ActiveRecord::Migration
  def self.up
    add_column :escrows, :for_store, :boolean, :default => false
  end

  def self.down
    remove_column :escrows, :for_store
  end
end
