class AddFromPopupToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :from_popup, :boolean, :default => false
  end

  def self.down
    remove_column :users, :from_popup
  end
end
