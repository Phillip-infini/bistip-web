class AddPriceUnitToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :price_unit_id, :integer, :references => :price_units
  end

  def self.down
    remove_column :items, :price_unit_id
  end
end
