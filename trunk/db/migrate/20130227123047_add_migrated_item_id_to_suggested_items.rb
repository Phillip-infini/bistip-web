class AddMigratedItemIdToSuggestedItems < ActiveRecord::Migration
  def self.up
    add_column :suggested_items, :migrated_item_id, :integer, :references => :items
  end

  def self.down
    remove_column :suggested_items, :migrated_item_id
  end
end
