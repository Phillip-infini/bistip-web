class AddApiAuthKeyToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :api_auth_key, :string
  end

  def self.down
    remove_column :users, :api_auth_key
  end
end
