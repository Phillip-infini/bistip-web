class AddSellerOutOfStockToStoreOrders < ActiveRecord::Migration
  def self.up
    add_column :store_orders, :seller_out_of_stock, :boolean, :default => false
  end

  def self.down
    remove_column :store_orders, :seller_out_of_stock
  end
end
