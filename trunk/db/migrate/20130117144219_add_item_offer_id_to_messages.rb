class AddItemOfferIdToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :item_id, :integer, :references => :items
  end

  def self.down
    remove_column :messages, :item_id
  end
end
