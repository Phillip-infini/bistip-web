class CreateStoreOrders < ActiveRecord::Migration
  def self.up
    create_table :store_orders do |t|
      t.column :buyer_id, :integer, :null => false, :references => :users
      t.column :seller_id, :integer, :null => false, :references => :users
      t.column :message_id, :integer, :references => :messages
      t.column :escrow_id, :integer, :references => :escrow
      t.column :delivery_fee, :decimal, :precision => 8, :scale => 2
      t.text :delivery_address
      t.column :buy_insurance, :boolean, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :store_orders
  end
end
