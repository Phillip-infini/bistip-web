class MigrateSuggestedItemVotesToItemVotes < ActiveRecord::Migration
  def self.up
    SuggestedItemVote.all.each do |siv|
      vote = ItemGoodVote.new
      vote.user_id = siv.user_id
      vote.item_id = siv.suggested_item.migrated_item_id
      vote.save
      puts "Done migrating suggested item votes id #{siv.id} to become item vote id #{vote.id}"
    end
  end

  def self.down
    ItemVote.destroy_all
  end
end
