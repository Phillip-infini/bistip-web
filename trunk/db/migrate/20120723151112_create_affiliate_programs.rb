class CreateAffiliatePrograms < ActiveRecord::Migration
  def self.up
    create_table :affiliate_programs do |t|
      t.belongs_to :affiliate
      t.datetime :start
      t.datetime :end
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :affiliate_programs
  end
end
