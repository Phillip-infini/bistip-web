class MigrateSuggestedItemToIdeaItem < ActiveRecord::Migration
  def self.up
    SuggestedItem.all.each do |si|
      idea = IdeaItem.new
      idea.name = si.name
      idea.origin_city_id = si.origin_city_id
      idea.user_id = si.user_id
      idea.reason = si.reason
      idea.notes = si.notes
      idea.deleted = si.deleted
      
      si.pictures.each_with_index do |si_picture, index|
        picture = idea.pictures.new
        picture.attachment = si_picture.attachment
        idea.pictures.insert(index, picture)
      end

      idea.save

      si.update_attribute(:migrated_item_id, idea.id)
      idea.update_attributes(:deleted => si.deleted, :created_at => si.created_at, :updated_at => si.updated_at)

      puts "Done migrating si with id #{si.id} to become idea item with id #{idea.id}"
      
    end
  end

  def self.down
    IdeaItem.all.each do |idea|
      idea.destroy
    end
  end
end
