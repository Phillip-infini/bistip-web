class ConvertReputationsToReviews < ActiveRecord::Migration
  
  def self.up
    Review.skip_callback("create", :after, :do_notify)
    PositiveReputation.all.each do |pr|
      pos_review = PositiveReview.new
      pos_review.giver = pr.giver
      pos_review.receiver = pr.receiver
      pos_review.body = pr.body
      pos_review.updated_at = pr.updated_at
      pos_review.created_at = pr.created_at
      pos_review.invitation = pr.invitation
      pos_review.save
    end

    NegativeReputation.all.each do |nr|
      neg_review = NegativeReview.new
      neg_review.giver = nr.giver
      neg_review.receiver = nr.receiver
      neg_review.body = nr.body
      neg_review.updated_at = nr.updated_at
      neg_review.created_at = nr.created_at
      neg_review.invitation = nr.invitation
      neg_review.save
    end
    Review.set_callback("create", :after, :do_notify)
  end

  def self.down
    Review.destroy_all
  end
end
