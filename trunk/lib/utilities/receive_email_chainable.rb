# class to represent Receive Email handler/chain of responsibility
module ReceiveEmailChainable

  def next_in_chain(link)
    @next = link
  end

  def method_missing(method, *args, &block)
    if @next == nil
      puts "This request cannot be handled!"
      return
    end
    @next.__send__(method, *args, &block)
  end

  def email_type(params)
    params['recipient'][0,2]
  end

  # if user reply using his email, lets validate the user's email
  def validate_email(user)
    user.update_attribute(:email_validation_key, nil)
  end
end