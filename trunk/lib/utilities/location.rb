module Location
  include CheckLocation
  
  # virtual attribute getter to get origin location
  def origin_location
    if self.origin_city
      self.origin_city.name + ", " + self.origin_city.country.name
    else
      @origin_location
    end
  end

  # virtual attribute setter to accept origin location
  def origin_location=(location)
    @origin_location = location
    self.origin_city = check_location(location)
  end

  # getter to the origin location virtual attribute
  def origin_location_value
    @origin_location
  end

  # virtual attribute getter to get destination location
  def destination_location
    if self.destination_city
      self.destination_city.name + ", " + self.destination_city.country.name
    else
      @destination_location
    end
  end

  # virtual attribute setter to accept destination location
  def destination_location=(location)
    @destination_location = location
    self.destination_city = check_location(location)
  end

end
