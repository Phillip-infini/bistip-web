require 'test_helper'

class ExcludeWordTest < ActiveSupport::TestCase
  
  test "should get all" do
    assert_equal 2, ExcludeWord.all.size
  end

  test "generate array" do
    result = ExcludeWord.generate_all_in_array
    assert_equal 2, result.size
  end
end
