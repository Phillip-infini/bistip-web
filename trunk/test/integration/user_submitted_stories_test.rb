require 'test_helper'

class UserSubmittedStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "create a new story" do

    full_login_as(:one)

    get stories_path

    assert_response :success
    assert_template 'index'

    get new_story_path

    post stories_path, :story => {
        :title => 'This is more than 10',
        :body => 'This body content must be more than 30 chars'
    }

    assert_response :redirect
    assert_redirected_to dashboard_stories_path

  end

  test "edit and update a story" do

    full_login_as(:one)

    story_to_use = stories(:storyone)

    get stories_path

    assert_response :success
    assert_template 'index'
    
    get edit_story_path(story_to_use)

    assert_response :success
    assert_template 'edit'

    put story_path(story_to_use), :story => {:title => 'New title for test'}

    assert_response :redirect
    assert_redirected_to dashboard_stories_path

  end

  test "approve story and get 20 points" do

    full_login_as(:mactavish)

    get stories_path

    assert_response :success
    assert_template 'index'

    get new_story_path

    post stories_path, :story => {
        :title => 'This is more than 10',
        :body => 'This body content must be more than 30 chars'
    }

    story = assigns(:story).id

    get("/makeprovit/story/approve", :id => story)

    influencer = users(:mactavish)

    assert_equal(21,influencer.points)

  end
end
