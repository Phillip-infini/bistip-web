require 'test_helper'

class NotificationStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "login and check notification" do

    #login
    full_login_as(:mactavish)

    user = users(:mactavish)

    #view user notification
    get notifications_path
    
    assert_response :success
  end

  test "one give invitation review to dono and dono gets a new notification" do
    mac = users(:mactavish)

    # login
    full_login_as(:one)

    assert_difference('Review.count') do
      post reviews_path(:user => mac.username), :type => 'NegativeReview', :review => {
        :body => "very good service",
        :invitation => true
      }
    end

    get logout_path

    # switch user and check notification
    full_login_as(:mactavish)

    # view user notification
    get notifications_path
    assert_equal 1, assigns(:notifications).size
    assert_response :success
    
  end
end
