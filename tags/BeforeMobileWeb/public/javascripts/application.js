// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

function buildSearchTripURL()
{
    var url = "/search_bistiper?";
    
    var origin = escape($("#origin").val());
    if (origin)
    {
        url += "&origin=" + escape($("#origin").val());
    }

    var destination = escape($("#destination").val());
    if (destination)
    {
        url += "&destination=" + escape($("#destination").val());
    }

    var departure_date = escape($("#departure_date").val());
    if (departure_date)
    {
        url += "&departure_date=" + departure_date;
    }

    var departure_date_predicate = escape($("input[name='departure_date_predicate']:checked").val());
    if (departure_date_predicate)
    {
        url += "&departure_date_predicate=" + departure_date_predicate;
    }

    var notes = escape($("#notes").val());
    if (notes)
    {
        url += "&notes=" + notes;
    }

    window.location.replace(url);
    return false;
}

function buildSearchSeekURL()
{
    var url = "/search_wanted_bistiper?";

    var origin = escape($("#origin").val());
    if (origin)
    {
        url += "&origin=" + origin;
    }

    var destination = escape($("#destination").val());
    if (destination)
    {
        url += "&destination=" + destination;
    }

    var notes = escape($("#notes").val());
    if (notes)
    {
        url += "&notes=" + notes;
    }

    window.location.replace(url);
    return false;
}
//
//function popUpCenter(url, width, height, name){
//    var left = (screen.width/2)-(width/2)
//    var top = (screen.height/2)-(height/2)
//    return window.open(url,name,"menubar=no,toolbar=no,status=no,width="+width+",height="+height+",tollbar=no, left="+left+"top="+top);
//}
//
//$("a.popup").click(function(e) {
//  popupCenter($(this).attr("href"), $(this).attr("data-width"), $(this).attr("data-height"), "authPopup");
//  e.stopPropagation();return false;
//});
//
//$('a[data-popup]').live('click', function(e) {
//    window.open($(this).href);
//    e.preventDefault();
//});

