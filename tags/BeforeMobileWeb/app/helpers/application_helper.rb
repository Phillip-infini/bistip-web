module ApplicationHelper

  # load jquery script tag to generate auto complete location
  def location_auto_complete_js(field_id, exclude_all_cities = false)
    source = "/cities/index"

    if exclude_all_cities
      source = source + '?exclude_all_cities=true'
    end

    parameter = {
        :source => source,
        :highlight => true
        }
    javascript_tag "$(document).ready(function() {
      $(\"\##{field_id}\").autocomplete(#{parameter.to_json});
    });"
  end

  # watermark_field
  def watermark_field(field_id, text)
    javascript_tag "$(document).ready(function() {
      $(\"\##{field_id}\").Watermark(\"#{text}\");
    });"
  end

  # generate date picker for a field
  def date_picker_js(field_id)
    parameter = {
        :minDate => 0,
        :dateFormat => "DD, dd MM yy"
        }
    javascript_tag "$(function() {
        $( \"\##{field_id}\" ).datepicker(#{parameter.to_json});
      });"
  end

  #enable character counter to validate a field's length
  def enable_character_counter(field_id,length)
    javascript_tag "$(function(){
      $(\"\##{field_id}\").jqEasyCounter({
        'maxChars':#{length},
        'maxCharsWarning':#{length}
      });
    });"
  end

  def popup_auth(field_id,path)
    javascript_tag "$(function(){
      $(\"\##{field_id}\").click(function(){
        $.oauthpopup({
          path: '#{path}'
        });
      });
    });"
  end

  # generate link to clear a field
  def render_link_to_clear_a_field(field_id)
    clear_field_js = "$(\"#" + field_id + "\").val(\"\"); return false;";
    content_tag :div, link_to('clear', '#', {:onclick => clear_field_js}), {:class => "red-link"}
  end

  # return date difference of a given date to today's date
  def date_age_in_words(date)
    (distance_of_time_in_words DateTime.current, date) + ' ' + t('datetime.distance_in_words.ago')
  end

  # highlight menu if it's current
  def render_menu_link(text, param)
    link_to_unless_current(text, param) do
       link_to(text, param, {:style => "color:#fed700"})
    end
  end

  # highlight menu if it's current
  def render_sub_menu_link(text, param)
    if current_page?(param)
      content_tag :li, link_to(text, param), {:class => "active"}
    else
      content_tag :li, link_to(text, param)
    end
  end

  # check if we need to render user sub menu
  def should_render_user_sub_menu?
    # user have to be logged in
    if logged_in?
      # if user instance variable is given then check if it's the current logged in user
      if @user
        current_user == @user
      # if requested page is trips index or seek index
      elsif current_page?({:controller => 'trips', :action => "index"}) or
          current_page?({:controller => 'seeks', :action => "index"})
        true
      end
    end
  end

  # boolean helper to check whether a string is nil or empty
  def string_nil_or_empty?(value)
    if value.nil? or value.empty?
      true
    else
      false
    end
  end

  # used to render page heading but will set title page too
  def page_title(title = nil)
    if title
      content_for(:page_title) { title }
    else
      content_for?(:page_title) ? content_for(:page_title) : t('default_page_title')  # or default page title
    end
  end

  # custom auto_link to enable open in new tab every hyperlink
  def auto_link_in_new_tab(content)
    return auto_link(content, :all, :target=>"_blank")
  end

  # change space separated keyword to array
  def search_notes_to_array(keyword)
    if !string_nil_or_empty?(keyword)
      keyword.split(' ')
    end
  end

  # subtract duplicated prefix from web url
  def subtract_dup_from_web_url(url)
    result = url;
    result = result.sub(User::WEB_PREFIX,'')
    result
  end

  # subtract duplicated prefix from facebook url
  def subtract_dup_from_facebook_url(url)
    result = url;
    result = result.sub(User::FACEBOOK_PREFIX,'')
    result = result.sub("http://facebook.com/",'')
    result
  end

  # subtract duplicated prefix from twitter url
  def subtract_dup_from_twitter_url(url)
    result = url;
    result = result.sub(User::TWITTER_PREFIX,'')
    result = result.sub("http://twitter.com/",'')
    result
  end
end
