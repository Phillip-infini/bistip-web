class ErrorsController < ApplicationController
  
  def render_error
    render :template =>'/error/404', :status => 404
  end
end
