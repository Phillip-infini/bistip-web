class PositiveReputationsController < ApplicationController

  before_filter :authenticate, :not_self, :only => [:new, :create]
  #new user
  # render new positive reputation form
  def new
    @user = User.find(params[:user_id])
  end

  # render positive reputations of a user
  def index
    @user = User.find(params[:user_id])
    @positives = @user.received_positive_reputations.paginate(:page => params[:page], :per_page => 5)
  end

  # create a positive reputation
  def create
    @user = User.find(params[:user_id])
    @positive = @user.received_positive_reputations.new(params[:positive_reputation])
    @positive.giver = current_user
    
    if @positive.save
      Notifier.delay.email_receiver_positive_reputation(@user, current_user, @positive) if email_receive_reputation(@user)
      flash[:notice] = t('reputation.create.success.positive')
      redirect_to @user
    else
      put_model_errors_to_flash(@positive.errors)
      render :action => "new"
    end

  end
end
