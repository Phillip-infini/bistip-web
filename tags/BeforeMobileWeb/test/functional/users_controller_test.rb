require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, :user => {:username => "barry", 
        :email => "barry@mail.com",
        :hashed_password => "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4"
        }
    end

    assert_redirected_to registered_path
  end

  test "should show user" do
    get :show, :id => @user.to_param
    assert_response :success
  end

  test "should get edit" do
    login_as(:one)
    get :edit, :id => @user.to_param
    assert_response :success
  end

  test "should update user" do
    login_as(:one)
    put :update, :id => @user.to_param, :user => {:bio => "one's bio"}
    assert_redirected_to user_path(assigns(:user))
  end

  test "should validate user's email" do
    # create a new user (not validated yet)
    username_to_use = "validate"
    post :create, :user => {:username => username_to_use,
        :email => "validate@mail.com",
        :hashed_password => "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4"
        }
    user_not_validated = User.find_by_username(username_to_use)
    get :validate, :id => user_not_validated.id, :key => user_not_validated.email_validation_key
    
    # refetch the user, it must be validated now
    user_validated = User.find_by_username(username_to_use)
    assert user_validated.email_validated?
    assert_redirected_to root_path
  end

end
