require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "creating a new message" do
    message = Message.new
    message.sender=users(:one)
    message.receiver=users(:mactavish)
    message.body='new message from one to mac'
    message.subject='one2mac'
    assert message.save
  end
end
