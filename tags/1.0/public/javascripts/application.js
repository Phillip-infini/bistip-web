// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

function buildSearchTripURL()
{
    var url = "/search_bistiper";
    url += "?origin=" + escape($("#origin").val());
    url += "&destination=" + escape($("#destination").val());
    url += "&departure_date=" + escape($("#departure_date").val());
    url += "&departure_date_predicate=" + escape($("input[name='departure_date_predicate']:checked").val());

    window.location.replace(url);
    return false;
}

function buildSearchSeekURL()
{
    var url = "/search_wanted_bistiper";
    url += "?origin=" + escape($("#origin").val());
    url += "&destination=" + escape($("#destination").val());

    window.location.replace(url);
    return false;
}
