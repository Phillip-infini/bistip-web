class SearchTripController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    origin = check_location(params[:origin])
    
    destination = check_location(params[:destination])

    departure_date = parse_date_parameter(params[:departure_date])
    departure_date_predicate = params[:departure_date_predicate]

    # build scoped Trip by checking parameter
    scope = Trip.scoped({})

    if !origin.nil?
      if origin.all_cities?
        scope = scope.origin_country_id origin.country
      else
        scope = scope.origin_city_id origin.id
      end
    end

    if !destination.nil?
      if destination.all_cities?
        scope = scope.destination_country_id destination.country
      else
        scope = scope.destination_city_id destination.id
      end
    end

    # build predicate on query for departure date based on predicate
    if !departure_date.nil? and !string_nil_or_empty?(departure_date_predicate)
      case departure_date_predicate
        when DatePredicate::AFTER
          scope = scope.departure_date_after departure_date
        when DatePredicate::BEFORE
          scope = scope.departure_date_before departure_date
      end
    end

    @trips = scope.paginate(:page=>params[:page], :per_page => 5)
  end
end
