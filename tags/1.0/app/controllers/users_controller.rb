require 'paperclip'

class UsersController < ApplicationController

  before_filter :authenticate, :only => [:edit, :update]

  # User profile view
  def show
    @user = User.find(params[:id])
  end

  # New user form
  def new
    @user = User.new
  end

  # Edit user form
  def edit
    @user = current_user
  end

  # Update user
  def update
    @user = User.find_by_id(current_user.id)
    if @user.update_attributes(params[:user])

      flash[:notice] = t('user.update.message.success')
      redirect_to(@user)
    else
      put_model_errors_to_flash(@user.errors)
      render :action => 'edit'
    end
  end

  # user not validated yet view
  def not_validated
    # do nothing just rendering view
  end

  # Create user
  def create
    @user = User.new(params[:user])

    if @user.save
      flash[:notice] = t('user.create.message.success')
      redirect_to login_path
    else
      put_model_errors_to_flash(@user.errors)
      render :action => "new"
    end
  end

  # Validate User account
  def validate
    if params[:id] && params[User::VALIDATION_KEY_PARAM]
      user = User.find(params[:id])
      if params[User::VALIDATION_KEY_PARAM] && user.email_validation_key
        user.email_validation_key = nil
        user.save

        # create session with user_id key to login the user
        session[:user_id] = user.id
        flash[:notice] = t('user.validate.message.success')
      end
    end
    redirect_back root_path
  end

  # Resend validation email of a User
  def resend_validation
    if logged_in?
      current_user.send_email_validation
      flash[:notice] = t('user.resend_validation.message.success')
    end
    redirect_to root_path
  end

  # get trips that belongs to a user
  def trips
    @user = User.find(params[:id])
    @trips = @user.trips.order("departure_date desc").paginate(:page => params[:page], :per_page => 5)
  end

  # get seeks that belongs to a user
  def seeks
    @user = User.find(params[:id])
    @seeks = @user.seeks.order("departure_date desc").paginate(:page => params[:page], :per_page => 5)
  end

end
