class NegativeReputationsController < ApplicationController

  before_filter :authenticate, :not_self, :only => [:new, :create]
  #new user
  def new
    @user = User.find(params[:user_id])
  end

  #list of user's positive reputation
  def index
    @user = User.find(params[:user_id])
    @negatives = @user.received_negative_reputations.paginate(:page => params[:page], :per_page => 5)
  end

  # post
  def create
    @user = User.find(params[:user_id])
    @negative = @user.received_negative_reputations.new(params[:negative_reputation])
    @negative.giver = current_user

    if @negative.save
      Notifier.delay.email_receiver_negative_reputation(@user, current_user, @negative) if email_receive_reputation(@user)
      flash[:notice] = t('reputation.create.success.negative')
      redirect_to @user
    else
      put_model_errors_to_flash(@negative.errors)
      render :action => "new"
    end
  end
end
