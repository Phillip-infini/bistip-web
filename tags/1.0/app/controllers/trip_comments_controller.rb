class TripCommentsController < ApplicationController
  before_filter :authenticate, :only => [:destroy,:create]

  # POST new comment
  def create
    @trip = Trip.find(params[:trip_id])
    @comment = @trip.comments.new(params[:comment])
    @comment.user = current_user
    if @comment.save
      
      # validate if commentator not owner, and send email notification to the trip owner
      if !@trip.owner?(current_user)
        Notifier.delay.email_trip_comment(@trip, current_user, @comment) if email_receive_trip_comment(@trip.user)
      end
      flash[:notice] = t('comment.message.create.success')
    else
      put_model_errors_to_flash(@comment.errors, 'redirect')
    end
    redirect_to @trip
  end

  # DELETE Comment only for comment owner
  def destroy
    @trip = Trip.find(params[:trip_id])
    @comment = @trip.comments.find(params[:id])

    # only the owner of the comment can do the deletion
    if @comment.owner?(current_user)
      # apply soft delete, set deleted attribute to true
      @comment.update_attribute(:deleted,true)
      flash[:notice] = t('comment.message.destroy.success')
    else
      flash[:alert] = t('comment.message.destroy.fails')
    end
    redirect_to @trip
  end
  
end
