class TripsController < ApplicationController

  before_filter :authenticate, :only => [:new, :edit, :create, :update, :destroy, :match]

  # GET /trips/1
  def show
    @trip = Trip.find(params[:id])
    @trcomments = @trip.comments.paginate(:page => params[:page], :per_page => 5)
    if logged_in?
      @comment = Comment.new
    end
  end

  # GET /trips/new
  def new
    @trip = Trip.new
  end

  # GET /trips/1/edit
  def edit
    @trip = current_user.trips.find(params[:id])
  end

  # POST /trips
  def create
    @trip = current_user.trips.new(params[:trip])

    if @trip.save
      flash[:notice] = t('trip.create.message.success')
      redirect_to(@trip)
    else
      put_model_errors_to_flash(@trip.errors)
      render :action => "new"
    end
  end

  # PUT /trips/1
  def update
    @trip = current_user.trips.find(params[:id])

    if @trip.update_attributes(params[:trip])
      flash[:notice] = t('trip.update.message.success')
      redirect_to(@trip)
    else
      put_model_errors_to_flash(@trip.errors)
      render :action => "edit"
    end
  end

  # DELETE /trips/1
  def destroy
    # only the owner of the comment can do the deletion
    @trip = current_user.trips.find(params[:id])
    # apply soft delete, set deleted attribute to true
    @trip.update_attribute(:deleted, true)
    flash[:notice] = t('trip.destroy.message..success')
    
    redirect_to(current_user)
  end

  # show matching seeks from a trip
  def match
    @trip = current_user.trips.find(params[:id])
    @matches = @trip.find_matching_seeks
  end
end
