class HomeController < ApplicationController

  def index
    @trips = Trip.random(5)
    @seeks = Seek.random(5)
  end
end
