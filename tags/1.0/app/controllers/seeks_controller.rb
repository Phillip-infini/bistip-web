class SeeksController < ApplicationController
  
  before_filter :authenticate, :only => [:new, :edit, :create, :update, :destroy, :match]

  # GET /seeks/1
  def show
    @seek = Seek.find(params[:id])
    @skcomments = @seek.comments.paginate(:page => params[:page], :per_page => 5)
    if logged_in?
      @comment = Comment.new
    end
  end

  # GET /seeks/new
  def new
    @seek = Seek.new
  end

  # GET /seeks/1/edit
  def edit
    @seek = current_user.seeks.find(params[:id])
  end

  # POST /seeks
  def create
    @seek = current_user.seeks.new(params[:seek])

    if @seek.save
      flash[:notice] = t('seek.create.message.success')
      redirect_to(@seek)
    else
      put_model_errors_to_flash(@seek.errors)
      render :action => "new"
    end
  end

  # PUT /seeks/1
  def update
    @seek = current_user.seeks.find(params[:id])

    if @seek.update_attributes(params[:seek])
      flash[:notice] = t('seek.update.message.success')
      redirect_to(@seek)
    else
      put_model_errors_to_flash(@seek.errors)
      render :action => "edit"
    end
  end

  # DELETE /seeks/1
  def destroy
    # only the owner of the comment can do the deletion
    @seek = current_user.seeks.find(params[:id])
    # apply soft delete, set deleted attribute to true
    @seek.update_attribute(:deleted,true)
    flash[:notice] = t('seek.destroy.message.success')
    
    redirect_to(current_user)
  end

  # show matching trips from a seek
  def match
    @seek = current_user.seeks.find(params[:id])
    @matches = @seek.find_matching_trips
  end
end
