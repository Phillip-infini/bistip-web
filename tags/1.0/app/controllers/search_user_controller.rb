class SearchUserController < ApplicationController
  def index
    keyword = params[:keyword]
    if keyword != nil and !keyword.empty?
      @users = User.where("username like :keyword or fullname like :keyword", {:keyword=>"%#{keyword}%"})
    else
      @users = Array.new
    end
  end
end
