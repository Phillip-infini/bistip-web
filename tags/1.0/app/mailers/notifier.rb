# Class to send email notification
class Notifier < ActionMailer::Base
  default :from => "Bistip.com <willy@bistip.com>"

  # Send validation email to user
  def email_validation(user)
    @user = user
    @email_validation_url = validate_user_url(@user) + "?" + User::VALIDATION_KEY_PARAM + "=" + user.email_validation_key

    mail :to => user.email
    mail :subject => t('notifier.email_validation.subject')
  end

  # Send reset password link to user
  def email_password(user)
    @user = user
    @reset_password_url = reset_edit_password_url + "?" + User::EMAIL_KEY_PARAM + "=" + user.email + "&" + User::RESET_PASSWORD_KEY_PARAM + "=" + user.reset_password_key

    mail :to => user.email
    mail :subject => t('notifier.email_password.subject')
  end

  # Send trip comment to the Trip owner
  def email_trip_comment(trip,commentator, comment)
    @trip = trip
    @commentator = commentator
    @user = User.find(trip.user)
    @content = comment.body
    @created = comment.created_at
    
    @trips_show_url = trips_show_url + "/" + @trip.id.to_s
    mail :to => @user.email, :subject => t('notifier.comment.subject.trip')
  end

  # Send seek comment to the Seek owner
  def email_seek_comment(seek,commentator, comment)
    @seek = seek
    @commentator = commentator
    @user = User.find(seek.user)
    @content = comment.body
    @created = comment.created_at

    @seeks_show_url = seeks_show_url + "/" + @seek.id.to_s
    mail :to => @user.email, :subject => t('notifier.comment.subject.seek')
  end

  def email_receiver_positive_reputation(receiver, giver, reputation)
    @user = receiver
    @receiver = receiver
    @giver = giver
    @content = reputation.body
    @created = reputation.created_at
    mail :to => @receiver.email, :subject => t('notifier.reputation.subject.positive')
  end

  def email_receiver_negative_reputation(receiver, giver, reputation)
    @user = receiver
    @receiver = receiver
    @giver = giver
    @content = reputation.body
    @created = reputation.created_at

    mail :to => @receiver.email, :subject => t('notifier.reputation.subject.negative')
  end

  def email_user_new_message(message)
    @sender = message.sender
    @user = message.receiver
    @content = message.body
    @created = message.created_at

    mail :to => @user.email, :subject => t('notifier.message.subject.new')
  end

  def email_user_reply_message(message)
    @sender = message.sender
    @user = message.receiver
    @content = message.body
    @created = message.created_at

    mail :to => @user.email, :subject => t('notifier.message.subject.reply')
  end

  def email_contact_us(name, email, subject, body)
    @name = name
    @email = email
    @subject = subject
    @body =  body
    mail :to => 'team@bistip.com', :subject => 'Masukan'
    render :layout => 'contact_us'
  end
end
