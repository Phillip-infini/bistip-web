# To change this template, choose Tools | Templates
# and open the template in the editor.

class ArrivalTimeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if (!value.nil?)
      record.errors[attribute] << I18n.t('trip.errors.custom.arrival_date') if record.departure_date > record.arrival_date
    end
  end
end
