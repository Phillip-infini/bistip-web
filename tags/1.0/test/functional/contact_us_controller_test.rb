require 'test_helper'

class ContactUsControllerTest < ActionController::TestCase

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should submit new contact us" do
    post :create, :name => 'name', :email => 'subject', :subject => 'subject', :body => 'body 123'
    assert_redirected_to root_path
  end
end
