require 'test_helper'

class TripTest < ActiveSupport::TestCase

  test "should create trip" do
    trip = Trip.new
    trip.origin_location = "jakarta"
    trip.destination_location = "sydney"
    trip.departure_date = DateTime.current.advance(:days => 1)
    trip.arrival_date = DateTime.current.advance(:days => 2)
    trip.user = users(:one)
    trip.notes = 'test'
    assert trip.save
  end

  test "should find trip" do
    trip_id = trips(:jktsyd).id
    assert_nothing_raised {Trip.find(trip_id)}
  end

  test "should update trip" do
    trip = trips(:jktsyd)
    assert trip.update_attributes(:notes => 'notes')
  end
  
end
