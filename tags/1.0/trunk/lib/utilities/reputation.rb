# To change this template, choose Tools | Templates
# and open the template in the editor.

module Reputation
  
  BODY_MAXIMUM_LENGTH = 600;

  def self.included(base)
    base.belongs_to :giver, :class_name => "User", :foreign_key => 'giver_id'
    base.belongs_to :receiver, :class_name => "User", :foreign_key => "receiver_id"
    base.validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH }
  end
  
end
