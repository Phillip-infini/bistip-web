require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "should create user" do
    user = User.new
    user.username = "testcreate"
    user.email = "testcreate@mail.com"
    user.hashed_password = "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4"
    assert user.save
  end

  test "should find user" do
    user_id = users(:one).id
    assert_nothing_raised {User.find(user_id)}
  end

  test "should update user" do
    user = users(:one)
    assert user.update_attributes(:web => "http://www.facebook.com",
      :location => "Jakarta, Indonesia", :bio => "Allan's bio")
  end

  test "should not create user with invalid user or email" do
    user = User.new
    # username contain invalid character
    user.username = "@validate"
    user.email = "testatemail.com"

    # there must be error with username and email
    assert !user.valid?
    assert user.errors[:username].any?
    assert user.errors[:email].any?
    assert !user.save
  end
end
