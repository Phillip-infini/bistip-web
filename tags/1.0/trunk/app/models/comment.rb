# Comment class represent a comment from a user to a trip or seek
class Comment < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 600

  # scope
  default_scope :order => 'created_at ASC', :conditions => ["deleted = ?",false]

  # relationship
  belongs_to :seek
  belongs_to :trip
  belongs_to :user

  # validation
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH }

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end
end
