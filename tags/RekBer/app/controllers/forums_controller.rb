class ForumsController < ApplicationController

  layout 'forum'

  # GET /forums
  # GET /forums.xml
  def index
    @forums = Forum.all
  end

  # GET /forums/1
  # GET /forums/1.xml
  def show
    @forum = Forum.find(params[:id])
    @topics = @forum.topics.paginate(:page => params[:page], :per_page => 10)
  end

end
