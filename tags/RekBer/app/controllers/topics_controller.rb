class TopicsController < ApplicationController
  before_filter :load_forum
  before_filter :authenticate, :only => [:new, :edit, :create, :update, :destroy]

  layout 'forum'

  def show
    @topic = Topic.find(params[:id])
    topic_view = @topic.topic_view
    topic_view.increment!(:count)
    
    @posts = @topic.posts.paginate(:page => params[:page], :per_page => Topic::POSTS_PER_PAGE)
  end

  def new
    @topic = Topic.new
    @topic.forum = @forum
    @first_post = Post.new
  end

  def edit
    @topic = current_user.topics.find(params[:id])
  end

  def create
    @topic = Topic.new(params[:topic])
    @topic.forum = @forum
    @topic.user = current_user
    @first_post = Post.new
    @first_post.body = params[:first_post_body]
    @first_post.user = current_user

    if @topic.valid? and @first_post.valid?
      @topic.save
      @first_post.topic = @topic
      @first_post.save
      send_forum_email(@forum, @topic, @first_post)
      redirect_to(forum_topic_path(@forum, @topic), :notice => 'Topic was successfully created.')
    else
      @first_post.valid?
      put_model_errors_to_flash(@topic.errors.merge(@first_post.errors))
      render :action => "new"
    end
  end

  def update
    @topic = current_user.topics.find(params[:id])

    if @topic.update_attributes(params[:topic])
      redirect_to(forum_path(@forum), :notice => 'Topic was successfully updated.')
    else
      put_model_errors_to_flash(@topic.errors)
      render :action => "edit"
    end
  end

  def destroy
    @topic = current_user.topics.find(params[:id])
    @topic.deleted = true
    @topic.save
    redirect_to forum_path(@topic.forum)
  end

  private
    def load_forum
      @forum = Forum.find(params[:forum_id])
    end

end
