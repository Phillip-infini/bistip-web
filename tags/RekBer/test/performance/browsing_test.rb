require 'test_helper'
require 'rails/performance_test_help'

# Profiling results for each test method are written to tmp/performance.
class BrowsingTest < ActionDispatch::PerformanceTest
  def setup
    full_login_as(:mactavish)
  end
  def test_homepage
    get '/'
  end

  def test_create_new_trip
    post '/trips', :trip => {:origin_location => "jakarta",
      :destination_location => "sydney",
      :departure_date => DateTime.current.advance(:day => 2),
      :arrival_date => DateTime.current.advance(:day => 3),
      :notes => "text"
      }
  end
end
