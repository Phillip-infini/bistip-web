class AddForIdeaItemLog < PointLog

  # do operation on point given
  def do_point(amount)
    amount + self.amount
  end

  # generate title label
  def title_label
    item = object
    I18n.t('dashboard.points.label.suggested_item_title', :name => link_to(item.name, path_helper.item_path(item)))
  end

  # generate point label
  def point_label
    I18n.t('general.points_plus', :points => self.amount)
  end

  def object
    IdeaItem.find(self.data_id)
  end

end
