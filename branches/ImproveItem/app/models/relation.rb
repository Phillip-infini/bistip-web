class Relation < ActiveRecord::Base

  # constant
  TOTAL_SHOW_ON_SUMMARY = 10

  # relation
  belongs_to :from, :class_name => "User", :foreign_key => 'from_id'
  belongs_to :to, :class_name => "User", :foreign_key => 'to_id'

  # build a relation from a review object
  def self.build_relation_from_review(review)
    from = review.giver
    to = review.receiver
    if Relation.where(:from_id => from.id, :to_id => to.id).count < 1
      relation = from.relations.new
      relation.to_id = to.id
      relation.save
    end
  end
end
