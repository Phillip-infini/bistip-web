# Item class represent a item of trip or seek
class Item < ActiveRecord::Base

  include ActionView::Helpers

  # include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper

  # constants
  NAME_MAXIMUM_LENGTH = 50
  NAME_MINIMUM_LENGTH = 3
  PICTURES_MAXIMUM = 3
  SEARCH_PER_PAGE = 12
  
  # relationship
  has_many :pictures, :class_name => "ItemPicture", :foreign_key => 'item_id'
  has_many :votes, :class_name => 'ItemVote', :foreign_key => 'item_id'
  accepts_nested_attributes_for :pictures, :allow_destroy => true, :reject_if => lambda { |a| a[:attachment].blank? }

  # add additional relationships just to get test fixtures working
  belongs_to :trip
  belongs_to :seek
  belongs_to :tip_unit
  belongs_to :price_unit
  
  # validation
  validates :name, :presence => true, :length => { :minimum => NAME_MINIMUM_LENGTH, :maximum => NAME_MAXIMUM_LENGTH }

  # scope
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ?", 60.days.ago]}}
  scope :name_contains, lambda { |keyword| {:conditions => ["match(items.name) against(?)", keyword]}}
  # scope :trip_only, :conditions => 'trip_id IS NOT NULL'
  # scope :seek_only, :conditions => 'seek_id IS NOT NULL'
  scope :id_not, lambda { |id| {:conditions => ["items.id != ?", id]}}
  scope :random, lambda { |*args| {:order => 'RAND()', :limit => args[0] || 1} }
  scope :active_only, :conditions => "items.deleted = false and (items.approved is NULL or items.approved = true)"
  scope :has_picture, :conditions => "(select count(*) from item_pictures where item_pictures.item_id = items.id) > 0"
  scope :trip_is_active, lambda { {:conditions => ["(trips.departure_date >= ? or trips.routine is true) and trips.deleted = ?", DateTime.current.beginning_of_day, false], :include => [:trip]} }
  scope :id_not, lambda { |id| {:conditions => ["items.id != ?", id]}}
  scope :random, lambda { |*args| {:order => 'RAND()', :limit => args[0] || 1} }
  scope :newest, :order => "items.created_at DESC"
  scope :market_type, :conditions => "items.type = 'TripItem' or items.type = 'SeekItem' or items.type = 'StoreItem' "

  # do soft delete on item
  def destroy
    raise NotImplementedError
  end

  # get first picture
  def first_picture(type)
    self.pictures.blank? ? '/images/default_original_item.png' : self.pictures.first.attachment.url(type)
  end

  # friendly URL for SEO
  def to_param
    raise NotImplementedError
  end

  def desc_string
    raise NotImplementedError
  end

  def user
    raise NotImplementedError
  end

  # helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end

  # edit path for this item
  def edit_path
    raise NotImplementedError
  end

  # return name of the show renderer
  def show_path
    raise NotImplementedError
  end

  # parent of this item, only used by TripItem and SeekItem
  def parent
    raise NotImplementedError
  end

  # build scoped SuggestedItem for searching trip purpose
  def self.build_scope_search(keyword = nil)
    scope = Item.scoped({})
    scope = scope.active_only
    scope = scope.has_picture
    scope = scope.newest

    # build predicate for keyword
    if !keyword.blank?
      scope = scope.name_contains keyword
    end

    return scope
  end

  # build scoped of market item
  def self.build_market_scope_search(keyword = nil)
    scope = Item.scoped({})
    scope = scope.market_type
    scope = scope.active_only
    scope = scope.has_picture
    scope = scope.newest

    # build predicate for keyword
    if !keyword.blank?
      scope = scope.name_contains keyword
    end

    return scope
  end


  def name_for_display
    if !self.name.blank? and self.name.size > 30
      self.name.slice(0,50) + '...'
    else
      self.name
    end
  end

  # check if given user is the owner of the item
  def owner?(user)
    self.user == user
  end

end
