class StoreOrder < ActiveRecord::Base

  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers

  DELIVERY_ADDRESS_MINIMUM_LENGTH = 30
  DELIVERY_ADDRESS_MAXIMUM_LENGTH = 300
  PREFIX = 'ORD'
  ID_TO_ADD = 88
  INTERNAL_ID = 'store_order'

  # state for store order
  STATE_BUYER_SUBMITTED_ORDER = 'buyer_submitted_order'
  STATE_SELLER_QUOTED_DELIVERY_FEE = 'seller_quoted_delivery_fee'
  STATE_INVALID = 'invalid'

  belongs_to :buyer, :class_name => "User", :foreign_key => 'buyer_id'
  belongs_to :seller, :class_name => "User", :foreign_key => 'seller_id'
  has_many :order_items, :class_name => "StoreOrderItem"
  belongs_to :message
  belongs_to :escrow

  validates :order_items, :presence => true
  validates :buyer, :presence => true
  validates :seller, :presence => true
  validates :delivery_address,
    :length => {:minimum => DELIVERY_ADDRESS_MINIMUM_LENGTH, :maximum => DELIVERY_ADDRESS_MAXIMUM_LENGTH}
  validates :delivery_fee, :presence => true, :numericality => {:only_integer => true, :greater_than => 0}, :if => lambda { |o| (!o.new_record?)}

  attr_writer :current_step

  # nested
  accepts_nested_attributes_for :order_items, :allow_destroy => true, :reject_if => lambda { |a| a[:item_id].blank? }

  # events
  after_create :notify_seller

    # form wizard: show the current step in wizard
  def current_step
    @current_step || steps.first
  end

  # form wizard: list of steps
  def steps
    %w[details confirmation]
  end

  # form wizard: get the next step
  def next_step
    self.current_step = steps[steps.index(current_step)+1]
  end

  # form wizard: get the previous step
  def previous_step
    self.current_step = steps[steps.index(current_step)-1]
  end

  # form wizard: is it currently on the first step
  def first_step?
    current_step == steps.first
  end

  # form wizard: is it currently on the last step
  def last_step?
    current_step == steps.last
  end

  # form wizard: is all step in the wizard already pass validation
  def all_valid?
    steps.all? do |step|
      self.current_step = step
      valid?
    end
  end

  # notify seller about the order and ask seller to fill in delivery cost
  def notify_seller
    Notifier.delay.email_store_order_to_seller(self)
    ContactUsNotifier.delay.email_new_store_order(self)
  end

  # store order is completed when delivery fee already been quoted
  def delivery_fee_quoted?
    if !self.delivery_fee.blank? and self.delivery_fee > 0
      true
    else
      false
    end
  end

  # total shopping amount
  def total_amount
    total = 0
    self.order_items.each do |oi|
      total = total + oi.total_price
    end
    total
  end

  def total_amount_with_unit
    number_to_currency(self.total_amount.to_s, :unit => ('Rp '), :precision => 0)
  end

  def delivery_fee_with_unit
    number_to_currency(self.delivery_fee.to_s, :unit => ('Rp '), :precision => 0)
  end

  def total_all_without_safepay_fee
    (self.total_amount + self.delivery_fee)
  end

  def total_all_with_unit
    number_to_currency(((self.total_amount + self.delivery_fee) * 1.02).to_s, :unit => ('Rp '), :precision => 0)
  end

  def admin_fee_with_unit
    number_to_currency(((self.total_amount + self.delivery_fee) * 0.02).to_s, :unit => ('Rp '), :precision => 0)
  end

  # generate notes for bank transfer
  def bank_transfer_notes
    self.buyer.username + ' ' + self.internal_id
  end

  # internal id
  def internal_id
    PREFIX + (self.id + ID_TO_ADD).to_s
  end

  # id for display
  def id_for_display
    datetime = Time.now.in_time_zone('Jakarta')
    '#' + PREFIX + datetime.strftime('%Y%m') + (self.id + ID_TO_ADD).to_s
  end

  # get the current state of store order determine by latest log type
  def current_state
    if !self.escrow.blank?
      self.escrow.current_state
    elsif self.delivery_fee.blank?
      STATE_BUYER_SUBMITTED_ORDER
    elsif !self.delivery_fee.blank?
      STATE_SELLER_QUOTED_DELIVERY_FEE
    else
      STATE_INVALID
    end
  end

  # current state display
  def current_state_display_string
    if !self.escrow.blank?
      self.escrow.last_log.to_display_string
    elsif self.delivery_fee.blank?
      I18n.t("store_order.state.#{STATE_BUYER_SUBMITTED_ORDER}", :buyer => self.buyer.username, :seller => self.seller.username)
    elsif !self.delivery_fee.blank?
      I18n.t("store_order.state.#{STATE_SELLER_QUOTED_DELIVERY_FEE}", :buyer => self.buyer.username, :seller => self.seller.username)
    end
  end

  # current state display
  def awaiting_string
    if !self.escrow.blank?
      self.escrow.last_log.get_awaiting_string
    elsif self.delivery_fee.blank?
      I18n.t("store_order.awaiting.#{STATE_BUYER_SUBMITTED_ORDER}", :buyer => self.buyer.username, :seller => self.seller.username)
    elsif !self.delivery_fee.blank?
      I18n.t("store_order.awaiting.#{STATE_SELLER_QUOTED_DELIVERY_FEE}", :buyer => self.buyer.username, :seller => self.seller.username)
    end
  end

end
