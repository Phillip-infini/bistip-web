class StoreOrderItem < ActiveRecord::Base

  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers

  DEFAULT_QUANTITY = 1

  belongs_to :order, :class_name => "StoreOrder"
  belongs_to :item
  belongs_to :price_unit

  validates :quantity, :numericality => {:only_integer => true, :greater_than => 0}
  validates :item, :presence => true
  validates :price, :numericality => {:only_integer => true, :greater_than => 0}

  before_create :set_price_unit

  # price display with unit append to it
  def price_with_unit
    number_to_currency(self.price.to_s, :unit => (self.price_unit.symbol + ' '), :precision => 0)
  end

  def total_price_with_unit
    number_to_currency(self.total_price.to_s, :unit => (self.price_unit.symbol + ' '), :precision => 0)
  end

  # set price unit
  def set_price_unit
    self.price_unit_id = PriceUnit.find_by_symbol('IDR').id
  end

  def total_price
    self.price * self.quantity
  end
end
