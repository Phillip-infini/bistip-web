# To change this template, choose Tools | Templates
# and open the template in the editor.

class BistipReleasedBuyerPayoutLog < EscrowLog
  attr_accessor :skip_broadcast

  # broadcast the creation of this log inform of notification and email
  # override
  def broadcast
    if self.skip_broadcast.blank?
      EscrowLogNotification.create!(:data_id => self.id, :sender => escrow.buyer, :receiver => escrow.buyer)
      Notifier.delay.email_escrow_log(escrow.buyer, self)
    end
  end
  
end
