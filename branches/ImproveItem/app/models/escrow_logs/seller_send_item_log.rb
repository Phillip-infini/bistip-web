class SellerSendItemLog < EscrowLog

  # broadcast the creation of this log inform of notification and email
  def broadcast
    EscrowLogNotification.create!(:data_id => self.id, :sender => escrow.seller, :receiver => escrow.buyer)
    Notifier.delay.email_store_escrow_log(escrow.buyer, self)
  end
  
end
