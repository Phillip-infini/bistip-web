# To change this template, choose Tools | Templates
# and open the template in the editor.

class BistipReleasedSellerPayoutLog < EscrowLog
  attr_accessor :skip_broadcast

  # broadcast the creation of this log inform of notification and email
  # override
  def broadcast
    if self.skip_broadcast.blank?
      EscrowLogNotification.create!(:data_id => self.id, :sender => escrow.seller, :receiver => escrow.seller)
      Notifier.delay.email_escrow_log(escrow.seller, self)
      Notifier.delay.email_story_invite(self.escrow.seller)
    end
  end
  
end
