class StoreItemApprovalNotification < Notification

  # override to_link method to perform specified action of ItemSuggestionApprovalNotification
  def to_link(current_user)
    store_item = StoreItem.find(data_id)
    link = I18n.t("notification.title.store_item_approval",
      :link => link_to(store_item.name, path_helper.item_path(store_item)))
    return link
  end
end
