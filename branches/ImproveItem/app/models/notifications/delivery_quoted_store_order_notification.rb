class DeliveryQuotedStoreOrderNotification < Notification

  # override to_link method to perform specified action of NewStoreOrderNotification
  def to_link(current_user)
    store_order = StoreOrder.find(data_id)
    link = I18n.t("notification.title.delivery_quoted_store_order",
      :seller => store_order.buyer.username,
      :delivery_fee => store_order.delivery_fee_with_unit,
      :link => link_to(I18n.t("store_order.payment_info"), path_helper.dashboard_my_shopping_path(:anchor => store_order.internal_id)))
    return link
  end
end
