class IdeaItemApprovalNotification < Notification

  # override to_link method to perform specified action of ItemSuggestionApprovalNotification
  def to_link(current_user)
    idea_item = IdeaItem.find(data_id)
    link = I18n.t("notification.title.suggested_item_approval",
      :link => link_to(idea_item.name, path_helper.item_path(idea_item)))
    return link
  end
end
