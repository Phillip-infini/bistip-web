class NewStoreOrderNotification < Notification

  # override to_link method to perform specified action of NewStoreOrderNotification
  def to_link(current_user)
    store_order = StoreOrder.find(data_id)
    link = I18n.t("notification.title.new_store_order",
      :buyer => store_order.buyer.username,
      :link => link_to(I18n.t("store_order.quote_delivery.link"), path_helper.quote_delivery_store_order_path(store_order)))
    return link
  end
end
