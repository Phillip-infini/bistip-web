# To change this template, choose Tools | Templates
# and open the template in the editor.

class ReviewNotification < Notification

  # override method, to provide info who gave review to current_user
  def to_link(current_user)
    receiver = User.find(data_id)
    link = I18n.t("notification.title.review",
      :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
      :review_label => link_to(I18n.t('notification.field.review'), path_helper.reviews_user_path(:username => receiver.username)))
    return link
  end
end
