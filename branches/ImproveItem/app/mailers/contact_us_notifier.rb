# Class to send contact us email notification
class ContactUsNotifier < ActionMailer::Base
  default :from => "Bistip.com <willy@bistip.com>"
  
  def email_contact_us(name, email, subject, body)
    @name = name
    @email = email

    # dont use @subject or @body here it's a reserved key word
    @topic = subject
    @content =  body
    mail :to => 'team@bistip.com', :subject => 'Masukan'
  end

  def email_exception(message, trace, ip, user_agent, fullpath, referer, type, suspicious)
    @message = message
    @trace = trace
    @ip = ip
    @agent = user_agent
    @fullpath = fullpath
    @referer = referer
    
    @user = suspicious

    @country_name = "n/a"
    @city = "n/a"
    @region_name = "n/a"
    @zipcode = "n/a"
    geocode = nil
    begin
      geocode = Geocoder.search(ip.to_s)
    rescue Exception => e
      geocode = nil
    end
    
#    if !geocode.nil?
#      if !geocode[0].data.nil?
#        @country_name = geocode[0].country_name if !geocode[0].country_name.empty?
#        @city = geocode[0].city if !geocode[0].city.empty?
#        @region_name = geocode[0].region_name if !geocode[0].region_name.empty?
#        @zipcode = geocode[0].zipcode if !geocode[0].zipcode.empty?
#      end
#    end
    mail :to => 'team@bistip.com', :subject => "Exception #{type}"
  end

  def email_forum_event(email_to, forum, topic, post)
    @forum = forum
    @topic = topic
    @post = post
    mail :to => email_to, :subject => 'Forum event'
  end

  def email_insurance(message, user, type)
    @message = message
    @user = user
    @type = type
    mail :to => 'team@bistip.com', :subject => 'Asuransi'
  end

  def email_statistics(user_count, trip_count, seek_count, comment_count, message_count, good_rep_count, bad_rep_count, item_count, fb_count, tw_count)
    @user_count = user_count
    @trip_count = trip_count
    @seek_count = seek_count
    @comment_count = comment_count
    @message_count = message_count
    @good_rep_count = good_rep_count
    @bad_rep_count = bad_rep_count
    @item_count = item_count
    @fb_count = fb_count
    @tw_count = tw_count
    mail :to => 'team@bistip.com', :subject => 'Statistics'
  end

  def email_escrow_log(escrow)
    @escrow = escrow
    mail :to => 'team@bistip.com', :subject => 'Escrow Transaction'
  end

  def email_insurance_log(insurance)
    @insurance = insurance
    mail :to => 'team@bistip.com', :subject => 'Insurance Application'
  end

  def email_stories(id, name, email, subject, body)
    @id = id
    @name = name
    @email = email
    @topic = subject
    @content =  body
    mail :to => 'team@bistip.com', :subject => 'User Story'
  end

  def email_suggested_item(suggested_item)
    @suggested_item = suggested_item
    mail :to => 'team@bistip.com', :subject => 'Suggested Item'
  end

  def email_cashback_request(user, escrow_id)
    @user = user
    @escrow_id = escrow_id
    mail :to => 'team@bistip.com', :subject => 'Cashback Request'
  end

  def email_deals_subscriber_to_wego
    @subscribers = Array.new
    @string = StringIO.new
    WegoTravelDealsSubscription.find_all_by_email_sent(false).each do |ds|
      user = ds.user

      unless user.disabled
        @subscribers << user
        @string << "#{user.email},#{user.fullname},#{user.city.name},#{user.city.country.name}\n"
        ds.update_attribute(:email_sent, true)
      end
      
    end
    attachments['file.csv']= { :data=> ActiveSupport::Base64.encode64(@string.string), :encoding => 'base64' }
    mail :to => 'willy@bistip.com, achmad@wego.com', :subject => "Wego Deals Subscribers: #{@subscribers.count} new subscribers"
  end

  def email_store_item_approval(store_item)
    @store_item = store_item
    mail :to => 'willy@bistip.com', :subject => 'Store Item need approval'
  end

  def email_new_store_order(store_order)
    @store_order = store_order
    mail :to => 'willy@bistip.com', :subject => 'New Store Order'
  end
end
