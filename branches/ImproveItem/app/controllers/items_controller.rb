class ItemsController < ApplicationController

  # display an item offer
  def show
    @item = Item.find(params[:id])
    @main_picture = @item.pictures.first
    
    # we only provide show for Trip and Idea
    if @item.is_a? TripItem
      @other_item_offers = TripItem.has_picture.active_only.trip_is_active.id_not(@item.id).random.limit(5)
      render @item.show_path
    elsif @item.is_a? IdeaItem
      @item_pictures = ItemPicture.find_all_by_item_id(@item.id)
      @results = Trip.build_scope_search(@item.origin_city, nil, nil, nil, nil, nil).paginate(:per_page => IdeaItem::SHOW_PER_PAGE, :page => params[:page])
      @total_result = Trip.build_scope_search(@item.origin_city, nil, nil, nil, nil, nil).count
      render @item.show_path
    elsif @item.is_a? SeekItem
      redirect_to seek_path(@item.seek)
    elsif @item.is_a? StoreItem
      @item_pictures = ItemPicture.find_all_by_item_id(@item.id)
      render @item.show_path
    else
      redirect_to root_path
    end
  end

  # render form for new ide item
  def new_idea
    @item = IdeaItem.new
    Item::PICTURES_MAXIMUM.times { @item.pictures.build }
  end
  
end
