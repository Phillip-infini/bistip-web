class SearchItemController < ApplicationController

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    keyword = params[:keyword]
    notice = Array.new
    
    # build scoped SuggestedItem by passing parameter to method build_scope_search
    scope = Item.build_scope_search(keyword)
    @no_valid_parameter = keyword.blank?
    @total_result = scope.size
    @items = scope.paginate(:page => params[:page], :per_page => Item::SEARCH_PER_PAGE)

    flash.now[:alert] = notice unless notice.empty?
  end
end
