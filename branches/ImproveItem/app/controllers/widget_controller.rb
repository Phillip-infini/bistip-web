class WidgetController < ApplicationController

  layout 'widget'

  def search
    @trips = Trip.build_scope_home_index(9).all
  end

end
