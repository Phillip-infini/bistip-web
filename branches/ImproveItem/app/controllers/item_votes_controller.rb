class ItemVotesController < ApplicationController

  before_filter :authenticate, :only => [:new]

  def new
    item = Item.find(params[:item_id])

    # owner cannot vote for himself
    if current_user == item.user
      flash[:notice] = t('suggested_item_votes.new.not_self')
    # check if vote already exist
    elsif ItemVote.where(:user_id => current_user.id, :item_id => item.id).count < 1
      vote = ItemGoodVote.new
      vote.item = item
      vote.user = current_user
      if vote.save
        Notifier.delay.email_idea_item_vote(vote)
        flash[:notice] = t('suggested_item_votes.new.success', :item => item.name)
      else
        flash[:notice] = t('suggested_item_votes.new.failure')
      end
    else
      flash[:notice] = t('suggested_item_votes.new.already_exist', :item => item.name)
    end

    redirect_to item_path(item)
  end

end
