class ApiController < ActionController::Base
  include ::SslRequirement
  ssl_required :login

  layout 'application'

  before_filter :authenticate, :only => [:get_user, :get_escrows, :seller_got_item, :buyer_received_item, :seller_cancelled,
    :seller_requested_payout,
    :buyer_resumed,
    :buyer_requested_payout,
    :seller_received_payout,
    :buyer_received_payout]
  before_filter :load_escrow, :only => [:buyer_received_item, :seller_got_item, :get_escrow, :seller_cancelled,
    :seller_requested_payout,
    :buyer_resumed,
    :buyer_requested_payout,
    :seller_received_payout,
    :buyer_received_payout]

  include CheckLocation
  PAGE = 1
  PER_PAGE = 10

  # Filter method to enforce a login requirement & email validation
  # Apply as a before_filter on any controller you want to protect
  def authenticate
    if logged_in?
        #if !current_user.email_validated?
        #  user_not_validated
        #end
        # do nothing
    else
      access_denied
    end
  end

  # Predicate method to test for a logged in user
  def logged_in?
    current_user.is_a? User
  end

  # Returns the currently logged in user or nil if there isn't one
  def current_user
    return unless params[:auth_token]
    @current_user ||= User.find_by_api_auth_key(params[:auth_token])
  end

  # Returns the currently logged in user or nil if there isn't one
  def load_escrow
    return unless params[:escrow_id]
    @escrow = Escrow.non_store.find(params[:escrow_id])
  end

  # handle access denied
  def access_denied
    respond_general({:logged_in => false, :message => I18n.t('api.error.access_denied')})
  end

  #method to produce xml/json from given key value result to return
  def respond_general(result)
    respond_to do |format|
      format.json { render :json => result}
      format.xml  { render :xml => result }
    end
  end
  
  #method to produce xml/json from given data structure with some option
  def respond_with_option(object, option)
    option = {:skip_instruct => true} if object.is_a?(Hash) and object[I18n.t('api.error.label.exception')]
    respond_to do |format|
      format.json { render :json => object.to_json(option)}
      format.xml  { render :xml => object.to_xml(option) }
    end
  end

  #method that call when result is empty, parameters doesn't correct
  def generate_error_not_found_report(result, *data)
    error = {}
    error[t('api.label.request')] = request.path
    case data.length
      when 0
        error[t("api.label.parameter")] = t("api.error.no_parameter")
      when 1
        error[t('api.label.keyword')] = t("api.error.name_parameter", :name => data[0]) if !data[0].nil?
      when 2
        error[t('api.label.from')] = t("api.error.from_parameter", :origin => data[0]) if check_location(data[0]).nil? and !data[0].nil?
        error[t('api.label.to')] = t("api.error.to_parameter", :destination => data[1]) if check_location(data[1]).nil? and !data[1].nil?
    end
    error[t('api.label.result')] = t("api.error.result_empty") if result.blank?
    error = {t('api.error.label.exception') => t("api.error.not_found"), t('api.error.label.message') => error}
    return error
  end

  #GET api/v1/trips/from/:origin/to/:destination(.:format)
  def trip_by_city
    origin = check_location(params[:from])
    destination = check_location(params[:to])
    
    result = nil
    if !origin.nil? or !destination.nil?
      scope = Trip.build_scope_search(origin, destination)
      
      result = scope.paginate(:page=>(params[:page].nil? ? PAGE : params[:page]),
        :per_page => (params[:per_page].nil? ? PER_PAGE : params[:per_page]))
    end

    #check if action has :from and/or :to parameters
    if !params[:from].nil? or !params[:to].nil?
      #if params[:from] couldn't produce an origin location and params[:to] not specified
      if origin.nil? and !params[:from].blank? and params[:to].blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if params[:to] couldn't produce a destination location and params[:from] not specified
      elsif destination.nil? and !params[:to].blank? and params[:from].blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if params[:from] couldn't produce an origin location also with params[:to] couldn't produce a destination location
      elsif origin.nil? and destination.nil?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if result is empty
      elsif result.blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      end
    elsif params[:from].nil? and params[:to].nil?
      result = generate_error_not_found_report(result)
    end
    
    respond_with_option(result, Trip.data_options)
  end

  #GET /api/v1/seeks/from/:origin/to/:destination(.:format)
  def seek_by_city
    origin = check_location(params[:from])
    destination = check_location(params[:to])

    result = nil
    if !origin.nil? or !destination.nil?
      scope = Seek.build_scope_search(origin, destination)
      result = scope.paginate(:page=>(params[:page].nil? ? PAGE : params[:page]),
        :per_page => (params[:per_page].nil? ? PER_PAGE : params[:per_page]))
    end

    #check if action has :from and/or :to parameters
    if !params[:from].nil? or !params[:to].nil?
      #if params[:from] couldn't produce an origin location and params[:to] not specified
      if origin.nil? and !params[:from].blank? and params[:to].blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if params[:to] couldn't produce a destination location and params[:from] not specified
      elsif destination.nil? and !params[:to].blank? and params[:from].blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if params[:from] couldn't produce an origin location also with params[:to] couldn't produce a destination location
      elsif origin.nil? and destination.nil?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if result is empty
      elsif result.blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      end
    elsif params[:from].nil? and params[:to].nil?
      result = generate_error_not_found_report(result)
    end
    respond_with_option(result, Seek.data_options)
  end

  def login
    user = User.authenticate(params[:email_or_username], params[:password])
    if user
      respond_general({:logged_in => true, :auth_key => user.api_auth_key, :username => user.username})
    else
      respond_general({:logged_in => false, :message => I18n.t('api.login.fail')})
    end
  end
  
  def get_user
    respond_with_option(current_user, User.data_options)
  end

  def get_escrows
    escrows = Escrow.non_store.where("buyer_id = #{current_user.id} OR seller_id = #{current_user.id}").order('created_at DESC')
    respond_with_option(escrows, Escrow.data_options)
  end

  def get_escrow
    if @escrow.buyer == current_user or @escrow.seller == current_user
      respond_with_option(@escrow, Escrow.data_options)
    else
      
    end
  end

  # create seller_got_item log when escrow's current state is paypal_notify
  def seller_got_item
    if current_user == @escrow.seller and
        (@escrow.current_state.eql?(Escrow::STATE_PAYPAL_NOTIFIED) or
          @escrow.current_state.eql?(Escrow::STATE_BUYER_RESUMED) or
          @escrow.current_state.eql?(Escrow::STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER))
      SellerGotItemLog.create!(:escrow_id => @escrow.id)
      respond_general({:escrow_success => true})
    else
      respond_general({:escrow_success => false, :message => i18n.t('api.escrow.not_allowed')})
    end
  end

  #create buyer_received_item log when escrow's current state is seller_got_item
  def buyer_received_item
    if current_user == @escrow.buyer and @escrow.current_state.eql?(Escrow::STATE_SELLER_GOT_ITEM)
      # this action must come with a review
      review = get_review

      if review and review.save
        # link review to escrow
        @escrow.update_attribute(:buyer_review_id, review.id)
        BuyerReceivedItemLog.create!(:escrow_id => @escrow.id)
        respond_general({:escrow_success => true})
      else
        respond_general({:escrow_success => false, :message => hash_pretty_print(review.errors)})
      end
    else
      respond_general({:escrow_success => false, :message => I18n.t('api.escrow.not_allowed')})
    end
  end

  #create seller_cancel log when escrow's current state is paypal_notify
  def seller_cancelled
    if current_user == @escrow.seller and
        (@escrow.current_state.eql?(Escrow::STATE_PAYPAL_NOTIFIED) or
         @escrow.current_state.eql?(Escrow::STATE_BUYER_RESUMED) or
         @escrow.current_state.eql?(Escrow::STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER))
      SellerCancelledLog.create!(:escrow_id => params[:escrow_id])
      respond_general({:escrow_success => true})
    else
      respond_general({:escrow_success => false, :message => I18n.t('api.escrow.not_allowed')})
    end
  end

  #create seller_request_payout log when escrow's current state is buyer_received_item
  def seller_requested_payout
    if current_user == @escrow.seller and @escrow.current_state.eql?(Escrow::STATE_BUYER_RECEIVED_ITEM)
      # this action must come with a review
      review = get_review

      if review and review.save
        # link review to escrow
        @escrow.update_attribute(:seller_review_id, review.id)
        SellerRequestedPayoutLog.create!(:escrow_id => params[:escrow_id])
        respond_general({:escrow_success => true})
      else
        session[:review] = review
        respond_general({:escrow_success => false, :message => hash_pretty_print(review.errors)})
      end
    else
      respond_general({:escrow_success => false, :message => I18n.t('api.escrow.not_allowed')})
    end
  end

  #create seller_received_payout log when escrow's current state is bistip_released_seller_payout
  def seller_received_payout
    if current_user == @escrow.seller and @escrow.current_state.eql?(Escrow::STATE_BISTIP_RELEASED_SELLER_PAYOUT)
      SellerReceivedPayoutLog.create!(:escrow_id => params[:escrow_id])
      respond_general({:escrow_success => true})
    else
      respond_general({:escrow_success => false, :message => I18n.t('api.escrow.not_allowed')})
    end
  end

  #create buyer_resume log when escrow's current state is seller_cancel
  def buyer_resumed
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_SELLER_CANCELLED
      BuyerResumedLog.create!(:escrow_id => params[:escrow_id])
      respond_general({:escrow_success => true})
    else
      respond_general({:escrow_success => false, :message => I18n.t('api.escrow.not_allowed')})
    end
  end

  #create buyer_request_payout log when escrow's current state is seller_cancel
  def buyer_requested_payout
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_SELLER_CANCELLED
      BuyerRequestedPayoutLog.create!(:escrow_id => params[:escrow_id])
      respond_general({:escrow_success => true})
    else
      respond_general({:escrow_success => false, :message => I18n.t('api.escrow.not_allowed')})
    end
  end

  #create buyer_received_payment log when escrow's current state is bistip_released_buyer_payout
  def buyer_received_payout
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_BISTIP_RELEASED_BUYER_PAYOUT
      BuyerReceivedPayoutLog.create!(:escrow_id => params[:escrow_id])
      respond_general({:escrow_success => true})
    else
      respond_general({:escrow_success => false, :message => I18n.t('api.escrow.not_allowed')})
    end
  end

  def get_inbox
    sender = params[:sender]
    status = params[:status]
    scope = Message.build_scope_search(current_user, sender, status, nil)
    threads = scope.paginate(:per_page => Message::DEFAULT_PER_PAGE, :page => params[:page])
    messages = Array.new()
    threads.each do |r|
      messages << Message.where("reply_to_id = #{r.reply_to_id} and receiver_id = #{current_user.id}").order("created_at desc").first
    end
    Thread.current[:api_current_user] = current_user
    respond_general({:total_pages => threads.total_pages, :current_page => threads.current_page, :threads => convert_threads(messages)})
  end

  def get_conversation
    message_id = params[:message_id]
    message = Message.find(message_id)
    # Only sender or receiver can read the conversation!
    if (message.sender == current_user or message.receiver == current_user)

      # find the thread starter
      thread_starter = Message.find(message.reply_to_id) unless message.thread_starter?
      message.update_attribute(:read, true) if current_user == message.sender

      messages = Message.all_in_thread(thread_starter.id)
      respond_general({:reply_to_id => thread_starter.id, :messages => convert_threads(messages)})
      
    else
      # do nothing
    end
  end

  private

    # build a review from param
    def get_review
      type = params[:review_type].blank? ? PositiveReview : params[:review_type].constantize
      review = type.new
      review.body = params[:review_body]
      review.giver = current_user
      review.receiver = @escrow.not_this_user(current_user)
      review.verified = true
      review
    end

    # pretty print a hash
    def hash_pretty_print(data)
      result = ''
      data.each {|key, value| result << "#{key}: #{value}" }
      result
    end

    # convert threads
    def convert_threads(threads)
      result = Array.new
      threads.each do |thread|
        result << thread.as_json
      end
      return result
    end
  
end

