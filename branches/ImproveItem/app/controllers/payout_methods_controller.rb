class PayoutMethodsController < ApplicationController
  before_filter :authenticate, :only => [:new, :create, :index, :delete]
  before_filter :load_user

  # index page for user payout method
  def index
    @payout = current_user.payout_methods.first
  end

  # render new payout method form
  def new
    @payout = payout_type.new
  end

  # create a payout method
  def create
    @payout = payout_type.new(params[:payout_method])
    @payout.user = current_user

    if current_user.has_payout_method?
      flash[:notice] = t('payout_method.create.already_exist')
    elsif @payout.save
      flash[:notice] = t('payout_method.create.success')
      redirect_to payout_methods_path
    else
      put_model_errors_to_flash(@payout.errors)
      render :action => "new"
    end
  end

  # DELETE /trips/1
  def destroy
    # only the owner of the comment can do the deletion
    @payout = current_user.payout_methods.find(params[:id])
    # apply soft delete, set deleted attribute to true
    @payout.update_attribute(:deleted, true)
    flash[:notice] = t('payout_method.destroy.success')

    redirect_to :back
  end

  private
  
    # return the review type in form of class
    def payout_type
      params[:type].blank? ? IndonesiaBankTransferPayoutMethod : params[:type].constantize
    end

    # load user and put it to @user, some views expect this to be defined
    def load_user
      @user = current_user
    end
end
