class MarketController < ApplicationController

  layout 'market'

  def index
    # set all parameter to be variable for easy access
    keyword = params[:keyword]

    # build scoped SuggestedItem by passing parameter to method build_scope_search
    scope = Item.build_market_scope_search(keyword)
    @no_valid_parameter = keyword.blank?
    @total_result = scope.size
    @items = scope.paginate(:page => params[:page], :per_page => Item::SEARCH_PER_PAGE)
  end
  
end
