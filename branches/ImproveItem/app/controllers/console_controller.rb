class ConsoleController < ApplicationController

  before_filter :authenticate
  before_filter :has_admin_role

  layout 'console'

  ESCROWS_PER_PAGE = 10

  def index
    redirect_to console_escrow_path
  end

  def escrow
    @escrows = Escrow.order("id DESC").paginate(:per_page => ESCROWS_PER_PAGE, :page => params[:page])
  end

  def escrow_edit
    @escrow = Escrow.find(params[:id])
  end

  def escrow_update
    @escrow = Escrow.find(params[:id])
    @escrow.internal = params[:do_internal] == 'on' ? true : false

    if @escrow.update_attributes(params[:escrow])
      flash[:notice] = 'Escrow Updated'
      redirect_to console_escrow_path(:page => find_escrow_page(@escrow))
    else
      put_model_errors_to_flash(@escrow.errors)
      render :action => 'escrow_edit'
    end
  end

    # utility to create bistip payout log.
  def escrow_create_released_seller_payout_log
    escrow_id = params[:escrow_id]
    skip_broadcast = params[:skip_broadcast]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_SELLER_REQUESTED_PAYOUT
      if !skip_broadcast.blank? and skip_broadcast == 'true'
        BistipReleasedSellerPayoutLog.create!(:escrow_id => escrow_id, :skip_broadcast => true)
      else
        BistipReleasedSellerPayoutLog.create!(:escrow_id => escrow_id)
      end
      flash[:notice] = 'Done :)'
      redirect_to console_escrow_path(:page => find_escrow_page(escrow))
    end
  end

  # utility to create bistip payout log.
  def escrow_create_released_buyer_payout_log
    escrow_id = params[:escrow_id]
    skip_broadcast = params[:skip_broadcast]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_BUYER_REQUESTED_PAYOUT
      if !skip_broadcast.blank? and skip_broadcast == 'true'
        BistipReleasedBuyerPayoutLog.create!(:escrow_id => escrow_id, :skip_broadcast => true)
      else
        BistipReleasedBuyerPayoutLog.create!(:escrow_id => escrow_id)
      end
      flash[:notice] = 'Done :)'
      redirect_to console_escrow_path(:page => find_escrow_page(escrow))
    end
  end

  # utility to create bistip received transfer log
  def escrow_create_received_buyer_bank_transfer_log
    escrow_id = params[:escrow_id]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_BUYER_INITIATED
      BistipReceivedBuyerBankTransferLog.create!(:escrow_id => escrow_id)
      flash[:notice] = 'Done :)'
      redirect_to console_escrow_path(:page => find_escrow_page(escrow))
    end
  end

  # find payout method to a user
  def payout_method
    username = params[:username]
    user = User.find_by_username(username)
    @payout_methods = user.payout_methods unless user.blank?
  end

  def has_admin_role
    # only apply admin role for production environment
    if ::Rails.env == 'production'
      unless current_user.admin?
        redirect_to root_path
      end
    end
  end

  # find which page an escrow belongs to
  def find_escrow_page(escrow)
    escrow_page = Escrow.order("id DESC").index(escrow)
    index = escrow_page.blank? ? 1 : escrow_page + 1 #find array index of current_post and +1 because page start from 1
    ((index * 1.0)/ ESCROWS_PER_PAGE).ceil #make index float and divide by PER_PAGE and ceil the result
  end

  # suggested item index
  def idea_item
    @idea_items = IdeaItem.order("id DESC").where('deleted = true').paginate(:per_page => 10, :page => params[:page])
  end

  # utility to approve suggested item
  def idea_item_approve
    idea_item = IdeaItem.find(params[:id])
    idea_item.update_attribute(:deleted, false)
    IdeaItemApprovalNotification.create!(:sender_id => idea_item.user.id, :receiver_id => idea_item.user.id, :data_id => idea_item.id)
    Notifier.delay.email_idea_item_approval(idea_item)
    redirect_to console_idea_item_path
  end

  # story index
  def story
    @stories = Story.order("id DESC").where('deleted = true').paginate(:per_page => 10, :page => params[:page])
  end

  # utility to create bistip approved story
  def story_approve
    story = Story.find(params[:id])
    story.deleted = false
    story.save
    StoryApprovalNotification.create!(:sender_id => story.user.id, :receiver_id => story.user.id, :data_id => story.id)
    Notifier.delay.email_story_approval(story)
    redirect_to console_story_path
  end

  def forums
    @posts = Post.unscoped.order("id DESC").paginate(:per_page => 10, :page => params[:page])
  end

  def delete_post
    @post = Post.find(params[:id])
    @topic = @post.topic
    page_to_go = find_post_page(@post)
    @post.deleted = true
    @post.save

    flash[:notice] = t('post.destroy.message.success')

    # delete the topic as well if this post is the only post in there
    if @topic.last_post.blank?
      @topic.deleted = true
      @topic.save
    end

    redirect_to console_forums_path(:page => page_to_go)
  end

  # find which page a post belongs to
  def find_post_page(post)
    post_page = Post.unscoped.order("id DESC").index(post)
    index = post_page.blank? ? 1 : post_page + 1 #find array index of current_post and +1 because page start from 1
    ((index * 1.0)/ 10).ceil #make index float and divide by PER_PAGE and ceil the result
  end

  def conversations
    @messages = Message.thread_starter_only.order("id DESC").paginate(:per_page => 10, :page => params[:page])
  end

  #get message
  def conversations_show
    @message = Message.find(params[:id])

    # find thread starter, if this message is a reply
    if !@message.thread_starter?
      @message = Message.find(@message.reply_to_id)
    end

    # find all the child messages
    @messages = Message.order('created_at asc').children(@message.id)
  end

  # utility to approve suggested item
  def store_item_approve
    store_item = StoreItem.find(params[:id])
    store_item.update_attribute(:approved, true)
    StoreItemApprovalNotification.create!(:sender_id => store_item.user.id, :receiver_id => store_item.user.id, :data_id => store_item.id)
    Notifier.delay.email_store_item_approval(store_item)
    flash[:notice] = 'Done :)'
    redirect_to console_store_item_path
  end

  # store item index
  def store_item
    @store_items = StoreItem.order("id DESC").where('approved = false').paginate(:per_page => 10, :page => params[:page])
  end

  # store order
  def store_order
    @store_orders = StoreOrder.order("id DESC").paginate(:per_page => 10, :page => params[:page])
  end

  def create_store_safepay
    @store_order = StoreOrder.find(params[:id])

    if !@store_order.delivery_fee_quoted?
      redirect_to :back
    elsif @store_order.escrow.blank?
      escrow = Escrow.new
      escrow.item = @store_order.order_items.first.item.name
      escrow.buyer = @store_order.buyer
      escrow.seller = @store_order.seller
      escrow.amount = @store_order.total_all_without_safepay_fee
      escrow.payment_method_id = 2
      escrow.for_store = true
      escrow.save

      @store_order.update_attribute(:escrow_id, escrow.id)

      BuyerInitiatedLog.create!(:escrow_id => escrow.id)
      BistipReceivedBuyerBankTransferLog.create!(:escrow_id => escrow.id)

      flash[:notice] = 'Done :)'

      page_to_go = find_store_order_page(@store_order)
      redirect_to console_store_order_path(:page => page_to_go)
    end
  end

  # find which page an store order belongs to
  def find_store_order_page(store_order)
    so_page = StoreOrder.order("id DESC").index(store_order)
    index = so_page.blank? ? 1 : so_page + 1 #find array index of current_post and +1 because page start from 1
    ((index * 1.0)/ 10).ceil #make index float and divide by PER_PAGE and ceil the result
  end

end
