class RelationsController < ApplicationController

  before_filter :authenticate, :only => [:new]

  def index
    @user = User.find_by_username(params[:user])
  end

  def new
    to_user = User.find_by_username(params[:user])

    # check if relation already exist
    if current_user == to_user
      flash[:notice] = t('relation.new.not_self')
    elsif Relation.where(:from_id => current_user.id, :to_id => to_user.id).count < 1
      relation = current_user.relations.new
      relation.to_id = to_user.id
      if relation.save
        RelationNotification.create!(:receiver=>to_user, :sender=>current_user)
        Notifier.delay.email_relation(current_user, to_user, Relation.where(:from_id => to_user.id, :to_id => current_user.id).count > 0) if to_user.user_configuration.email_received_relation
        flash[:notice] = t('relation.new.success', :username => to_user.username)
      else
        flash[:notice] = t('relation.new.failure')
      end
    else
      flash[:notice] = t('relation.new.already_exist', :username => to_user.username)
    end

    # redirect to given uri
    uri = params[:original_uri]
    
    if uri.blank?
      redirect_to profile_path(to_user.username)
    else
      redirect_to uri
    end
  end

end
