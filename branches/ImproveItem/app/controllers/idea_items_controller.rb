class IdeaItemsController < ApplicationController
  
  # must be logged in
  before_filter :authenticate, :only => [:new, :create, :edit, :update]

  # display a idea item new form
  def new
    @idea_item = IdeaItem.new
    IdeaItem::PICTURES_MAXIMUM.times { @idea_item.pictures.build }
  end

  # accept form submission to create idea item
  def create
    # associate suggested item with the current user
    @idea_item = IdeaItem.new(params[:idea_item])
    @idea_item.user = current_user
    
    if @idea_item.save
      flash[:notice] = t('suggested_items.create.message.success')
      ContactUsNotifier.delay.email_suggested_item(@idea_item)
      redirect_to item_path(@idea_item)
    else
      # if there is form error, upload picture will be gone so lets clean the assets and rebuild
      flash.now[:alert] = t('general.reupload_picture') unless @idea_item.pictures.blank?
      @idea_item.pictures.clear
      IdeaItem::PICTURES_MAXIMUM.times { @idea_item.pictures.build }
      put_model_errors_to_flash(@idea_item.errors)
      render :action => "new"
    end
  end

  # display a idea item edit form
  def edit
    @idea_item = current_user.idea_items.find(params[:id])
    (IdeaItem::PICTURES_MAXIMUM - @idea_item.pictures.size).times { @idea_item.pictures.build }
  end

  # update suggested item
  def update
    @idea_item = current_user.idea_items.find(params[:id])

    if @idea_item.update_attributes(params[:idea_item])
      flash[:notice] = t('suggested_items.update.message.success')
      redirect_to item_path(@idea_item)
    else
      put_model_errors_to_flash(@idea_item.errors)
      
      # if there is form error, upload picture will be gone so lets clean the assets and rebuild
      rejected = @idea_item.pictures.reject {|picture| !picture.new_record?}
      flash.now[:alert] = t('general.reupload_picture') unless rejected.blank?
      (IdeaItem::PICTURES_MAXIMUM - @idea_item.pictures.size).times { @idea_item.pictures.build }
      
      render :action => "edit"
    end
  end
  
end
