class UserConfigurationController < ApplicationController
  ssl_required :edit, :update
  before_filter :authenticate, :only => [:edit, :update]

  def edit
    @user = current_user
    @config = current_user.user_configuration
  end
  
  def update
    if current_user.user_configuration.update_attributes(params[:user_configuration])
      flash[:notice] = t('configuration.update.message.success')
      redirect_to dashboard_profile_path
    else
      put_model_errors_to_flash(current_user.errors)
      render :action => 'edit'
    end

  end
end
