class ContentController < ApplicationController

  def event_item_suggestion_voting
    @suggested_items = SuggestedItem.active_only.created_between(SuggestedItem::COMP_START, SuggestedItem::COMP_END).find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
      :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id LEFT OUTER JOIN users ON users.id = suggested_item_votes.user_id',
      :group => 'suggested_items.id',
      :conditions => ["users.disabled = false"],
      :order => 'counter DESC').paginate(:page => params[:page], :per_page => SuggestedItem::VOTING_PER_PAGE)
  end

  def event_europe_wish_voting
    europe = [1, 6, 13, 16, 18, 21, 23, 35, 42, 53, 54, 55, 57, 62, 66, 68, 69, 73, 74, 76, 80, 82, 88, 97, 99, 101, 103, 108, 109, 110, 128, 132, 133, 134, 137, 138, 139, 142, 151, 164, 165, 177, 182, 187, 188, 189, 195, 198, 199, 200, 202, 226, 232]
    @suggested_items = SuggestedItem.active_only.origin_country_ids(europe).find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
      :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id LEFT OUTER JOIN users ON users.id = suggested_item_votes.user_id',
      :group => 'suggested_items.id',
      :order => 'counter DESC').paginate(:page => params[:page], :per_page => SuggestedItem::VOTING_PER_PAGE)
  end
end
