class CreateWegoTravelDealsSubscriptions < ActiveRecord::Migration
  def self.up
    create_table :wego_travel_deals_subscriptions do |t|
      t.belongs_to :user
      t.column :email_sent, :boolean, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :wego_travel_deals_subscriptions
  end
end
