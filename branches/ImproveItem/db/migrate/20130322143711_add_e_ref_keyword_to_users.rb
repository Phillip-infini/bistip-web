class AddERefKeywordToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :eref_keyword, :string
  end

  def self.down
    remove_column :users, :eref_keyword
  end
end
