class AddItemIdToComments < ActiveRecord::Migration
  def self.up
    add_column :comments, :item_id, :integer, :references => :items
  end

  def self.down
    remove_column :comments, :item_id
  end
end
