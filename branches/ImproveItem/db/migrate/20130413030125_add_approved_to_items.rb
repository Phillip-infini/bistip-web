class AddApprovedToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :approved, :boolean
  end

  def self.down
    remove_column :items, :approved
  end
end
