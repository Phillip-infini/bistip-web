class CreateItemVotes < ActiveRecord::Migration
  def self.up
    create_table :item_votes do |t|
      t.column :item_id, :integer, :null => false, :references => :items
      t.belongs_to :user
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :item_votes
  end
end
