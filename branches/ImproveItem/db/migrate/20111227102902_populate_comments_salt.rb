class PopulateCommentsSalt < ActiveRecord::Migration
  def self.up
    Comment.all.each do |comment|
      comment.generate_salt
      comment.save
    end
  end

  def self.down
    Comment.all.each do |comment|
      comment.update_attribute(:salt, nil)
    end
  end
end
