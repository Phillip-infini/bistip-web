class CreateSeekVotes < ActiveRecord::Migration
  def self.up
    create_table :seek_votes do |t|
      t.column :seek_id, :integer, :null => false, :references => :seeks
      t.belongs_to :user
      t.timestamps
    end
  end

  def self.down
    drop_table :seek_votes
  end
end
