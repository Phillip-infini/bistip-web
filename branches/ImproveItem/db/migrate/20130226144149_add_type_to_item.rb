class AddTypeToItem < ActiveRecord::Migration
  def self.up
    add_column :items, :type, :string
  end

  def self.down
    add_column :items, :type, :string
  end
end
