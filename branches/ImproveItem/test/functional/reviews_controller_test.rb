require 'test_helper'

class ReviewsControllerTest < ActionController::TestCase
  
  setup do
    @userx = users(:dono)
    @usery = users(:one)
  end

  test "should get new" do
    login_as(:dono)
    get :new, :user => @usery.username, :message_id => messages(:donotoone1).id
    assert_response :redirect
  end


  test "should get new for invitation" do
    login_as(:dono)
    get :new, :user => @usery.username, :invitation => true
    assert_response :success
  end

  test "should not allow new" do
    login_as(:dono)
    get :new, :user => @usery.username
    assert_redirected_to profile_path(@usery.username)
  end

  test "should not allow create" do
    login_as(:dono)
    assert_no_difference('PositiveReview.count') do
      post :create, :user => @usery.username, :type => 'PositiveReview', :review => {
        :body =>"very good service"
      }
      assert_redirected_to profile_path(@usery.username)
    end
  end


  test "userX should not be allowed to give a positive review to userY" do
    login_as(:dono)
    assert_no_difference('PositiveReview.count') do
      post :create, :user => @usery.username, :type => 'PositiveReview', :review => {
        :body =>"very good service",
        :message_id => messages(:donotoone1).id
      }
    end
  end

  test "userX should not be allowed to give a negative review to userY" do
    login_as(:dono)
    assert_no_difference('NegativeReview.count') do
      post :create, :user => @usery.username, :type => 'NegativeReview', :review => {
        :body =>"very good service",
        :message_id => messages(:donotoone1).id
      }
    end
  end

  test "userX should give a invitation positive review to userY" do
    login_as(:dono)
    assert_difference('PositiveReview.count') do
      post :create, :user => @usery.username, :type => 'PositiveReview', :review => {
        :body =>"very good service",
        :invitation => true
      }
    end
    assert_redirected_to reviews_user_path(:username => @usery.username)
  end

  test "userX should give a invitation negative review to userY" do
    login_as(:dono)
    assert_difference('NegativeReview.count') do
      post :create, :user => @usery.username, :type => 'NegativeReview', :review => {
        :body =>"very good service",
        :invitation => true
      }
    end
    assert_redirected_to reviews_user_path(:username => @usery.username)
  end
end
