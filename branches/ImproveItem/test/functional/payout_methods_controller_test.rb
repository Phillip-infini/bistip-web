require 'test_helper'

class PayoutMethodsControllerTest < ActionController::TestCase
  setup do
    @pay = 'IndonesiaBankTransferPayoutMethod'
    @user = users(:dono)
  end

  #should be able to render payout methods index
  test "should get payout methods index" do
    login_as(:dono)
    get :index, :user_id=>@user.id
    assert_response :success

  end
  #testing rendering new form
  test "should get payout methods new" do
    login_as(:dono)
    get :new, :user_id=>@user.id
    assert_response :success

  end
  #testing creating a new payout method
  test "should create a new payout method" do
    login_as(:dono)
    get :new, :user_id=>@user.id
    assert_difference ('PayoutMethod.count') do
      post :create, :user_id => @user.id, :payout_method => { :type =>@pay, 
        :bank_name => 'Bank Central Asia',
        :account_number => '231313123',
        :account_name => 'Willy Ekosalim',
        :branch => 'Mangga Dua',
        :city => 'Jakarta'
      }
    end
    assert_response :redirect

  end
  #testing creating multiple payout methods
  test "shouldn't create multiple payout methods" do
    login_as(:dono)
    get :new, :user_id=>@user.id
    assert_difference ('PayoutMethod.count') do
      post :create, :user_id => @user.id, :payout_method => { :type =>@pay, 
        :bank_name => 'Bank Central Asia',
        :account_number => '231313123',
        :account_name => 'Willy Ekosalim',
        :branch => 'Mangga Dua',
        :city => 'Jakarta'
      }
    end
    assert_response :redirect
    get :new, :user_id=>@user.id
    !assert_difference('PayoutMethod.count') do
      post :create, :user_id => @user.id, :payout_method => { :type =>@pay,
        :bank_name => 'Bank Central Asia',
        :account_number => '231313123',
        :account_name => 'Willy Ekosalim',
        :branch => 'Mangga Dua',
        :city => 'Jakarta'
      }
    end
    assert_response :redirect

  end

  #testing deleting a payout methods
  test "should delete payout methods" do
    login_as(:dono)
    get :new, :user_id=>@user.id
    assert_difference ('PayoutMethod.count') do
      post :create, :user_id => @user.id, :payout_method => { :type =>@pay,
        :bank_name => 'Bank Central Asia',
        :account_number => '231313123',
        :account_name => 'Willy Ekosalim',
        :branch => 'Mangga Dua',
        :city => 'Jakarta'
      }
    end
    assert_response :redirect
    @request.env['HTTP_REFERER'] = 'http://localhost:3000/users/'
    assert_difference ('PayoutMethod.count') ,-1 do
      delete :destroy, :user_id => @user.id, :id=>@user.payout_methods.last.id
    end

  end

end
