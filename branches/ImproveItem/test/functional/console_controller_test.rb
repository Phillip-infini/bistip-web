require 'test_helper'

class ConsoleControllerTest < ActionController::TestCase

  test "console create_released_seller_payout_log" do
    login_as(:one)
    get :escrow_create_released_seller_payout_log, :escrow_id => escrows(:SellerRequestedPayout).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test "console create_released_buyer_payout_log" do
    login_as(:one)
    get :escrow_create_released_buyer_payout_log, :escrow_id => escrows(:BuyerRequestedPayout).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test 'console create_received_buyer_bank_transfer_log' do
    login_as(:one)
    get :escrow_create_received_buyer_bank_transfer_log, :escrow_id => escrows(:donotomac).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end
  
end
