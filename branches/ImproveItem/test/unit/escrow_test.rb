require 'test_helper'

class EscrowTest < ActiveSupport::TestCase

  test "should create escrow of a message" do
    escrow = Escrow.new
    escrow.message = messages(:onetomac)
    escrow.buyer = users(:one)
    escrow.seller = users(:mactavish)
    escrow.currency = Currency.first
    escrow.payment_method = PaymentMethod.first
    escrow.amount = 30
    escrow.item = 'ipad 2'
    assert escrow.save
  end

  test 'check escrow state' do
    assert escrows(:donotomac).current_state == Escrow::STATE_BUYER_INITIATED
    assert escrows(:donotomac).last_log.class == BuyerInitiatedLog
  end

  test 'escrow paypal encrypted' do
    assert_nothing_raised {escrows(:donotomac).paypal_encrypted('http://paypal.com', 'http://paypal.com')}
  end

  test 'escrow UI state methods' do
    escrow = escrows(:donotomac)
    assert_equal 'details', escrow.current_step
    escrow.next_step
    assert_equal 'confirmation', escrow.current_step
    escrow.previous_step
    assert_equal 'details', escrow.current_step
    escrow.next_step
    assert_equal 'confirmation', escrow.current_step
    assert escrow.all_valid?
  end

  test 'escrow amount calculation' do
    escrow = escrows(:donotomac)
    assert_nothing_raised {escrow.amount_with_currency}
    assert_nothing_raised {escrow.amount_after_fee_with_currency}
    assert_nothing_raised {escrow.amount_after_fee}
  end

  test 'escrow payment method' do
    escrow = escrows(:donotomac)
    assert_nothing_raised {escrow.bank_transfer_notes}
    assert !escrow.payment_method_bank_trasnfer?
  end

  test 'escrow user method' do
    escrow = escrows(:donotomac)
    assert escrow.is_buyer_or_seller?(users(:dono))
    assert escrow.is_buyer_or_seller?(users(:mactavish))
    assert !escrow.is_buyer_or_seller?(users(:one))

    assert_equal users(:mactavish), escrow.not_this_user(users(:dono))
    assert_equal users(:dono), escrow.not_this_user(users(:mactavish))
  end

  test 'escrow static methods' do
    assert_equal 13, Escrow.user_all_escrows(users(:dono)).count
  end

end
