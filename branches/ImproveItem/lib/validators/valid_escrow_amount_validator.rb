# validator class to ensure that a date is a today's date or future date

class ValidEscrowAmountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !value.nil? and !record.payment_method.blank?
      if record.for_store?
        if value < 0 or value > 1000000000
          record.errors[attribute] << (options[:message] || I18n.t("escrow.errors.custom.valid_amount.#{record.payment_method.cname}"))
        end
      elsif !record.payment_method.amount_allowable?(value)
        record.errors[attribute] << (options[:message] || I18n.t("escrow.errors.custom.valid_amount.#{record.payment_method.cname}"))
      end
    end
  end
end