class AffiliatePayment < ActiveRecord::Base
  scope :start_and_end, lambda {|start_period, end_period| {:conditions => ["start = ? and end = ?", start_period, end_period]}}
end
