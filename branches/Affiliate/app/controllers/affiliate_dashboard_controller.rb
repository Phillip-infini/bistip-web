class AffiliateDashboardController < AffiliateController
  
  before_filter :authenticate, :except => [:login, :login_create]
  ssl_required :login, :login_create

  def login_create
    
    affiliate = Affiliate.authenticate(params[:email], params[:password])

    # If already logged in then return to root path
    if affiliate_logged_in?
      redirect_to affiliate_dashboard_path
    # Try to authenticate given email and password
    elsif affiliate
      create_auth_cookie(affiliate)
      flash[:notice] = t('login.message.success')
      redirect_to affiliate_dashboard_path
    else
      flash.now[:alert] = t('login.message.fail')
      render :action => 'login'
    end
  end

  # logout
  def logout
    if affiliate_logged_in?
      cookies.delete AFFILIATE_AUTH_COOKIE
      flash[:notice] = t('logout.message.success')
    end
    redirect_to affiliate_dashboard_login_path
  end

end
