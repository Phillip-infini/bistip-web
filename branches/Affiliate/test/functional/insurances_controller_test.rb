require 'test_helper'

class InsurancesControllerTest < ActionController::TestCase
  
  setup do
    @message = messages(:onetomac)
    @current_user = users(:one)
    login_as(:one)
  end

  test "should get new" do
    get :new, :message_id => @message.id, :seller_id => users(:dono).id
    assert_response :success
    assert_template :new
    assert_not_nil assigns(:insurance)
  end

  test "should get create (confirmation step)" do
    session[:insurance_params] = {}
    session[:insurance_step] = 'confirmation'
    assert_difference('Insurance.count') do
      post :create, :insurance => {
        :message_id => @message.id,
        :cover_value => 1000000,
        :buyer_id => users(:one).id,
        :seller_id => users(:mactavish).id,
        :item => 'ipad 2',
        :currency_id => currencies(:usd).id,
        :amount => 200
        }
      assert_redirected_to user_message_path(@current_user, @message)
    end
  end

  test "should get create (details step)" do
    session[:insurance_params] = {}
    session[:insurance_step] = 'details'
    assert_no_difference('Insurance.count') do
      post :create, :insurance => {
        :message_id => @message.id,
        :cover_value => 1000000,
        :buyer_id => users(:one).id,
        :seller_id => users(:mactavish).id,
        :item => 'ipad 2',
        :currency_id => currencies(:usd).id,
        :amount => 200
        }
      assert_response :success
      assert_template :new
    end
  end

  test 'should get index' do
    get :index, :locale => 'id'
    assert_response :success

    get :index, :locale => 'en'
    assert_response :success
  end

end
