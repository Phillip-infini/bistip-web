class PopulateUsersApiAuthKey < ActiveRecord::Migration
  def self.up
    User.all.each do |user|
      user.generate_api_auth_key
      user.save
    end
  end

  def self.down
    User.all.each do |user|
      user.update_attribute(:api_auth_key, nil)
    end
  end
end
