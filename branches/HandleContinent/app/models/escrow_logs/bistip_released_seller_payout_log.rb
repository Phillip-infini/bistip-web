# To change this template, choose Tools | Templates
# and open the template in the editor.

class BistipReleasedSellerPayoutLog < EscrowLog

  # broadcast the creation of this log inform of notification and email
  # override
  def broadcast
    EscrowLogNotification.create!(:data_id => self.id, :sender => escrow.seller, :receiver => escrow.seller)
    Notifier.delay.email_escrow_log(escrow.seller, self)
  end
  
end
