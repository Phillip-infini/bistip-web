# a class to represent a payout method
class PaypalPayoutMethod < PayoutMethod
  
  PAYPAL_CNAME = 'paypal'

  validates :email,
    :length => { :within => 5..100 },
    :format => { :with => /^[^@][\w.-]+@[\w.-]+[.][a-z]{2,4}$/i}

  # get cname of payment method
  def cname
    PAYPAL_CNAME
  end
end
