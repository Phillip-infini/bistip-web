# Class to send contact us email notification
class ContactUsNotifier < ActionMailer::Base
  default :from => "Bistip.com <willy@bistip.com>"
  
  def email_contact_us(name, email, subject, body)
    @name = name
    @email = email

    # dont use @subject or @body here it's a reserved key word
    @topic = subject
    @content =  body
    mail :to => 'team@bistip.com', :subject => 'Masukan'
  end

  def email_exception(message, trace, ip, user_agent, fullpath, referer, type, suspicious)
    @message = message
    @trace = trace
    @ip = ip
    @agent = user_agent
    @fullpath = fullpath
    @referer = referer
    
    @user = suspicious

    @country_name = "n/a"
    @city = "n/a"
    @region_name = "n/a"
    @zipcode = "n/a"
    geocode = nil
    begin
      geocode = Geocoder.search(ip.to_s)
    rescue Exception => e
      geocode = nil
    end
    
#    if !geocode.nil?
#      if !geocode[0].data.nil?
#        @country_name = geocode[0].country_name if !geocode[0].country_name.empty?
#        @city = geocode[0].city if !geocode[0].city.empty?
#        @region_name = geocode[0].region_name if !geocode[0].region_name.empty?
#        @zipcode = geocode[0].zipcode if !geocode[0].zipcode.empty?
#      end
#    end
    mail :to => 'team@bistip.com', :subject => "Exception #{type}"
  end

  def email_forum_event(email_to, forum, topic, post)
    @forum = forum
    @topic = topic
    @post = post
    mail :to => email_to, :subject => 'Forum event'
  end

  def email_insurance(message, user, type)
    @message = message
    @user = user
    @type = type
    mail :to => 'team@bistip.com', :subject => 'Asuransi'
  end

  def email_statistics(user_count, trip_count, seek_count, comment_count, message_count, good_rep_count, bad_rep_count, item_count, fb_count, tw_count)
    @user_count = user_count
    @trip_count = trip_count
    @seek_count = seek_count
    @comment_count = comment_count
    @message_count = message_count
    @good_rep_count = good_rep_count
    @bad_rep_count = bad_rep_count
    @item_count = item_count
    @fb_count = fb_count
    @tw_count = tw_count
    mail :to => 'team@bistip.com', :subject => 'Statistics'
  end

  def email_escrow_log(escrow)
    @escrow = escrow
    mail :to => 'team@bistip.com', :subject => 'Escrow Paypal Transaction'
  end

  def email_insurance_log(insurance)
    @insurance = insurance
    mail :to => 'team@bistip.com', :subject => 'Insurance Application'
  end

  def email_stories(id, name, email, subject, body)
    @id = id
    @name = name
    @email = email
    @topic = subject
    @content =  body
    mail :to => 'team@bistip.com', :subject => 'User Story'
  end

  def email_suggested_item(suggested_item)
    @suggested_item = suggested_item
    mail :to => 'team@bistip.com', :subject => 'Suggested Item'
  end

  def email_cashback_request(user, escrow_id)
    @user = user
    @escrow_id = escrow_id
    mail :to => 'team@bistip.com', :subject => 'Cashback Request'
  end
end
