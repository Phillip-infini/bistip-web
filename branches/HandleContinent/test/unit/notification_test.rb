require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  
  test "should create notification on trip" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = TripCommentNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => trips(:jktsyd).id,
      :type => 'TripCommentNotification')
    assert new.to_link(new.receiver)
  end

  test "should create notification on seek" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = SeekCommentNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => seeks(:jktsyd).id,
      :type => 'SeekCommentNotification')
    assert new.to_link(new.receiver)
  end

  test "should create review notification" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = ReviewNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => receiver.id,
      :type => 'ReviewNotification')
    assert new.to_link(new.receiver)
  end

  test "should create relation notification" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = RelationNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => relations(:one).id,
      :type => 'RelationNotification')
    assert new.to_link(new.receiver)
  end

  test "should create quote post notification" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = QuotePostNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => posts(:ipadpost).id,
      :type => 'QuotePostNotification')
    assert new.to_link(new.receiver)
  end

  test "should create topic post notification" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = TopicPostNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => posts(:ipadpost).id,
      :type => 'TopicPostNotification')
    assert new.to_link(new.receiver)
  end

  test "should create match trip notification" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = MatchTripNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => trips(:jktsyd).id,
      :type => 'MatchTripNotification')
    assert new.to_link(new.receiver)
  end

  test "should create match seek notification" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = MatchSeekNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => seeks(:jktsyd).id,
      :type => 'MatchSeekNotification')
    assert new.to_link(new.receiver)
  end

  test "should create insurance log notification" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = InsuranceLogNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => insurance_logs(:donotomac).id,
      :type => 'InsuranceLogkNotification')
    assert new.to_link(new.receiver)
  end

  test "should create escrow log notification" do
    sender = users(:dono)
    receiver = users(:one)
    assert new = EscrowLogNotification.create!(:receiver => receiver,
      :sender => sender,
      :data_id => escrow_logs(:donotomac).id,
      :type => 'EscrowLogNotification')
    assert new.to_link(new.receiver)
  end

end
