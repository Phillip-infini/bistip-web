require 'test_helper'

class GenderTest < ActiveSupport::TestCase

  test "get all gender" do
    assert_equal 3, Gender.all.size
  end

  test "get gender select list" do
    # +1 because it includes nil select
    assert_equal 4, Gender.select_list.size
  end
end
