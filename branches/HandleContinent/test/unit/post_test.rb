require 'test_helper'

class PostTest < ActiveSupport::TestCase

  test "should create post" do
    post = Post.new
    post.body = 'Ipad Ipad'
    post.user = users(:one)
    post.topic = topics(:ipad)
    assert post.save
  end

  test "should create quoted post" do
    post = Post.new
    post.body = 'Ipad Ipad'
    post.quoted = 'yeah buy it man'
    post.user = users(:one)
    post.topic = topics(:ipad)
    post.quoted_from = users(:dono)
    assert post.save
    assert post.quote?
  end

  test "should find post" do
    post_id = posts(:ipadpost).id
    assert_nothing_raised {Post.find(post_id)}
  end

  test "should update post" do
    post = posts(:ipadpost)
    assert post.update_attributes(:body => 'Ipad Ipad')
    assert post.edited?
  end

  test "should delete post" do
    post_id = posts(:ipadpost).id
    assert_nothing_raised {Post.find(post_id)}
    post = Post.find(post_id)
    assert post.update_attributes(:deleted=>true)
  end

  test 'post find page' do
    assert_equal 2, posts(:p11).find_page
  end

end