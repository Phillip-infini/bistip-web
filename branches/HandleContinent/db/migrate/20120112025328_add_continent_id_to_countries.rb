class AddContinentIdToCountries < ActiveRecord::Migration
  def self.up
    add_column :countries, :continent_id, :integer, :null => true, :references => :continents
    
    # create and populate continent id for countries
    File.open("#{Rails.root}/db/data/countries-continents.txt", "r") do |infile|
      infile.read.each_line do |country_row|
        country_code, country_name, continent = country_row.chomp.split("|")
        if Country.find_by_code(country_code.downcase)
          country = Country.find_by_code(country_code.downcase)
          continent = Continent.find_by_code(continent.downcase)
          country.continent_id = continent.id
          country.save
          puts 'setting continent id for: ' + country_code + ' with value ' + continent.name
        else
          puts 'not found: ' + country_code + ' ' + country_name + " " + continent + ' in the database'
        end
      end
    end
  end

  def self.down
    remove_column :countries, :continent_id
  end
end
