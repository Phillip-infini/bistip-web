class CreateContinentsCountriesHandler < ActiveRecord::Migration
  def self.up

    # europe country already created, lets handle migration for this
    euro = Country.find_by_code('eu')
    euro.code = Country::CONTINENT_CODE
    euro.continent = Continent.find_by_code('eu')
    euro.save

    # create all 5 continent placeholder in country database

    # Asia
    country = Country.new
    country.name = 'Asia'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('as')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # North America
    country = Country.new
    country.name = 'North America'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('na')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # South America
    country = Country.new
    country.name = 'South America'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('sa')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # Africa
    country = Country.new
    country.name = 'Africa'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('af')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # Africa
    country = Country.new
    country.name = 'Oceania'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('oc')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # Antartica
    country = Country.new
    country.name = 'Antartica'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('an')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

  end

  def self.down
    # nothing
  end
end
