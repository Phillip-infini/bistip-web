# validator class to ensure that a date is a today's date or future date

class ValidCoverValueAmountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !value.nil? and (value < 1000000)
      record.errors[attribute] << (options[:message] || I18n.t("insurance.errors.custom.cover_value"))
    end
  end
end