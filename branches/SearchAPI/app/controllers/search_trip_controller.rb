class SearchTripController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    origin = check_location(params[:origin])
    destination = check_location(params[:destination])
    departure_date = parse_date_parameter(params[:departure_date])
    departure_date_predicate = params[:departure_date_predicate]
    notes = params[:notes]

    # build scoped Trip by passing parameter to method build_scope_search
    scope = Trip.build_scope_search(origin, destination, departure_date, departure_date_predicate, notes)

    @trips = scope.paginate(:page=>params[:page], :per_page => 5)
  end
end
