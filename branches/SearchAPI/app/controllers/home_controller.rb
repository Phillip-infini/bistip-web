class HomeController < ApplicationController

  def index
    count = 9
    if mobile_version?
      count = 2
    end

    @trips = Trip.random(count)
    @seeks = Seek.random(count)
  end

  def expire_cloud
    password = params[:password]

    if password == 'gad1ng_c00g33'

      # regenerate trending item
      TrendingItem.generate

      # expire route tag cloud cache
      expire_fragment('all_route')
      redirect_to root_path
    end
  end
end
