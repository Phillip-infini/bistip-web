# validator class to ensure that a date is a today's date or future date

class FutureDateValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if (!value.nil?)
      record.errors[attribute] << (options[:message] || I18n.t('trip.errors.custom.future_date')) unless
      value >= DateTime.current.beginning_of_day
    end
  end
end

