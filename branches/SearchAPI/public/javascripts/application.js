// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

function buildSearchTripURL(mobile)
{
    var url = "/search_bistiper?";
    
    var origin = escape($("#origin").val());
    if (origin)
    {
        url += "&origin=" + escape($("#origin").val());
    }

    var destination = escape($("#destination").val());
    if (destination)
    {
        url += "&destination=" + escape($("#destination").val());
    }

    var departure_date = escape($("#departure_date").val());
    if (mobile)
    {
        // this is DODGY! change it in the future
        // this is forcing us to use the text in the dropdown instead of the valus
        date_month = escape($("#date_month option:selected").val());
        if (date_month)
        {
            date_month = escape($("#date_month option:selected").text());
        }
        
        departure_date = escape($("#date_day option:selected").val()) + " " + date_month + " " + escape($("#date_year option:selected").val());
        departure_date = jQuery.trim(departure_date);
    }
    
    if (departure_date)
    {
        url += "&departure_date=" + departure_date;
    }

    var departure_date_predicate = escape($("input[name='departure_date_predicate']:checked").val());
    if (departure_date_predicate && departure_date_predicate != 'undefined')
    {
        url += "&departure_date_predicate=" + departure_date_predicate;
    }

    var notes = escape($("#notes").val());
    if (notes)
    {
        url += "&notes=" + notes;
    }

    window.location.replace(url);
    return false;
}

function buildSearchSeekURL()
{
    var url = "/search_wanted_bistiper?";

    var origin = escape($("#origin").val());
    if (origin)
    {
        url += "&origin=" + origin;
    }

    var destination = escape($("#destination").val());
    if (destination)
    {
        url += "&destination=" + destination;
    }

    var notes = escape($("#notes").val());
    if (notes)
    {
        url += "&notes=" + notes;
    }

    window.location.replace(url);
    return false;
}

// function to remove dynamicaly an item form fields
function remove_fields(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".fields").hide();
}

// function to add dynamicaly an item form fields
function add_fields(link, association, content, maximum) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g")
    $(link).parent().parent().before(content.replace(regexp, new_id));
    var now_fields = $(".fields").length
    if (now_fields >= maximum)
    {
        $(link).parent().parent().remove();
    }
}

// function handle from anywhere
function handle_from_anywhere(link)
{
    if (link.checked == true)
    {
        $("#seek_origin_location").val('<anywhere>');
        $("#seek_origin_location").attr('readonly', true);
        $("#seek_origin_location").attr('disabled', true);
    }
    else
    {
        $("#seek_origin_location").val('');
        $("#seek_origin_location").attr('readonly', false);
        $("#seek_origin_location").attr('disabled', false);
    }
    return true;
}


