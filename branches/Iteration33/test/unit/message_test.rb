require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  
  test "creating a new message" do
    message = Message.new
    message.sender=users(:one)
    message.receiver=users(:mactavish)
    message.body='new message from one to mac'
    message.subject='one2mac'
    assert message.save
  end

  test "message to myself" do
    message = Message.new
    message.sender=users(:dono)
    message.receiver=users(:dono)
    message.body='new message from dono'
    message.subject='dono ke dono'
    assert message.save
  end

  test "message to user with value space" do
    message = Message.new
    message.sender=users(:mactavish)
    message.receiver=users(:dono)
    message.body='     '
    message.subject='     '
    assert !message.save
  end

  test 'message thread starter' do
    assert messages(:donotomac).thread_starter?
    assert !messages(:donotoone2).thread_starter?
  end

  test 'message has escrow' do
    assert messages(:donotomac).has_escrow?
  end

  test 'check message sender receiver' do
    assert messages(:donotomac).is_sender_or_receiver?(users(:dono))
    assert messages(:donotomac).is_sender_or_receiver?(users(:mactavish))
    assert messages(:donotomac).not_this_user(users(:dono)) == users(:mactavish)
    assert messages(:donotomac).not_this_user(users(:mactavish)) == users(:dono)
    assert !messages(:donotomac).is_sender_or_receiver?(users(:one))
  end

  test 'create a reply message' do
    # one to mac
    message = Message.new
    message.sender = users(:one)
    message.receiver = users(:mactavish)
    message.body='reply from one to mac'
    message.thread_starter = messages(:onetomac)
    assert message.save

    # mac to one
    message = Message.new
    message.sender = users(:mactavish)
    message.receiver = users(:one)
    message.body='reply from mac to one'
    message.thread_starter = messages(:onetomac)
    assert message.save

    # check message subject is from thread starter
    assert_equal 'One ke Mac', message.subject

    # now the thread has 3 messages
    assert_equal 3, message.count_thread_messages
  end

  test 'method for email reply' do
    message = Message.new
    message.sender=users(:one)
    message.receiver=users(:mactavish)
    message.body='new message from one to mac'
    message.subject='one2mac'
    assert message.save
    assert !message.salt.blank?
    assert message.email_reply_address == ("#{Message::EMAIL_REPLY_PREFIX}-#{message.id}-#{message.salt}@#{Message::EMAIL_REPLY_DOMAIN}")
  end

  test "message scope search" do
    assert_nothing_raised { Message.build_scope_search(users(:dono), users(:one), MessageStatus::UNREAD, 'pesan') }
  end

  test "message subject and body method" do
    assert_nothing_raised {messages(:onetomac).subject}
    assert_nothing_raised {messages(:onetomac).body}
  end

end
