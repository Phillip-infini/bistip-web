require 'test_helper'

class ResendEmailValidationControllerTest < ActionController::TestCase

  test "should execute resend email validation" do
    get :index, :username => users(:one), :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
    get :index
    assert_response :redirect
  end
end
