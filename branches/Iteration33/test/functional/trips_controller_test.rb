require 'test_helper'

class TripsControllerTest < ActionController::TestCase
  setup do
    @trip = trips(:jktsyd)
    @rtrip = trips(:bogjak)
  request.env["HTTP_REFERER"] = trip_path(@trip)
  end

  test "should get show" do
    get :show, :id => @trip.id
    assert_response :success
    assert_not_nil assigns(:trip)
  end

  test "should get new" do
    login_as(:one)
    get :new
    assert_response :success
  end

  test "should create trip" do
    login_as(:one)
    assert_difference('Trip.count') do
      post :create, :trip => {:origin_location => "jakarta",
        :destination_location => "sydney",
        :departure_date => DateTime.current.advance(:days => 1),
        :arrival_date => DateTime.current.advance(:days => 2),
        :notes => "Text"
      }
    end

    assert_redirected_to trip_path(assigns(:trip))
  end

  test "should create trip with item" do
    login_as(:one)
    assert_difference('Trip.count') do
      post :create, :trip => {:origin_location => "jakarta",
        :destination_location => "sydney",
        :departure_date => DateTime.current.advance(:days => 1),
        :arrival_date => DateTime.current.advance(:days => 2),
        :notes => "Text",
        :items_attributes => {:item => {:name => 'ipad 2'}}}
    end

    assert_redirected_to trip_path(assigns(:trip))
  end

  test "should create a routine trip" do
    login_as(:dono)
    assert_difference('Trip.count') do
      post :create, :trip => {
        :origin_location => "bogor",
        :destination_location => "jakarta",
        :day => ["Saturday", "Tuesday"],
        :period => 'biweekly',
        :notes => "lorem ipsum dolor sit amet",
        :routine => true
      }
    end
  end

  test "should create multiple trip" do
    login_as(:dono)
    assert_difference('Trip.count', 3) do
      post :create,
        :trip => {:origin_location => "jakarta",
          :destination_location => "sydney",
          :departure_date => DateTime.current.advance(:days => 1),
          :arrival_date => DateTime.current.advance(:days => 2),
          :notes => "Text",
          :items_attributes => {:item => {:name => 'ipad 2'}}},
        :other_trips => {
          "12345678" => {
          :origin_location => "jakarta",
          :destination_location => "singapore",
          :departure_date => DateTime.current.advance(:days => 1)},
          "12345679" => {
          :origin_location => "singapore",
          :destination_location => "kuala lumpur",
          :departure_date => DateTime.current.advance(:days => 1)}
        }
    end
  end

  test "should get edit" do
    login_as(:one)
    get :edit, :id => @trip.to_param
    assert_response :success
  end

  test "should update trip" do
    login_as(:one)
    put :update, :id => @trip.to_param, :trip => {:notes => "changed notes"}
    assert_redirected_to trip_path(assigns(:trip))
  end

  test "should update routine trip" do
    login_as(:dono)
    put :update, :id => @rtrip.to_param, :trip => {:day=>["Sunday"], :notes => "sunday morning"}
    assert_redirected_to trip_path(assigns(:trip))
  end

  test "should destroy trip" do
    login_as(:one)
    assert_difference('Trip.count', -1) do
      delete :destroy, :id => @trip.to_param
    end
    
    assert_redirected_to trip_path(@trip)
  end
end
