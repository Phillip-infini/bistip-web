require 'test_helper'

class TopicsStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  setup do
    @topic = topics(:ipad)
    @forum = @topic.forum
  end

  test "create new topic" do

    # login
    full_login_as(:one)

    get new_forum_topic_path(@forum)
    assert_response :success
    assert_template 'new'

    # create trip
    post forum_topics_path(@forum), :topic => {:title => "sadasdasdas das d as dase"},
      :first_post_body => 'Ipad is good'

    assert assigns(:topic).valid?
    assert_response :redirect
    follow_redirect!

  end

  test "edit and update a topic" do

    # login
    full_login_as(:one)

    # edit
    get edit_forum_topic_path(@forum, @topic)

    assert_response :success
    assert_template 'edit'

    # update
    put forum_topic_path(@forum, @topic), :topic => {:title => "sadasdasdas das d as dase"}

    assert_response :redirect
    assert_redirected_to forum_path(@forum)

    follow_redirect!
  end
  
  test "delete a trip" do
     # login
    full_login_as(:one)

    # view the trip
    get forum_path(@forum)
    assert_response :success
    assert_template 'show'

    # delete the trip
    delete forum_topic_path(@forum, @topic)

    assert_response :redirect
    assert_redirected_to forum_path(@forum)

    follow_redirect!
  end

end
