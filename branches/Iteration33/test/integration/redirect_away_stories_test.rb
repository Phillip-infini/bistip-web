require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "click post new trip without login should get redirected" do

    # has not login yet so should get redirected away to login path
    get new_trip_path

    assert_response :redirect
    assert_redirected_to login_path
    assert session[:original_uri]
    follow_redirect!

    # login
    user_to_use = users(:one)
    post session_path, :email_or_username => user_to_use.email, :password => 'secret'

    # should get redirect back to new trip
    assert_response :redirect
    assert_redirected_to new_trip_path
    assert_nil session[:original_uri]

  end

end
