# Comment class represent a comment from a user to a trip or seek
class Comment < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 600
  PER_PAGE = 5
  INTERNAL_ID = 'comment'
  POST_INTERNAL_ID = 'postComment'
  REPLY_INTERNAL_ID = 'postCommentReply'
  EMAIL_REPLY_DOMAIN = APP_CONFIG[:email_reply_address]
  EMAIL_REPLY_PREFIX = "cr"
  EMAIL_REPLY_SEPARATOR = '-'

  # include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper

  # scope
  default_scope :order => 'created_at ASC'
  scope :body_contains, lambda { |keyword| {:conditions => ["match(body) against(?)", keyword]}}
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ? and deleted = ?", 60.days.ago, false]}}
  scope :find_comments_in_thread_and_except, lambda { |thread_id, except_id| {:conditions => ["reply_to_id = ? and id <> ?", thread_id, except_id]}}

  # relationship
  belongs_to :seek
  belongs_to :trip
  belongs_to :user
  belongs_to :thread_starter, :class_name => "Comment", :foreign_key => 'reply_to_id'
  has_many :replies, :class_name => "Comment", :foreign_key => 'reply_to_id'

  # event
  before_create :generate_salt
  after_create :broadcast

  # validation
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH },
    :presence => true

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  # this method should be call from instance of Comment subclass or will be raise NotImplemented error
  def get_path
    raise NotImplementedError
  end

  # this method should be call from instance of Comment subclass or will be raise NotImplemented error
  def get_url
    raise NotImplementedError
  end

  # this method generate a link_to to this comment
  def get_path_link
    raise NotImplementedError
  end

  # helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end

  # method to populate salt for new account
  def generate_salt
    self.salt = ActiveSupport::SecureRandom.hex(4)
  end

  # check if comment is a reply commnet
  def reply?
    !self.reply_to_id.blank?
  end

  # check if this comment is a thread starter
  def thread_starter?
    self.reply_to_id.nil?
  end

  # reply internal id to use for view anchor
  def reply_internal_id
    "#{REPLY_INTERNAL_ID}-#{self.id}"
  end

  # internal id of comment
  def internal_id
    "#{INTERNAL_ID}-#{self.id}"
  end

  # return email reply address
  def email_reply_address
    "#{EMAIL_REPLY_PREFIX}-#{self.id}-#{self.salt}@#{EMAIL_REPLY_DOMAIN}"
  end

  # override method body to handle deleted case
  def body
    if self.deleted?
      I18n.t('comment.deleted')
    else
      self[:body]
    end
  end

end
