module SearchUserHelper
  # check if any search parameter s given
  def any_search_user_parameter_specified?
    unless params[:keyword].blank?
      true
    else
      false
    end
  end
end
