class AddReplyToIdToComments < ActiveRecord::Migration
  def self.up
    add_column :comments, :reply_to_id, :integer, :null => true, :references => :comments
  end

  def self.down
    remove_column :comments, :reply_to_id
  end
end
