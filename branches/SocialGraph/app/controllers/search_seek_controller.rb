class SearchSeekController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    origin = check_location(params[:origin])
    destination = check_location(params[:destination])
    keyword = params[:keyword]
    notice = Array.new

    # check if a given location is a valid city
    if origin.blank? and params[:origin]
      notice << t('search_seek.index.notice.not_valid_location', :location => params[:origin])
    end

    if destination.blank? and params[:destination]
      notice << t('search_seek.index.notice.not_valid_location', :location => params[:destination])
    end

    # build scoped Seek by passing parameter to method build_scope_search
    scope = Seek.build_scope_search(origin, destination, keyword)
    @no_valid_parameter = origin.blank? and destination.blank? and keyword.blank?
    @total_result = scope.size
    @seeks = scope.paginate(:page=>params[:page], :per_page => Seek::SEARCH_PER_PAGE)

    flash.now[:alert] = notice unless notice.empty?
  end

end
