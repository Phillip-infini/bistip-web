class SeekCommentsController < ApplicationController
  before_filter :authenticate, :only => [:destroy,:create]

  # POST new comment
  def create
    @seek = Seek.unscoped.find(params[:seek_id])
    @comment = @seek.comments.new(params[:comment])
    @comment.user = current_user
    session[:seek_comment] = @comment.body
    
    if @comment.save

      # validate if commentator not owner, and send email and live notification to the seek owner
      if !@seek.owner?(current_user)
        Notifier.delay.email_seek_comment(@seek, current_user, @comment) if email_receive_seek_comment(@seek.user)
        SeekCommentNotification.create!(:receiver=>@seek.user, :sender=>@comment.user, :data_id=>@seek.id)
      end
      flash[:notice] = t('comment.message.create.success')
    else
      put_model_errors_to_flash(@comment.errors, 'redirect')
    end
    redirect_to seek_path(@seek, :page => @seek.last_comment_page)
  end

  # DELETE Comment only for comment owner
  def destroy
    @seek = Seek.unscoped.find(params[:seek_id])
    @comment = @seek.comments.find(params[:id])

    # only the owner of the comment can do the deletion
    if @comment.owner?(current_user)
      @comment.update_attribute(:deleted,true)
      flash[:notice] = t('comment.message.destroy.success')
    else
      flash[:alert] = t('comment.message.destroy.fails')
    end
    redirect_to @seek
  end

end
