class DashboardController < ApplicationController
  before_filter :authenticate
  DEFAULT_PAGING = 10
  #GET index
  def index
    @user = current_user
    @incoming_trips = @user.incoming_to_user_location
  end

  #GET current_user's reputations
  def reputation
    @user = current_user
    @positives = @user.received_positive_reputations
    @negatives = @user.received_negative_reputations
  end
  
  #GET current_user's trips
  def trips
    @user = current_user
    @trips = Trip.unscoped.find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => Trip::DEFAULT_PER_PAGE)
  end

  #GET current_user's seeks
  def seeks
    @user = current_user
    @seeks = Seek.unscoped.find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => Seek::DEFAULT_PER_PAGE)
  end

  #GET current_user's profile
  def profile
    @user = current_user
  end
end
