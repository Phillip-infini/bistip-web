class ContentController < ApplicationController
  before_filter :authenticate, :only => [:registered]

  def testfb
    uid1 = current_user.uid
    uid2 = params[:uid2]
    @mutual = GraphWrapper.get_mutual_friends(uid1, uid2, session[:fb_token])
#    raise @mutual.length
  end
end
