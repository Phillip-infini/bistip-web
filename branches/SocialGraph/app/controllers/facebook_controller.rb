class FacebookController < ApplicationController

  # get mutual facebook friends count
  def mutual_friends_count
    handle_request
  end

  # get mutual facebook friends list
  def mutual_friends
    handle_request
  end

  private
  
  def handle_request
    user_id = check_facebook_user_id(params[:user_id])
    target_user_id = params[:target_user_id]
    target_user = User.find(target_user_id)

    # if user is logged in, then check if he is a facebooker and target user is facebooker as well.
    # if yes then query mutual friends
    if logged_in?
      if current_user.is_facebooker? and target_user.is_facebooker? and current_user != target_user
        @user = target_user
        @mutual_friends = GraphWrapper.get_mutual_friends(current_user.uid, target_user.uid)
      else
        render :nothing => true
      end
    # this is the case user is not logged in, but user currently log in in facebook
    elsif !user_id.blank?
      puts "#{target_user}"
      if target_user.is_facebooker?
        @user = target_user
        @mutual_friends = GraphWrapper.get_mutual_friends(user_id, target_user.uid)
      else
        render :nothing => true
      end
    else
      render :nothing => true
    end
  end

  # check validity of facebook user id
  def check_facebook_user_id(value)
    if value.blank? or value == 'undefined'
      nil
    else
      value
    end
  end

end
