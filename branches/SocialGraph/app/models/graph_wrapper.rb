require 'fb_graph'

class GraphWrapper < ActiveRecord::Base
  BISTIP_LOCAL = '118162271593723|1554082e2d1625a819ba15fe.0-728670282|Zq_mA7oLvWWOII6WeFCx9m7ie5A'
  def self.is_facebook_friend?(uid1, uid2, token)
    array = FbGraph::Query.new("select uid1, uid2 FROM friend where uid1 = #{uid1} AND uid2 = #{uid2}").fetch(token)
    !array.empty? ? true : false
  end
  
  def self.get_mutual_friends(uid1, uid2)
    sql = "SELECT name, pic_square, profile_url FROM user WHERE uid IN (SELECT uid1 FROM friend WHERE uid1 IN (SELECT uid2 FROM friend WHERE uid1=#{uid1}) AND uid2=#{uid2})"
    begin
      array = FbGraph::Query.new(sql).fetch(get_access_token)
    rescue Exception
      array = Array.new
    end
    return array
  end
  
  def self.get_access_token
    if Rails.env == 'production'
      return '161019173951344|2.AQBI2bQXxleAbVOL.3600.1314262800.0-728670282|VlNSDlDRMfVEPpwLxSODrAOtW9U'
    elsif Rails.env == 'staging' or Rails.env == 'ci'
      return '189138291128089|2.AQDzhckrkzQ1cbVF.3600.1314262800.0-728670282|Uj-cgk2di8eMueWG_MhLcHIoNF8'
    elsif Rails.env == 'development'
      return '118162271593723|1554082e2d1625a819ba15fe.0-728670282|Zq_mA7oLvWWOII6WeFCx9m7ie5A'
    end
  end

  def self.get_app_id
    if Rails.env == 'production'
      return '161019173951344'
    elsif Rails.env == 'staging' or Rails.env == 'ci'
      return '189138291128089'
    elsif Rails.env == 'development'
      return '118162271593723'
    end
  end
end
