class Relation < ActiveRecord::Base

  # constant
  TOTAL_SHOW_ON_SUMMARY = 10

  # relation
  belongs_to :from, :class_name => "User", :foreign_key => 'from_id'
  belongs_to :to, :class_name => "User", :foreign_key => 'to_id'
  
  def self.build_relation_from_reputation(reputation)
    from = reputation.giver
    to = reputation.receiver
    if Relation.where(:from_id => from.id, :to_id => to.id).count < 1
      relation = from.relations.new
      relation.to_id = to.id
      relation.save
    end
  end
end
