# Trip class represent a trip or travel plan
class Trip < ActiveRecord::Base
  include Location

  # constants
  NOTES_MAXIMUM_LENGTH = 300
  SUMMARY_MATCH_COUNT = 2
  SUMMARY_SIMILAR_COUNT = 2
  DEFAULT_PER_PAGE = 10
  SEARCH_PER_PAGE = 5

  # scope
  default_scope lambda {{ :conditions => ["(departure_date >= ? or routine is true) and deleted = ?", DateTime.current.beginning_of_day, false], :order => "created_at desc", :include => [:origin_city, :destination_city] }}
  scope :origin_city_id, lambda { |origin_city_id| {:conditions => ["origin_city_id = ?", origin_city_id]}}
  scope :destination_city_id, lambda { |destination_city_id| {:conditions => ["destination_city_id = ?", destination_city_id]}}
  scope :origin_country_id, lambda { |country_id| { :conditions => ["cities.country_id = ?", country_id]}}
  scope :destination_country_id, lambda { |country_id| {:conditions => ["destination_cities_trips.country_id = ?", country_id]}}
  scope :departure_date_after, lambda { |departure_date| {:conditions => ["departure_date >= ? or routine is true", departure_date]}}
  scope :departure_date_before, lambda { |departure_date| {:conditions => ["departure_date <= ? or routine is true", departure_date]}}
  scope :notes_contains, lambda { |keyword| {:conditions => ["match(notes) against(?)", keyword]}}
  scope :notes_and_items_contains, lambda { |keyword| {:conditions => ["match(trips.notes) against(?) or match(items.name) against(?)", keyword, keyword], :include => [:items] }}
  scope :user_is_not, lambda { |user_id| {:conditions => ["user_id != ?", user_id]}}
  scope :user_is, lambda { |user_id| {:conditions => ["user_id = ?", user_id]}}
  scope :random, lambda { |*args| {:order => 'RAND()', :limit => args[0] || 1 } }
  scope :similar_trips, lambda { |origin_city_id, destination_city_id, trip_id| {:conditions => ["origin_city_id = ? AND destination_city_id = ? AND trips.id != ?", origin_city_id, destination_city_id, trip_id]}}
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ?", 60.days.ago]}}
  scope :incoming_to_user_location, lambda { |user| {:conditions => ["destination_city_id = ? and user_id <> ?", user.city.id, user.id], :limit => 5}}

  # validation
  validates :origin_location, :presence => true, :format => { :with => /^[^<>]*$/i }

  validates :destination_location, :presence => true, :format => { :with => /^[^<>]*$/i }

  validates :departure_date, :presence => true, :future_date => true, :if => :not_routine?

  validates :notes, :length => { :maximum => NOTES_MAXIMUM_LENGTH }

  validates :arrival_date, :arrival_time => true, :if => :not_routine?

  validates :day, :presence => true, :valid_day => true, :if => :routine?

  validates :period, :inclusion => {:in => Period::ALL, :allow_nil => false, :allow_blank => false}, :if => :routine?

  # relationship
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'
  belongs_to :destination_city, :class_name => "City", :foreign_key => 'destination_city_id'
  belongs_to :user
  has_many :comments, :class_name => "TripComment"
  has_many :items, :class_name => "TripItem"

  # nested
  accepts_nested_attributes_for :items, :allow_destroy => true, :reject_if => lambda { |a| a[:name].blank? }
  
  # serialize
  serialize :day

  # event
  after_create :send_match_notification

  # send email to owner of matching seek
  def send_match_notification
    matching_seeks = find_matching_seeks
    matching_seeks.each do |seek|
      MatchTripNotification.create!(:receiver=>seek.user,:sender=>user, :data_id=>id)
      if seek.user.user_configuration.email_match_trip
        Notifier.email_matching_seek(self, seek).deliver
      end
    end
  end
  handle_asynchronously :send_match_notification
  
  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  # check is a trip still active
  def active?
    if !self.departure_date.nil?
      self.departure_date >= DateTime.current.beginning_of_day and self.deleted == false
    elsif routine?
      return self.deleted==false
    end
  end

  # find match in seek table
  def find_matching_seeks
    if self.active?
      scope = Seek.scoped({})
      scope2 = Seek.scoped({})

      # include also the all cities of the origin city country
      origin_all_cities_id = City.find_by_country_id_and_name(self.origin_city.country.id, City::ALL_CITIES)
      scope = scope.origin_city_or_all_cities_id self.origin_city_id, origin_all_cities_id

      destination_all_cities_id = City.find_by_country_id_and_name(self.destination_city.country.id, City::ALL_CITIES)
      scope = scope.destination_city_or_all_cities_id self.destination_city_id, destination_all_cities_id
      scope = scope.user_is_not self.user_id

      # include origin from anywhere and match the destination_city
      scope2 = scope2.from_anywhere
      scope2 = scope2.destination_city_or_all_cities_id self.destination_city_id, destination_all_cities_id
      scope2 = scope2.user_is_not self.user_id
      
      scope | scope2
    else
      Array.new
    end
  end

  def to_s
    I18n.t('trip.short', :from => self.origin_city.name, :to => self.destination_city.name)
  end

  def not_routine?
    !routine?
  end

  #options that apply in every data in webservice that fetch trip record
  def self.data_options
    return {:only => [:id, :notes, :period, :day, :routine], :skip_types=>true, :skip_instruct=>true, :methods => [:origin_location, :destination_location, :departure_date_medium_format, :arrival_date_medium_format, :username]}
  end

  def items_name_all
    unless self.items.empty?
      result = ''
      self.items.each_with_index do |item, index|
        # append separator if not the first one
        result << ', ' if index > 0
        result << item.name
      end
      return result
    end
    ''
  end

  def items_name_for_display
    items_all = items_name_all
    if !items_all.blank? and items_all.size > 30
      items_all.slice(0,30) + '...'
    else
      items_all
    end
  end

  # build scoped Trip for searching trip purpose
  def self.build_scope_search(origin = nil, destination = nil, departure_date = nil, departure_date_predicate = nil, keyword = nil)
    scope = Trip.scoped({})

    if !origin.nil?
      if origin.all_cities?
        scope = scope.origin_country_id origin.country
      else
        scope = scope.origin_city_id origin.id
      end
    end

    if !destination.nil?
      if destination.all_cities?
        scope = scope.destination_country_id destination.country
      else
        scope = scope.destination_city_id destination.id
      end
    end

    # build predicate on query for departure date based on predicate
    if !departure_date.nil? and !departure_date_predicate.blank?
      case departure_date_predicate
        when DatePredicate::AFTER
          scope = scope.departure_date_after departure_date
        when DatePredicate::BEFORE
          scope = scope.departure_date_before departure_date
      end
    end

    # build predicate for keyword
    if !keyword.blank?
      scope = scope.notes_and_items_contains keyword
    end
    return scope
  end

  def departure_date_medium_format
    I18n.l(departure_date, :format => :medium) if departure_date
  end
  
  def arrival_date_medium_format
    I18n.l(arrival_date, :format => :medium) if arrival_date
  end

  def username
    return user.username
  end
  
  def last_comment_page
    comment_count = self.comments.count
    [((comment_count - 1) / Comment::PER_PAGE) + 1, 1].max
  end

  def week_days_for_display
    unless self.day.empty?
      result = ''
      self.day.each_with_index do |day, index|
        # append separator if not the first one
        result << ', ' if index > 0
        result << day
      end
      return result
    end
    ''
  end

  # equality for trip
  def same_route?(other)
    return (self.origin_city_id == other.origin_city_id) && (self.destination_city_id == other.destination_city_id)
  end
end