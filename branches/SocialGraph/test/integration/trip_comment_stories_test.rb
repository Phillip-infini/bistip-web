require 'test_helper'

class TripCommentStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "creating comment for a trip" do
    
    trip = trips(:tokjak)
   
    # login
    full_login_as(:one)

    # view trip
    get trip_path(trip)
    assert_response :success
    assert_template 'new'

    # add new comment on trip
    post trip_comments_path(trip), :comment => {
      :body => 'tripnya dono tokyo jakarta',
      :user_id => :one,
      :type => 'TripComment'
    }

    assert_response :redirect
    assert_redirected_to trip_path(trip, :page => trip.last_comment_page)
    follow_redirect!

  end

  test "deleting trip comment" do
    trip = trips(:tokjak)
    comment = trip.comments.first
     
    # login
    full_login_as(:one)

    # view a trip
    get trip_path(trip)
    assert_response :success

    # deleting a comment
    delete trip_comment_path(trip, comment)

    assert_response :redirect
    assert_redirected_to trip
    follow_redirect!
  end


end
