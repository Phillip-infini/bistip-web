require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  # initialize current user
  setup do
    login_as(:mactavish)
  end

  #get /dashboard
  test "should get dashboard index" do
    get :index
    assert_response :success
  end

  #get /reputation
  test "should get current_user's reputation" do
    get :reputation
    assert_response :success
  end

  #get /trips
  test "should get current_user's trips" do
    get :trips
    assert_response :success
  end

  #get /seeks
  test "should get current_user's seeks" do
    get :seeks
    assert_response :success
  end

  #get /profile
  test "should get current_user's profile" do
    get :profile
    assert_response :success
  end
end
