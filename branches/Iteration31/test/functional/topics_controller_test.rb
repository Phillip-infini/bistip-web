require 'test_helper'

class TopicsControllerTest < ActionController::TestCase
  setup do
    @topic = topics(:ipad)
    @forum = @topic.forum
  end

  test "should get new" do
    login_as(:one)
    get :new, :forum_id => @forum.id
    assert_response :success
  end

  test "should create topic" do
    login_as(:one)
    assert_difference('Topic.count') do
      post :create, :topic => {:title => "sadasdasdas das d as dase"}, :first_post_body => 'Ipad is good', :forum_id => @forum.id
    end

    assert_redirected_to forum_topic_path(assigns(:forum), assigns(:topic))
  end

  test "should show topic" do
    get :show, :id => @topic.to_param, :forum_id => @forum.id
    assert_response :success
  end

  test "should get edit" do
    login_as(:one)
    get :edit, :id => @topic.to_param, :forum_id => @forum.id
    assert_response :success
  end

  test "should update topic" do
    login_as(:one)
    put :update, :id => @topic.id, :forum_id => @forum.id

    assert_redirected_to forum_path(assigns(:forum))
  end

  test "should destroy topic" do
   login_as(:one)
   assert_difference('Topic.count', -1) do
     delete :destroy, :id => @topic.to_param, :forum_id => @forum.id
   end

    assert_redirected_to forum_path(assigns(:forum))
  end
end
