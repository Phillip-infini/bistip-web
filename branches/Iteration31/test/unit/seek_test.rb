require 'test_helper'

class SeekTest < ActiveSupport::TestCase

  test "should create seek" do
    seek = Seek.new
    seek.origin_location = "jakarta"
    seek.destination_location = "sydney"
    seek.departure_date = DateTime.current.advance(:days => 1)
    seek.departure_date_predicate = 'after'
    seek.user = users(:one)
    seek.notes = 'test'
    seek.items_attributes = [{:name => 'ipad'}]
    assert seek.save
  end

  test "should create anywhere seek" do
    seek = Seek.new
    seek.from_anywhere = true
    seek.destination_location = "sydney"
    seek.departure_date = DateTime.current.advance(:days => 1)
    seek.departure_date_predicate = 'after'
    seek.user = users(:one)
    seek.notes = 'test'
    seek.items_attributes = [{:name => 'ipad'}]
    assert seek.save
  end

  test "should create all cities seek" do
    seek = Seek.new
    seek.origin_location = "<all cities>, Indonesia"
    seek.destination_location = "<all cities>, Australia"
    seek.departure_date = DateTime.current.advance(:days => 1)
    seek.departure_date_predicate = 'after'
    seek.user = users(:one)
    seek.notes = 'test'
    seek.items_attributes = [{:name => 'ipad'}]
    assert seek.save
  end

  test "should find seek" do
    seek_id = seeks(:jktsyd).id
    assert_nothing_raised {Seek.find(seek_id)}
  end

  test "should update seek" do
    seek = seeks(:jktsyd)
    assert seek.update_attributes(:notes => 'notes')
  end

  test "should delete seek" do
    sedel = seeks(:jktsyd)
    seeks = Seek.find(sedel.id) 
    assert seeks.update_attributes(:deleted => true)
  end

  test 'seek matching' do
    assert_equal 3, seeks(:anywheretosyd).find_matching_trips.count
  end

  test 'seek item methods' do
    assert_nothing_raised {seeks(:jktsyd).items_name_all}
    assert_nothing_raised {seeks(:jktsyd).items_name_for_display}
  end

  test "seek scope and search method" do
    assert_equal 3, Seek.build_scope_search(cities(:jakarta), cities(:sydney), nil).count
    assert_equal 5, Seek.build_scope_search(nil, cities(:sydney), nil).count
    assert_equal 4, Seek.build_scope_search(cities(:allindo), cities(:sydney), nil).count
  end

  test 'trip method for email thread starter' do
    seek = Seek.new
    seek.origin_location = "jakarta"
    seek.destination_location = "sydney"
    seek.departure_date = DateTime.current.advance(:days => 1)
    seek.departure_date_predicate = 'after'
    seek.user = users(:one)
    seek.notes = 'test'
    seek.items_attributes = [{:name => 'ipad'}]
    assert seek.save
    assert !seek.salt.blank?
    assert seek.email_reply_address == ("#{Seek::EMAIL_REPLY_PREFIX}-#{seek.id}-#{seek.salt}@#{Seek::EMAIL_REPLY_DOMAIN}")
  end

  test 'seek equal same route' do
    assert seeks(:jktsyd).same_route? seeks(:jktsyd2)
  end
  
end
