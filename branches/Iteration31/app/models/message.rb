class Message < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 1000
  CARRIAGE_RETURN_BUFFER = 100
  SUBJECT_MAXIMUM_LENGTH = 100
  ASSETS_MAXIMUM = 3
  DEFAULT_PER_PAGE = 5
  REPLY_INTERNAL_ID = 'message_reply'
  KEYWORD = '<keyword>'
  EMAIL_REPLY_DOMAIN = APP_CONFIG[:email_reply_address]
  EMAIL_REPLY_PREFIX = "mr"
  EMAIL_REPLY_SEPARATOR = '-'
  EMAIL_REPLY_FROM = 'mail@bistip.com'
  REVIEWABLE_COUNT = 6

  # rules to filter email reply body
  EMAIL_REPLY_RULES = [lambda { |body_line, message| body_line.include? EMAIL_REPLY_FROM},
    lambda { |body_line, message| body_line.squeeze == '>_' or body_line.squeeze == '>-' or body_line.squeeze == '_'}
  ]

  # validation
  validates :body, :length => {:minimum => 5, :maximum => (BODY_MAXIMUM_LENGTH + CARRIAGE_RETURN_BUFFER) },
            :presence => true
          
  validates :subject, :length => {:maximum => SUBJECT_MAXIMUM_LENGTH }
  validates :assets, :length => {:maximum => ASSETS_MAXIMUM}
  
  # relationships
  belongs_to :seek
  belongs_to :trip
  belongs_to :receiver, :class_name => "User", :foreign_key => 'receiver_id'
  belongs_to :sender, :class_name => "User", :foreign_key => 'sender_id'
  belongs_to :thread_starter, :class_name => "Message", :foreign_key => 'reply_to_id'
  has_one :escrow, :class_name => 'Escrow', :foreign_key => 'message_id'
  has_one :insurance, :class_name => 'Insurance', :foreign_key => 'message_id'
  has_many :assets
  accepts_nested_attributes_for :assets, :reject_if => lambda { |a| a[:attachment].blank? }

  # scope
  scope :body_contains, lambda { |keyword| {:conditions => ["match(body) against(?)", keyword]}}
  scope :body_or_subject_contains, lambda {|keyword| {:conditions => ["match(body) against(?) OR subject like ?", keyword, "%#{keyword}%"]}}
  scope :order_by_created_at_descending, order("created_at desc")
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ?", 60.days.ago]}}
  scope :replied, lambda{ |sender_id, reply_to_id, created_at| {:conditions => ["sender_id = ? AND reply_to_id = ? AND created_at > ?", sender_id, reply_to_id, created_at]}}
  scope :sender_list, lambda{ |receiver_id| { :conditions => ["receiver_id = ?", receiver_id], :select => ['sender_id']}}
  scope :sender, lambda{ |sender_id| {:conditions => ["sender_id = ?", sender_id]}}
  scope :send_by_in_thread, lambda{ |sender_id, thread_id| {:conditions => ["sender_id = ? AND (id = ? OR reply_to_id = ?)", sender_id, thread_id, thread_id]}}
  scope :receiver, lambda{ |receiver_id| {:conditions => ["receiver_id = ?", receiver_id]}}
  scope :unread, :conditions => "`read` is false"

  # events
  before_create :generate_salt, :populate_subject
  after_create :do_notify

  # override subject method handle no subject scenario
  def subject
    if self[:subject].blank?
      I18n.t('message.label.no_subject')
    else
      self[:subject]
    end
  end

  # method to display subject in view, handle no subject scenario
  def subject_display
    if self.thread_starter?
      self.subject
    else
      I18n.t('message.label.reply') + self.subject
    end
  end

  # override body
  def body
    if self.email_reply?
      Message.get_main_part(self[:body], self)
    else
      self[:body]
    end
  end

  def thread_starter?
    self.reply_to_id.nil?
  end

  # check if given user is sender or receiver
  def is_sender_or_receiver?(user)
    self.sender == user or self.receiver == user
  end

  # check if the message has an attachment/asset
  def has_asset?
    unless self.assets.blank?
      self.assets.count > 0
    else
      false
    end
  end

  # check if a message belongs to an escrow
  def has_escrow?
    if thread_starter?
      if self.escrow.blank?
        false
      else
        true
      end
    else
      thread_starter = self.thread_starter
      if thread_starter.escrow.blank?
        false
      else
        true
      end
    end
  end

  # check if a message is replied my current user
  def replied?
    reply_to_id = self.thread_starter? ? self.id : self.reply_to_id
    Message.replied(self.receiver_id, reply_to_id, self.created_at).count > 0
  end

  # return participant of the message which are not the given user
  def not_this_user(user)
    if is_sender_or_receiver?(user)
      user == self.sender ? self.receiver : self.sender
    end
  end

  # get id for internal link
  def internal_link_id
    "msg_" + self.id.to_s
  end

  # find list of users who has send at least a message to given user
  def self.all_sender_list(user)
    senders_id = Message.sender_list(user.id)
    User.find(senders_id.collect{ |s| s.sender_id }, :order => :username)
  end

  # method to populate salt for new account
  def generate_salt
    self.salt = ActiveSupport::SecureRandom.hex(4)
  end

  # return email reply address
  def email_reply_address
    "#{EMAIL_REPLY_PREFIX}-#{self.id}-#{self.salt}@#{EMAIL_REPLY_DOMAIN}"
  end

  # count number of message in this message's thread
  def count_thread_messages
    id_to_use = self.thread_starter? ? self.id : self.thread_starter.id
    Message.find_all_by_reply_to_id(id_to_use).count + 1
  end

  # is message ready for review by the given user
  def reviewable_by(user)
    id_to_use = self.thread_starter? ? self.id : self.thread_starter.id
    other_user = not_this_user(user)

    # a conversation can become reviewable by user if:
    # - Message count in thread is more than REVIEWABLE_COUNT
    # - User is the participant in the conversation
    # - User has send (REVIEWABLE_COUNT / 2) number of message
    # - A review does not exist yet from this user
    self.count_thread_messages >= REVIEWABLE_COUNT and
      self.is_sender_or_receiver?(user) and
      Message.send_by_in_thread(user.id, id_to_use).count >= (REVIEWABLE_COUNT / 2) and
      Message.send_by_in_thread(other_user.id, id_to_use).count >= (REVIEWABLE_COUNT / 2) and
      Review.find_all_by_giver_id_and_message_id(user.id, id_to_use).count == 0
  end

  # build message search scope
  def self.build_scope_search(current_user, sender, status, keyword)
    
    # no current user, this search scope cannot be performed
    if current_user.blank?
      return nil
    end

    scope = Message.scoped({})
    scope = scope.receiver(current_user.id)
    scope = scope.order_by_created_at_descending

    unless sender.blank?
      scope = scope.sender(sender)
    end

    unless status.blank?
      scope = MessageStatus.build(status, scope)
    end

    unless keyword.blank?
      scope = scope.body_or_subject_contains(keyword)
    end

    return scope
  end

  # cut forwarded or quoted text, if mailgun cannot detect them
  def self.get_main_part(body, message)
    # do nothing if body is blank
    return body if body.blank?
    body_lines = body.split(/\n/)
    cut_index = body_lines.size
    body_lines.each_with_index do |body_line, index|
      unless body_line.blank?
        EMAIL_REPLY_RULES.each do |rule|
          if rule.call(body_line, message)
            cut_index = index if index < cut_index
          end
        end
      end
    end

    main_lines = body_lines.take(cut_index)
    main_lines.join("\n")
  end

  # check if a giver user is the requester in the message context
  def is_requester?(user)
    return false unless is_sender_or_receiver?(user)
    if normal_type? or trip_type?
      return (self.sender == user ? true : false)
    elsif seek_type?
      return (self.receiver == user ? true : false)
    end
    false
  end

  # check if message is normal type (does not belong to seek and trip)
  def normal_type?
    self.trip_id.blank? and self.seek_id.blank?
  end

  # check if message is trip type
  def trip_type?
    !self.trip_id.blank?
  end

  # check if message is seek type
  def seek_type?
    !self.seek_id.blank?
  end

  private
    # before save populate subject from thread starter for search purpose
    def populate_subject
      unless self.thread_starter?
        self.subject = self.thread_starter.subject
      end
    end

    # after create message, notify the receiver
    def do_notify
      Notifier.delay.email_user_new_message(self) if self.receiver.user_configuration.email_receive_message
    end

end
