# a class to represent a payout method
class PayoutMethod < ActiveRecord::Base

  # scope
  default_scope lambda {{ :conditions => ["deleted = ?", false] }}

  belongs_to :user

  # get cname of payment method
  def cname
    raise NotImplementedError
  end
end
