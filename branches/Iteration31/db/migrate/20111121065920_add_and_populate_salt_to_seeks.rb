class AddAndPopulateSaltToSeeks < ActiveRecord::Migration
  def self.up
    add_column :seeks, :salt, :string
    Seek.all.each do |seek|
      seek.generate_salt
      seek.save
    end
  end

  def self.down
    remove_column :seeks, :salt
  end
end
