class AddTripsNotesIndexing < ActiveRecord::Migration
  def self.up
    execute('ALTER TABLE trips ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_trips_notes ON trips (notes(500))')
  end

  def self.down
    execute('DROP INDEX fulltext_trips_notes ON trips')
    execute('ALTER TABLE trips ENGINE = innodb')
  end
end
