class CreateTipUnits < ActiveRecord::Migration
  def self.up
    create_table :tip_units do |t|
      t.string :name
      t.string :symbol
      t.column :before_number, :boolean, :default => true
    end
  end

  def self.down
    drop_table :tip_units
  end
end
