class CreateAnchorWords < ActiveRecord::Migration
  def self.up
    create_table :anchor_words do |t|
      t.string :word
      t.timestamps
    end
  end

  def self.down
    drop_table :anchor_words
  end
end
