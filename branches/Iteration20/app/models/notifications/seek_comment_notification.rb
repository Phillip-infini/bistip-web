# To change this template, choose Tools | Templates
# and open the template in the editor.

class SeekCommentNotification < Notification

  #overide to_link method to perform specified action of SeekComment notification
  def to_link(current_user)
     seek = Seek.unscoped.find(data_id)
     link = ""

    #clasification to determine type of comment, like 'kamu', 'yang sama'
    if seek.owner?(receiver)
      link = I18n.t("notification.title.seek_comment_owner",
        :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
        :seek_label => link_to(I18n.t('notification.field.seek'), path_helper.seek_path(seek)))
    else
      link = I18n.t("notification.title.seek_comment_other",
        :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
        :seek_label=>link_to(I18n.t('notification.field.seek'), path_helper.seek_path(seek)))
    end
    return link
    
  end
end
