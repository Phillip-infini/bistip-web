module SearchSeekHelper

  # check if any search parameter s given
  def any_search_seek_parameter_specified?
    if !params[:origin].blank? or
       !params[:destination].blank? or
       !params[:keyword].blank?
      true
    else
      false
    end
  end
end
