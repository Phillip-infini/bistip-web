# validator class to handle city location

class OriginCityValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.blank?
      if record.origin_location.blank?
        record.errors[:origin_location] << (options[:message] || I18n.t('errors.location_not_given'))
      else
        record.errors[:origin_location] << (options[:message] || I18n.t('errors.location_not_valid', :value => record.origin_location))
      end
    end
  end
end
