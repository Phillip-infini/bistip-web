# a class that represent an insurance
class Insurance < ActiveRecord::Base

  belongs_to :buyer, :class_name => "User", :foreign_key => 'buyer_id'
  belongs_to :seller, :class_name => "User", :foreign_key => 'seller_id'
  belongs_to :message, :class_name => "Message", :foreign_key => 'message_id'
  belongs_to :currency, :class_name => "Currency", :foreign_key => 'currency_id'
  has_many :logs, :class_name => 'InsuranceLog', :foreign_key => 'insurance_id'

  attr_writer :current_step

  validates :cover_value, :presence => true, :valid_cover_value_amount => true, :if => lambda { |o| o.current_step == "details" }
  validates :item, :presence => true, :length => {:within => 5..50}, :if => lambda { |o| o.current_step == "details" }

  # list of escrow transaction state
  STATE_BUYER_APPLIED = 'buyer_applied'
  STATE_BISTIP_APPROVED = 'bistip_approved'
  STATE_BISTIP_REJECTED = 'bistip_rejected'
  STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER = 'bistip_received_buyer_bank_transfer'
  STATE_INVALID = 'invalid'

  LOG_STATE_MAP = {BuyerAppliedInsuranceLog => STATE_BUYER_APPLIED,
                   BistipApprovedInsuranceLog => STATE_BISTIP_APPROVED,
                   BistipRejectedInsuranceLog => STATE_BISTIP_REJECTED,
                   BistipReceivedBuyerBankTransferInsuranceLog => STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER}

  # event
  before_create :set_currency
  
  # get the current state of escrow determine by latest log type
  def current_state
    unless last_log.blank?
      Escrow.get_log_state(last_log)
    end
  end

  # get last action from user on the escrow
  def last_log
    logs = self.logs(:order_by => 'created_at asc')
    unless logs.blank?
      logs.last
    end
  end

  # form wizard: show the current step in wizard
  def current_step
    @current_step || steps.first
  end

  # form wizard: list of steps
  def steps
    %w[details confirmation]
  end

  # form wizard: get the next step
  def next_step
    self.current_step = steps[steps.index(current_step)+1]
  end

  # form wizard: get the previous step
  def previous_step
    self.current_step = steps[steps.index(current_step)-1]
  end

  # form wizard: is it currently on the first step
  def first_step?
    current_step == steps.first
  end

  # form wizard: is it currently on the last step
  def last_step?
    current_step == steps.last
  end

  # form wizard: is all step in the wizard already pass validation
  def all_valid?
    steps.all? do |step|
      self.current_step = step
      valid?
    end
  end

  # get ampunt with currency
  def amount_with_currency
    self.currency.name + ' ' + self.amount.to_s
  end

  # get ampunt with currency
  def cover_value_with_currency
    self.currency.name + ' ' + self.cover_value.to_s
  end

  # generate notes for bank transfer
  def bank_transfer_notes
    self.buyer.username + ' INS' + self.id.to_s
  end

  # static method to get the state string based on a log
  def self.get_log_state(log)
    state = LOG_STATE_MAP[log.class]
    unless state.blank?
      state
    else
      STATE_INVALID
    end
  end

  # get the current state of escrow determine by latest log type
  def current_state
    unless last_log.blank?
      Insurance.get_log_state(last_log)
    end
  end

  # get last action from user on the escrow
  def last_log
    logs = self.logs(:order_by => 'created_at asc')
    unless logs.blank?
      logs.last
    end
  end
  
  private

    # method to set currency before create
    def set_currency
      self.currency = Currency.find_by_name('IDR')
    end

end
