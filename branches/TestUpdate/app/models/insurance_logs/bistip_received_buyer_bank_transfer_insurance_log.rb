class BistipReceivedBuyerBankTransferInsuranceLog < InsuranceLog

  # broadcast the creation of this log inform of notification and email
  def broadcast
    InsuranceLogNotification.create!(:data_id => self.id, :sender => insurance.buyer, :receiver => insurance.buyer)
    InsuranceLogNotification.create!(:data_id => self.id, :sender => insurance.seller, :receiver => insurance.seller)
    Notifier.delay.email_insurance_log(insurance.buyer, self)
    Notifier.delay.email_insurance_log(insurance.seller, self)
  end
  
end
