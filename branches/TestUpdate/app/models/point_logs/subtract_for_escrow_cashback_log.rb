class SubtractForEscrowCashbackLog < PointLog

  NEEDED_POINT = 50

  # do operation on point given
  def do_point(amount)
    amount - self.amount
  end

end
