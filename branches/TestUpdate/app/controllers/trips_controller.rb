class TripsController < ApplicationController

  before_filter :authenticate, :only => [:new, :edit, :create, :update, :destroy, :match, :new_routine]

  # GET /trips/1
  def show
    @trip = Trip.unscoped.find(params[:id])
    generate_suggestions

    @trcomments = @trip.comments.paginate(:page => params[:page], :per_page => Comment::PER_PAGE)
    if logged_in?
      @comment = Comment.new
    end
  end

  # GET /trips/new
  def new
    @trip = Trip.new
    @trip.items.build
    @other_trips = Array.new
  end

  # GET /trips/new_routine
  def new_routine
    @trip = Trip.new
    @trip.items.build
  end
  
  # GET /trips/1/edit
  def edit
    @trip = current_user.trips.find(params[:id])
    if @trip.items.empty?
      @trip.items.build
    end
  end

  # POST /trips
  def create
    @trip = current_user.trips.new(params[:trip])

    # build other trip list, remove he empty ones and combine it with the original trip
    @other_trips = Array.new
    @other_trips = sort_hash_by_keys(params[:other_trips]).collect { |trip| current_user.trips.new(trip) } if params[:other_trips]
    @other_trips = filter_empty_trip(@other_trips)
    multiple_trips = combine_multiple_trips(@trip, @other_trips)
    
    # try to run valid on all the trips
    if multiple_trips.all?(&:valid?)
      
      # if all pass then save one by one and also set the previous trip reference
      previous_trip = nil
      multiple_trips.each do |trip|
        copy_items(@trip, trip)
        trip.notes = @trip.notes

        # set reference to previous trip
        trip.previous_trip_id = previous_trip.id unless previous_trip.blank?

        # save and set the previous trip as the current trip
        trip.save!
        previous_trip = trip
      end
     
      flash[:notice] = t('trip.create.message.success')
      redirect_to(@trip)
    else
      put_model_errors_to_flash(collect_error_messages(multiple_trips))
      @trip.items.build if @trip.items.empty?
      if !@trip.routine?
        render :action => "new"
      else
        render :action => "new_routine"
      end
    end
  end

  # PUT /trips/1
  def update
    @trip = current_user.trips.find(params[:id])
    
    # if params is nil, then trip's day should be nil too
    if params[:trip][:day].nil?
      @trip.day = nil
    end
    if @trip.update_attributes(params[:trip])
      flash[:notice] = t('trip.update.message.success')
      redirect_to(@trip)
    else
      put_model_errors_to_flash(@trip.errors)
      render :action => "edit"
    end
  end

  # DELETE /trips/1
  def destroy
    # only the owner of the comment can do the deletion
    @trip = current_user.trips.find(params[:id])
    # apply soft delete, set deleted attribute to true
    @trip.update_attribute(:deleted, true)
    flash[:notice] = t('trip.destroy.message.success')
    
    redirect_to :back
  end

  # show matching seeks from a trip
  def match
    @trip = current_user.trips.find(params[:id])
    @matches = @trip.find_matching_seeks
  end

  # show suggested trips of a trip
  def suggestions
    @trip = Trip.unscoped.find(params[:id])
    generate_suggestions
  end
  
end

private

  # generate suggested trips
  def generate_suggestions
    unless logged_in? and @trip.owner?(current_user)
      @suggested_trips = Trip.get_suggestions(@trip)
    end
  end

  # copy items data between 2 trip
  def copy_items(source_trip, dest_trip)
    temp_items = Array.new
    source_trip.items.each{|item|
      temp_items << TripItem.create(:name => item.name, :tip_unit => item.tip_unit, :tip => item.tip)
    }
    dest_trip.items = temp_items
  end

  # remove empty 'other trip'
  def filter_empty_trip(trips)
    temp_trips = Array.new
    trips.each{|trip|
      temp_trips << trip if !trip.empty_fields?
    }
    temp_trips
  end

  # combine initial trip and multiple trip
  def combine_multiple_trips(initial_trip, other_trips)
    temp = Array.new
    temp << initial_trip
    other_trips.each do |trip|
      temp << trip
    end
    return temp
  end

  # collect all error messages of a trip
  def collect_error_messages(trips)
    all_errors = Array.new
    trips.each do |trip|
      all_errors << trip.errors unless trip.valid?
    end
    all_errors
  end

