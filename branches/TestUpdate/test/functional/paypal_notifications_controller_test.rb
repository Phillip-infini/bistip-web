require 'test_helper'

class PaypalNotificationsControllerTest < ActionController::TestCase

  test 'create paypal notification' do
    assert_differences([['PaypalNotification.count', 1], ['PaypalNotifiedLog.count', 1]]) do
      post :create, :payment_status => 'Completed',
        :secret => APP_CONFIG[:paypal_secret],
        :receiver_email => APP_CONFIG[:paypal_email],
        :invoice => '1234567890',
        :tzn_id => 'tzn1234567890'
      assert_response :success
    end
  end

end
