require 'test_helper'

class NotifierTest < ActionMailer::TestCase
  
  test "email_validation" do
    user = users(:not_validated)
    mail = Notifier.email_validation(user)
    assert_match user.username, mail.body.encoded
    assert_match "validate?key", mail.body.encoded
  end

  test "email_validation reminder" do
    user = users(:not_validated)
    mail = Notifier.email_validation_reminder(user)
    assert_match user.username, mail.body.encoded
    assert_match "validate?key", mail.body.encoded
  end

  test "email_password" do
    user = users(:not_validated)
    mail = Notifier.email_password(user)
    assert_match user.username, mail.body.encoded
    assert_match "passwords/reset_edit", mail.body.encoded
  end

  test "email_seek_comment" do
    seek = seeks(:banjak)
    commentator = users(:dono)
    comment = comments(:seek2)
    mail = Notifier.email_seek_comment(seek,commentator,comment)
    assert_match comment.body, mail.body.encoded
    assert_match "/seeks/", mail.body.encoded
    
  end

  test "email_trip_comment" do
    trip = trips(:tokjak)
    commentator = users(:one)
    comment = comments(:trip1)
    mail = Notifier.email_trip_comment(trip,commentator,comment)
    assert_match comment.body, mail.body.encoded
    assert_match "/trips/", mail.body.encoded
  end

  test "email_review_receiver" do
    receiver = users(:dono)
    giver = users(:mactavish)
    positive = reviews(:positive1)
    mail = Notifier.email_review_receiver(positive)
    assert_match positive.body, mail.body.encoded
  end

  test "email_seek_comment_from_user_notvalid" do
    seek = seeks(:banjak)
    commentator = users(:not_validated)
    comment = comments(:seek2)
    mail = Notifier.email_seek_comment(seek,commentator,comment)
    assert_match comment.body, mail.body.encoded
    assert_match "/seeks/", mail.body.encoded
  end

  test "email_trip_comment_from_user_notvalid" do
    trip = trips(:tokjak)
    commentator = users(:not_validated)
    comment = comments(:trip1)
    mail = Notifier.email_trip_comment(trip,commentator,comment)
    assert_match comment.body, mail.body.encoded
    assert_match "/trips/", mail.body.encoded
  end

  test 'email_other_trip_comment' do
    trip = trips(:tokjak)
    commentator = users(:one)
    comment = comments(:trip1)
    mail = Notifier.email_other_trip_comment(comment, commentator, trip)
    assert_match comment.body, mail.body.encoded
    assert_match "/trips/", mail.body.encoded
  end

  test 'email_other_seek_comment' do
    seek = seeks(:jktsyd)
    commentator = users(:one)
    comment = comments(:seek2)
    mail = Notifier.email_other_seek_comment(comment, commentator, seek)
    assert_match comment.body, mail.body.encoded
    assert_match "/seeks/", mail.body.encoded
  end

  test "email_user_new_message" do
    message = messages(:onetomac)
    mail = Notifier.email_user_new_message(message)
    assert_match message.body, mail.body.encoded
    assert_match "/messages/", mail.body.encoded
  end

  test 'email_user_message_not_valid' do
    message = messages(:onetomac)
    mail = Notifier.email_user_message_not_valid('reply@mail.com', users(:one), users(:dono), ['aaaa'])
    assert_match "aaaa", mail.body.encoded
  end

  test 'email_matching_seek' do
    trip = trips(:jktsyd)
    seek = seeks(:jktsyd)
    mail = Notifier.email_matching_seek(trip, seek)
    assert_match '/trips/', mail.body.encoded
  end

  test 'email_matching_trip' do
    trip = trips(:jktsyd)
    seek = seeks(:jktsyd)
    mail = Notifier.email_matching_trip(seek, trip)
    assert_match '/seeks/', mail.body.encoded
  end

  test 'email_topic_post' do
    topic = topics(:ipad)
    post = posts(:ipadpost)
    mail = Notifier.email_topic_post(topic, post)
    assert_match post.body, mail.body.encoded
  end

  test 'email_quote_post' do
    topic = topics(:ipad)
    post = posts(:ipadpost)
    quoted_post = posts(:p1)
    mail = Notifier.email_quote_post(topic, post, quoted_post)
    assert_match post.body, mail.body.encoded
  end

  test 'email_relation' do
    from = users(:one)
    to = users(:dono)
    mail = Notifier.email_relation(from, to, false)
    assert_match '/social/', mail.body.encoded
  end

  test 'email_escrow_log' do
    receiver = users(:one)
    log = escrow_logs(:SellerRequestedPayoutLog)
    mail = Notifier.email_escrow_log(receiver, log)
    assert_match '/messages/', mail.body.encoded
  end

  test 'email_insurance_log' do
    receiver = users(:one)
    log = insurance_logs(:BistipApproved)
    mail = Notifier.email_insurance_log(receiver, log)
    assert_match '/messages/', mail.body.encoded
  end

  test 'email_trusted_badge' do
    receiver = users(:one)
    mail = Notifier.email_trusted_badge(receiver)
    assert_match receiver.username, mail.body.encoded
  end
end
