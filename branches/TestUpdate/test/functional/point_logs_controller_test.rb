require 'test_helper'

class PointLogsControllerTest < ActionController::TestCase

  test 'create paypal notification' do
    login_as(:dono)
    assert_difference('SubtractForEscrowCashbackLog.count') do
      get :claim_escrow_cashback
      assert_response :redirect
    end
  end

end
