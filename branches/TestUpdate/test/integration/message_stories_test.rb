require 'test_helper'

class MessageStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  # Replace this with your real tests.
  test "mac send a new message to the dono" do
    dono = users(:dono)
    mac = users(:mactavish)

    #login
    full_login_as(:mactavish)
    get new_user_message_path(dono)
    assert_response :success
    assert_template 'new'

    #post
    post user_messages_path :user_id=>dono, :receiver_id=>dono, :message => {
      :body=>"new message from mac to dono",
      :subject=>"Mac2Dono",
      :sender_id=>mac,
      :receiver_id=>dono
    }

    assert_response :redirect
    assert_redirected_to user_message_path(:user_id => mac, :id => assigns(:message), :anchor => assigns(:message).internal_link_id)
    follow_redirect!
  end

  test "one reply a mac's message" do
    one = users(:one)
    mac = users(:mactavish)
    msg = messages(:onetomac)
    #login
    full_login_as(:one)
    get user_messages_path(mac)
    assert_response :success

    #get message
    get user_message_path :user_id=>one.id, :id=>msg.id
    assert_response :success
    assert_template 'show'
    assert_difference('Message.count') do
      post user_messages_path :user_id => mac, :receiver_id => mac, :message => {
        :body => "reply from dono to mac",
        :subject => "RE:Mac2One",
        :reply_to_id => msg,
        :receiver_id => mac
      }
      assert_response :redirect
      assert_redirected_to user_message_path(:user_id => one, :id => assigns(:message), :anchor => assigns(:message).internal_link_id)
    end

  end

  test '2 user send each other 3 message then post review' do
    one = users(:one)
    mac = users(:mactavish)

    full_login_as(:one)
    get new_user_message_path(mac)
    assert_response :success
    assert_template 'new'

    # post 1
    assert_difference('Message.count') do
      post user_messages_path :user_id => mac, :receiver_id => mac, :message => {
        :body=>"lalal",
        :subject=>"One2Mac",
        :sender_id => one,
        :receiver_id => mac
      }
    end
    message = assigns(:message)

    # post 2
    assert_difference('Message.count') do
      post user_messages_path :user_id => mac, :receiver_id => mac, :message => {
        :body=>"lalal",
        :subject=>"One2Mac",
        :sender_id => one,
        :receiver_id => mac,
        :reply_to_id => message,
      }
    end

    # post 3
    assert_difference('Message.count') do
      post user_messages_path :user_id => mac, :receiver_id => mac, :message => {
        :body=>"lalal",
        :subject=>"One2Mac",
        :sender_id => one,
        :receiver_id => mac,
        :reply_to_id => message,
      }
    end

    # try to post review, not allowed
    get new_user_review_path(:user_id => mac.id, :message_id => message.id)
    assert_redirected_to profile_path(mac.username)

    # logout as one
    get logout_path

    # login as mactavish
    full_login_as(:mactavish)

    # post 1
    assert_difference('Message.count') do
      post user_messages_path :user_id => one, :receiver_id => one, :message => {
        :body=>"lalal",
        :subject=>"MacToOne",
        :sender_id => mac,
        :receiver_id => one,
        :reply_to_id => message,
      }
    end

    # post 2
    assert_difference('Message.count') do
      post user_messages_path :user_id => one, :receiver_id => one, :message => {
        :body=>"lalal",
        :subject=>"MacToOne",
        :sender_id => mac,
        :receiver_id => one,
        :reply_to_id => message,
      }
    end

    # post 3
    assert_difference('Message.count') do
      post user_messages_path :user_id => one, :receiver_id => one, :message => {
        :body=>"lalal",
        :subject=>"MacToOne",
        :sender_id => mac,
        :receiver_id => one,
        :reply_to_id => message,
      }
    end
    
    # try to post review, should be allowed
    get new_user_review_path(:user_id => one.id, :message_id => message.id)
    assert_response :success
    assert_difference('Review.count') do
      post user_reviews_path(one), :type => 'NegativeReview', :review => {
        :body => "very good service",
        :message_id => message.id
      }
    end

    # logout as one
    get logout_path

    # login as mactavish
    full_login_as(:one)
    # try to post review, should be allowed
    get new_user_review_path(:user_id => mac.id, :message_id => message.id)
    assert_response :success
    assert_difference('Review.count') do
      post user_reviews_path(mac), :type => 'NegativeReview', :review => {
        :body => "very good service",
        :message_id => message.id
      }
    end

  end

end
