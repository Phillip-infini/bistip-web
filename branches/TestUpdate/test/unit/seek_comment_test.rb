require 'test_helper.rb'

class SeekCommentTest < ActiveSupport::TestCase
  test "should create comment on seek" do
    comment = SeekComment.new
    comment.body = 'body 123'
    comment.seek = seeks(:jktsyd)
    comment.user = users(:one)
    assert_difference('SeekCommentNotification.count') do
      assert comment.save
    end
  end

  test "should delete comment on seek" do
    cmo = comments(:seek1)
    comment = SeekComment.find(cmo.id)
    assert comment.update_attribute(:deleted, true)
  end

  test "should create comment on seek with value space" do
    comment = SeekComment.new
    comment.body = '     '
    comment.seek = seeks(:jktsyd)
    comment.user = users(:dono)
    assert_no_difference('SeekCommentNotification.count') do
      assert !comment.save
    end
  end

  test "should create comment on seek with myself" do
    comment = SeekComment.new
    comment.body = '     '
    comment.seek = seeks(:tokjak)
    comment.user = users(:dono)
    assert_no_difference('SeekCommentNotification.count') do
      assert !comment.save
    end
  end

  test 'comment scope method' do
    assert Comment.sixty_days_ago
  end

end
