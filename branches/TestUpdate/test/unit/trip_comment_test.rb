require 'test_helper'

class TripCommentTest < ActiveSupport::TestCase
  test "should create comment on trip" do
    comment = TripComment.new
    comment.body = 'body 123'
    comment.trip = trips(:jktsyd)
    comment.user = users(:one)
    assert_difference('TripCommentNotification.count') do
      assert comment.save
    end
  end

  test "should delete comment on trip" do
    cmt = comments(:trip1)
    comment = TripComment.find(cmt.id)
    assert comment.update_attribute(:deleted, true)
  end

    test "should create comment on trip with value space" do
    comment = TripComment.new
    comment.body = '     '
    comment.trip = trips(:tokjak)
    comment.user = users(:one)
    assert_no_difference('TripCommentNotification.count') do
      assert !comment.save
    end
  end

  test "should create comment on trip with myself" do
    comment = TripComment.new
    comment.body = '     '
    comment.trip = trips(:bogjak)
    comment.user = users(:dono)
    assert_no_difference('TripCommentNotification.count') do
      assert !comment.save
    end
  end

end
