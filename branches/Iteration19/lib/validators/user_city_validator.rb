# validator class to handle city location

class UserCityValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.blank?
      if record.city_location.blank?
        record.errors[:city_location] << (options[:message] || I18n.t('errors.location_not_given'))
      else
        record.errors[:city_location] << (options[:message] || I18n.t('errors.location_not_valid', :value => record.city_location))
      end
    end
  end
end
