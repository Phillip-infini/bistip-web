require 'test_helper'

class SearchStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "submit new trip assert that it's searchable" do

    full_login_as(:one)

    create_trip 'jakarta', 'medan', DateTime.current.advance(:day => 2), 'notes'

    get search_trip_path
    assert_template 'index'
    assert_response :success

    get search_trip_path, :origin => 'jakarta', :destination => 'medan',
      :departure_date => Date.current.advance(:days => 3).to_s,
      :departure_date_predicate => 'before'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:trips)
    assert assigns(:trips).size == 1

  end

end
