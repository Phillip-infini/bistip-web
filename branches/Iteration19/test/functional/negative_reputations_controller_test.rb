require 'test_helper'

class NegativeReputationsControllerTest < ActionController::TestCase
  
  setup do
    @userx = users(:dono)
    @usery = users(:mactavish)
  end
  
  test "should get new" do
    login_as(:one)
    get :new, :user_id=>@usery.id
    assert_response :success
  end

  test "userX should give a negative reputation to userY" do
    login_as(:dono)
    assert_difference('NegativeReputation.count') do
      post :create, :user_id=>@usery.id, :negative_reputation => {
        :body=>"very good service",
        :giver=> @userx
      }
    end
    assert_redirected_to profile_path(@usery.username)
  end
end
