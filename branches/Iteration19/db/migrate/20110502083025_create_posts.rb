class CreatePosts < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.text :body
      t.text :quoted
      t.belongs_to :user
      t.belongs_to :topic
      t.column :deleted, :boolean, :default => false
      t.column :quoted_from_id, :integer, :null => true, :references => :users

      t.timestamps
    end
  end

  def self.down
    drop_table :posts
  end
end
