class CreateIncludeItems < ActiveRecord::Migration
  def self.up
    create_table :include_items do |t|
      t.string :word
      t.timestamps
    end
  end

  def self.down
    drop_table :include_items
  end
end
