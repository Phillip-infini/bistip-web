require 'test_helper'

class ContentControllerTest < ActionController::TestCase

  test "should get how" do
    get :how
    assert_response :success
  end

  test "should get guide" do
    get :guide
    assert_response :success
  end

  test "should get terms of service" do
    get :terms_of_service
    assert_response :success
  end

  test "should get privacy policy" do
    get :privacy_policy
    assert_response :success
  end
end
