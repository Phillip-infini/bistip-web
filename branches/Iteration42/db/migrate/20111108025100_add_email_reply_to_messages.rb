class AddEmailReplyToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :email_reply, :boolean, :default => false
  end

  def self.down
    remove_column :messages, :email_reply
  end
end
