class CreateTopics < ActiveRecord::Migration
  def self.up
    create_table :topics do |t|
      t.string :title
      t.belongs_to :forum
      t.belongs_to :user
      t.column :deleted, :boolean, :default => false
      t.integer :rank, :default => 1

      t.timestamps
    end
  end

  def self.down
    drop_table :topics
  end
end
