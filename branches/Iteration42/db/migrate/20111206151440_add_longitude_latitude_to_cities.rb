class AddLongitudeLatitudeToCities < ActiveRecord::Migration
  def self.up
    add_column :cities, :longitude, :float
    add_column :cities, :latitude, :float
    count = 0;
    File.open("#{Rails.root}/db/data/cities-with-longlat.txt", "r") do |infile|
      infile.read.each_line do |city_row|
        country_code, name, latitude, longitude = city_row.chomp.split("|")
        country = Country.find_by_code(country_code)

        unless country.blank?
          city = City.find_by_name_and_country_id(name, country.id)
          if city.blank?
            puts "#{name} with #{country_code} was not found"
          else
            city.latitude = latitude
            city.longitude = longitude
            city.save
            count = count + 1
          end
        end
      end
    end
    puts "populate longlat for #{count} out of #{City.count} cities"
  end

  def self.down
    remove_column :cities, :longitude
    remove_column :cities, :latitude
  end
end
