# suggested item from tracked conversation
class ConversationSuggestedItem < SuggestedItem
  before_create :set_admin_settings

  # constant
  CNAME = 'conversation'
  ADMIN_USERNAME = 'admin'

  private
    def set_admin_settings
      self.deleted = false
      self.user = User.find_by_username(ADMIN_USERNAME)
    end
end
