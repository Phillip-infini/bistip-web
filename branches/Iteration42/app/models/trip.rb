# Trip class represent a trip or travel plan
class Trip < ActiveRecord::Base
  include Location

  # constants
  NOTES_MAXIMUM_LENGTH = 1000
  CARRIAGE_RETURN_BUFFER = 100
  SUMMARY_MATCH_COUNT = 1
  SUMMARY_SIMILAR_COUNT = 2
  DEFAULT_PER_PAGE = 10
  SEARCH_PER_PAGE = 10
  MAXIMUM_MULTIPLE = 3
  EMAIL_REPLY_DOMAIN = APP_CONFIG[:email_reply_address]
  EMAIL_REPLY_PREFIX = "tm"
  EMAIL_REPLY_SEPARATOR = '-'
  EMAIL_REPLY_FROM = 'mail@bistip.com'
  DISTANCE_MILES = 621 # 1000 km
  SEARCH_SUGGESTIONS_COUNT = 6

  # scope
  # default_scope lambda { {:conditions => ["(departure_date >= ? or routine is true) and deleted = ?", DateTime.current.beginning_of_day, false], :include => [:origin_city, :destination_city]} }
  scope :active_only, lambda { {:conditions => ["(departure_date >= ? or routine is true) and deleted = ?", DateTime.current.beginning_of_day, false]} }
  scope :join_with_cities, lambda { {:include => [:origin_city, :destination_city] }}
  scope :origin_city_id, lambda { |origin_city_id| {:conditions => ["origin_city_id = ?", origin_city_id]} }
  scope :destination_city_id, lambda { |destination_city_id| {:conditions => ["destination_city_id = ?", destination_city_id]} }
  scope :origin_country_id, lambda { |country_id| {:conditions => ["cities.country_id = ?", country_id], :include => [:origin_city, :destination_city]} }
  scope :destination_country_id, lambda { |country_id| {:conditions => ["destination_cities_trips.country_id = ?", country_id], :include => [:origin_city, :destination_city]} }
  scope :origin_country_id_in, lambda { |country_ids| {:conditions => ["cities.country_id IN (?)", country_ids], :include => [:origin_city, :destination_city]} }
  scope :destination_country_id_in, lambda { |country_ids| {:conditions => ["destination_cities_trips.country_id IN (?)", country_ids], :include => [:origin_city, :destination_city]} }
  scope :departure_date_after, lambda { |departure_date| {:conditions => ["departure_date >= ? or routine is true", departure_date]} }
  scope :departure_date_before, lambda { |departure_date| {:conditions => ["departure_date <= ? or routine is true", departure_date]} }
  scope :notes_contains, lambda { |keyword| {:conditions => ["match(notes) against(?)", keyword]} }
  scope :notes_and_items_contains, lambda { |keyword| {:conditions => ["match(trips.notes) against(?) or match(items.name) against(?)", keyword, keyword], :include => [:items]} }
  scope :user_is_not, lambda { |user_id| {:conditions => ["user_id != ?", user_id]} }
  scope :user_is, lambda { |user_id| {:conditions => ["user_id = ?", user_id]} }
  scope :random, lambda { |*args| {:order => 'RAND()', :limit => args[0] || 1} }
  scope :similar_trips, lambda { |origin_city_id, destination_city_id, trip_id| {:conditions => ["origin_city_id = ? AND destination_city_id = ? AND trips.id != ?", origin_city_id, destination_city_id, trip_id]} }
  scope :same_origin_country_trips, lambda { |origin_country_id, destination_city_id, origin_city_id| {:conditions => ["cities.country_id = ? AND destination_city_id = ? AND origin_city_id != ?", origin_country_id, destination_city_id, origin_city_id], :include => [:origin_city, :destination_city]} }
  scope :from_restricted_list, lambda { |origin_list, destination_city_id| {:conditions => ["origin_city_id IN (?) AND destination_city_id = ?", origin_list, destination_city_id]} }
  scope :same_origin_country_routes, lambda { |origin_country_id, destination_city_id, origin_city_id| {:conditions => ["cities.country_id = ? AND destination_city_id = ? AND origin_city_id != ? ", origin_country_id, destination_city_id, origin_city_id], :include => [:origin_city, :destination_city], :group => ['origin_city_id', 'destination_city_id']} }
  scope :routes_from_restricted_list, lambda { |origin_list, destination_city_id| {:conditions => ["origin_city_id IN (?) AND destination_city_id = ?", origin_list, destination_city_id], :group => ['origin_city_id', 'destination_city_id']} }
  scope :sixty_days_ago, lambda { {:conditions => ["created_at > ?", 60.days.ago]} }
  scope :incoming_to_user_location, lambda { |user| {:conditions => ["destination_city_id = ? and user_id <> ?", user.city.id, user.id], :order => "created_at desc"} }
  scope :owner_has_avatar, lambda { {:conditions => ["users.avatar_file_name is not null  or users.avatar_file_name != ''"], :include => [:user]} }
  scope :origin_country_not, lambda { |country_id| {:conditions => ["cities.country_id != ?", country_id], :include => [:origin_city, :destination_city]} }
  scope :destination_country_not, lambda { |country_id| {:conditions => ["destination_cities_trips.country_id != ?", country_id], :include => [:origin_city, :destination_city]} }
  scope :newest, :order => "trips.created_at DESC"
  scope :earliest, :order => "trips.departure_date ASC, trips.period DESC"

  # validation
  validates :origin_city, :origin_city => true
  validates :destination_city, :destination_city => true
  validates :departure_date, :presence => true, :future_date => true, :if => :not_routine?
  validates :notes, :length => {:maximum => (NOTES_MAXIMUM_LENGTH + CARRIAGE_RETURN_BUFFER)}
  validates :arrival_date, :arrival_time => true, :if => :not_routine?
  validates :day, :presence => true, :valid_day => true, :if => :routine?
  validates :period, :inclusion => {:in => Period::ALL, :allow_nil => false, :allow_blank => false}, :if => :routine?

  # relationship
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'
  belongs_to :destination_city, :class_name => "City", :foreign_key => 'destination_city_id'
  belongs_to :user

  has_many :comments, :class_name => "TripComment"
  has_many :items, :class_name => "TripItem", :order => "id"

  # nested
  accepts_nested_attributes_for :items, :allow_destroy => true, :reject_if => lambda { |a| a[:name].blank? }

  # serialize
  serialize :day

  # event
  before_create :generate_salt
  after_create :send_match_notification

  # send email to owner of matching seek
  def send_match_notification
    matching_seeks = find_matching_seeks
    notified_user = Array.new
    matching_seeks.each do |seek|
      # do not notify a user more than once
      unless notified_user.include?(seek.user)
        MatchTripNotification.create!(:receiver => seek.user, :sender => user, :data_id => id)
        if seek.user.user_configuration.email_match_trip
          Notifier.email_matching_seek(self, seek).deliver
        end
        notified_user << seek.user
      end
    end
  end

  handle_asynchronously :send_match_notification

  # friendly URL for SEO

  def to_param
    ori_city = origin_city.all_cities? ? origin_city.country.name : origin_city.name
    dest_city = destination_city.all_cities? ? destination_city.country.name : destination_city.name

    I18n.t('trip.index.url', :id => id, :flag => TrackLink::BS_V1,:ori => ori_city, :dest => dest_city).parameterize
  end

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  # check is a trip still active
  def active?
    if !self.departure_date.nil?
      self.departure_date >= DateTime.current.beginning_of_day and self.deleted == false
    elsif routine?
      return self.deleted==false
    end
  end

  # find match in seek table
  def find_matching_seeks
    if self.active?
      scope = Seek.scoped({})

      # make scope to have active only
      scope = scope.active_only

      # find seek with the same origin city, all cities country id, all cities continent id, and anywhere
      origin_all_cities = City.find_by_country_id_and_name(self.origin_city.country.id, City::ALL_CITIES)
      origin_continent = self.origin_city.country.continent.get_city_placeholder
      scope = scope.origin_city_id_in [self.origin_city_id, origin_all_cities.id, origin_continent.id]

      destination_all_cities_id = City.find_by_country_id_and_name(self.destination_city.country.id, City::ALL_CITIES)
      scope = scope.destination_city_or_all_cities_id self.destination_city_id, destination_all_cities_id
      scope = scope.user_is_not self.user_id

      # make scope to get seek from <anywhere>
      scope2 = Seek.scoped({})
      scope2 = scope2.active_only

      scope2 = scope2.from_anywhere

      destination_all_cities_id = City.find_by_country_id_and_name(self.destination_city.country.id, City::ALL_CITIES)
      scope2 = scope2.destination_city_or_all_cities_id self.destination_city_id, destination_all_cities_id
      scope2 = scope2.user_is_not self.user_id

      scope | scope2
    else
      Array.new
    end
  end

  def to_s
    I18n.t('trip.short', :from => self.origin_city.name, :to => self.destination_city.name)
  end

  def not_routine?
    !routine?
  end

  #options that apply in every data in webservice that fetch trip record
  def self.data_options
    return {:only => [:id, :notes, :period, :day, :routine], :skip_types=>true, :skip_instruct=>true, :methods => [:origin_location, :destination_location, :departure_date_medium_format, :arrival_date_medium_format, :username]}
  end

  def items_name_all
    unless self.items.empty?
      result = ''
      self.items.each_with_index do |item, index|
        # append separator if not the first one
        result << ', ' if index > 0
        result << item.name
      end
      return result
    end
    ''
  end

  def items_name_for_display
    items_all = items_name_all
    if !items_all.blank? and items_all.size > 30
      items_all.slice(0, 30) + '...'
    else
      items_all
    end
  end

  def self.build_scope_home_index(count)
    scope = Trip.scoped({})
    scope = scope.active_only
    scope = scope.owner_has_avatar
    scope = scope.random(count)
  end

  def self.build_scope_dashboard_index(count, user)
    scope = Trip.scoped({})
    scope = scope.active_only
    scope = scope.owner_has_avatar
    scope = scope.random(count)
    scope = scope.user_is_not(user.id)
  end

  # build scoped Trip for searching trip purpose
  def self.build_scope_search(origin = nil, destination = nil, departure_date = nil, departure_date_predicate = nil, keyword = nil, sort = nil)
    scope = Trip.scoped({})
    scope = scope.active_only

    if !origin.nil?
      if origin.is_continent_country?
        scope = scope.origin_country_id_in origin.country.continent.get_country_ids
      elsif origin.all_cities?
        scope = scope.origin_country_id origin.country
      else
        scope = scope.origin_city_id origin.id
      end
    end

    if !destination.nil?
      if destination.is_continent_country?
        scope = scope.destination_country_id_in destination.country.continent.get_country_ids
      elsif destination.all_cities?
        scope = scope.destination_country_id destination.country
      else
        scope = scope.destination_city_id destination.id
      end
    end

    # build predicate on query for departure date based on predicate
    if !departure_date.nil? and !departure_date_predicate.blank?
      case departure_date_predicate
        when DatePredicate::AFTER
          scope = scope.departure_date_after departure_date
        when DatePredicate::BEFORE
          scope = scope.departure_date_before departure_date
      end
    end

    # build predicate for keyword
    if !keyword.blank?
      scope = scope.notes_and_items_contains keyword
    end

    # build sort predicate
    if !sort.blank?
      scope = TripSort.build(sort, scope)
    else
      scope = TripSort.default(scope)
    end

    return scope
  end

  def departure_date_medium_format
    I18n.l(departure_date, :format => :medium) if departure_date
  end

  def arrival_date_medium_format
    I18n.l(arrival_date, :format => :medium) if arrival_date
  end

  def username
    return user.username
  end

  def last_comment_page
    comment_count = self.comments.where("reply_to_id is null").count
    [((comment_count - 1) / Comment::PER_PAGE) + 1, 1].max
  end

  def week_days_for_display
    unless self.day.empty?
      result = ''
      self.day.each_with_index do |day, index|
        # append separator if not the first one
        result << ', ' if index > 0
        result << day
      end
      return result
    end
    ''
  end

  # equality for trip
  def same_route?(other)
    return (self.origin_city_id == other.origin_city_id) && (self.destination_city_id == other.destination_city_id)
  end

  # check nullable of origin, destination and date a trip
  def empty_fields?
    if self.origin_location.blank? and self.destination_location.blank? and self.departure_date.blank?
      true
    else
      false
    end
  end

  # get previous trip
  def previous
    unless self.previous_trip_id.blank?
      Trip.find(self.previous_trip_id)
    else
      nil
    end
  end

  # get next trip
  def upcoming
    Trip.find_by_previous_trip_id(self.id)
  end

  # method to populate salt for new account
  def generate_salt
    self.salt = ActiveSupport::SecureRandom.hex(4)
  end

  # return email reply address
  def email_reply_address
    "#{EMAIL_REPLY_PREFIX}-#{self.id}-#{self.salt}@#{EMAIL_REPLY_DOMAIN}"
  end

  # get suggestions for a trip
  def self.get_suggestions(trip)
    suggested_trips = Array.new

    # domestic trip? show only similar trips
    if (trip.origin_city.country == trip.destination_city.country)
      suggested_trips = self.active_only.similar_trips(trip.origin_city.id, trip.destination_city.id, trip.id)
    else
      # the trip with the exactly same route only
      similar_trips = self.active_only.similar_trips(trip.origin_city.id, trip.destination_city.id, trip.id)

      # get trip that originated from the same country
      same_origin_country_trips = self.active_only.same_origin_country_trips(trip.origin_city.country.id, trip.destination_city, trip.origin_city.id)

      # get trip origin from different country but location is nearby
      nearby_cities = trip.origin_city.nearbys(DISTANCE_MILES)
      suggested_nearby_trips = self.active_only.from_restricted_list(nearby_cities, trip.destination_city).origin_country_not(trip.destination_city.country.id)

      suggested_trips = similar_trips + same_origin_country_trips + suggested_nearby_trips
    end
    suggested_trips
  end

  # suggestions for a route
  def self.get_suggestions_routes(origin, destination)
    suggested_routes = Array.new

    # domestic route? suggest nothing
    if (origin.country == destination.country)
      # do nothing
    else
      # find route originated from the same country
      same_origin_country_routes = self.active_only.same_origin_country_routes(origin.country.id, destination.id, origin.id).count

      # find route from different country but the origin is nearby
      nearby_routes = origin.nearbys(DISTANCE_MILES)
      nearby_routes = self.active_only.routes_from_restricted_list(nearby_routes, destination).origin_country_not(destination.country.id).count

      # merge the ordered hash together
      suggested_routes = same_origin_country_routes.merge(nearby_routes)
    end
    
    suggested_routes
  end

end