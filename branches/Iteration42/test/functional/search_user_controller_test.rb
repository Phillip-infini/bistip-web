require 'test_helper'

class SearchUserControllerTest < ActionController::TestCase
  
  test "should find user allan" do
    post :index, :keyword => 'allan'
    assert_not_nil assigns(:users)
    assert_equal 1, assigns(:users).size
    assert_response :success
    assert_template 'index'
  end
end
