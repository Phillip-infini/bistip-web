require 'test_helper'

class ContentControllerTest < ActionController::TestCase

  test "should get how" do
    get :how, :locale => 'id'
    assert_response :success

    get :how, :locale => 'en'
    assert_response :success
  end

  test "should get guide" do
    get :guide, :locale => 'id'
    assert_response :success

    get :guide, :locale => 'en'
    assert_response :success
  end

  test "should get guide all" do
    get :guide_all, :locale => 'id'
    assert_response :success

    get :guide_all, :locale => 'en'
    assert_response :success
  end

  test "should get guide traveler" do
    get :guide_traveler, :locale => 'id'
    assert_response :success

    get :guide_traveler, :locale => 'en'
    assert_response :success
  end

  test "should get guide requester" do
    get :guide_requester, :locale => 'id'
    assert_response :success

    get :guide_requester, :locale => 'en'
    assert_response :success
  end

  test "should get terms of service" do
    get :terms_of_service, :locale => 'id'
    assert_response :success

    get :terms_of_service, :locale => 'en'
    assert_response :success
  end

  test "should get privacy policy" do
    get :privacy_policy, :locale => 'id'
    assert_response :success

    get :privacy_policy, :locale => 'en'
    assert_response :success
  end

  test "should get about" do
    get :about, :locale => 'id'
    assert_response :success

    get :about, :locale => 'en'
    assert_response :success
  end

  test "should get awards" do
    get :awards, :locale => 'id'
    assert_response :success

    get :awards, :locale => 'en'
    assert_response :success
  end

  test "should get bistip point" do
    get :bistip_point, :locale => 'id'
    assert_response :success

    get :bistip_point, :locale => 'en'
    assert_response :success
  end

  test "should get event bistipes" do
    get :event_bistipes, :locale => 'id'
    assert_response :success

    get :event_bistipes, :locale => 'en'
    assert_response :success
  end

  test "should get event blog" do
    get :event_blog, :locale => 'id'
    assert_response :success

    get :event_blog, :locale => 'en'
    assert_response :success
  end

  test "should get event slogan" do
    get :event_slogan, :locale => 'id'
    assert_response :success

    get :event_slogan, :locale => 'en'
    assert_response :success
  end

  test "should get faq" do
    get :faq, :locale => 'id'
    assert_response :success

    get :faq, :locale => 'en'
    assert_response :success
  end

  test "should get jobs" do
    get :jobs, :locale => 'id'
    assert_response :success

    get :jobs, :locale => 'en'
    assert_response :success
  end

  test "should get media" do
    get :media, :locale => 'id'
    assert_response :success

    get :media, :locale => 'en'
    assert_response :success
  end

  test "should get new" do
    get :new, :locale => 'id'
    assert_response :success

    get :new, :locale => 'en'
    assert_response :success
  end

  test "should get safepay" do
    get :safepay, :locale => 'id'
    assert_response :success

    get :safepay, :locale => 'en'
    assert_response :success
  end
end
