require 'test_helper'

class RelationTest < ActiveSupport::TestCase

  test "should create relation" do
    relation = Relation.new
    relation.from = users(:one)
    relation.to = users(:dono)
    assert relation.save
  end

  test 'should create relation from review' do
    assert_difference('Relation.count') do
      Relation.build_relation_from_review(reviews(:positive1))
    end
  end
  
end
