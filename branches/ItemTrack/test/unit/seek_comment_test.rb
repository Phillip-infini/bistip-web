require 'test_helper.rb'

class SeekCommentTest < ActiveSupport::TestCase
  test "should create comment on seek" do
    comment = SeekComment.new
    comment.body = 'body 123'
    comment.seek = seeks(:jktsyd)
    comment.user = users(:one)
    assert comment.save
  end

  test "should delete comment on seek" do
    cmo = comments(:seek1)
    comment = SeekComment.find(cmo.id) #comment.find(comments(:cmt))
    assert comment.destroy
  end

  test "should create comment on seek with value space" do
    comment = SeekComment.new
    comment.body = '     '
    comment.seek = seeks(:jktsyd)
    comment.user = users(:dono)
    assert comment.save
  end

  test "should create comment on seek with myself" do
    comment = SeekComment.new
    comment.body = '     '
    comment.seek = seeks(:tokjak)
    comment.user = users(:dono)
    assert comment.save
  end

end
