class TransactionsController < ApplicationController
  before_filter :authenticate, :only => [:new, :create, :edit, :update, :show]
  before_filter :trip_owner, :only => [:new]
  
  def new
    @trip = Trip.find(params[:trip_id]) if params[:trip_id]
    @transaction = Transaction.new
    @transaction.trip_id = @trip.id
    @transaction.wanted_bistiper_id = current_user.id
    @transaction.bistiper_id = @trip.user.id

    @transaction.messages.build
#    @message = Message.new
    @receiver = @trip.user
    @sender = current_user
  end

  def create
    @transaction = Transaction.new(params[:transaction])
#    raise @transaction.to_yaml
    if @transaction.save
      redirect_to trip_transaction_path(@transaction.trip,@transaction)
    else
      put_model_errors_to_flash(@transaction.errors,'redirect')
      redirect_to new_trip_transaction_path(@transaction.trip)
    end
  end

  def update
    @transaction = Transaction.find(params[:id])
    if @transaction.update_attributes(params[:transaction])
      redirect_to transactions_path
    end
  end

  def show
    @transaction = Transaction.find(params[:id])
  end

  def index
    if current_user
      @incoming = Transaction.find_all_by_bistiper_id current_user.id
      @outgoing = Transaction.find_all_by_wanted_bistiper_id current_user.id
    else
      redirect_to root_path
    end
    
  end

  def edit
     @trip = Trip.find(params[:trip_id])
     @transaction = Transaction.find(params[:id])
     last = @transaction.messages.last
     @receiver = last.sender
     @sender = last.receiver

    @transaction.messages << Message.new
  end

  def trip_owner
    trip = Trip.find(params[:trip_id])
    redirect_to trip if current_user == trip.user
  end
end
