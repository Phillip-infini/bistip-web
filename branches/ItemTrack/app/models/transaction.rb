class Transaction < ActiveRecord::Base
  STATUS_WAIT = 0
  STATUS_ACTIVE = 1
  STATUS_CANCELED = 2
  STATUS_CLOSED = 4

  has_many :messages
  
  belongs_to :bistiper, :class_name=>'User', :foreign_key => 'bistiper_id'
  belongs_to :wanted_bistiper, :class_name=>'User', :foreign_key => 'wanted_bistiper_id'
  belongs_to :trip, :class_name=>'Trip', :foreign_key => 'trip_id'

  validates :transaction_code, :uniqueness=>true
  
  after_create :generate_code

  accepts_nested_attributes_for :messages, :allow_destroy => true
  
  
  
  def self.mapAll
    return [['wait', STATUS_WAIT],['active', STATUS_ACTIVE],['cancel', STATUS_CANCELED],['close', STATUS_CLOSED]]
  end

  def map(user)
    if is_wait? || is_new?
      if is_wanted_bistiper?(user)
        [['wait', STATUS_WAIT],['cancel', STATUS_CANCELED]]
      else is_bistiper?(user)
        [['wait', STATUS_WAIT], ['active', STATUS_ACTIVE],['cancel', STATUS_CANCELED]]
      end
    elsif is_active?
      if is_wanted_bistiper?(user)
        [['active', STATUS_ACTIVE],['cancel', STATUS_CANCELED], ['close', STATUS_CLOSED]]
      else is_bistiper?(user)
        [['active', STATUS_ACTIVE],['cancel', STATUS_CANCELED]]
      end
    else
      [['cancel', STATUS_CANCELED],['close', STATUS_CLOSED]]
    end
  end

  def is_new?
    self.id.nil?
  end

  def generate_code
    tx_code = self.created_at.to_i.to_s(36)
    self.update_attribute('transaction_code', tx_code)
  end

  def is_wait?
    self.status == STATUS_WAIT || self.status.nil?
  end

  def is_active?
    self.status == STATUS_ACTIVE
  end

  def is_cancel?
    self.status == STATUS_CANCELED
  end

  def is_close?
    self.status == STATUS_CLOSED
  end

  def is_bistiper?(user)
    self.bistiper == user
  end

  def is_wanted_bistiper?(user)
    self.wanted_bistiper == user
  end

  def edit_status?(user)
    result = true
    if is_bistiper?(user)
      puts "user = bistiper"
      if is_wait? || is_active?
        result = false
      elsif is_cancel? || is_close?
        result = true
      end
    elsif is_wanted_bistiper?(user)
      puts "user = wanted bistiper"
      if is_wait? || is_close? || is_new?
        result = true
      elsif is_active?
        result = false
      end
    end
    
    result
  end
end
