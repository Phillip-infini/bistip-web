class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages do |t|
      t.column :receiver_id, :integer, :null => false, :references => :users
      t.column :sender_id, :integer, :null => false, :references => :users
      t.text :body
      t.column :read, :boolean, :default => false
      t.string :subject
      t.column :reply_to_id, :integer, :null => true, :references => :messages
      t.belongs_to :transaction
      t.timestamps
    end
  end

  def self.down
    drop_table :messages
  end
end
