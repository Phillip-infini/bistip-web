class CreateTransactions < ActiveRecord::Migration
  def self.up
    create_table :transactions do |t|
      t.string :title
      t.column :transaction_code, :string, :unique => true
      t.column :status, :integer, :default => 0
      t.column :bistiper_id, :integer, :null => false, :references => :users
      t.column :wanted_bistiper_id, :integer, :null => false, :references => :users
      t.belongs_to :trip
      t.timestamps
    end
  end

  def self.down
    drop_table :transactions
  end
end
