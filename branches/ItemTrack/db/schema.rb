# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110418045457) do

  create_table "cities", :force => true do |t|
    t.string   "name"
    t.string   "region"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cities", ["id"], :name => "index_cities_on_id", :unique => true

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "trip_id"
    t.integer  "seek_id"
    t.boolean  "deleted",    :default => false
    t.text     "body"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", :force => true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "countries", ["id"], :name => "index_countries_on_id", :unique => true

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["locked_by"], :name => "delayed_jobs_locked_by"
  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "messages", :force => true do |t|
    t.integer  "receiver_id",                       :null => false
    t.integer  "sender_id",                         :null => false
    t.text     "body"
    t.boolean  "read",           :default => false
    t.string   "subject"
    t.integer  "reply_to_id"
    t.integer  "transaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "negative_reputations", :force => true do |t|
    t.integer  "giver_id",    :null => false
    t.integer  "receiver_id", :null => false
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "positive_reputations", :force => true do |t|
    t.integer  "giver_id",    :null => false
    t.integer  "receiver_id", :null => false
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seeks", :force => true do |t|
    t.integer  "origin_city_id",                              :null => false
    t.integer  "destination_city_id",                         :null => false
    t.boolean  "deleted",                  :default => false
    t.integer  "user_id"
    t.string   "departure_date_predicate"
    t.datetime "departure_date"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "seeks", ["id"], :name => "index_seeks_on_id", :unique => true
  add_index "seeks", ["notes"], :name => "fulltext_seeks_notes"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "transactions", :force => true do |t|
    t.string   "title"
    t.string   "transaction_code"
    t.integer  "status",             :default => 0
    t.integer  "bistiper_id",                       :null => false
    t.integer  "wanted_bistiper_id",                :null => false
    t.integer  "trip_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trips", :force => true do |t|
    t.integer  "origin_city_id",                         :null => false
    t.integer  "destination_city_id",                    :null => false
    t.boolean  "deleted",             :default => false
    t.integer  "user_id"
    t.datetime "departure_date"
    t.datetime "arrival_date"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "period"
    t.string   "day"
    t.boolean  "routine",             :default => false
  end

  add_index "trips", ["id"], :name => "index_trips_on_id", :unique => true
  add_index "trips", ["notes"], :name => "fulltext_trips_notes"

  create_table "user_configurations", :force => true do |t|
    t.boolean  "email_receive_message",      :default => true
    t.boolean  "email_receive_trip_comment", :default => true
    t.boolean  "email_receive_seek_comment", :default => true
    t.boolean  "email_receive_reputation",   :default => true
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "email_match_trip",           :default => true
    t.boolean  "email_match_seek",           :default => true
    t.boolean  "email_other_trip_comment",   :default => true
    t.boolean  "email_other_seek_comment",   :default => true
  end

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "fullname"
    t.string   "email"
    t.string   "email_validation_key"
    t.string   "hashed_password"
    t.string   "reset_password_key"
    t.string   "location"
    t.string   "contact_number"
    t.string   "extra_contact_number"
    t.string   "web"
    t.string   "facebook"
    t.string   "twitter"
    t.text     "bio"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
  end

  add_index "users", ["id"], :name => "index_users_on_id", :unique => true

end
