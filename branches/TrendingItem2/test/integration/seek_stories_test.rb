require 'test_helper'

class SeeksStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "create new seek" do

    # login
    full_login_as(:one)

    get new_seek_path
    assert_response :success
    assert_template 'new'

    # create seek
    post seeks_path, :seek => {:origin_location => "jakarta",
      :destination_location => "sydney",
      :departure_date => DateTime.current.advance(:day => 2),
      :departure_date_predicate => 'after',
      :notes => "text"
      }

    assert assigns(:seek).valid?
    assert_response :redirect
    follow_redirect!

  end

  test "edit and update a seek" do

    # login
    full_login_as(:one)

    seek_to_use = seeks(:jktsyd)

    # edit
    get edit_seek_path(seek_to_use)

    assert_response :success
    assert_template 'edit'

    # update
    put seek_path(seek_to_use), :seek => {:notes => "new notes"}

    assert_response :redirect
    assert_redirected_to seek_path(seek_to_use)

    follow_redirect!
  end

  test "delete a seek" do
     # login
    full_login_as(:one)

    seek_to_use = seeks(:jktsyd)

    # view the seek
    get seek_path(seek_to_use)
    assert_response :success
    assert_template 'show'

    # delete the seek
    delete seek_path(seek_to_use)

    assert_response :redirect
    assert_redirected_to profile_path(users(:one).username)

    follow_redirect!
  end

end
