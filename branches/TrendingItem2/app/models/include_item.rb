class IncludeItem < ActiveRecord::Base
  validates :word, :uniqueness => true

  def self.generate_all_in_array
    include_items = IncludeItem.all
    result = []
    include_items.each do |include_item|
      result << include_item.word
    end
    result
  end
end
