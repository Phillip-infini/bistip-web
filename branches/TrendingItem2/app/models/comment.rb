# Comment class represent a comment from a user to a trip or seek
class Comment < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 600

  # scope
  default_scope :order => 'created_at ASC', :conditions => ["deleted = ?",false]
  scope :body_contains, lambda { |keyword| {:conditions => ["match(body) against(?)", keyword]}}
  scope :ninety_days_ago, lambda {{ :conditions => ["created_at > ? and deleted = ?", 90.days.ago, false]}}

  # relationship
  belongs_to :seek
  belongs_to :trip
  belongs_to :user

  # event
  after_create :notify_other

  # validation
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH },
    :presence => true

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  def notify_other
    if !self.trip_id.nil?
      self.notify_other_trip_comment
    elsif !self.seek_id.nil?
      self.notify_other_seek_comment
    end
  end

end
