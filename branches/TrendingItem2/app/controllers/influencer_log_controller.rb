class InfluencerLogController < ApplicationController

  def transit
    
  end

  def index
    @total = InfluencerLog.all.size
    @influencer = InfluencerLog.top_influencer.paginate(:page=>params[:page],:per_page=>8)
  end

  #create an influencer point
  def create
    # get decodeurl parameter value
    decodedurl = params[:decodedurl]
#    transit = false
#    transit = params[:transit] if params[:transit]
#    if !transit
#      redirect_to root_path
#    end
    
    if !decodedurl.nil?
      # encode decodeurl using Base32
      encodedurl = Base32::Crockford.encode(decodedurl.to_i)

      # split encoded with X character
      data = encodedurl.to_s.split('X')
      trip_id = data[0]
      influencer_id = data[1]
      trip = Trip.unscoped.find(trip_id)
      influencer = User.find(influencer_id)

      # check if trip.user not same with influencer
      if trip.user!=influencer
        ip_address = request.ip
        log = InfluencerLog.new(:ip_address=>ip_address,:influencer_id=>influencer_id,:trip_id=>trip_id)
        # save influencer log if trip's active an request not come from robot
        log.save if trip.active? && !is_robot
        redirect_to trip_path(trip)
      end
    end
  end
end
