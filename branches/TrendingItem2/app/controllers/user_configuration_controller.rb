class UserConfigurationController < ApplicationController
  before_filter :authenticate, :only => [:edit, :update]

  def edit
    @user = current_user
    @config = current_user.user_configuration
  end
  
  def update
    if current_user.user_configuration.update_attributes(params[:user_configuration])
      flash[:notice] = t('configuration.update.message.success')
      redirect_to(profile_path(current_user.username))
    else
      put_model_errors_to_flash(current_user.errors)
      render :action => 'edit'
    end

  end
end
