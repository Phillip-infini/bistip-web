class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :prepare_for_mobile, :set_locale_by_ip
  
  MOBILE_BROWSERS = ["android","blackberry","palm","mobile","iphone","mobi",
                     "palm","palmos","palmsource","blackberry","nokia","phone","midp","pda",
                     "wap","java","nokia","hand","symbian","chtml","wml","ericsson","lg","audiovox","motorola",
                    "samsung","sanyo","sharp","telit","tsm","mobile","mini","windows ce","smartphone|",
                    "240x320","320x320","mobileexplorer","j2me","sgh","portable","sprint","vodafone",
                    "docomo","kddi","softbank","pdxgw","j-phone","astel","minimo","plucker","netfront",
                    "xiino","mot-v","mot-e","portalmmm","sagem","sie-s","sie-m","ipod","pre"]

  AM_I_ROBOT = ["googlebot","twitterbot",
    "facebookexternalhit", "http://www.google.com/bot.html",
    "http://www.facebook.com/externalhit_uatext.php",
    "tweetmemebot", "sitebot", "msnbot", "robot", "bot"]

  # Check if request's user agent is a robot
  def is_robot
    AM_I_ROBOT.each do |robot|
      if request.user_agent.downcase.include?(robot)
        return true
      end
    end
    return false
  end
  
  # Returns the currently logged in user or nil if there isn't one
  def current_user
    return unless session[:user_id]
    @current_user ||= User.find_by_id(session[:user_id])
  end

  # Make current_user available in templates as a helper
  helper_method :current_user

  # Filter method to enforce a login requirement & email validation
  # Apply as a before_filter on any controller you want to protect
  def authenticate
    if logged_in?
      if !current_user.email_validated?
        redirect_to not_validated_user_path(current_user)
      end
    else
      access_denied
    end
  end

  # Predicate method to test for a logged in user
  def logged_in?
    current_user.is_a? User
  end

  # Make logged_in? available in templates as a helper
  helper_method :logged_in?

  # check if current user is the same as the user_id in params
  def not_self
    user = User.find(params[:user_id])
    if current_user == user
      # user try to give himself a reputation, redirect away!
      redirect_to root_path
    end
  end

  # handle access denied
  def access_denied
    flash.now[:notice] = t('application.access_denied')
    redirect_away login_path
  end

  # redirect somewhere that will eventually return back to here
  def redirect_away(*params)
    session[:original_uri] = request.fullpath
    redirect_to(*params)
  end

  # returns the person to either the original url from a redirect_away or to a default url
  def redirect_back(*params)
    uri = session[:original_uri]
    session[:original_uri] = nil
    if uri
      redirect_to uri
    else
      redirect_to(*params)
    end
  end

  # put model error into flash in form of array
  def put_model_errors_to_flash(errors, type='render')
    errors_array = Array.new
    errors.full_messages.each do |msg|
      errors_array << msg
    end
    if type == 'render'
      flash.now[:model_alert] = errors_array
    elsif type == 'redirect'
      flash[:model_alert] = errors_array
    end
  end

  # helper to parse date parameter
  # return nil if it's invalid date
  def parse_date_parameter(value)
    if value.nil?
      nil
    else
      begin
        Date.parse(value)
      rescue ArgumentError
        nil
      end
    end
  end
  helper_method :parse_date_parameter
  
  # boolean helper to check whether a string is nil or empty
  def string_nil_or_empty?(value)
    if value.nil? or value.empty?
      true
    else
      false
    end
  end

  def email_receive_message(receiver)
    return receiver.user_configuration.email_receive_message
  end

  def email_receive_trip_comment(receiver)
    return receiver.user_configuration.email_receive_trip_comment
  end

  def email_receive_seek_comment(receiver)
    return receiver.user_configuration.email_receive_seek_comment
  end

  def email_receive_reputation(receiver)
    return receiver.user_configuration.email_receive_reputation
  end

  def mobile_version?
    
    if session[:mobile_param]
      session[:mobile_param] == "1"
    else
      MOBILE_BROWSERS.each do |m|
        if !request.user_agent.nil? && request.user_agent.downcase.include?(m)
          return true
        elsif !request.user_agent.nil? && request.user_agent.downcase.include?("x11")
          return false
        end
      end
      return false
    end
    
  end
  helper_method :mobile_version?

  def prepare_for_mobile
    session[:mobile_param] = params[:mobile] if params[:mobile]
    if mobile_version? && !request.xhr?
      request.format = :mobile
    end
  end

  # not used now but keep it in case if in the future we need it
  def set_locale
    #if params[:locale] is nil then I18n.default_locale will be used
    if params[:locale].nil?
       I18n.locale = locale_from_accept_language_header if session[:locale].nil?
    else
      session[:locale] = params[:locale]
      I18n.locale = session[:locale]
    end
  end

  # set website language based on IP
  def set_locale_by_ip

    if params[:locale].nil?
      # this is the first time user access the website, no local in session
      if session[:locale].nil?
        begin
          # get the country location from geocoder
          data = Geocoder.search(request.ip)
          country_code = data[0].country_code.downcase if !data.nil?

          # only consider id or en value for now
          if country_code == "id" or country_code == "en"
            I18n.locale = country_code
            session[:locale] = country_code
          end
        # anything happen with geocoder then default to id
        rescue Exception => e
          username = suspicious
          ContactUsNotifier.delay.email_exception(e.inspect, e.backtrace, request.ip, request.user_agent, request.fullpath, "LANG", username) unless is_robot
          I18n.locale = :id
        end
      else
        I18n.locale = session[:locale]
      end
    # user trying to switch language
    else
      # only consider id or en value for now
      if params[:locale] == 'id' or params[:locale] == 'en'
        session[:locale] = params[:locale]
        I18n.locale = session[:locale]
      end
    end
  end

  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception, :with => :render_error
    rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found
    rescue_from ActionController::UnknownController, :with => :render_not_found
    rescue_from ActionController::UnknownAction, :with => :render_not_found
  end

  def send_forum_email(forum, topic, post)
    email_to_send = forum.email
    if !email_to_send.blank?
      emails = email_to_send.split(' ')
      emails.each do |email|
        ContactUsNotifier.delay.email_forum_event(email, forum, topic, post)
      end
    end
  end
  
  private
    def render_not_found(exception)
      username = suspicious
      ContactUsNotifier.delay.email_exception(exception.inspect, exception.backtrace, request.ip, request.user_agent, request.fullpath, "404", username) unless is_robot
      render :template =>"/error/404", :status => 404
    end

    def render_error(exception)
      username = suspicious
      ContactUsNotifier.delay.email_exception(exception.inspect, exception.backtrace, request.ip, request.user_agent, request.fullpath, "500", username) unless is_robot
      render :template =>"/error/500", :status => 500
    end

    def locale_from_accept_language_header
      puts request.env['HTTP_ACCEPT_LANGUAGE']
      request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
    end

    def suspicious
      username = ""
      username = current_user.username if current_user
      return username
    end

end
