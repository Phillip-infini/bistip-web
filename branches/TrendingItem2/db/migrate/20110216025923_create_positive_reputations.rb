class CreatePositiveReputations < ActiveRecord::Migration
  def self.up
    create_table :positive_reputations do |t|
      t.column :giver_id, :integer, :null => false, :references => :users
      t.column :receiver_id, :integer, :null => false, :references => :users
      t.text :body
      t.timestamps
    end
  end

  def self.down
    drop_table :positive_reputations
  end
end
