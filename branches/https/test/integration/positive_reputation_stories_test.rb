require 'test_helper'

class PositiveReputationStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "user :one giving a positive reputation to user :dono" do
    one = users(:one)
    dono = users(:dono)

    #login
    full_login_as(:one)

    #view user detail
    get profile_path(dono.username)
    assert_response :success
    assert_template 'show'

    #give one positive reputation to :dono
    post user_positive_reputations_path(dono),:positive_reputation => {
      :body=>"very bad service",
      :giver_id=> one
    }

    assert_response :redirect
    assert_redirected_to profile_path(dono.username)
    follow_redirect!
  end
end
