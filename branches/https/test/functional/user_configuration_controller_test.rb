require 'test_helper'

class UserConfigurationControllerTest < ActionController::TestCase
  # Replace this with your real tests.

  setup do
    @config = user_configurations(:mac_config)
    @user = users(:mactavish)
  end
  
  test "should_get_edit_user_configuration" do
    login_as(:mactavish)
    get :edit, :user_id=>@user, :id=>@config
    assert_response :success
  end
  
  test "should_update_configuration_from_current_user" do
    login_as(:mactavish)
    put :update, :user_id=>@user, :id=>@config, :user_configuration => {:email_receive_message => false,:email_receive_trip_comment => false}
    assert_redirected_to dashboard_path
  end
end
