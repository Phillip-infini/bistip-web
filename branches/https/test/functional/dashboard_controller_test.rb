require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  # initialize current user
  setup do
    login_as(:mactavish)
  end

  #get /dashboard
  test "should get dashboard index" do
    get :index
    assert_response :success
  end

  #get /positive_reputations
  test "should get current_user's positive reputation" do
    get :positive_reputations
    assert_response :success
  end

  #get /negative_reputations
  test "should get current_user's negative reputation" do
    get :negative_reputations
    assert_response :success
  end

  #get /trips
  test "should get current_user's trips" do
    get :trips
    assert_response :success
  end

  #get /seeks
  test "should get current_user's seeks" do
    get :seeks
    assert_response :success
  end

  #get /profile
  test "should get current_user's profile" do
    get :profile
    assert_response :success
  end
end
