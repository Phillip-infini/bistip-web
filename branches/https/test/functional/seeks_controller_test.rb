require 'test_helper'

class SeeksControllerTest < ActionController::TestCase
  fixtures :all

  setup do
    @seek = seeks(:jktsyd)
  end

  test "should get new" do
    login_as(:one)
    get :new
    assert_response :success
  end

  test "should create seek" do
    login_as(:one)
    assert_difference('Seek.count') do
      post :create, :seek => {:origin_location => "jakarta",
      :destination_location => "sydney",
      :departure_date => DateTime.current.advance(:day => 2),
      :departure_date_predicate => 'after',
      :notes => "text",
      :items_attributes => {:item => {:name => 'ipad 2'}}}

    end

    assert_redirected_to seek_path(assigns(:seek))
  end

  test "should show seek" do
    get :show, :id => @seek.to_param
    assert_response :success
  end

  test "should get edit" do
    login_as(:one)
    get :edit, :id => @seek.to_param
    assert_response :success
  end

  test "should update seek" do
    login_as(:one)
    put :update, :id => @seek.to_param, :seek => @seek.attributes
    assert_redirected_to seek_path(assigns(:seek))
  end

  test "should destroy seek" do
    login_as(:one)
    assert_difference('Seek.count', -1) do
      delete :destroy, :id => @seek.to_param
    end

    assert_redirected_to profile_path(users(:one).username)
  end
end
