module ApplicationHelper

  # load jquery script tag to generate auto complete location
  def location_auto_complete_js(field_id, exclude_all_cities = false)
    source = "/cities/index"

    if exclude_all_cities
      source = source + '?exclude_all_cities=true'
    end

    parameter = {
        :source => source,
        :highlight => true
        }
    javascript_tag "$(document).ready(function() {
      $(\"\##{field_id}\").autocomplete(#{parameter.to_json});
    });"
  end

  # watermark_field
  def watermark_field(field_id, text)
    javascript_tag "$(document).ready(function() {
      $(\"\##{field_id}\").Watermark(\"#{text}\");
    });"
  end

  # quicktip
  def quicktip_js()
    javascript_tag "$(document).ready(function() {
        $('.quicktip').quicktip({
            speed:700
        });
    });"
  end


  # generate date picker for a field
  def date_picker_js(field_id)
    parameter = {
        :minDate => 0,
        :dateFormat => "DD, dd MM yy"
        }
    javascript_tag "$(function() {
        $( \"\##{field_id}\" ).datepicker(#{parameter.to_json});
      });"
  end

  #enable character counter to validate a field's length
  def enable_character_counter(field_id,length)
    javascript_tag "$(function(){
      $(\"\##{field_id}\").jqEasyCounter({
        'maxChars':#{length},
        'maxCharsWarning':#{length}
      });
    });"
  end

  def popup_auth(field_id,path)
    javascript_tag "$(function(){
      $(\"\##{field_id}\").click(function(){
        $.oauthpopup({
          path: '#{path}'
        });
      });
    });"
  end

  # generate link to clear a field
  def render_link_to_clear_a_field(field_id)
    clear_field_js = "$(\"#" + field_id + "\").val(\"\"); return false;";
    content_tag :span, link_to('clear', '#', {:onclick => clear_field_js, :href => void_javascript})
  end

  # return date difference of a given date to today's date
  def date_age_in_words(date)
    (distance_of_time_in_words DateTime.current, date) + ' ' + t('datetime.distance_in_words.ago')
  end

  # highlight menu if it's current
  def render_menu_link(text, param)
    link_to_unless_current(text, param) do
       link_to(text, param, {:style => "color:#fed700"})
    end
  end

#  # highlight menu in dashboard if it's current selection
  def is_sub_menu_selected(param)
    param.each do |path|
      if current_page?(path)
        return true
      end
    end
    return false
  end

#  def dashboard_inner_sub_menu_link(text,param)
#    param.each do |path|
#      if current_page?(path)
#        return content_tag :span, link_to(text, param), {:class => "active"}
#      end
#    end
#    content_tag :span, link_to(text, param)
#  end
  
  # check if secondary navigation show or not
  def hide_secondary_navigation?
    param = [root_path, new_user_path, social_login_path(:provider=>'twitter'), social_login_path(:provider=>'facebook')]
    param.each do |path|
      return true if current_page?(path)
    end
    return false
  end
  
  # check sub menu in dashboard
  def check_selected_sub_menu(param)
    param.each do |path|
      return true if current_page?(path)
    end
    return false
  end
  
  # highlight menu if it's current, param[0] to choose first child
  def render_sub_menu_link(text, param)
    param.each do |path|
      if current_page?(path)
        return link_to(text, param[0], :class => "selected")
      end
    end
    link_to(text, param[0])
  end

  # highlight menu if it's current, param[0] to choose first child
  # EXCLUSIVE FOR MOBILE
  def render_sub_menu_link_mobile(text, param)
    param.each do |path|
      if current_page?(path)
        return content_tag :li, link_to(text, param[0]), {:class => "active"}
      end
    end
    link_to(text, param[0])
  end

  # highlight menu if it's current
  def render_inner_sub_menu_link(text, param, tag = :li)
    if current_page?(param)
      return content_tag tag, link_to(text, param), {:class => "active"}
    else
      content_tag tag, link_to(text, param)
    end
  end

  # check if we need to render user sub menu
  def should_render_user_sub_menu?
    # user have to be logged in
    if logged_in?
      # if user instance variable is given then check if it's the current logged in user
      if @user
        current_user == @user
      # if requested page is trips index or seek index
      elsif current_page?({:controller => 'trips', :action => "index"}) or
          current_page?({:controller => 'seeks', :action => "index"})
        true
      end
    end
  end

  # boolean helper to check whether a string is nil or empty
  def string_nil_or_empty?(value)
    if value.nil? or value.empty?
      true
    else
      false
    end
  end

  # used to render page heading but will set title page too
  def page_title(title = nil)
    if title
      content_for(:page_title) { title }
    else
      content_for?(:page_title) ? content_for(:page_title) : t('default_page_title')  # or default page title
    end
  end

  # custom auto_link to enable open in new tab every hyperlink
  def auto_link_in_new_tab(content)
    return auto_link(simple_format(content), :all, :target => "_blank")
  end

  # auto link without simple format
  def auto_link_in_new_tab_without_simple_format(content)
    return auto_link(content, :all, :target => "_blank")
  end

  # change space separated keyword to array
  def search_notes_to_array(keyword)
    if !string_nil_or_empty?(keyword)
      keyword.split(' ')
    end
  end

  # subtract duplicated prefix from web url
  def subtract_dup_from_web_url(url)
    result = url;
    result = result.sub(User::WEB_PREFIX,'')
    result
  end

  # subtract duplicated prefix from facebook url
  def subtract_dup_from_facebook_url(url)
    result = url;
    result = result.sub(User::FACEBOOK_PREFIX,'')
    result = result.sub("http://facebook.com/",'')
    result
  end

  # subtract duplicated prefix from twitter url
  def subtract_dup_from_twitter_url(url)
    result = url;
    result = result.sub(User::TWITTER_PREFIX,'')
    result = result.sub("http://twitter.com/",'')
    result
  end

  # generate breadcrumb for forum
  def get_forum_breadcrumb(url)
    # remove parameter from url
    param_index = url.index('?')
    param_index = url.length unless !param_index.nil?

    url = url[0, param_index]

    breadcrumb = ''
    sofar = '/'
    elements = url.split('/')
    min = [elements.size, 5].min
    for i in 1...min
      sofar += elements[i]
      if i%2 == 0
        begin
          if (i+1) < min
            breadcrumb += "<a href='#{sofar}'>"  + eval("#{elements[i - 1].singularize.camelize}.find(#{elements[i]}).name").to_s + '</a>'
            breadcrumb += ' >> '
          else
            breadcrumb += eval("#{elements[i - 1].singularize.camelize}.find(#{elements[i]}).name").to_s
          end
        rescue
          breadcrumb += elements[i]
        end
      end
      sofar += '/'
    end
    if breadcrumb.length > 0
      t('forum.breadcrumb.pre') + "<a href='/forums'>Forum Home</a> >> " + breadcrumb
    else
      t('forum.breadcrumb.pre') + "Forum Home" + breadcrumb
    end
  end

  # not used for now, to convert time to a certain time zone according to locale
  def convert_time_according_locale(datetime)
    if I18n.locale == :id
      datetime.in_time_zone('Jakarta')
    else
      datetime
    end
  end

  # sanitize function to clean html
  def clean_sanitize(html)
    raw(Sanitize.clean(html, :elements => %w[
    a abbr b center blockquote br cite code dd dfn dl dt em i img kbd li mark ol p pre
    q s samp small strike strong sub sup time u ul var h1 h2 h3 div
  ],
      :attributes => {'a' => ['href', 'title'],
                      'img' => ['src','title']},
      :protocols => {'a' => {'href' => ['http', 'https', 'mailto','ymsgr']},
                     'img' => {'src' => ['http', 'https']}},
      :add_attributes => {'a' => {'target' => '_blank'}}))
  end

  # helper to generate recommend user
  def generate_recommend_url(user)
    recommend_url(:username => user.username)
  end

  # helper to generate recommend user
  def generate_recommend_title(user)
    t('reputation.recommendation.title', :username => user.username)
  end

  # dynamically remove item fields
  def link_to_remove_fields(name, f)
    function_name = 'remove_fields'
    function_name = 'remove_fields_mobile' if mobile_version?
    f.hidden_field(:_destroy) + link_to_function(name, "#{function_name}(this)", {:class => 'remove', :href => void_javascript})
  end

  # dynamically add item fields
  def link_to_add_fields(name, f, association, maximum)
    function_name = 'add_fields'
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "#{function_name}(this, '#{association}', '#{escape_javascript(fields)}', '#{maximum}')", {:class => 'add', :href => void_javascript})
  end

  def void_javascript
    'javascript:void(0);'
  end

  # build params for new seek link
  def build_params_for_new_seek
    if params[:origin] or params[:destination]
      result = '?'
      result << 'origin_location=' + params[:origin] if params[:origin]
      result << '&' if params[:origin] && params[:destination]
      result << 'destination_location=' + params[:destination] if params[:destination]
      result
    else
      ''
    end
  end
  
  def get_will_paginate(collection)
    will_paginate collection, :previous_label => t('pagination.previous'), :next_label => t('pagination.next'), :params => { :format => nil } 
  end

  def get_more_will_paginate(collection, id)
    will_paginate(collection, :renderer => TwitterPagination::LinkRenderer, :id => id, :class=>"morePagination")
  end
  
  #collect flash messages into series of <p> tag
  def collect_flash_messages(symbol)
    notices = ''
    msg = flash[symbol]
    if msg.is_a?(Array)
      msg.each do |m|
        notices += '<p>' + m + '</p>'
      end
    else
      notices += '<p>' + msg + '</p>'
    end
    notices.to_json
  end

  #generate country image tag
  def generate_country_image_tag(country, size = "16x11")
    country_code_image_path = "flags/" + country.code + ".png"
    image_tag country_code_image_path, :size => size, :alt => country.name, :title => country.name
  end

  # if logged in and not current user
  def logged_in_and_not_current_user?(user)
    if logged_in? and !(current_user == user)
      true
    else
      false
    end
  end

  # if logged in and current user
  def logged_in_and_current_user?(user)
    if logged_in? and (current_user == user)
      true
    else
      false
    end
  end

  # generate javascript page redirect call
  def javascript_redirect(url)
    "javascript:redirect(\'" + url + "\')"
  end

end
