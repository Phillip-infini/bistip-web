# Session controller to manage login and logout
class SessionsController < ApplicationController
  #ssl_required :new, :create
  
  # For Login Submission
  def create
    omniauth = request.env["omniauth.auth"]
    # If already logged in then return to root path
    if logged_in?
      redirect_to dashboard_path
    # Try to authenticate given email and password
    elsif user = User.authenticate(params[:email_or_username], params[:password])
      session[:user_id] = user.id
      flash[:notice] = t('login.message.success')
      redirect_back dashboard_path
    elsif !omniauth.nil? && omniauth.length>0
      user = User.oauth_authenticate(omniauth)
      if user.nil?
        @user = User.create_with_omniauth(omniauth)
        render auth_path
      else
        session[:user_id] = user.id
        if user.social_login?
          if user.provider.eql?("twitter")
            redirect_to dashboard_path
          else
            render "success"
          end
        else
          render "success"
        end
      end
      # Incorrect login
    else
      flash.now[:alert] = t('login.message.fail')
      render :action => 'new'
    end
  end

  # For Login Form
  def new
    @uri = params[:original_uri]
    session[:original_uri] = @uri if @uri!=nil
    if logged_in?
      redirect_to dashboard_path
    end
  end

  # For Logout
  def destroy
    if logged_in?

      # if in mobile, then after logout stay in mobile
      # also after logout stay on the selected language
      stay_mobile = mobile_version?
      selected_locale = session[:locale]
      
      reset_session
      if stay_mobile
        session[:mobile_param] = "1"
      end

      session[:locale] = selected_locale

      flash[:notice] = t('logout.message.success')
    end
    redirect_to root_path
  end
end
