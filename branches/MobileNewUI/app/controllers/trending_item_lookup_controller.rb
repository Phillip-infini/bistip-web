class TrendingItemLookupController < ApplicationController

  def show
    item_to_lookup = params[:item]
    @item = TrendingItem.find_by_item(item_to_lookup)
    redirect_to root_path
  end
end
