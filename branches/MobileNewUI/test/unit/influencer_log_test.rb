require 'test_helper'

class InfluencerLogTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "should create one point in influencer log" do
    trip = trips(:jktsyd)
    influencer = users(:mactavish)

    if trip.user != influencer
      ip_address = '192.168.1.44'
      log = InfluencerLog.new(:ip_address=>ip_address,:influencer_id=>influencer.id,:trip_id=>trip.id)
      assert log.save if trip.active?
    end
  end
end
