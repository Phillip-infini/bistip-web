class CreateForums < ActiveRecord::Migration
  def self.up
    create_table :forums do |t|
      t.string :title
      t.string :email
      t.text :description
      t.belongs_to :user

      t.timestamps
    end
  end

  def self.down
    drop_table :forums
  end
end
