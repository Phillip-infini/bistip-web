class PaypalNotificationsController < ApplicationController
  protect_from_forgery :except => [:create]
  
  # receive and store paypal IPN
  def create
    escrow = Escrow.find_by_invoice(params[:invoice])
    if params[:payment_status] == "Completed" &&
      params[:secret] == APP_CONFIG[:paypal_secret] &&
      params[:receiver_email] == APP_CONFIG[:paypal_email] &&
      !escrow.blank?

      # create paypal notification object and PayPalNotifyLog
      PaypalNotification.create!(:params => params, :escrow_id => escrow.id, :status => params[:payment_status], :transaction_id => params[:txn_id])
      PaypalNotifiedLog.create!(:escrow_id => escrow.id)
    end
    render :nothing => true    
  end

  
end
