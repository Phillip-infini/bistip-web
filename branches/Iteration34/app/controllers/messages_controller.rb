class MessagesController < ApplicationController
  ssl_required :index, :show, :create, :sent, :new
  before_filter :authenticate, :only => [:index, :show, :create, :sent, :new]
  #new message
  def new
    @user = User.find(params[:user_id])
    @curr = current_user
    @message = Message.new
    Message::ASSETS_MAXIMUM.times { @message.assets.build }

    redirect_to root_path if @user == @curr
  end

  #get messages
  def index
    sender = params[:sender]
    status = params[:status]
    keyword = params[:keyword]
    scope = Message.build_scope_search(current_user, sender, status, keyword)
    @no_valid_parameter = (sender.blank? and status.blank? and keyword.blank?)
    @threads = scope.paginate(:per_page => Message::DEFAULT_PER_PAGE, :page => params[:page])
    @messages = Array.new()
    @threads.each do |r|
      @messages << Message.where("reply_to_id = #{r.reply_to_id} and receiver_id = #{current_user.id}").order("created_at desc").first
    end

  end

  #get message
  def show
    @message = Message.find(params[:id])

    # Only sender or receiver can read the conversation!
    if (@message.sender == current_user or @message.receiver == current_user)
      # OK
      else
      redirect_to root_path
    end

    # find thread starter, if this message is a reply
    if !@message.thread_starter?
      @message = Message.find(@message.reply_to_id)
    end

    # if it's a thread starter and current user is not the sender then update the read attribute
    if current_user != @message.sender
      @message.update_attribute(:read, true)
    end

    # find all the child messages
    @messages = Message.order('created_at asc').children(@message.id)
    for message in @messages do
      # only update the read attribute if current_user is the receiver
      if current_user != message.sender
        message.update_attribute(:read, true)
      end
    end

    # create a conversation between two users, prevent from user self messaging
    @sender = current_user
    @receiver = @message.receiver

    if @sender.eql?(@receiver)
    @receiver = @message.sender
    end

    @reply_to_id = @message.id

    # initialize new message for reply purpose
    @new_message = Message.new
    # prepopulate body for error handling
    @new_message.body = session[:message_body] if session[:message_body]
    Message::ASSETS_MAXIMUM.times { @new_message.assets.build }
  end

  #post message
  def create
    @receiver = User.find(params[:receiver_id])
    @message = @receiver.received_messages.new(params[:message])
    @message.sender = current_user

    # record sender IP
    @message.sender_ip = request.ip

    if @message.save
      session[:message_body] = nil
      flash[:notice] = t('message.create.success')
      redirect_to user_message_path(:user_id => current_user, :id => @message, :anchor => @message.internal_link_id)
    else
      @curr = current_user
      @user = @receiver
      session[:message_body] = @message.body

      # if there is form error, upload picture will be gone so lets clean the assets and rebuild
      flash.now[:alert] = t('general.reupload_picture') unless @message.assets.blank?
      @message.assets.clear
      Message::ASSETS_MAXIMUM.times { @message.assets.build }

      if @message.reply_to_id.nil?
        put_model_errors_to_flash(@message.errors)
        render :action => "new"
      else
        put_model_errors_to_flash(@message.errors, 'redirect')
        redirect_to user_message_path(:user_id => current_user, :id => @message.reply_to_id, :anchor => Message::REPLY_INTERNAL_ID)
      end
    end
  end

  def sent
    @sents = current_user.sent_messages.order_by_created_at_descending.paginate(:per_page => 5, :page => params[:page])
  end
end
