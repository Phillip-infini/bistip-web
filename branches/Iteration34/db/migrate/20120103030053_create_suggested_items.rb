class CreateSuggestedItems < ActiveRecord::Migration
  def self.up
    create_table :suggested_items do |t|
      t.string :name, :null => false
      t.integer :origin_city_id, :references => :cities, :null => false
      t.belongs_to :user
      t.text :reason
      t.text :notes
      t.column :deleted, :boolean, :default => true
      t.timestamps
    end
  end

  def self.down
    drop_table :suggested_items
  end
end
