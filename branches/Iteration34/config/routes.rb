BistipWeb::Application.routes.draw do

  # Paypal IPN recevier
  resources :paypal_notifications, :only => [:create]
  
  resources :escrows, :only => [:new, :create] do
    collection do
      get 'calculate_fee'
      get 'redirect_paypal'
      get 'redirect_indonesia_bank_transfer'
      post 'seller_got_item'
      post 'buyer_received_item'
      post 'buyer_cancelled'
      post 'seller_requested_payout'
      post 'seller_cancelled'
      post 'buyer_resumed'
      post 'buyer_requested_payout'
      post 'buyer_received_payout'
      post 'seller_received_payout'
    end
  end

  resources :forums, :only => [:index, :show] do
    resources :topics do
      resources :posts do
        member do
          get 'quote'
          post 'create_quote'
        end
      end
    end
  end

  resources :seeks, :except => [:index] do
    resources :seek_comments, :as => "comments", :path => "comments"
    member do
      get 'match'
    end
    collection do
      get 'new_anywhere'
    end
  end

  resources :trips, :except => [:index] do
    resources :trip_comments, :as => "comments", :path => "comments"
    member do
      get 'match'
      get 'suggestions'
    end
    collection do
      get 'new_routine'
      post 'create_multiple'
    end
  end
  
  # user route
  resources :users, :except => [:index, :destroy] do
    # keep reputations path for legacy
    resources :positive_reputations
    resources :negative_reputations
    # new reviews path
    resources :reviews, :except => [:index, :destroy]

    # payout methods
    resources :payout_methods

    resources :notifications
    resources :user_configuration, :as => "configuration", :path => "configuration"
    resources :relations, :as => 'social', :path => 'social', :only => [:index, :new] do
      collection do
        get 'fb_mutual_friends'
      end
    end
    resources :messages do
      collection do
        get 'sent'
      end
    end
    member do
      get 'validate'
      get 'not_validated'
      get 'resend_validation'
      get 'trips'
      get 'seeks'
      # keep reputations path for legacy
      get 'reputation'
      get 'reviews'
    end
    collection do
      get 'registered'
    end
  end

  # alias for users path
  match '/join' => "users#new", :as => "join"
  match '/users/auth' => "users#auth", :as=> "auth"

  # receive email test
  match '/web_email_hook' => 'receive_email#post', :as => "receive_email_post"
  
  # home route
  root :to => "home#index"

  # contact us
  resource :contact_us, :only => [:new, :create]

  # insurances
  resources :insurances, :only => [:new, :create, :index]

  # story
  resources :stories, :only => [:new, :create, :index, :edit, :update]

  # suggested item
  resources :suggested_items, :path => 'items', :only => [:new, :create, :edit, :update, :show] do
    resources :suggested_item_votes, :as => 'vote', :path => 'like', :only => [:new]
    collection do
      get 'approve'
    end
  end
  
  # suggested item picture
  resources :suggested_item_pictures, :path => 'suggested_item_pictures', :only => [:show]
  
  # session route
  resource :session
  
  match '/login' => "sessions#new", :as => "login"
  match '/logout' => "sessions#destroy", :as => "logout"

  # path to calculate escrow amount after cost
  match '/escrow_amount_after_cost' => "escrows#calculate_amount_after_cost", :as => 'escrow_amount_after_cost'

  # search trip route
  match '/search_traveler' => 'search_trip#index', :as => "search_trip"
  match '/search_bistiper' => 'search_trip#index'

  # search seek route
  match '/search_request_post' => 'search_seek#index', :as => "search_seek"
  match '/search_wanted_bistiper' => 'search_seek#index'

  # search user route
  match '/search_user' => 'search_user#index', :as => "search_user"

  # search item route
  match '/search_item' => 'search_item#index', :as => "search_item"

  
  # password route
  match '/passwords/reset' => "passwords#reset", :as => "reset_password"
  match '/passwords/send_email' => "passwords#send_email", :as => "send_email_password"
  match '/passwords/reset_edit' => "passwords#reset_edit", :as => "reset_edit_password"
  match '/passwords/reset_update' => "passwords#reset_update", :as => "reset_update_password"
  match '/passwords/edit' => "passwords#edit", :as => "edit_password"
  match '/passwords/update' => "passwords#update", :as => "update_password"
  
  # cities route
  match '/cities/index' => 'cities#index', :as => 'cities_index'
  match '/cities/search' => 'cities#search', :as => 'cities_search'

  # languages route
  match '/languages/index' => 'languages#index', :as => 'languages_index'
  
  # comments trip route
  match '/trips' => 'trips#show', :as => 'trips_show'
  
  # comments seek route
  match '/seeks' => 'seeks#show', :as => 'seeks_show'

  # content
  match '/terms_of_service' => 'content#terms_of_service', :as => 'terms_of_service'
  match '/privacy_policy' => 'content#privacy_policy', :as => 'privacy_policy'
  match '/guide' => 'content#guide', :as => 'guide'
  match '/guide_all' => 'content#guide_all', :as => 'guide_all'
  match '/guide_traveler' => 'content#guide_traveler', :as => 'guide_traveler'
  match '/guide_requester' => 'content#guide_requester', :as => 'guide_requester'
  match '/about' => 'content#about', :as => 'about'
  match '/how' => 'content#how', :as => 'how'
  match '/faq' => 'content#faq', :as => 'faq'
  match '/new' => 'content#new', :as => 'new'
  match '/media' => 'content#media', :as => 'media'
  match '/event_bid_the_trip' => 'content#event_bid_the_trip', :as => 'event_bid_the_trip'
  match '/event_bistipes' => 'content#event_bistipes', :as => 'event_bistipes'
  match '/event_bistipes_2' => 'content#event_bistipes_2', :as => 'event_bistipes_2'
  match '/event_bistipes_3' => 'content#event_bistipes_3', :as => 'event_bistipes_3'
  match '/event_slogan' => 'content#event_slogan', :as => 'event_slogan'
  match '/event_blog' => 'content#event_blog', :as => 'event_blog'
  match '/jobs' => 'content#jobs', :as => 'jobs'
  match '/safepay' => 'content#safepay', :as => 'safepay'
  match '/awards' => 'content#awards', :as => 'awards'
  match '/bistip_point' => 'content#bistip_point', :as => 'bistip_point'

  # fail authentication
  match '/auth/failure', :to => 'sessions#failure'

  # match callback :provider
  match '/auth/:provider/callback', :to => 'sessions#create', :as => 'social_login'

  # errors
  match '/error_not_found' => 'errors#render_error', :as => 'error_not_found'

  # profile vanity url
  match '/u/:username', :controller => 'users', :action => 'show', :as => 'profile'

  # resend email validation
  match '/resend_email_validation' => 'resend_email_validation#index', :as => 'resend_email_validation'

  # trending item lookup
  match '/trending_item' => 'trending_item_lookup#show', :as => "trending_item_lookup"

  # expire cloud cache
  match '/expire_cloud_cache' => "home#expire_cloud", :as => "expire_cloud"

  # send statistics
  match '/send_statistics' => "home#send_statistics", :as => "send_statistics"

  # admin tools to escrow log
  match '/create_released_seller_payout_log' => 'home#create_released_seller_payout_log'
  match '/create_released_buyer_payout_log' => 'home#create_released_buyer_payout_log'
  match '/create_received_buyer_bank_transfer_log' => 'home#create_received_buyer_bank_transfer_log'
  match '/create_bistip_approved_insurance_log' => 'home#create_bistip_approved_insurance_log'
  match '/create_bistip_rejected_insurance_log' => 'home#create_bistip_rejected_insurance_log'
  match '/create_bistip_received_buyer_bank_transfer_insurance_log' => 'home#create_bistip_received_buyer_bank_transfer_insurance_log'
  match '/grant_trusted_badge' => 'home#grant_trusted_badge'
  match '/stats' => 'home#stats'

  # admin tools for bistip points log
  match '/claim_escrow_cashback' => 'point_logs#claim_escrow_cashback', :as => 'claim_escrow_cashback'

  # influencer log
  match '/i/:code' => 'influencer_log#transit', :as => 'influence'
  match '/in/:code', :controller => 'influencer_log', :action => 'create', :as => 'insert'
  match '/top_bistip_point' => "influencer_log#index"
  match '/bistip_point' => "influencer_log#bistip_point"

  # recommend link
  match '/recommend/:username' => 'recommend#handle', :as => 'recommend'
  match '/get_friends_recommendation' => 'recommend#get_recommendation', :as => 'get_recommendation'

  # dashboard
  match '/dashboard' => "dashboard#index"
  
  # keep reputations path for legacy
  match '/dashboard/positive_reputations' => 'dashboard#reviews'
  match '/dashboard/negative_reputations' => 'dashboard#reviews'
  match '/dashboard/reputation' => 'dashboard#reviews'
  match '/dashboard/reviews' => 'dashboard#reviews'
  match '/dashboard/trips' => 'dashboard#trips'
  match '/dashboard/seeks' => 'dashboard#seeks'
  match '/dashboard/stories' => 'dashboard#stories'
  match '/dashboard/suggested_items' => 'dashboard#suggested_items'
  match '/dashboard/profile' => 'dashboard#profile'
  match '/dashboard/transactions' => 'dashboard#escrows'
  match '/dashboard/points' => 'dashboard#points'

  # service oriented path
  match '/api' => 'api#index'
  match '/api/v1/trips' => 'api#trip_by_city'
  match '/api/v1/seeks' => 'api#seek_by_city'
  # match '/api/v1/people/:keyword' => 'api#user_by_name_or_username'

  # view message attachment
  match '/attachments' => 'assets#show'

  # view story picture
  match '/story_pictures' => 'story_pictures#show'

  # view suggested item picture
  match '/suggested_item_pictures' => 'suggested_item_pictures#show'

  # match approve story
  match '/create_bistip_approved_story' => 'stories#create_bistip_approved_story'

  # fb graph path
  match '/facebook_mutual_friends' => "facebook#mutual_friends", :as => 'facebook_mutual_friends'
  match '/facebook_mutual_friends_count' => "facebook#mutual_friends_count", :as => 'facebook_mutual_friends_count'
  
  # match everything else and route to render error
  match '*a', :to => 'errors#render_error', :constraints => lambda{|request| !request.path.starts_with?("/admin")}

  # DO NOT PUT ANYTHING AFTER HERE!!!!




  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
  #match '/trip_comments/destroy' => 'trip_comments#destroy', :as => 'trip_comments_destroy'
end
