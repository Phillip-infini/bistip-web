require 'test_helper'

class CitiesControllerTest < ActionController::TestCase

  test "should get city match" do
    get :index, :format => 'js', :term => 'jakarta'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:result)
    assert_equal 1, assigns(:result).size
  end

  test "should get city match + route" do
    get :search, :format => 'js', :term => 'jakarta'
    assert_response :success
    assert_template 'search'
    assert_not_nil assigns(:result)
    assert_equal 6, assigns(:result).size
  end

  test "should get all city match + route" do
    get :index, :format => 'js', :term => 'indonesia'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:result)
    assert_equal 5, assigns(:result).size
  end
  
end
