require 'test_helper'

class AssetsControllerTest < ActionController::TestCase

  test "should show asset" do
    login_as(:dono)
    get :show, :message_id => messages(:donotomac).id, :asset_id => assets(:one).id
    assert_response :success
    assert_not_nil assigns(:asset)

    login_as(:mactavish)
    get :show, :message_id => messages(:donotomac).id, :asset_id => assets(:one).id
    assert_response :success
    assert_not_nil assigns(:asset)
  end

  test "should not show asset" do
    login_as(:one)
    get :show, :message_id => messages(:donotomac).id, :asset_id => assets(:one).id
    assert_response :success
    assert_nil assigns(:asset)
  end
end
