class ExcludeWord < ActiveRecord::Base
   validates :word, :uniqueness => true

  def self.generate_all_in_array
    exclude_words = ExcludeWord.all
    result = []
    exclude_words.each do |exclude_word|
      result << exclude_word.word
    end
    result
  end
end
