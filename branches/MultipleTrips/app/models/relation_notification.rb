class RelationNotification < Notification

  #overide to_link method to perform specified action of SeekComment notification
  def to_link
    link = I18n.t("notification.title.relation", :sender => link_to(sender.username, path_helper.profile_path(sender.username)))
    return link

  end
end
