require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "creating a new message" do
    message = Message.new
    message.sender=users(:one)
    message.receiver=users(:mactavish)
    message.body='new message from one to mac'
    message.subject='one2mac'
    assert message.save
  end

  test "message to myself" do
    message = Message.new
    message.sender=users(:dono)
    message.receiver=users(:dono)
    message.body='new message from dono'
    message.subject='dono ke dono'
    assert message.save
  end

  test "message to user with value space" do
    message = Message.new
    message.sender=users(:mactavish)
    message.receiver=users(:dono)
    message.body='     '
    message.subject='     '
    assert !message.save
  end

end
