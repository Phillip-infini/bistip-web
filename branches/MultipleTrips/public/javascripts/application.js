// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

function buildSearchTripURL(mobile)
{
    var url = "/search_bistiper?";
    
    var origin = escape($("#origin").val());
    if (origin)
    {
        url += "&origin=" + escape($("#origin").val());
    }

    var destination = escape($("#destination").val());
    if (destination)
    {
        url += "&destination=" + escape($("#destination").val());
    }

    if (!mobile)
    {
        var departure_date = escape($("#departure_date").val());

        if (departure_date)
        {
            url += "&departure_date=" + departure_date;
        }

        var departure_date_predicate = escape($("input[name='departure_date_predicate']:checked").val());
        if (departure_date_predicate && departure_date_predicate != 'undefined')
        {
            url += "&departure_date_predicate=" + departure_date_predicate;
        }

        var keyword = escape($("#keyword").val());
        if (keyword)
        {
            url += "&keyword=" + keyword;
        }
    }

    window.location.replace(url);
    return false;
}

function buildSearchSeekURL(mobile)
{
    var url = "/search_wanted_bistiper?";

    var origin = escape($("#origin").val());
    if (origin)
    {
        url += "&origin=" + origin;
    }

    var destination = escape($("#destination").val());
    if (destination)
    {
        url += "&destination=" + destination;
    }
    
    if (!mobile)
    {
        var keyword = escape($("#keyword").val());
        if (keyword)
        {
            url += "&keyword=" + keyword;
        }
    }

    window.location.replace(url);
    return false;
}

// function to remove dynamicaly an item form fields
function remove_fields_mobile(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".fields").hide();
}

// function to remove dynamicaly an item form fields
function remove_fields(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".itemsRow").hide();
}

// function to remove other field trip
function remove_other_trip_field(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".other_trip").remove();
    $(".add_more_trips").show();
}

// function to add dynamicaly an item form fields
function add_fields_mobile(link, association, content, maximum) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g")
    $(link).parent().parent().before(content.replace(regexp, new_id));
    var now_fields = $(".fields").length
    if (now_fields >= maximum)
    {
        $(link).parent().parent().remove();
    }
}

// function to add dynamicaly an item form fields
function add_fields(link, association, content, maximum) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g")
    $(link).parent().before(content.replace(regexp, new_id));
    var now_fields = $(".itemsRow").length
    if (now_fields >= maximum)
    {
        $(link).parent().remove();
    }
    return false;
}

// function to add dynamically an other trip form fields
function add_other_trip_fields(link, content, maximum){
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_trip", "g")
    var now_fields = $(".other_trip").length
    if(now_fields==0){
        $(".add_more_trips").prev().children().children().children().last().css("background","red");
    }else{
        $(".add_more_trips").prev().children().children().children().first().css("background","red");
    }
    $(".other_trip").children().css("background","green");
//    $("#other_trips_new_trip_origin_location").css("background", "green");
    $(link).parent().before(content.replace(regexp, new_id));
    
    if (now_fields >= maximum)
    {
        $(link).parent().hide();
    }
    return false;
}

// function handle from anywhere
function handle_from_anywhere(link)
{
    if (link.checked == true)
    {
        $("#seek_origin_location").val('<anywhere>');
        $("#seek_origin_location").attr('readonly', true);
        $("#seek_origin_location").attr('disabled', true);
    }
    else
    {
        $("#seek_origin_location").val('');
        $("#seek_origin_location").attr('readonly', false);
        $("#seek_origin_location").attr('disabled', false);
    }
    return true;
}

// function to handle enter key on search trip
function search_trip_field_key_up(e)
{
    if (e.keyCode == 13){
        buildSearchTripURL();
    }
}

// function to handle enter key on search trip
function search_seek_field_key_up(e)
{
    if (e.keyCode == 13){
        buildSearchSeekURL();
    }
}

// redirect to url function
function redirect(url)
{
    window.location = url
}

// function to fill origin location with previous destination location
function fill_origin(id_current_field, id_previous_field){
    var now_fields = $(".other_trip").length
    if (now_fields == 0)
    {
      source = $("#trip_destination_location").text()
    }else{
      source = $(id_previous_field).text()
    }
    $(id_current_field).text(source)
}