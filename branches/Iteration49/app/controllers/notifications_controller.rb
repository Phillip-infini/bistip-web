class NotificationsController < ApplicationController
  before_filter :authenticate, :only => [:index]

  #show list of notification that receive by current_user
  def index
    @notifications = current_user.received_notifications.paginate(:per_page => Notification::DEFAULT_PER_PAGE, :page => params[:page])
    @notifications.each do |d|
      d.update_attribute(:read, true)
    end
  end
end
