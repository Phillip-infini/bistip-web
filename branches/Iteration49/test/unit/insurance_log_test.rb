require 'test_helper'

class InsuranceLogsTest < ActiveSupport::TestCase

  test "should create escrow log and test all the methods" do
    escrow = insurances(:donotomac)
    buyer = escrow.buyer
    seller = escrow.seller
    insurance_log = nil

    assert_difference('InsuranceLogNotification.count') do
      assert insurance_log = BuyerAppliedInsuranceLog.create!(:insurance_id => escrow.id)
    end
    assert_nothing_raised {
      assert insurance_log.to_display_string
      assert insurance_log.get_awaiting_string
      assert insurance_log.to_notification_link(seller)
    }
    
    assert_difference('InsuranceLogNotification.count', 2) do
      assert insurance_log = BistipApprovedInsuranceLog.create!(:insurance_id => escrow.id)
    end
    assert_nothing_raised {
      assert insurance_log.to_display_string
      assert insurance_log.get_awaiting_string
      assert insurance_log.to_notification_link(seller)
      assert insurance_log.to_notification_link(buyer)
    }
    
    assert_difference('InsuranceLogNotification.count', 2) do
      assert insurance_log = BistipRejectedInsuranceLog.create!(:insurance_id => escrow.id)
    end
    assert_nothing_raised {
      assert insurance_log.to_display_string
      assert insurance_log.get_awaiting_string
      assert insurance_log.to_notification_link(seller)
      assert insurance_log.to_notification_link(buyer)
    }

    assert_difference('InsuranceLogNotification.count', 2) do
      assert insurance_log = BistipReceivedBuyerBankTransferInsuranceLog.create!(:insurance_id => escrow.id)
    end
    assert_nothing_raised {
      assert insurance_log.to_display_string
      assert insurance_log.get_awaiting_string
      assert insurance_log.to_notification_link(seller)
      assert insurance_log.to_notification_link(buyer)
    }

  end

end
