class CreateGenders < ActiveRecord::Migration
  def self.up
    create_table :genders do |t|
      t.string :name
      t.timestamps
    end

    Gender.new(:name => 'Male').save
    Gender.new(:name => 'Female').save
    Gender.new(:name => 'Other').save
  end

  def self.down
    drop_table :genders
  end
end
