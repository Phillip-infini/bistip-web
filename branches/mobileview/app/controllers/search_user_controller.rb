class SearchUserController < ApplicationController
  def index
    keyword = params[:keyword]
    if keyword != nil and !keyword.empty?
      @users = User.where("username like :keyword or fullname like :keyword", {:keyword=>"%#{keyword}%"}).paginate(:page=>params[:page], :per_page => 5)
    else
      @users = Array.new
    end
  end
end
