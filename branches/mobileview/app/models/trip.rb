# Trip class represent a trip or travel plan
class Trip < ActiveRecord::Base
  include Location

  # constants
  NOTES_MAXIMUM_LENGTH = 300

  # scope
  default_scope lambda {{ :conditions => ["departure_date >= ? and deleted = ?", DateTime.current.beginning_of_day, false], :include => [:origin_city, :destination_city] }}
  scope :origin_city_id, lambda { |origin_city_id| {:conditions => ["origin_city_id = ?", origin_city_id]}}
  scope :destination_city_id, lambda { |destination_city_id| {:conditions => ["destination_city_id = ?", destination_city_id]}}
  scope :origin_country_id, lambda { |country_id| { :conditions => ["cities.country_id = ?", country_id]}}
  scope :destination_country_id, lambda { |country_id| {:conditions => ["destination_cities_trips.country_id = ?", country_id]}}
  scope :departure_date_after, lambda { |departure_date| {:conditions => ["departure_date >= ?", departure_date]}}
  scope :departure_date_before, lambda { |departure_date| {:conditions => ["departure_date <= ?", departure_date]}}
  scope :notes_contains, lambda { |keyword| {:conditions => ["match(notes) against(?)", keyword]}}
  scope :user_is_not, lambda { |user_id| {:conditions => ["user_id != ?", user_id]}}
  scope :user_is, lambda { |user_id| {:conditions => ["user_id = ?", user_id]}}
  scope :random, lambda { |*args| {:order => 'RAND()', :limit => args[0] || 1 } }

  # validation
  validates :origin_location, :presence => true

  validates :destination_location, :presence => true

  validates :departure_date, :presence => true, :future_date => true

  validates :notes, :length => { :maximum => NOTES_MAXIMUM_LENGTH }

  validates :arrival_date, :arrival_time => true

  # relationship
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'
  belongs_to :destination_city, :class_name => "City", :foreign_key => 'destination_city_id'
  belongs_to :user
  has_many :comments, :class_name => "TripComment"

  # event
  after_create :send_match_notification

  # send email to owner of matching seek
  def send_match_notification
    matching_seeks = find_matching_seeks
    matching_seeks.each do |seek|
      if seek.user.user_configuration.email_match_trip
        Notifier.email_matching_seek(self, seek).deliver
      end
    end
  end
  handle_asynchronously :send_match_notification

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  # check is a seek still active
  def active?
    self.departure_date >= DateTime.current.beginning_of_day and self.deleted == false
  end

  # find match in seek table
  def find_matching_seeks
    if self.active?
      scope = Seek.scoped({})

      # include also the all cities of the origin city country
      origin_all_cities_id = City.find_by_country_id_and_name(self.origin_city.country.id, City::ALL_CITIES)
      scope = scope.origin_city_or_all_cities_id self.origin_city_id, origin_all_cities_id
      destination_all_cities_id = City.find_by_country_id_and_name(self.destination_city.country.id, City::ALL_CITIES)
      scope = scope.destination_city_or_all_cities_id self.destination_city_id, destination_all_cities_id
      scope = scope.user_is_not self.user_id
      scope
    else
      Array.new
    end
  end

  def to_s
    I18n.t('trip.short', :from => self.origin_location, :to => self.destination_location)
  end
  
  protected
    def validate_departure_and_arrival
      @errors.add(:arrival_date,"arrival date harus >= dari departure date") if self.departure_date > self.arrival_date
    end
end