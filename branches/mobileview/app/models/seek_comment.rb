# sub class of comment that represent Comment for Seek
class SeekComment < Comment

  def notify_other_seek_comment
    seek = self.seek
    comments = seek.comments
    already_sent = []
    comments.each do |comment|
      # only send comment notification if the previous comment owner
      # is not the owner of the seek and not the owner of the new comment
      commentator = comment.user
      if commentator.user_configuration.email_other_seek_comment and
          !already_sent.include?(commentator) and
          !seek.owner?(commentator) and
          !self.owner?(commentator)
        Notifier.email_other_seek_comment(self, commentator, seek).deliver
        already_sent << commentator
      end
    end
  end
  handle_asynchronously :notify_other_seek_comment
end
