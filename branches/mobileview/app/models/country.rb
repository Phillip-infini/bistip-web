# Country class represent a Country
class Country < ActiveRecord::Base

  # relationships
  has_many :cities
end
