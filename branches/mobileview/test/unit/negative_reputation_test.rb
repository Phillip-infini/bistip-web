require 'test_helper'

class NegativeReputationTest < ActiveSupport::TestCase

  test "user X should give a positive reputation to user Y" do
    userx = users(:dono)
    usery = users(:one)
    negative = usery.received_negative_reputations.new
    negative.giver = userx
    negative.body = "poor quality of service"
    assert negative.save
  end
end
