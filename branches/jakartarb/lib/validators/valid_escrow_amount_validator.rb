# validator class to ensure that a date is a today's date or future date

class ValidEscrowAmountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !value.nil? and (value < 20 or value > 300)
      record.errors[attribute] << (options[:message] || I18n.t('escrow.errors.custom.valid_amount'))
    end
  end
end