require 'test_helper'

class GenderTest < ActiveSupport::TestCase

  test "get all gender" do
    assert Gender.all.size == 3
  end
end
