class AddBirthYearToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :birth_year, :integer
  end

  def self.down
    remove_column :users, :birth_year
  end
end
