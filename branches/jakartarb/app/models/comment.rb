# Comment class represent a comment from a user to a trip or seek
class Comment < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 600
  PER_PAGE = 5

  # scope
  default_scope :order => 'created_at ASC', :conditions => ["deleted = ?",false]
  scope :body_contains, lambda { |keyword| {:conditions => ["match(body) against(?)", keyword]}}
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ? and deleted = ?", 60.days.ago, false]}}

  # relationship
  belongs_to :seek
  belongs_to :trip
  belongs_to :user

  # event
  after_create :notify_other

  # validation
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH },
    :presence => true

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  #fire email and live notification based on comment type
  def notify_other
    if !self.trip_id.nil?
      self.notify_other_trip_comment
      self.live_notification_trip_comment
    elsif !self.seek_id.nil?
      self.notify_other_seek_comment
      self.live_notification_seek_comment
    end
  end

  #this method should be call from instance of Comment subclass or will be raise NotImplemented error
  def get_path
    raise NotImplementedError
  end

  #helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end

end
