class BuyerReceivedItemLog < EscrowLog

  def create_notification
    escrow = self.escrow
    EscrowBuyerReceivedItemNotification.create!(:sender => escrow.buyer, :receiver => escrow.seller, :data_id => escrow.id)
    Notifier.email_buyer_received_item
  end
end
