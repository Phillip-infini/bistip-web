# class to handle operations for escrow account
class EscrowsController < ApplicationController
  
  ssl_required :new,
    :create,
    :redirect_paypal,
    :seller_got_item,
    :buyer_received_item,
    :seller_cancelled,
    :seller_requested_payout,
    :buyer_resumed,
    :buyer_requested_payout,
    :seller_received_payout,
    :buyer_received_payout
  ssl_allowed :calculate_amount_after_cost
  before_filter :authenticate, :only => [:new, 
    :create,
    :redirect_paypal,
    :seller_got_item,
    :buyer_received_item,
    :seller_cancelled,
    :seller_requested_payout,
    :buyer_resumed,
    :buyer_requested_payout,
    :seller_received_payout,
    :buyer_received_payout]
  before_filter :load_escrow, :except => [:new, :create, :calculate_amount_after_cost]
  after_filter :create_notice, :except => [:new, :create, :calculate_amount_after_cost]
  
  # new escrow form
  def new
    # escrow must have seller and message reference
    seller_id = params[:seller_id]
    message_id = params[:message_id]
    session[:escrow_params] ||= {}

    if seller_id and message_id
      # build new escrow object
      message = Message.find(message_id)

      # check if message already has a active escrow
      unless message.escrow.blank?
        redirect_to :back
      else
        session[:escrow_params] = {}
        session[:escrow_step] = nil
        @escrow = Escrow.new
        @escrow.buyer = current_user
        @escrow.seller = User.find(seller_id)
        @escrow.current_step = session[:escrow_step]
        @escrow.message = message
        @escrow.currency = Escrow::DEFAULT_CURRENCY
      end
    elsif session[:escrow_params]
      # in the middle of wizard, build escrow from parameter
      @escrow = Escrow.new(session[:escrow_params])
      @escrow.current_step = session[:escrow_step]
    else
      # not valid, redirect back then
      redirect_to :back
    end
  end

  # create escrow, handle wizard form submission of escrow
  def create

    # build the escrow data
    session[:escrow_params].deep_merge!(params[:escrow]) if params[:escrow]
    @escrow = Escrow.new(session[:escrow_params])
    @escrow.current_step = session[:escrow_step]

    # is escrow valid for the current step?
    if @escrow.valid?
      if params[:back_button]
        @escrow.previous_step
      elsif @escrow.last_step?
        # escrow is valid, save the escrow and create the initiate log
        @escrow.save if @escrow.all_valid?
        BuyerInitiatedLog.create!(:escrow_id => @escrow.id)
      else
        @escrow.next_step
      end
      session[:escrow_step] = @escrow.current_step
    else
      put_model_errors_to_flash(@escrow.errors)
    end

    # if escrow still have not been saved yet, that means we still need to render new form
    if @escrow.new_record?
      render "new"
    else
      # escrow initiate done, redirect to paypal
      session[:escrow_step] = session[:escrow_params] = nil
      redirect_to redirect_paypal_escrows_path(:escrow_id => @escrow)
    end
  end

  # return the amount after cost
  def calculate_amount_after_cost
    @amount = params[:amount]
  end
  
  #create seller_got_item log when escrow's current state is paypal_notify
  def seller_got_item
    if current_user == @escrow.seller and 
        (@escrow.current_state.eql?(Escrow::STATE_PAYPAL_NOTIFIED) or @escrow.current_state.eql?(Escrow::STATE_BUYER_RESUMED))
      SellerGotItemLog.create!(:escrow_id => params[:escrow_id]) 
      redirect_to user_message_path(current_user, @escrow.message.id)
    else
      redirect_to root_path
    end
  end
  
  #create buyer_received_item log when escrow's current state is seller_got_item
  def buyer_received_item
    if current_user == @escrow.buyer and @escrow.current_state.eql?(Escrow::STATE_SELLER_GOT_ITEM)
      BuyerReceivedItemLog.create!(:escrow_id => params[:escrow_id]) 
      redirect_to user_message_path(current_user, @escrow.message.id)
    else
      redirect_to root_path
    end
  end
  
  #create seller_cancel log when escrow's current state is paypal_notify
  def seller_cancelled
    if current_user == @escrow.seller and @escrow.current_state.eql?(Escrow::STATE_PAYPAL_NOTIFIED)
      SellerCancelledLog.create!(:escrow_id => params[:escrow_id])
      redirect_to user_message_path(current_user, @escrow.message.id)
    else
      redirect_to root_path
    end
  end
  
  #create seller_request_payout log when escrow's current state is buyer_received_item
  def seller_requested_payout
    if current_user == @escrow.seller and @escrow.current_state.eql?(Escrow::STATE_BUYER_RECEIVED_ITEM)
      SellerRequestedPayoutLog.create!(:escrow_id => params[:escrow_id])
      redirect_to user_message_path(current_user, @escrow.message.id)
    else
      redirect_to root_path
    end
  end

  #create seller_received_payout log when escrow's current state is bistip_released_seller_payout
  def seller_received_payout
    if current_user == @escrow.seller and @escrow.current_state.eql?(Escrow::STATE_BISTIP_RELEASED_SELLER_PAYOUT)
      SellerReceivedPayoutLog.create!(:escrow_id => params[:escrow_id])
      redirect_to user_message_path(current_user, @escrow.message.id)
    else
      redirect_to root_path
    end
  end
  
  #create buyer_resume log when escrow's current state is seller_cancel
  def buyer_resumed
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_SELLER_CANCELLED
      BuyerResumedLog.create!(:escrow_id => params[:escrow_id])
      redirect_to user_message_path(current_user, @escrow.message.id)
    else
      redirect_to root_path
    end
  end
  
  #create buyer_request_payout log when escrow's current state is seller_cancel
  def buyer_requested_payout
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_SELLER_CANCELLED
      BuyerRequestedPayoutLog.create!(:escrow_id => params[:escrow_id])
      redirect_to user_message_path(current_user, @escrow.message.id)
    else
      redirect_to root_path
    end
  end

  #create buyer_received_payment log when escrow's current state is bistip_released_buyer_payout
  def buyer_received_payout
    if current_user == @escrow.buyer and @escrow.current_state.eql? Escrow::STATE_BISTIP_RELEASED_BUYER_PAYOUT
      BuyerReceivedPayoutLog.create!(:escrow_id => params[:escrow_id])
      redirect_to user_message_path(current_user, @escrow.message.id)
    else
      redirect_to root_path
    end
  end

  private
    def load_escrow
      @escrow = Escrow.find(params[:escrow_id])
    end

    def create_notice
      unless @escrow.blank?
        flash[:escrow] = t("escrow.message.success.#{@escrow.current_state}", :buyer => @escrow.buyer.username, :seller => @escrow.seller.username)
      end
    end
end
