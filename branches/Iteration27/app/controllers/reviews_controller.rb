# controller to handle reviews related option
class ReviewsController < ApplicationController
  before_filter :authenticate, :not_self, :only => [:new, :create]

  # render new review
  def new
    @user = User.find(params[:user_id])
    @review = review_type.new
    
    if Review.last_7_days(current_user.id, @user.id).count >= 1
      flash[:alert] = t('review.new.not_allowed')
      redirect_to profile_path(@user.username)
    end

    if params[:invitation]
      flash.now[:notice] = t('review.new.invitation', :receiver => @user.username)
    end
  end

  # create a review
  def create
    @user = User.find(params[:user_id])
    @review = review_type.new(params[:review])
    @review.giver = current_user
    @review.receiver = @user

    if Review.last_7_days(current_user.id, @user.id).count >= 1
      flash[:alert] = t('review.new.not_allowed')
      redirect_to profile_path(@user.username)
    else
      if @review.save
        flash[:notice] = t('review.create.success')
        redirect_to reviews_user_path(@user.id)
      else
        put_model_errors_to_flash(@review.errors)
        render :action => "new"
      end
    end
  end

  private
    # return the review type in form of class
    def review_type
      params[:type].blank? ? PositiveReview : params[:type].constantize
    end

end
