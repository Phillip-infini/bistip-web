class BalancesController < ApplicationController
  
  before_filter :authenticate, :only => [:claim_escrow_cashback_path]

  def claim_escrow_cashback_path
    if current_user.has_non_cashback_escrow?
      escrow_id = Escrow.non_cashback_escrow(current_user).first.id
      SubtractForEscrowCashbackLog.create!(:user_id => current_user.id, :amount => SubtractForEscrowCashbackLog::NEEDED_POINT, :escrow_id => escrow_id)
      flash[:notice] = t('point_logs.escrow_cashback.success')
    end
    redirect_to dashboard_path
  end

end
