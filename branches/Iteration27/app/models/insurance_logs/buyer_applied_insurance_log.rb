class BuyerAppliedInsuranceLog < InsuranceLog

  def broadcast
    super
    ContactUsNotifier.delay.email_insurance_log(insurance)
  end
  
end
