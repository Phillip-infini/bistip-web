class PointLog < ActiveRecord::Base
  belongs_to :balance
  belongs_to :currency

  SHARE_POINT = 1
  STORY_POINT = 30

  # event
  before_create :set_currency

  # this method is to do operation on balance amount
  def do_point(amount)
    raise NotImplementedError
  end

  private
    def set_currency
      self.currency = Currency.find_by_name('USD')
    end
end
