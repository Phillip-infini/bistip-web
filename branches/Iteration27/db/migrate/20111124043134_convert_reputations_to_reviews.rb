class ConvertReputationsToReviews < ActiveRecord::Migration
  
  def self.up
    PositiveReputation.all.each do |pr|
      pos_review = PositiveReview.new
      pos_review.giver = pr.giver
      pos_review.receiver = pr.receiver
      pos_review.body = pr.body
      pos_review.save
    end

    NegativeReputation.all.each do |nr|
      neg_review = NegativeReview.new
      neg_review.giver = nr.giver
      neg_review.receiver = nr.receiver
      neg_review.body = nr.body
      neg_review.save
    end
  end

  def self.down
    Review.destroy_all
  end
end
