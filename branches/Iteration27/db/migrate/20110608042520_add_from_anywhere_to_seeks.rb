class AddFromAnywhereToSeeks < ActiveRecord::Migration
  def self.up
    add_column :seeks, :from_anywhere, :boolean, :default => false
  end

  def self.down
    remove_column :seeks, :from_anywhere
  end
end
