class AddSenderIpToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :sender_ip, :string
  end

  def self.down
    remove_column :messages, :sender_ip
  end
end
