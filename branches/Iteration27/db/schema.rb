# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111128033148) do

  create_table "anchor_words", :force => true do |t|
    t.string   "word"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assets", :force => true do |t|
    t.integer  "message_id",              :null => false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "badges", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", :force => true do |t|
    t.string   "name"
    t.string   "region"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alias"
  end

  add_index "cities", ["id"], :name => "index_cities_on_id", :unique => true

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "trip_id"
    t.integer  "seek_id"
    t.boolean  "deleted",    :default => false
    t.text     "body"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["body"], :name => "fulltext_comments_body"

  create_table "countries", :force => true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alias"
  end

  add_index "countries", ["id"], :name => "index_countries_on_id", :unique => true

  create_table "currencies", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["locked_by"], :name => "delayed_jobs_locked_by"
  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "escrow_logs", :force => true do |t|
    t.integer  "escrow_id",  :null => false
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "escrows", :force => true do |t|
    t.integer  "buyer_id",                                         :null => false
    t.integer  "seller_id",                                        :null => false
    t.integer  "message_id",                                       :null => false
    t.decimal  "amount",            :precision => 15, :scale => 2
    t.string   "invoice"
    t.integer  "currency_id"
    t.string   "item"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "payment_method_id",                                :null => false
    t.integer  "buyer_review_id"
    t.integer  "seller_review_id"
  end

  create_table "exclude_words", :force => true do |t|
    t.string   "word"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "forums", :force => true do |t|
    t.string   "title"
    t.string   "email"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",     :default => false
  end

  create_table "genders", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "include_items", :force => true do |t|
    t.string   "word"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "influencer_logs", :force => true do |t|
    t.string   "ip_address",    :null => false
    t.integer  "trip_id",       :null => false
    t.integer  "influencer_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "insurance_logs", :force => true do |t|
    t.integer  "insurance_id", :null => false
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "insurances", :force => true do |t|
    t.integer  "buyer_id",                                   :null => false
    t.integer  "seller_id",                                  :null => false
    t.integer  "message_id",                                 :null => false
    t.decimal  "amount",      :precision => 15, :scale => 2
    t.decimal  "cover_value", :precision => 15, :scale => 2
    t.string   "item"
    t.integer  "currency_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", :force => true do |t|
    t.string   "name",        :null => false
    t.integer  "trip_id"
    t.integer  "seek_id"
    t.integer  "tip_unit_id"
    t.integer  "tip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "items", ["name"], :name => "fulltext_items_name"

  create_table "languages", :force => true do |t|
    t.string   "english_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "languages_users", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "language_id"
  end

  create_table "messages", :force => true do |t|
    t.integer  "receiver_id",                      :null => false
    t.integer  "sender_id",                        :null => false
    t.text     "body"
    t.boolean  "read",          :default => false
    t.string   "subject"
    t.integer  "reply_to_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sender_ip"
    t.integer  "trip_id"
    t.integer  "seek_id"
    t.string   "salt"
    t.boolean  "email_reply",   :default => false
    t.text     "body_original"
  end

  add_index "messages", ["body"], :name => "fulltext_messages_body"

  create_table "negative_reputations", :force => true do |t|
    t.integer  "giver_id",    :null => false
    t.integer  "receiver_id", :null => false
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", :force => true do |t|
    t.integer  "receiver_id",                    :null => false
    t.integer  "sender_id",                      :null => false
    t.integer  "data_id"
    t.string   "type"
    t.boolean  "read",        :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_methods", :force => true do |t|
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "paypal_notifications", :force => true do |t|
    t.text     "params"
    t.integer  "escrow_id"
    t.string   "status"
    t.string   "transaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "point_logs", :force => true do |t|
    t.integer  "user_id"
    t.string   "type"
    t.integer  "amount"
    t.integer  "data_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "positive_reputations", :force => true do |t|
    t.integer  "giver_id",                       :null => false
    t.integer  "receiver_id",                    :null => false
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "invitation",  :default => false
  end

  create_table "posts", :force => true do |t|
    t.text     "body"
    t.text     "quoted"
    t.integer  "user_id"
    t.integer  "topic_id"
    t.boolean  "deleted",        :default => false
    t.integer  "quoted_from_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "relations", :force => true do |t|
    t.integer  "from_id",    :null => false
    t.integer  "to_id",      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reviews", :force => true do |t|
    t.integer  "giver_id",                       :null => false
    t.integer  "receiver_id",                    :null => false
    t.text     "body"
    t.string   "type"
    t.boolean  "verified",    :default => false
    t.boolean  "invitation",  :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seeks", :force => true do |t|
    t.integer  "origin_city_id",                              :null => false
    t.integer  "destination_city_id",                         :null => false
    t.boolean  "deleted",                  :default => false
    t.integer  "user_id"
    t.string   "departure_date_predicate"
    t.datetime "departure_date"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "from_anywhere",            :default => false
    t.string   "salt"
  end

  add_index "seeks", ["id"], :name => "index_seeks_on_id", :unique => true
  add_index "seeks", ["notes"], :name => "fulltext_seeks_notes"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "stories", :force => true do |t|
    t.string   "title"
    t.integer  "user_id"
    t.text     "body"
    t.boolean  "deleted",    :default => true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "story_pictures", :force => true do |t|
    t.integer  "story_id",                :null => false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "testimonials", :force => true do |t|
    t.text     "body"
    t.string   "from"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tip_units", :force => true do |t|
    t.string  "name"
    t.string  "symbol"
    t.boolean "before_number", :default => true
  end

  create_table "topic_views", :force => true do |t|
    t.integer  "topic_id"
    t.integer  "count",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "topics", :force => true do |t|
    t.string   "title"
    t.integer  "forum_id"
    t.integer  "user_id"
    t.boolean  "deleted",    :default => false
    t.integer  "rank",       :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trending_items", :force => true do |t|
    t.string   "item"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trips", :force => true do |t|
    t.integer  "origin_city_id",                         :null => false
    t.integer  "destination_city_id",                    :null => false
    t.boolean  "deleted",             :default => false
    t.integer  "user_id"
    t.datetime "departure_date"
    t.datetime "arrival_date"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "period"
    t.string   "day"
    t.boolean  "routine",             :default => false
    t.integer  "previous_trip_id"
    t.string   "salt"
  end

  add_index "trips", ["id"], :name => "index_trips_on_id", :unique => true
  add_index "trips", ["notes"], :name => "fulltext_trips_notes"

  create_table "user_configurations", :force => true do |t|
    t.boolean  "email_receive_message",      :default => true
    t.boolean  "email_receive_trip_comment", :default => true
    t.boolean  "email_receive_seek_comment", :default => true
    t.boolean  "email_receive_review",       :default => true
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "email_match_trip",           :default => true
    t.boolean  "email_match_seek",           :default => true
    t.boolean  "email_other_trip_comment",   :default => true
    t.boolean  "email_other_seek_comment",   :default => true
    t.boolean  "email_topic_post",           :default => true
    t.boolean  "email_quote_post",           :default => true
  end

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "fullname"
    t.string   "email"
    t.string   "email_validation_key"
    t.string   "hashed_password"
    t.string   "reset_password_key"
    t.string   "location"
    t.string   "contact_number"
    t.string   "extra_contact_number"
    t.string   "web"
    t.string   "facebook"
    t.string   "twitter"
    t.text     "bio"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.integer  "times_reminded",       :default => 0
    t.integer  "city_id"
    t.integer  "gender_id"
    t.integer  "birth_year"
    t.string   "work"
    t.string   "salt"
    t.string   "ip"
  end

  add_index "users", ["id"], :name => "index_users_on_id", :unique => true

end
