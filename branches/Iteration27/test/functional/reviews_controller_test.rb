require 'test_helper'

class ReviewsControllerTest < ActionController::TestCase
  
  setup do
    @userx = users(:dono)
    @usery = users(:one)
  end

  test "should get new" do
    login_as(:dono)
    get :new, :user_id => @usery.id
    assert_response :success
  end

  test "userX should give a positive review to userY" do
    login_as(:dono)
    assert_difference('PositiveReview.count') do
      post :create, :user_id => @usery.id, :type => 'PositiveReview', :review => {
        :body =>"very good service"
      }
    end
    assert_redirected_to reviews_user_path(@usery.id)
  end

  test "userX should give a negative review to userY" do
    login_as(:dono)
    assert_difference('NegativeReview.count') do
      post :create, :user_id => @usery.id, :type => 'NegativeReview', :review => {
        :body =>"very good service"
      }
    end
    assert_redirected_to reviews_user_path(@usery.id)
  end
end
