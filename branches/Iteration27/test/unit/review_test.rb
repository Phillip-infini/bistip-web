require 'test_helper'

class ReviewTest < ActiveSupport::TestCase

  test "user X should give a positive review to user Y" do
    userx = users(:dono)
    usery = users(:one)
    positive = usery.received_positive_reviews.new
    positive.giver = userx
    positive.body = "good quality of service"
    assert positive.save
  end

  test "user X should give a negative review to user Y" do
    userx = users(:dono)
    usery = users(:one)
    negative = usery.received_negative_reviews.new
    negative.giver = userx
    negative.body = "poor quality of service"
    assert negative.save
  end
  
end
