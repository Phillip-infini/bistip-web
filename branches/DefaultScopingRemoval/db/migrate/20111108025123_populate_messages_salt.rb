class PopulateMessagesSalt < ActiveRecord::Migration
  def self.up
    Message.all.each do |message|
      message.generate_salt
      message.save
    end
  end

  def self.down
    Message.all.each do |message|
      message.update_attribute(:salt, nil)
    end
  end
end
