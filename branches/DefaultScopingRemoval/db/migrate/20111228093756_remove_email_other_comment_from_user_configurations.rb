class RemoveEmailOtherCommentFromUserConfigurations < ActiveRecord::Migration
  def self.up
    remove_column :user_configurations, :email_other_trip_comment
    remove_column :user_configurations, :email_other_seek_comment
  end

  def self.down
    add_column :user_configurations, :email_other_trip_comment, :boolean, :default => true
    add_column :user_configurations, :email_other_seek_comment, :boolean, :default => true
  end
end
