class AddReviewsToEscrows < ActiveRecord::Migration
  def self.up
    add_column :escrows, :buyer_review_id, :integer, :references => :reviews
    add_column :escrows, :seller_review_id, :integer, :references => :reviews
  end

  def self.down
    remove_column :escrows, :buyer_review_id
    remove_column :escrows, :seller_review_id
  end
end
