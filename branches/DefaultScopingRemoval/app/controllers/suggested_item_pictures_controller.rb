class SuggestedItemPicturesController < ApplicationController
  def show
     @suggested_item_picture = SuggestedItemPicture.find(params[:id])
  end
end
