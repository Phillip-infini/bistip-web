class DashboardController < ApplicationController
  before_filter :authenticate
  DEFAULT_PAGING = 10
  LIMIT_INCOMING_TRIPS = 5
  LIMIT_RANDOM_TRIPS = 8
  
  #GET index
  def index
    @user = current_user
    @incoming_trips = current_user.incoming_to_user_location(LIMIT_INCOMING_TRIPS)
    @incoming_trips_count = current_user.incoming_to_user_location_count
    @trips = Trip.build_scope_dashboard_index(LIMIT_RANDOM_TRIPS, current_user)
  end

  #GET current_user's review
  def reviews
    @user = current_user
    @reviews = @user.received_reviews
  end
  
  #GET current_user's trips
  def trips
    @user = current_user
    @trips = Trip.order('created_at desc').find_all_by_user_id(current_user.id).paginate(:page => params[:page], :per_page => Trip::DEFAULT_PER_PAGE)
  end

  #GET current_user's seeks
  def seeks
    @user = current_user
    @seeks = Seek.order('created_at desc').find_all_by_user_id(current_user.id).paginate(:page => params[:page], :per_page => Seek::DEFAULT_PER_PAGE)
  end

  #GET current_user's profile
  def profile
    @user = current_user
  end
  
  def escrows
    @user = current_user
    @escrows = Escrow.user_all_escrows(current_user.id)
  end
end
