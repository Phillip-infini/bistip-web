require 'digest'
require 'paperclip'
require 'nilify_blanks'

# User class represent membership
class User < ActiveRecord::Base
  include CheckLocation
  
  nilify_blanks :only => [:avatar]

  # constant
  VALIDATION_KEY_PARAM = "key"
  RESET_PASSWORD_KEY_PARAM = "key"
  EMAIL_KEY_PARAM = "email"
  WEB_PREFIX = "http://"
  FACEBOOK_PREFIX = "http://www.facebook.com/"
  TWITTER_PREFIX = "http://www.twitter.com/"
  BIO_MAXIMUM_LENGTH = 500
  WORK_MAXIMUM_LENGTH = 100
  DAYS_AGE_TO_BE_REMINDED = 7
  NO_VALIDATION_PERIOD = 3
  SEARCH_PER_PAGE = 10
  LANGUAGES_MAXIMUM = 5
  SAME_IP_REGISTRATION_24_HOUR = 50

  # constant for omini auth hash key
  OMNI_EXTRA = 'extra'
  OMNI_USER_INFO = 'user_info'
  OMNI_USER_HASH = 'user_hash'
  OMNI_NAME = 'name'
  OMNI_BIO = 'bio'
  OMNI_UID = 'uid'
  OMNI_PROVIDER = 'provider'
  OMNI_URLS = 'urls'
  OMNI_WEBSITE = 'Website'
  OMNI_FACEBOOK = 'Facebook'
  OMNI_TWITTER = 'Twitter'
  OMNI_EMAIL = 'email'

  # named scope
  scope :name_or_username, lambda {|keyword| {:conditions => ["username like ? or fullname like ?", "%#{keyword}%", "%#{keyword}%"]}}
  scope :by_ip_last_24_hour, lambda {|ip| {:conditions => ["ip = ? and created_at > ?", ip, 1.day.ago]}}
  
  # relationship
  has_and_belongs_to_many :languages
  
  has_one :user_configuration
  belongs_to :city, :class_name => "City", :foreign_key => 'city_id'
  belongs_to :gender, :class_name => "Gender", :foreign_key => 'gender_id'

  has_many :trips
  has_many :seeks
  has_many :topics
  has_many :posts
  has_many :badges
  has_many :payout_methods
  
  has_many :received_reviews, :class_name => "Review", :foreign_key => 'receiver_id'

  has_many :received_positive_reviews, :class_name => "PositiveReview", :foreign_key => 'receiver_id'
  has_many :given_positive_reviews, :class_name => "PositiveReview", :foreign_key => 'giver_id'

  has_many :received_negative_reviews, :class_name => "NegativeReview", :foreign_key => 'receiver_id'
  has_many :given_negative_reviews, :class_name => "NegativeReview", :foreign_key => 'giver_id'

  has_many :received_notifications, :class_name => "Notification", :foreign_key => 'receiver_id'
  has_many :sent_notifications, :class_name => "Notification", :foreign_key => 'sender_id'

  has_many :received_messages, :class_name => "Message", :foreign_key => 'receiver_id'
  has_many :sent_messages, :class_name => "Message", :foreign_key => 'sender_id'

  has_many :relations, :class_name => 'Relation', :foreign_key => 'from_id'
  has_many :received_relations, :class_name => 'Relation', :foreign_key => 'to_id'

  has_many :escrows_as_buyer, :class_name => 'Escrow', :foreign_key => 'buyer_id'
  has_many :escrows_as_seller, :class_name => 'Escrow', :foreign_key => 'seller_id'

  has_many :stories
  has_many :suggested_items

  has_many :influencer_logs, :class_name => "InfluencerLog", :foreign_key => 'influencer_id'
  has_many :point_logs, :class_name => 'PointLog', :foreign_key => 'user_id'
  
  has_attached_file :avatar, :styles => lambda{ |a| ['image/gif', 'image/png', 'image/jpg', 'image/jpeg'].include?( a.content_type ) ? { :original => "100x100>", :thumb => "48x48>" } : {}},
                             :default_url => '/images/default_:style_avatar.jpg',
                             :storage => :s3,
                             :s3_credentials => "#{Rails.root}/config/s3.yml",
                             :path => "/:attachment/:id/:style/:filename"

  validates_attachment_content_type :avatar, :content_type => ['image/gif', 'image/png', 'image/jpg', 'image/jpeg'],
                                             :message => 'must be image'

  validates_attachment_size :avatar, :less_than => 2.megabytes

  attr_accessor :password
  attr_readonly :username, :email

  # validation
  validates :username,
    :uniqueness => true,
    :length => { :within => 4..10 },
    :format => { :with => /^[a-zA-Z0-9]+$/i }

  validates :email,
    :uniqueness => true,
    :length => { :within => 5..100 },
    :format => { :with => /^[^@][\w.-]+@[\w.-]+[.][a-z]{2,4}$/i}

  validates :password,
    :presence => true, :if => :password_required?,
    :length => { :within => 4..25 }

  validates :fullname, :presence => true, :length => { :maximum => 100 }
  validates :city, :user_city => true
  validates :location, :length => { :maximum => 100 }
  validates :contact_number, :length => { :maximum => 100 }
  validates :extra_contact_number, :length => { :maximum => 100 }
  validates :web, :length => { :maximum => 100 }
  validates :facebook, :length => { :maximum => 100 }
  validates :twitter, :length => { :maximum => 100 }
  validates :bio, :length => { :maximum => BIO_MAXIMUM_LENGTH }
  validates :work, :length => { :maximum => WORK_MAXIMUM_LENGTH }
  validates :languages, :length => { :maximum => LANGUAGES_MAXIMUM }

  # events
  before_create :generate_salt
  before_save :encrypt_new_password, :generate_email_validation_key
  after_create :send_email_validation, :create_configuration

  def self.create_with_omniauth(omniauth)
    data = omniauth[OMNI_EXTRA][OMNI_USER_HASH]
    fullname = data[OMNI_NAME]
    
    # location is unused for now
    # location = data["location"]["name"] if !data["location"].nil?
    bio = data[OMNI_BIO]

    uid = omniauth[OMNI_UID]
    provider = omniauth[OMNI_PROVIDER]
    web = omniauth[OMNI_USER_INFO][OMNI_URLS][OMNI_WEBSITE] if !omniauth[OMNI_USER_INFO][OMNI_URLS].nil?
    fb = omniauth[OMNI_USER_INFO][OMNI_URLS][OMNI_FACEBOOK] if !omniauth[OMNI_USER_INFO][OMNI_URLS].nil?
    twitter = omniauth[OMNI_USER_INFO][OMNI_URLS][OMNI_TWITTER] if !omniauth[OMNI_USER_INFO][OMNI_URLS].nil?
    email = omniauth[OMNI_USER_INFO][OMNI_EMAIL]
    
    user = User.new
    user.uid = uid
    user.provider = provider
    
    user.email = email if !email.nil?
    user.fullname = fullname if !fullname.nil?
    user.facebook = fb if provider == 'facebook' && !fb.nil?
    user.twitter = twitter if provider == 'twitter' && !twitter.nil?
    user.web = web if !web.nil?
    user.bio = bio if !bio.nil?
    return user
  end

  # create configuration class for each user
  def create_configuration
    config = UserConfiguration.new
    config.user_id = self.id
    config.save
  end
  
  # send email validation
  def send_email_validation
    Notifier.delay.email_validation(self) if !social_login?
  end

  # send email validation
  def send_email_validation_reminder
    Notifier.delay.email_validation_reminder(self) if !social_login?
  end

  # overwrite username setter to always downcase the value
  def username=(username)
    write_attribute(:username, username.downcase)
  end

  # static method to handle authentication
  def self.authenticate(email_or_username, password)
    user = find_by_email(email_or_username)
    if !user
      user = find_by_username(email_or_username)
    end
    return user if user && user.authenticated?(password)
  end

  # static method to handle OAuth authentication
  def self.oauth_authenticate(auth)
    return find_by_provider_and_uid(auth["provider"],auth["uid"])
  end

  # static method to update times reminded
  def self.update_times_reminded(user)
    user.times_reminded += 1
    user.save(:validate => false)
  end

  # check method to see whether a password is a match
  def authenticated?(password)
    self.hashed_password == encrypt(password)
  end

  # check method to see whether email has been validated/account been activated
  def email_validated?
    # the first five days user can use without validation
    if self.created_at > NO_VALIDATION_PERIOD.days.ago
      true
    else
      self.email_validation_key.nil? or self.email_validation_key.empty?
    end
  end

  # equality for user
  def ==(other)
    return self.id == other.id
  end

  def social_login?
    !self.uid.nil? && !self.uid.empty? && !self.provider.nil? && !self.provider.empty?
  end
  
  def is_facebooker?
    self.provider.eql?('facebook')
  end

  def need_to_be_reminded_for_activation?
    if !email_validated? and self.times_reminded == 0 and self.created_at < DAYS_AGE_TO_BE_REMINDED.days.ago
      true
    else
      false
    end
  end

  def member_since
    self.created_at.strftime("%B %Y")
  end

  def need_to_complete_profile?
    count = 0
    count = count + 1 if self.fullname.blank?
    count = count + 1 if self.location.blank?
    count = count + 1 if self.contact_number.blank?
    count = count + 1 if self.twitter.blank?
    count = count + 1 if self.facebook.blank?
    count = count + 1 if self.web.blank?
    count = count + 1 if self.bio.blank?
    count = count + 1 if self.birth_year.blank?
    count = count + 1 if self.work.blank?
    count = count + 1 if self.gender.blank?

    if count > 2
      true
    else
      false
    end
  end

  def has_trip?
    count = self.trips.size
    if count > 0
      return true
    else
      return false
    end
  end

  def has_seek?
    count = self.seeks.size
    if count > 0
      return true
    else
      return false
    end
  end

  def has_location?
    if !self.city.nil?
      return true
    else
      return false
    end
  end

  def has_avatar?
    if !self.avatar_file_name.blank?
      return true
    else
      return false
    end
  end

  def has_payout_method?
    !self.payout_methods.blank?
  end

  # virtual attribute getter to get origin location
  def city_location
    if city
      city.name + ", " + city.country.name
    else
      @city_location
    end
  end

  # virtual attribute setter to accept origin location
  def city_location=(location)
    @city_location = location
    self.city = check_location(location)
  end

  def need_notify?
    return need_to_complete_profile? || received_positive_reviews.size == 0 || !has_location? || !has_avatar? || !has_payout_method?
  end

  def incoming_to_user_location(limit)
    if self.city
      Trip.active_only.incoming_to_user_location(self).limit(limit)
    else
      Array.new
    end
  end

  def incoming_to_user_location_count
    if self.city
      Trip.active_only.incoming_to_user_location(self).count
    else
      0
    end
  end
  
  def count_dashboard_notification
    unread_messages = self.received_messages.where(:read => false).count
    received_notification = self.received_notifications.where(:read => false).count
    unread_messages + received_notification
  end

  def count_unread_message
    self.received_messages.where(:read => false).count
  end

  def count_received_notification
    self.received_notifications.where(:read => false).count
  end

  def self.valid_attribute?(attr, value)
    mock = self.new(attr => value)
    unless mock.valid?
      return !mock.errors.has_key?(attr)
    end
    true
  end

  def self.data_options
    return {:except => [:updated_at,:created_at, :hashed_password, :reset_password_key, :email_validation_key, :email], :skip_types=>true, :skip_instruct=>true}
  end

  # generate drop down value for birth year
  def self.birth_year_list
    start_year = Time.now.year
    end_year = start_year - 100
    result = Array.new
    result << ['', nil]
    while start_year >= end_year
      result << [start_year, start_year]
      start_year -= 1
    end
    result
  end

  # get current age of user
  def age
    unless self.birth_year.blank?
      Time.now.year - self.birth_year
    end
  end

  # check if the user has relation to the given user
  def relate_to?(user)
    self.relations.where(:to_id => user.id).count > 0
  end

  # get user response rate to incoming message
  def response_rate

    # get all message thread started that this user received
    received_messages = self.received_messages.where(:reply_to_id => nil)

    unless received_messages.empty?
      received_messages_id = Array.new

      # collect all the ids of thread started
      received_messages.each do |received_message|
        received_messages_id << received_message.id
      end

      responded_messages = self.sent_messages.find(:all, :select => 'DISTINCT sender_id, reply_to_id', :conditions => ['reply_to_id IN (?)', received_messages_id])
      ((responded_messages.count * 1.0)/(received_messages.count)*100).ceil
    else
      nil
    end
  end

  # display languages in comma separated
  def display_languages
    result = ''
    languages = self.languages
    languages.each{ |lang|
      unless lang.eql?(languages.last)
        result << lang.english_name
        result << ', '
      else
        result << lang.english_name
      end
    }
    result
  end

  # authenticate user with salt
  def self.authenticate_with_salt(user_id, salt)
    user = User.find(user_id)
    if user and user.salt == salt
      user
    else
      nil
    end
  end

  # method to populate salt for new account
  def generate_salt
    self.salt = ActiveSupport::SecureRandom.base64(8)
  end

  # calculate total number of registered user using an IP in the last 24 hour
  def self.allow_create_by_ip?(ip)
    if by_ip_last_24_hour(ip).count > SAME_IP_REGISTRATION_24_HOUR
      false
    else
      true
    end
  end

  # check if user has trusted badge
  def has_trusted_badge?
    !self.badges.find_by_type(TrustedBadge.to_s).blank?
  end

  # get bistip point of a user
  def points
    amount = self.influencer_logs.count

    # add point for picture
    self.stories.active_only.each do |s|
      amount += s.has_picture? ? PointLog::STORY_WITH_PICTURE_POINT : PointLog::STORY_POINT
    end

    # add point for picture
    self.suggested_items.active_only.each do |si|
      amount += si.has_picture? ? PointLog::SUGGESTED_ITEM_WITH_PICTURE_POINT : PointLog::SUGGESTED_ITEM_POINT
    end

    # find points used in point logs
    self.point_logs.each do |bp|
      amount = bp.do_point(amount)
    end
    amount
  end

  # get used bistip point of a user
  def points_used
    amount = 0
    self.point_logs.each do |bp|
      amount = bp.do_point(amount)
    end
    amount*(-1)
  end

  # check user's non cash non cashback escrow
  def has_non_cashback_escrow?
    if self.escrows_as_buyer.count > 0
      Escrow.non_cashback_escrow(self).size > 0
    else
      false
    end
  end
  
  protected
  
    def clear_carriage_return
      self.bio.gsub!(/\r\n/m, "\n")
    end
    
    # method to encrypt password before store it to database
    def encrypt_new_password
      return if password.blank?
      self.hashed_password = encrypt(password)
    end

    # method to populate email validation key for new account
    def generate_email_validation_key
      if self.new_record? and !social_login?
        self.email_validation_key = UUIDTools::UUID.timestamp_create
      end
    end

    # check method to help trigger password validation
    def password_required?
      hashed_password.blank? || password.present?
    end

    # helper to encrypt password
    def encrypt(string)
      Digest::SHA1.hexdigest(string)
    end

    def not_oauth_type?
      return uid == nil && provider == nil
    end

end
