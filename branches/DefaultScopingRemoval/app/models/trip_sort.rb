# to represent trip sorting
class TripSort
  
  # constants
  NEWEST = 'newest'
  DEPARTURE_DATE_ASC = 'departure_date_asc'

  # return label of sort and its value
  def self.map
    return [[I18n.t('trip.sort.newest'), NEWEST],[I18n.t('trip.sort.departure_date_asc'), DEPARTURE_DATE_ASC]]
  end

  # build a scope
  def self.build(sort, scope)
    if sort == NEWEST
      scope.newest
    elsif sort == DEPARTURE_DATE_ASC
      scope.earliest
    end
  end

  # return default order scope for trip
  def self.default(scope)
    scope.newest
  end
end
