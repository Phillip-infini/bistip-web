class SuggestedItem < ActiveRecord::Base
  include Location

  # scope
  scope :active_only, lambda { {:conditions => ["deleted = ?", false]} }
  scope :name_match_and_not_id_and_not_origin_city_id, lambda { |name, id, origin_city_id, limit| {:conditions => ["match(name) against(?) and id <> ? and origin_city_id <> ?", name, id, origin_city_id], :limit => limit} }

  # restriction
  attr_readonly :name
  attr_readonly :origin_city

  # constant
  NAME_MAX_LENGTH = 50
  NAME_MIN_LENGTH = 3
  REASON_MAX_LENGTH = 250
  REASON_MIN_LENGTH = 5
  NOTES_MAX_LENGTH = 600
  PICTURES_MAXIMUM = 3
  TOTAL_SHOW_ON_SUMMARY = 10

  # validation
  validates :name, :presence => true,
            :length => {:maximum => NAME_MAX_LENGTH, :minimum => NAME_MIN_LENGTH}
  validates :reason, :presence => true,
            :length => {:maximum => REASON_MAX_LENGTH, :minimum => REASON_MIN_LENGTH}
  validates :notes, :length => {:maximum => NOTES_MAX_LENGTH}
  validates :user, :presence => true
  validates :origin_city, :origin_city => true
  
  # associations
  belongs_to :user
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'
  has_many :pictures, :class_name => "SuggestedItemPicture", :foreign_key => 'suggested_item_id'
  has_many :votes, :class_name => 'SuggestedItemVote', :foreign_key => 'suggested_item_id'
  accepts_nested_attributes_for :pictures, :reject_if => lambda { |a| a[:attachment].blank? }

  # check if item suggestion has a picture
  def has_picture?
    self.pictures.count > 0
  end

  # check if the user has relation to the given user
  def voted_by?(user)
    return true if self.user == user
    self.votes.where(:user_id => user.id).count > 0
  end

  # find top voted items from a certain area
  def self.get_top_voted_from(origin_city, limit)
    all_cities = City.find_by_name_and_country_id(City::ALL_CITIES, origin_city.country_id)
    cities = Array.new
    cities << origin_city
    cities << all_cities unless all_cities.blank?

    SuggestedItem.active_only.find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
      :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id',
      :group => 'suggested_items.id',
      :order => 'counter DESC',
      :conditions => ["suggested_items.origin_city_id IN (#{cities.map{|c| c.id}.join(',')})"],
      :limit => limit)
  end

  # find similar item from other area
  def self.get_similar_from_other_city(suggested_item, limit)
    SuggestedItem.active_only.find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
      :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id',
      :group => 'suggested_items.id',
      :order => 'counter DESC',
      :conditions => ["match(name) against(?) and suggested_items.id <> ? and suggested_items.origin_city_id <> ?", suggested_item.name, suggested_item.id, suggested_item.origin_city_id],
      :limit => limit)
  end

  # find other item from the same origin city of the suggested item
  def self.get_other_suggested_item_from_same_city(suggested_item, limit)
    origin_city = suggested_item.origin_city
    all_cities = City.find_by_name_and_country_id(City::ALL_CITIES, origin_city.country_id)
    cities = Array.new
    cities << origin_city
    cities << all_cities unless all_cities.blank?
    
    SuggestedItem.active_only.find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
      :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id',
      :group => 'suggested_items.id',
      :order => 'counter DESC',
      :conditions => ["suggested_items.id <> #{suggested_item.id} and suggested_items.origin_city_id IN (#{cities.map{|c| c.id}.join(',')})"],
      :limit => limit)
  end
  
end
