require 'test_helper'

class EscrowsControllerTest < ActionController::TestCase
  
  setup do
    @message = messages(:onetomac)
    @one = users(:one)
    @dono = users(:dono)
    login_as(:one)
  end

  test "should get new" do
    get :new, :message_id => @message.id, :seller_id => users(:dono).id
    assert_response :success
    assert_template :new
    assert_not_nil assigns(:escrow)
  end

  test "should get create with paypal (confirmation step)" do
    session[:escrow_params] = {}
    session[:escrow_step] = 'confirmation'
    assert_difference('Escrow.count') do
      post :create, :escrow => {:buyer_id => @one.id,
        :seller_id => @dono.id,
        :message_id => @message.id,
        :payment_method_id => payment_methods(:paypal).id,
        :amount => 200,
        :item => 'ipad 2'
      }
      escrow = assigns(:escrow)
      assert_redirected_to redirect_paypal_escrows_path(:escrow_id => escrow)
    end
  end

  test "should get create with bank transfer (confirmation step)" do
    session[:escrow_params] = {}
    session[:escrow_step] = 'confirmation'
    assert_difference('Escrow.count') do
      post :create, :escrow => {:buyer_id => @one.id,
        :seller_id => @dono.id,
        :message_id => @message.id,
        :payment_method_id => payment_methods(:ibt).id,
        :amount => 2000000,
        :item => 'ipad 2'
      }
      escrow = assigns(:escrow)
      assert_redirected_to redirect_indonesia_bank_transfer_escrows_path(:escrow_id => escrow)
    end
  end

  test "should get create with bank transfer (details step)" do
    session[:escrow_params] = {}
    session[:escrow_step] = 'details'
    assert_no_difference('Escrow.count') do
      post :create, :escrow => {:buyer_id => @one.id,
        :seller_id => @dono.id,
        :message_id => @message.id,
        :payment_method_id => payment_methods(:paypal).id,
        :amount => 200,
        :item => 'ipad 2'
      }
      assert_response :success
      assert_template :new
    end
  end

  test 'should get redirect paypal' do
    login_as(:one)
    escrow = escrows(:onetodono)
    get :redirect_paypal, :escrow_id => escrow
    assert_response :success
    assert_template :redirect_paypal
  end
  
  test 'should get redirect bank transfer' do
    login_as(:one)
    escrow = escrows(:onetodono)
    get :redirect_indonesia_bank_transfer, :escrow_id => escrow
    assert_response :success
    assert_template :redirect_indonesia_bank_transfer
  end

  test 'should get calculate amount after cost' do
    get :calculate_amount_after_cost, :format => 'js', :amount => 30, :payment_method_id => payment_methods(:paypal)
    assert_response :success
    assert_template :calculate_amount_after_cost
    assert_equal 'USD 31.2', @response.body

    get :calculate_amount_after_cost, :format => 'js', :amount => 300000, :payment_method_id => payment_methods(:ibt)
    assert_response :success
    assert_template :calculate_amount_after_cost
    assert_equal 'IDR 306000.0', @response.body
  end

  test 'try seller_got_item' do
    login_as(:mactavish)
    mactavish = users(:mactavish)
    escrow = escrows(:PaypalNotified)
    message = escrow.message
    assert_difference('SellerGotItemLog.count') do
      post :seller_got_item, :escrow_id => escrow.id
      assert_redirected_to user_message_path(mactavish, message.id)
    end

    escrow = escrows(:BistipReceivedBuyerBankTransfer)
    message = escrow.message
    assert_difference('SellerGotItemLog.count') do
      post :seller_got_item, :escrow_id => escrow.id
      assert_redirected_to user_message_path(mactavish, message.id)
    end

    escrow = escrows(:BuyerResumed)
    message = escrow.message
    assert_difference('SellerGotItemLog.count') do
      post :seller_got_item, :escrow_id => escrow.id
      assert_redirected_to user_message_path(mactavish, message.id)
    end
  end

  test 'try buyer_received_item' do
    login_as(:dono)
    dono = users(:dono)
    escrow = escrows(:SellerGotItem)
    message = escrow.message
    assert_difference('BuyerReceivedItemLog.count') do
      post :buyer_received_item, :escrow_id => escrow.id, :review_type => 'PositiveReview', :review_body => 'dsadasdsaassdassa'
      assert_redirected_to user_message_path(dono, message.id)
    end
  end

  test 'try seller_cancelled' do
    login_as(:mactavish)
    mactavish = users(:mactavish)
    escrow = escrows(:PaypalNotified)
    message = escrow.message
    assert_difference('SellerCancelledLog.count') do
      post :seller_cancelled, :escrow_id => escrow.id
      assert_redirected_to user_message_path(mactavish, message.id)
    end

    escrow = escrows(:BistipReceivedBuyerBankTransfer)
    message = escrow.message
    assert_difference('SellerCancelledLog.count') do
      post :seller_cancelled, :escrow_id => escrow.id
      assert_redirected_to user_message_path(mactavish, message.id)
    end

    escrow = escrows(:BuyerResumed)
    message = escrow.message
    assert_difference('SellerCancelledLog.count') do
      post :seller_cancelled, :escrow_id => escrow.id
      assert_redirected_to user_message_path(mactavish, message.id)
    end
  end

  test 'try seller_requested_payout' do
    login_as(:mactavish)
    mactavish = users(:mactavish)
    escrow = escrows(:BuyerReceivedItem)
    message = escrow.message
    assert_difference('SellerRequestedPayoutLog.count') do
      post :seller_requested_payout, :escrow_id => escrow.id, :review_type => 'PositiveReview', :review_body => 'dsadasdsaassdassa'
      assert_redirected_to user_message_path(mactavish, message.id)
    end
  end

  test 'try seller_received_payout' do
    login_as(:mactavish)
    mactavish = users(:mactavish)
    escrow = escrows(:BistipReleasedSellerPayout)
    message = escrow.message
    assert_difference('SellerReceivedPayoutLog.count') do
      post :seller_received_payout, :escrow_id => escrow.id
      assert_redirected_to user_message_path(mactavish, message.id)
    end
  end

  test 'try buyer_resumed' do
    login_as(:dono)
    dono = users(:dono)
    escrow = escrows(:SellerCancelled)
    message = escrow.message
    assert_difference('BuyerResumedLog.count') do
      post :buyer_resumed, :escrow_id => escrow.id
      assert_redirected_to user_message_path(dono, message.id)
    end
  end

  test 'try buyer_requested_payout' do
    login_as(:dono)
    dono = users(:dono)
    escrow = escrows(:SellerCancelled)
    message = escrow.message
    assert_difference('BuyerRequestedPayoutLog.count') do
      post :buyer_requested_payout, :escrow_id => escrow.id
      assert_redirected_to user_message_path(dono, message.id)
    end
  end

  test 'try buyer_received_payout' do
    login_as(:dono)
    dono = users(:dono)
    escrow = escrows(:BistipReleasedBuyerPayout)
    message = escrow.message
    assert_difference('BuyerReceivedPayoutLog.count') do
      post :buyer_received_payout, :escrow_id => escrow.id
      assert_redirected_to user_message_path(dono, message.id)
    end
  end
end
