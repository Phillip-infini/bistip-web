require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  
  test "home page" do
    get :index
    assert_not_nil assigns(:trips)
    assert_response :success
    assert_template 'index'
  end

  test 'home expire cache' do
    get :expire_cloud
    assert_response :redirect
  end

  test "home create_released_seller_payout_log" do
    get :create_released_seller_payout_log, :escrow_id => escrows(:SellerRequestedPayout).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test "home create_released_buyer_payout_log" do
    get :create_released_buyer_payout_log, :escrow_id => escrows(:BuyerRequestedPayout).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test 'home create_received_buyer_bank_transfer_log' do
    get :create_received_buyer_bank_transfer_log, :escrow_id => escrows(:donotomac).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test 'home create_bistip_approved_insurance_log' do
    get :create_bistip_approved_insurance_log, :insurance_id => insurances(:donotomac).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test 'home create_bistip_rejected_insurance_log' do
    get :create_bistip_rejected_insurance_log, :insurance_id => insurances(:donotomac).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test 'home create_bistip_received_buyer_bank_transfer_insurance_log' do
    get :create_bistip_received_buyer_bank_transfer_insurance_log, :insurance_id => insurances(:BistipApproved).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test 'home grant_trusted_badge' do
    get :grant_trusted_badge, :user_id => users(:dono).id, :password => ApplicationController::REST_PASSWORD
    assert_response :redirect
  end

  test 'home stats page' do
    get :stats, :password => 'ayoayo_kitabisa'
    assert_response :success
    assert_template 'stats'
  end
end
