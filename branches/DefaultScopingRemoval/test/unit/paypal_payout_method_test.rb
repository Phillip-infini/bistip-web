require 'test_helper'

class PaypalPayoutMethodTest < ActiveSupport::TestCase
  #checking if we can find payment method
  test "should find a payout methods" do
    assert_nothing_raised {PaypalPayoutMethod.find_by_type(payment_methods(:paypal).type)}

  end
  test "should create a paypal payout" do
  #creating a new and valid payout
    payout=PaypalPayoutMethod.new
    test_user=users(:dono)
    payout.user=test_user
    payout.email=test_user.email
    payout.type=payment_methods(:paypal).type
    assert payout.save

  end
  test "should delete a paypal payout" do
  #creating a new and valid payout
    payout=PaypalPayoutMethod.new
    test_user=users(:dono)
    payout.user=test_user
    payout.email=test_user.email
    payout.type=payment_methods(:paypal).type
    assert payout.save
    payout.type=payment_methods(:ibt).type
    assert payout.save
    #trying to delete
    assert payout.update_attributes(:deleted => true)
  end

  test "should throw an exception if not valid" do
  #creating a new paypal payout with invalid email
    payout=PaypalPayoutMethod.new
    test_user=users(:dono)
    payout.user=test_user
    assert !payout.valid?
    assert payout.errors[:email].any?
    payout.email = 'a@cm'
    assert !payout.valid?
    assert payout.errors[:email].any?

  end

end
