require 'test_helper'

class EscrowLogsTest < ActiveSupport::TestCase

  test "should create escrow log and test all the methods" do
    escrow = escrows(:donotomac)
    buyer = escrow.buyer
    seller = escrow.seller
    escrow_log = nil
    
    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = BuyerInitiatedLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(seller)
    }
   
    assert_difference('EscrowLogNotification.count', 2) do
      assert escrow_log = PaypalNotifiedLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(seller)
      assert escrow_log.to_notification_link(buyer)
    }

    assert_difference('EscrowLogNotification.count', 2) do
      assert escrow_log = BistipReceivedBuyerBankTransferLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(seller)
      assert escrow_log.to_notification_link(buyer)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = SellerGotItemLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(buyer)
    }
    
    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = BuyerReceivedItemLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(seller)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = SellerRequestedPayoutLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(buyer)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = SellerReceivedPayoutLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(buyer)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = BistipReleasedSellerPayoutLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(seller)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = BistipReleasedBuyerPayoutLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(buyer)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = SellerCancelledLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(buyer)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = BuyerResumedLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(seller)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = BuyerRequestedPayoutLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(seller)
    }

    assert_difference('EscrowLogNotification.count') do
      assert escrow_log = BuyerReceivedPayoutLog.create!(:escrow_id => escrow.id)
    end
    assert_nothing_raised {
      assert escrow_log.to_display_string
      assert escrow_log.get_awaiting_string
      assert escrow_log.to_notification_link(seller)
    }
    
  end

end
