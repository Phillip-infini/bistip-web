require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "should create user" do
    user = User.new
    user.username = "testcreate"
    user.fullname = 'testcreate'
    user.city_location = 'jakarta'
    user.email = "testcreate@mail.com"
    user.hashed_password = "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4"
    assert_difference('UserConfiguration.count') do
      assert user.save
    end
  end

  test 'should create omniauth user' do
    urls = Hash.new
    urls[User::OMNI_WEBSITE] = 'http://www.bistip.com'
    urls[User::OMNI_FACEBOOK] = 'http://www.facebook.com/bistip'
    urls[User::OMNI_TWITTER] = 'http://www.twitter.com/bistip'

    user_info = Hash.new
    user_info[User::OMNI_URLS] = urls
    user_info[User::OMNI_EMAIL] = 'team@bistip.com'

    provider = 'facebook'
    uid = '2312312312312'

    user_hash = Hash.new
    user_hash[User::OMNI_NAME] = 'Willy Eka'
    user_hash[User::OMNI_BIO] = 'my member at bistip'

    omni_extra = Hash.new
    omni_extra[User::OMNI_USER_HASH] = user_hash

    omniauth = Hash.new
    omniauth[User::OMNI_EXTRA] = omni_extra
    omniauth[User::OMNI_UID] = uid
    omniauth[User::OMNI_PROVIDER] = provider
    omniauth[User::OMNI_USER_INFO] = user_info

    assert_nothing_raised {User.create_with_omniauth(omniauth)}
  end

  test "should find user" do
    user_id = users(:one).id
    assert_nothing_raised {User.find(user_id)}
  end

  test "should update user" do
    user = users(:one)
    assert user.update_attributes(:web => "http://www.facebook.com",
      :location => "Jakarta, Indonesia", :bio => "Allan's bio")
  end

  test "should not create user with invalid user or email" do
    user = User.new
    # username contain invalid character
    user.username = "@validate"
    user.email = "testatemail.com"

    # there must be error with username and email
    assert !user.valid?
    assert user.errors[:username].any?
    assert user.errors[:email].any?
    assert !user.save
  end
  
  test "password" do
    user = User.new(:password => "secret")
    assert user.password
  end

  test "encrypt password" do
    password = "secret"
    assert encrypted_password = encrypt(password)
    assert submitted_password = "secret"
    assert encrypted_password == encrypt(submitted_password)
  end

  test "update user view" do
    user = User.first
    user.update_attributes(:email => "example@rubymail.org",
                           :password => "foobar")
    assert !user.save
  end

  test "user all" do
    User.all
    assert User.all.any?
  end

  test "user error" do
    user = users(:not_validated)
    assert user.valid?
    assert user.errors.full_messages
  end

  test "authenticated user and password" do
    user = users(:one)
    assert User.authenticate('Allan', 'secret') == user
    assert User.authenticate('Allan@email.com', 'secret') == user
    assert User.authenticate_with_salt(user.id, user.salt)

    facebook = users(:facebook)
    oauth = Hash.new
    oauth['provider'] = facebook.provider
    oauth['uid'] = facebook.uid
    assert user = User.oauth_authenticate(oauth) == facebook
  end

  test 'user times reminded' do
    user = users(:one)
    assert_nothing_raised {User.update_times_reminded(user)}
    assert user.times_reminded == 1
  end

  test 'user email validated' do
    assert users(:one).email_validated?
    assert users(:not_validated).email_validated?
    assert !users(:old).email_validated?
  end

  test 'user social login method' do
    facebook = users(:facebook)
    assert facebook.social_login?
    assert facebook.is_facebooker?
  end
  
  test 'user equality' do
    assert users(:one) == users(:one)
  end

  test 'user activation reminder' do
    assert !users(:one).need_to_be_reminded_for_activation?
    assert users(:old).need_to_be_reminded_for_activation?
  end

  test 'user trusted badge' do
    assert users(:one).has_trusted_badge?
    assert !users(:dono).has_trusted_badge?
  end

  test 'user point method' do
    assert_equal 32, users(:one).points
    assert_equal 0, users(:one).points_used
  end

end
