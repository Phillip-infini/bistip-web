# To change this template, choose Tools | Templates
# and open the template in the editor.

class NegativeReputationNotification < Notification

  #overide method, to provide info who gave negative reputation to current_user
  def to_link
    negative_reputation = User.find(data_id)
    link = I18n.t("notification.title.negative_reputation",
      :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
      :reputation_label=>link_to(I18n.t('notification.field.negative_reputation'), path_helper.user_negative_reputations_path(negative_reputation)))
    return link
  end
end
