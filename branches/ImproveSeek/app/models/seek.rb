# Seek class represent a search for a traveller's trip
class Seek < ActiveRecord::Base
  include Location

  # constants
  NOTES_MAXIMUM_LENGTH = 300

  MAXIMUM_AGE = 60

  # scope
  default_scope lambda {{ :conditions => ["seeks.created_at > ? and seeks.deleted = ?", MAXIMUM_AGE.days.ago, false], :include => [:origin_city, :destination_city] }}
  scope :origin_city_id, lambda { |origin_city_id| {:conditions => ["origin_city_id = ?", origin_city_id]}}
  scope :origin_city_or_all_cities_id, lambda { |origin_city_id, all_origin_cities_id| {:conditions => ["origin_city_id = ? OR origin_city_id = ?", origin_city_id, all_origin_cities_id]}}
  scope :destination_city_id, lambda { |destination_city_id| {:conditions => ["destination_city_id = ?", destination_city_id]}}
  scope :destination_city_or_all_cities_id, lambda { |destination_city_id, all_destination_cities_id| {:conditions => ["destination_city_id = ? OR destination_city_id = ?", destination_city_id, all_destination_cities_id]}}
  scope :origin_country_id, lambda { |country_id| {:conditions => ["cities.country_id = ?", country_id]}}
  scope :destination_country_id, lambda { |country_id| {:conditions => ["destination_cities_seeks.country_id = ?", country_id]}}
  scope :notes_contains, lambda { |keyword| {:conditions => ["match(notes) against(?)", keyword]}}
  scope :user_is_not, lambda { |user_id| {:conditions => ["user_id != ?", user_id]}}
  scope :user_is, lambda { |user_id| {:conditions => ["user_id = ?", user_id]}}
  scope :random, lambda { |*args| {:order => 'RAND()', :limit => args[0] || 1 } }
  scope :from_anywhere, :conditions => "from_anywhere is true"

  # validation
  validates :origin_location, :presence => {:if => :not_from_anywhere?}

  validates :destination_location, :presence => true

  validates :departure_date, :future_date => true

  validates :departure_date_predicate, :presence => {:if => :departure_date_given?}, :inclusion => {:in => DatePredicate::ALL, :allow_nil => true, :allow_blank => true}

  validates :notes, :length => { :maximum => NOTES_MAXIMUM_LENGTH }

  validates :items, :presence => true

  # relationship
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'
  belongs_to :destination_city, :class_name => "City", :foreign_key => 'destination_city_id'
  belongs_to :user
  has_many :comments, :class_name => "SeekComment"
  has_many :items, :class_name => "SeekItem"

  # nested
  accepts_nested_attributes_for :items, :allow_destroy => true, :reject_if => lambda { |a| a[:name].blank? }

  # event
  after_create :send_match_notification
  before_save :handle_from_anywhere

  # send email to owner of matching trip
  def send_match_notification
    matching_trips = find_matching_trips
    matching_trips.each do |trip|
      MatchSeekNotification.create!(:receiver=>trip.user,:sender=>user, :data_id=>id)
      if trip.user.user_configuration.email_match_seek
        Notifier.email_matching_trip(self, trip).deliver
      end
    end
  end
  handle_asynchronously :send_match_notification

  # handle from anywhere seek
  def handle_from_anywhere
    if self.from_anywhere?
      self.origin_location = City::ANYWHERE
    end
  end

  def departure_date_given?
    !self.departure_date.nil?
  end

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  # check is a seek still active
  def active?
    self.created_at > Seek::MAXIMUM_AGE.days.ago and self.deleted == false
  end

  # find match in trip table
  def find_matching_trips
    if self.active?
      scope = Trip.scoped({})
      origin = self.origin_city
      destination = self.destination_city

      if !self.from_anywhere?
        if origin.all_cities?
          scope = scope.origin_country_id origin.country
        else
          scope = scope.origin_city_id origin.id
        end
      end

      if destination.all_cities?
        scope = scope.destination_country_id destination.country
      else
        scope = scope.destination_city_id destination.id
      end

      scope = scope.user_is_not self.user_id

      # build predicate on query for departure date based on predicate
      departure_date = self.departure_date
      departure_date_predicate = self.departure_date_predicate
      if !departure_date.nil?
        case departure_date_predicate
          when DatePredicate::AFTER
            scope = scope.departure_date_after departure_date
          when DatePredicate::BEFORE
            scope = scope.departure_date_before departure_date
        end
      end

      scope
    else
      Array.new
    end
  end

  # negate of from anywhere
  def not_from_anywhere?
    !self.from_anywhere?
  end

  def items_name
    unless self.items.empty?
      result = ''
      self.items.each_with_index do |item, index|
        # append separator if not the first one
        result << ', ' if index > 0
        result << item.name
        # append ... if item is more than one, and there more items still
        if (index > 0) and (index < (self.items.size - 1))
          result << '...'
          break
        end
      end
      result
    end
  end

  def to_s
    I18n.t('seek.short', :from => self.origin_city.name, :to => self.destination_city.name)
  end

end
