# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

# seed data for list of countries
Country.delete_all
File.open("#{Rails.root}/db/data/countries.txt", "r") do |infile|
  # specify id explicitly, id starts from 1
  id = 1;
  infile.read.each_line do |country_row|
    code, name = country_row.chomp.split("|")
    country = Country.new
    country.id = id
    country.code = code.downcase
    country.name = name
    country.save
    id = id + 1
  end
end

# seed data for list of cities and set the relationship with countries
City.delete_all
File.open("#{Rails.root}/db/data/cities.txt", "r") do |infile|
  # specify id explicitly, id starts from 1
  id = 1;
  infile.read.each_line do |city_row|
    country_code, name, region = city_row.chomp.split("|")
    city = City.new
    city.id = id
    city.country = Country.find_by_code(country_code)
    city.name = name
    city.region = region
    city.save
    id = id + 1
  end
end

# populate <any cities> seed data into cities table using list of countries
# singapore and hong kong is a single cities in a country, so no all_cities
id = City.count + 1
Country.all.each do |country|
  if country.code == 'sg'
  elsif country.code == 'hk'
  else
    city = City.new
    city.id = id
    city.country = country
    city.name = City::ALL_CITIES
    city.region = "00"
    city.save
    id = id + 1
  end
end