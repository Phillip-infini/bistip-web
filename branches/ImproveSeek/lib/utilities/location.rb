module Location
  include CheckLocation
  
  # virtual attribute getter to get origin location
  def origin_location
    origin_city.name + ", " + origin_city.country.name if origin_city
  end

  # virtual attribute setter to accept origin location
  def origin_location=(location)
    self.origin_city = check_location(location)
  end

  # virtual attribute getter to get destination location
  def destination_location
    destination_city.name + ", " + destination_city.country.name if destination_city
  end

  # virtual attribute setter to accept destination location
  def destination_location=(location)
    self.destination_city = check_location(location)
  end

end
