require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  
  test "should find country" do
    country_id = countries(:indonesia).id
    assert_nothing_raised {Country.find(country_id)}
  end
  
end
