require 'test_helper'

class NotificationStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "user :one giving a positive reputation to user :dono" do

    #login
    full_login_as(:mactavish)

    user = users(:mactavish)

    #view user notification
    get user_notifications_path(user)
    
    assert_response :success
  end
end
