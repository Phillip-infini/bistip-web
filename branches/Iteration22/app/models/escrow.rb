# a class that represent a escrow transaction between 2 user
class Escrow < ActiveRecord::Base

  PAYPAL_CERT_PEM = File.read("#{Rails.root}/certs/#{APP_CONFIG[:paypal_cert_file]}")
  APP_CERT_PEM = File.read("#{Rails.root}/certs/app_cert.pem")
  APP_KEY_PEM = File.read("#{Rails.root}/certs/app_key.pem")

  FEE_COST = 0.08

  DEFAULT_CURRENCY = Currency.find_by_name('USD')

  belongs_to :buyer, :class_name => "User", :foreign_key => 'buyer_id'
  belongs_to :seller, :class_name => "User", :foreign_key => 'seller_id'
  belongs_to :message, :class_name => "Message", :foreign_key => 'message_id'
  belongs_to :currency, :class_name => "Currency", :foreign_key => 'currency_id'
  has_many :logs, :class_name => 'EscrowLog', :foreign_key => 'escrow_id'

  attr_writer :current_step

  validates :amount, :presence => true, :valid_escrow_amount => true, :if => lambda { |o| o.current_step == "details" }
  validates :item, :presence => true, :length => {:within => 5..50}, :if => lambda { |o| o.current_step == "details" }

  # list of escrow transaction state
  STATE_BUYER_INITIATED = 'buyer_initiated'
  STATE_PAYPAL_NOTIFIED = 'paypal_notified'
  STATE_SELLER_GOT_ITEM = 'seller_got_item'
  STATE_BUYER_RECEIVED_ITEM = 'buyer_received_item'
  STATE_SELLER_REQUESTED_PAYOUT = 'seller_requested_payout'
  STATE_SELLER_RECEIVED_PAYOUT = 'seller_received_payout'
  STATE_BISTIP_RELEASED_SELLER_PAYOUT = 'bistip_released_seller_payout'
  STATE_BISTIP_RELEASED_BUYER_PAYOUT = 'bistip_released_buyer_payout'
  STATE_BUYER_RESUMED = 'buyer_resumed'
  STATE_SELLER_CANCELLED = 'seller_cancelled'
  STATE_BUYER_REQUESTED_PAYOUT = 'buyer_requested_payout'
  STATE_BUYER_RECEIVED_PAYOUT = 'buyer_received_payout'
  STATE_INVALID = 'invalid'

  # event
  before_create :generate_invoice_number
  
  # scope
  scope :user_all_escrows, lambda {|user_id| {:conditions => ["buyer_id = ? or seller_id = ?", user_id, user_id]}}

  LOG_STATE_MAP = {BuyerInitiatedLog => STATE_BUYER_INITIATED,
                   PaypalNotifiedLog => STATE_PAYPAL_NOTIFIED,
                   SellerGotItemLog => STATE_SELLER_GOT_ITEM,
                   BuyerReceivedItemLog => STATE_BUYER_RECEIVED_ITEM,
                   SellerRequestedPayoutLog => STATE_SELLER_REQUESTED_PAYOUT,
                   SellerReceivedPayoutLog => STATE_SELLER_RECEIVED_PAYOUT,
                   BistipReleasedSellerPayoutLog => STATE_BISTIP_RELEASED_SELLER_PAYOUT,
                   BistipReleasedBuyerPayoutLog => STATE_BISTIP_RELEASED_BUYER_PAYOUT,
                   SellerReceivedPayoutLog => STATE_SELLER_RECEIVED_PAYOUT,
                   SellerCancelledLog => STATE_SELLER_CANCELLED,
                   BuyerResumedLog => STATE_BUYER_RESUMED,
                   BuyerRequestedPayoutLog => STATE_BUYER_REQUESTED_PAYOUT,
                   BuyerReceivedPayoutLog => STATE_BUYER_RECEIVED_PAYOUT}

  # get the current state of escrow determine by latest log type
  def current_state
    unless last_log.blank?
      Escrow.get_log_state(last_log)
    end
  end

  # get last action from user on the escrow
  def last_log
    logs = self.logs(:order_by => 'created_at asc')
    unless logs.blank?
      logs.last
    end
  end

  # get encrypted paypal parameters and argument
  def paypal_encrypted(return_url, notify_url)
    values = {
      :business => APP_CONFIG[:paypal_email],
      :cmd => '_xclick',
      :upload => 1,
      :invoice => self.invoice,
      :amount => self.amount,
      :item_name => I18n.t('escrow.paypal.description', :item => self.item, :buyer => self.buyer.username, :seller => self.seller.username),
      :notify_url => notify_url,
      :return => return_url,
      :rm => 1,
      :cbt => 'back to Bistip',
      :no_shipping => 1,
      :no_note => 1,
      :shipping => 0.00,
      :handling => 0.00,
      :cert_id => APP_CONFIG[:paypal_cert_id]
    }
    encrypt_for_paypal(values)
  end

  # form wizard: show the current step in wizard
  def current_step
    @current_step || steps.first
  end

  # form wizard: list of steps
  def steps
    %w[details confirmation]
  end

  # form wizard: get the next step
  def next_step
    self.current_step = steps[steps.index(current_step)+1]
  end

  # form wizard: get the previous step
  def previous_step
    self.current_step = steps[steps.index(current_step)-1]
  end

  # form wizard: is it currently on the first step
  def first_step?
    current_step == steps.first
  end

  # form wizard: is it currently on the last step
  def last_step?
    current_step == steps.last
  end

  # form wizard: is all step in the wizard already pass validation
  def all_valid?
    steps.all? do |step|
      self.current_step = step
      valid?
    end
  end

  # static method to calculate processing fee
  def self.calculate_amount_after_fee(amount)
    unless amount.blank?
      amount = amount.to_f
      amount - (amount * FEE_COST)
    else
      0.00
    end
  end

  # static method to get the state string based on a log
  def self.get_log_state(log)
    state = LOG_STATE_MAP[log.class]
    unless state.blank?
      state
    else
      STATE_INVALID
    end
  end

  # get ampunt with currency
  def amount_with_currency
    self.currency.name + ' ' + self.amount.to_s
  end

  # get ampunt with currency
  def amount_after_fee_with_currency
    self.currency.name + ' ' + Escrow.calculate_amount_after_fee(self.amount).to_s
  end

  private

    # encrypt info to send to paypal
    def encrypt_for_paypal(values)
      signed = OpenSSL::PKCS7::sign(OpenSSL::X509::Certificate.new(APP_CERT_PEM), OpenSSL::PKey::RSA.new(APP_KEY_PEM, ''), values.map { |k, v| "#{k}=#{v}" }.join("\n"), [], OpenSSL::PKCS7::BINARY)
      OpenSSL::PKCS7::encrypt([OpenSSL::X509::Certificate.new(PAYPAL_CERT_PEM)], signed.to_der, OpenSSL::Cipher::Cipher::new("DES3"), OpenSSL::PKCS7::BINARY).to_s.gsub("\n", "")
    end

    # method to generate invoice number
    def generate_invoice_number
      self.invoice = UUIDTools::UUID.timestamp_create
    end

end
