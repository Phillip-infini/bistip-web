module CheckLocation
  # check give location, whether its a valid city, countries
  # if not then return nil
  def check_location(location)
    # return if nothing given
    if location.nil? or location.empty?
      return
    end

    # remove all whitespace
    location.strip!

    # check if the string value is in <city>, <country> format
    if location =~ /^.+,.+$/
      splitted = location.split(',')
      city = splitted.shift
      city.strip!

      country = splitted.join(',')
      country.strip!

      # do a look up for the city
      city_match_results = City.find_all_by_name(city)
      if city_match_results.size > 0
        city_match_results.each do |city_match_result|
          if city_match_result.country.name.casecmp(country) == 0
            return city_match_result
          end
        end
        return nil
      else
        return nil
      end

    # just <city> input
    else
      city = location

      # do a look up for the city
      city_match_results = City.find_all_by_name(city)

      # must only match one city, if not then it's ambiguous without country
      if city_match_results.size == 1
        return city_match_results[0]
      else
        return nil
      end
    end
  end

end