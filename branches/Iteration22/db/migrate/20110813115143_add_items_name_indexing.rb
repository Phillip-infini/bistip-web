class AddItemsNameIndexing < ActiveRecord::Migration
  def self.up
    execute('ALTER TABLE items ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_items_name ON items (name(500))')
  end

  def self.down
    execute('DROP INDEX fulltext_items_name ON items')
    execute('ALTER TABLE items ENGINE = innodb')
  end
end
