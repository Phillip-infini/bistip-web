require 'nokogiri'

class PopulatedLanguageTable < ActiveRecord::Migration
  def self.up
    f = File.open("#{Rails.root}/db/data/languages.xml")
    doc = Nokogiri::XML(f)
    f.close
    
    languages_name = doc.xpath("//englishName/text()").to_a
    
    languages_name.each{|lang|
      english_name = lang.to_s
      puts "inserting : #{english_name} into Languages table..."
      Language.create(:english_name => english_name)
    }
  end

  def self.down
  end
end
