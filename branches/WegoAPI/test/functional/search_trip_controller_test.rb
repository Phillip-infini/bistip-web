require 'test_helper'

class SearchTripControllerTest < ActionController::TestCase
  
  test "render search trip form" do
    get :index
    assert_response :success
    assert_template 'index'
  end

  test "search should return result" do
    get :index, :origin => 'jakarta', :destination => 'sydney',
      :departure_date => Date.current.advance(:days => 3).to_s,
      :departure_date_predicate => 'before'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:trips)
    assert_not_nil assigns(:suggested_routes)
    assert_equal 1, assigns(:trips).size
    assert_equal 2, assigns(:suggested_routes).size
  end

  test "search should return result destination only" do
    get :index, :destination => 'sydney'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:trips)
    assert_equal 3, assigns(:trips).size
  end

  test "search should return result origin all cities" do
    get :index, :origin => '<all cities>, indonesia', :destination => 'sydney',
      :departure_date => Date.current.advance(:days => 3).to_s,
      :departure_date_predicate => 'before'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:trips)
    assert_equal 3, assigns(:trips).size
  end

  test "search should return result nearby" do
    get :index, :origin => 'singapore', :destination => 'jakarta',
      :departure_date => Date.current.advance(:days => 3).to_s,
      :departure_date_predicate => 'before'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:trips)
    assert_not_nil assigns(:suggested_routes)
    assert_equal 1, assigns(:trips).size
    assert_equal 1, assigns(:suggested_routes).size
  end

  test "search should not return result" do
    get :index, :origin => 'jakarta', :destination => 'sydney',
      :departure_date => Date.current.advance(:days => 3).to_s,
      :departure_date_predicate => 'after'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:trips)
    assert_equal 0, assigns(:trips).size
  end
end
