require 'test_helper'

class SearchSeekControllerTest < ActionController::TestCase

  test "render search seek form" do
    get :index
    assert_response :success
    assert_template 'index'
  end

  test "search seek should return result" do
    get :index, :origin => 'jakarta', :destination => 'sydney'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:seeks)
    assert_equal 3, assigns(:seeks).size
  end
  
  test "search seek should return result destination only" do
    get :index, :destination => 'sydney'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:seeks)
    assert_equal 5, assigns(:seeks).size
  end

  test "search seek should return result origin allcity" do
    get :index, :origin => '<all cities>, indonesia', :destination => 'sydney'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:seeks)
    assert_equal 4, assigns(:seeks).size
  end

  test "search seek should not return result" do
    get :index, :origin => 'jakarta', :destination => 'medan'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:seeks)
    assert assigns(:seeks).size == 0
  end

end
