require 'test_helper'

class RelationsControllerTest < ActionController::TestCase
  
  test "should create relation" do
    login_as(:one)
    assert_difference('Relation.count') do
      post :new, :user => users(:dono).username
    end

    assert_redirected_to profile_path(users(:dono).username)
  end

  test 'should show user social page' do
    get :index, :user => users(:dono).username
    assert_response :success
  end

end
