require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "should login user and redirect" do 
    get login_path

    assert_response :success
    assert_template 'new'

    user_to_use = users(:one)
    post session_path, :email_or_username => user_to_use.email, :password => 'secret'

    assert_response :redirect
    assert_redirected_to dashboard_path

    follow_redirect!
    
    assert_response :success 
    assert_template 'index'
    assert_not_nil cookies[ApplicationController::AUTH_COOKIE]
  end

  test "should login user and update profile" do
    # login
    get login_path

    assert_response :success
    assert_template 'new'

    user_to_use = users(:one)
    post session_path, :email_or_username => user_to_use.email, :password => 'secret'

    assert_response :redirect
    assert_redirected_to dashboard_path

    follow_redirect!

    # edit
    get edit_profile_path

    assert_response :success
    assert_template 'edit'

    # update
    put user_path(user_to_use), :user => {:bio => "one's bio"}

    assert_response :redirect
    assert_redirected_to dashboard_profile_path

    follow_redirect!
  end

  test "should register new user and login" do
    # new user form
    get login_path

    assert_response :success
    assert_template 'new'

    # create user
    post users_path, :user => {:username => "newuser", :email => "newuser@gmail.com",
      :fullname => 'newuser',
      :city_location => 'jakarta',
      :password => "secret"}

    assert assigns(:user).valid?
    assert_response :redirect
    assert_redirected_to registered_users_path
    follow_redirect!

    # login
    post session_path, :email_or_username => "newuser@gmail.com", :password => 'secret'

    assert_response :redirect
    assert_redirected_to dashboard_path

    follow_redirect!
  end

  test 'should register new user and then do email validation' do
    # create user
    post users_path, :user => {:username => "newuser", :email => "newuser@gmail.com",
      :fullname => 'newuser',
      :city_location => 'jakarta',
      :password => "secret"}

    user = assigns(:user)
    assert assigns(:user).valid?
    assert_response :redirect
    assert_redirected_to registered_users_path
    follow_redirect!
    
    get validate_user_path(:id => user.id, :key => user.email_validation_key)
    assert user.email_validated?
  end

  test "should logout user and redirect" do
    # login
    
    get login_path

    assert_response :success
    assert_template 'new'

    user_to_use = users(:one)
    post session_path, :email_or_username => user_to_use.email, :password => 'secret'

    assert_response :redirect
    assert_redirected_to dashboard_path

    follow_redirect!

    # logout
    get logout_path
    
    assert_response :redirect 
    assert_redirected_to root_path
    assert_nil cookies[ApplicationController::AUTH_COOKIE]
    
    follow_redirect!
    
    assert_template 'index'
  end
  
end
