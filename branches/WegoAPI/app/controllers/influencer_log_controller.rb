class InfluencerLogController < ApplicationController

  # render top bistip point earner
  def index
    @total = InfluencerLog.all.size
    @influencer = InfluencerLog.top_influencer.paginate(:page=>params[:page],:per_page=>InfluencerLog::PAGE)
  end

  # parse the url if ok then render a transit page
  def transit
    encodedurl = params[:code]

    if !encodedurl.nil?
      # decode param using Base32
      begin
        decodedurl = Base32.decode(encodedurl)
        # split encoded with whitespace character
        data = decodedurl.to_s.split(' ')
        raise ActiveRecord::RecordNotFound if data.length != 2
        trip_id = data[0]
        @trip = Trip.find(trip_id)
        raise ActiveRecord::RecordNotFound if @trip.nil?
      rescue Exception
        raise ActiveRecord::RecordNotFound
      end
    end
  end

  # create an influencer point
  def create
    # get decodeurl parameter value
    encodedurl = params[:code]
    
    if !encodedurl.nil?
      # decode param using Base32
      begin
        decodedurl = Base32.decode(encodedurl)
        # split encoded with whitespace character
        data = decodedurl.to_s.split(' ')
        raise ActiveRecord::RecordNotFound if data.length != 2
        trip_id = data[0]
        influencer_id = data[1]
        trip = Trip.find(trip_id)
        influencer = User.find(influencer_id)
        raise ActiveRecord::RecordNotFound if trip.nil? || influencer.nil?
        # check if trip.user not same with influencer
        if trip.user!=influencer
          ip_address = request.ip
          log = InfluencerLog.new(:ip_address=>ip_address,:influencer_id=>influencer_id,:trip_id=>trip_id)
          # save influencer log if trip's active an request not come from robot
          log.save if trip.active? && !is_robot && influencer.email_validated?
          redirect_to trip_path(trip)
        end
      rescue Exception
        raise ActiveRecord::RecordNotFound
      end
    end
  end
end
