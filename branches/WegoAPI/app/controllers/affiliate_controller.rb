class AffiliateController < ApplicationController
  
  include ::SslRequirement
  protect_from_forgery
  layout 'affiliate'

  AFFILIATE_AUTH_COOKIE = 'bistip_affiliate_auth'

  # filter method to force authentication
  def affiliate_authenticate
    if affiliate_logged_in?
      # do nothing
    else
      redirect_to affiliate_dashboard_login_path
    end
  end

  # Returns the currently logged in user or nil if there isn't one
  def current_affiliate
    return unless cookies.signed[AFFILIATE_AUTH_COOKIE]
    @current_affiliate ||= Affiliate.authenticate_with_salt(*cookies.signed[AFFILIATE_AUTH_COOKIE])
  end

  helper_method :current_affiliate

  # Predicate method to test for a logged in affiliate
  def affiliate_logged_in?
    current_affiliate.is_a? Affiliate
  end

  helper_method :affiliate_logged_in?

  # store user_id in cookie
  def create_auth_cookie(affiliate)
    # create permanent cookie if remember me
    if params[:remember_me]
      cookies.permanent.signed[AFFILIATE_AUTH_COOKIE] = {:value => [affiliate.id, affiliate.salt]}
    else
      # create normal cookie
      cookies.signed[AFFILIATE_AUTH_COOKIE] = {:value => [affiliate.id, affiliate.salt]}
    end
  end

  def handle
    redirect_to root_path
  end

end
