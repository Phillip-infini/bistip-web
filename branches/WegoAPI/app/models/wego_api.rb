require 'net/http'

class WegoApi

  KEY = '9fcbaf61131d957e9a10'
  TS_CODE = 'a1928'
  DEALS_JSON_URL = 'http://deals.wego.com/api/affiliates/deals.js'

  def self.get_deals_for_dashboard(current_user)
    per_page = 6
    location_code = 'ID'
    location_code = current_user.city.country.code unless current_user.city.blank?

    url = URI.parse(WegoApi::DEALS_JSON_URL)
    req = Net::HTTP::Get.new(url.path + "?key=#{WegoApi::KEY}&market=#{location_code}&per_page=#{per_page}&ts_code=#{WegoApi::TS_CODE}")
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    deals_json = JSON.parse(res.body)
    deals_json
  end

  def self.get_deals_for_email(current_user)
    per_page = 6
    location_code = 'ID'
    location_code = current_user.city.country.code unless current_user.city.blank?

    url = URI.parse(WegoApi::DEALS_JSON_URL)
    req = Net::HTTP::Get.new(url.path + "?key=#{WegoApi::KEY}&market=#{location_code}&per_page=#{per_page}&ts_code=#{WegoApi::TS_CODE}")
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    deals_json = JSON.parse(res.body)
    deals_json
  end

end
