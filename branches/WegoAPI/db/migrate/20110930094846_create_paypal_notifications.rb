class CreatePaypalNotifications < ActiveRecord::Migration
  def self.up
    create_table :paypal_notifications do |t|
      t.text :params
      t.column :escrow_id, :integer, :references => :escrows
      t.string :status
      t.string :transaction_id

      t.timestamps
    end
  end

  def self.down
    drop_table :paypal_notifications
  end
end
