class CreateInfluencerLogs < ActiveRecord::Migration
  def self.up
    create_table :influencer_logs do |t|
      t.column :ip_address, :string, :null => false
      t.column :trip_id, :integer, :null => false
      t.column :influencer_id, :integer, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :influencer_logs
  end
end
