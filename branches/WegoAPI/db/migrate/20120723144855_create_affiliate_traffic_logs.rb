class CreateAffiliateTrafficLogs < ActiveRecord::Migration
  def self.up
    create_table :affiliate_traffic_logs do |t|
      t.belongs_to :affiliate
      t.timestamps
    end
  end

  def self.down
    drop_table :affiliate_traffic_logs
  end
end
