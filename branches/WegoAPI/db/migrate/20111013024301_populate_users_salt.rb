class PopulateUsersSalt < ActiveRecord::Migration
  def self.up
    User.all.each do |user|
      user.generate_salt
      user.save
    end
  end

  def self.down
    User.all.each do |user|
      user.update_attribute(:salt, nil)
    end
  end
end
