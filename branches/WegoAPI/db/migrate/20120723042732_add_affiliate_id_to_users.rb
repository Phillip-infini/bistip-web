class AddAffiliateIdToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :affiliate_id, :integer, :references => :affiliates
  end

  def self.down
    remove_column :users, :affiliate_id
  end
end
