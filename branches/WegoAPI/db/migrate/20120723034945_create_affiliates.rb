# Default value on MySQL
#   string :limit => 255
#   text, :limit => 65536
class CreateAffiliates < ActiveRecord::Migration
  def self.up
    create_table :affiliates do |t|
      t.string :name
      t.string :email
      t.string :hashed_password
      t.string :code
      t.string :salt
      
      t.timestamps
    end
    add_index(:affiliates, :id, :unique=>true)
  end

  def self.down
    drop_table :affiliates
  end
end
