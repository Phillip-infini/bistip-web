class CreateExternalLinkTracks < ActiveRecord::Migration
  def self.up
    create_table :external_link_tracks do |t|
      t.column :user_id, :integer, :null => true, :references => :users
      t.string :ref
      t.string :url
      t.timestamps
    end
  end

  def self.down
    drop_table :external_link_tracks
  end
end
