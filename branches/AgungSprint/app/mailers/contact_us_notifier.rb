# Class to send contact us email notification
class ContactUsNotifier < ActionMailer::Base
  default :from => "Bistip.com <willy@bistip.com>"
  
  def email_contact_us(name, email, subject, body)
    @name = name
    @email = email

    # dont use @subject or @body here it's a reserved key word
    @topic = subject
    @content =  body
    mail :to => 'team@bistip.com', :subject => 'Masukan'
  end

  def email_exception(message, trace)
    @message = message
    @trace = trace
    mail :to => 'team@bistip.com', :subject => 'Exception'
  end
end
