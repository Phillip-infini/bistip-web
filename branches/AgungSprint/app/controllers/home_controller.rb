class HomeController < ApplicationController

  def index
    count = 5
    if mobile_version?
      count = 2
    end
    @trips = Trip.random(count)
    @seeks = Seek.random(count)
  end
end
