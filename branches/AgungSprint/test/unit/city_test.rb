require 'test_helper'

class CityTest < ActiveSupport::TestCase

  test "should find city" do
    city_id = cities(:jakarta).id
    countries_id = countries(:indonesia).id
    city = City.find(city_id)
    assert_equal(countries_id, city.country.id)
  end
  
end
