require 'test_helper'

class PositiveReputationsControllerTest < ActionController::TestCase

  setup do
    @userx = users(:dono)
    @usery = users(:mactavish)
  end

  test "should get new" do
    login_as(:one)
    get :new, :user_id=>@usery.id
    assert_response :success
  end
  
  test "should get index of user's positive reputation" do
    login_as(:dono)
    get :index, :user_id=>@usery.id
    assert_response :success
    assert_not_nil assigns(:positives)
  end

  test "userX should give a positive reputation to userY" do
    login_as(:dono)
    assert_difference('PositiveReputation.count') do
      post :create, :user_id=>@usery.id, :positive_reputation => {
        :body=>"very good service",
        :giver=> @userx
      }
    end
    assert_redirected_to user_path(@usery)
  end
end
