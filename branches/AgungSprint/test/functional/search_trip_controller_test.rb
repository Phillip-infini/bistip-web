require 'test_helper'

class SearchTripControllerTest < ActionController::TestCase
  
  test "render search trip form" do
    get :index
    assert_response :success
    assert_template 'index'
  end

  test "search should return result" do
    get :index, :origin => 'jakarta', :destination => 'sydney',
      :departure_date => Date.current.advance(:days => 3).to_s,
      :departure_date_predicate => 'before'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:trips)
    assert assigns(:trips).size == 1
  end

  test "search should not return result" do
    get :index, :origin => 'jakarta', :destination => 'sydney',
      :departure_date => Date.current.advance(:days => 3).to_s,
      :departure_date_predicate => 'after'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:trips)
    assert assigns(:trips).size == 0
  end
end
