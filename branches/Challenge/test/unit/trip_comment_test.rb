require 'test_helper'

class TripCommentTest < ActiveSupport::TestCase
  test "should create comment on trip" do
    comment = TripComment.new
    comment.body = 'body 123'
    comment.trip = trips(:jktsyd)
    comment.user = users(:dono)
    assert_difference('CommentNotification.count') do
      assert comment.save
    end
  end

  test "should delete comment on trip" do
    cmt = comments(:trip1)
    comment = TripComment.find(cmt.id)
    assert comment.update_attribute(:deleted, true)
  end

  test "should create comment on trip with value space" do
    comment = TripComment.new
    comment.body = '     '
    comment.trip = trips(:tokjak)
    comment.user = users(:one)
    assert_no_difference('CommentNotification.count') do
      assert !comment.save
    end
  end

  test "should create comment on trip with myself" do
    comment = TripComment.new
    comment.body = '     '
    comment.trip = trips(:bogjak)
    comment.user = users(:one)
    assert_no_difference('CommentNotification.count') do
      assert !comment.save
    end
  end

  test "shouldn't create invalid comment" do
    comment = TripComment.new
    comment.body='123'
    comment.trip = trips(:bogjak)
    comment.user = users(:one)
    assert_no_difference('CommentNotification.count') do
      assert !comment.save
    end
    assert !comment.valid?
    assert comment.errors[:body].any?

  end

  test "should create comment reply" do
    comment = TripComment.new
    comment.body = 'body 123'
    comment.trip = trips(:jktsyd)
    comment.user = users(:dono)
    assert_difference('CommentNotification.count') do
      assert comment.save
    end

    rcomment = TripComment.new
    rcomment.body = 'replying dono'
    rcomment.user = users(:one)
    rcomment.trip = trips(:jktsyd)
    rcomment.reply_to_id=comment.id
    assert_difference('Comment.count') do
      assert rcomment.save
    end
    assert rcomment.reply?
    assert !rcomment.thread_starter?

  end
   
  test "should delete comment reply" do
    rcomment = TripComment.new
    rcomment.body = 'replying dono'
    rcomment.user = users(:one)
    rcomment.trip = trips(:jktsyd)
    rcomment.reply_to_id=comments(:trip3).id
    assert_difference('Comment.count') do
      assert rcomment.save
    end
    assert rcomment.reply?
    assert !rcomment.thread_starter?
    assert rcomment.update_attribute(:deleted, true)
  end

end
