class SubtractForEscrowCashbackLog < PointLog

  NEEDED_POINT = 80

  # do operation on point given
  def do_point(amount)
    amount - self.amount
  end

# generate title label
def title_label
  escrow = Escrow.find(self.data_id)
  I18n.t('dashboard.points.label.escrow_cashback_title', :seller => escrow.seller.username, :item => escrow.item)
end

# generate point label
def point_label
  I18n.t('general.points_minus', :points => self.amount)
end

end
