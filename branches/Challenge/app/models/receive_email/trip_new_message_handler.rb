# handler for MessageReply
class TripNewMessageHandler
  include ReceiveEmailChainable

  # set next in chain
  def initialize(link = nil)
    next_in_chain(link)
  end

  # try to handle the request
  def handle(params)
    if email_type(params) == Trip::EMAIL_REPLY_PREFIX
      # check if receiver email is valid referring to a message
      receiver_email = params['recipient']
      trip = retrieve_trip(receiver_email)
      return if trip.blank?

      # check if sender really exist
      sender_email = params['sender']
      sender = User.find_by_email(sender_email)
      return if (sender.blank? or trip.owner?(sender))

      # build a message/thread starter
      message = Message.new
      message.sender = sender
      message.email_reply = true
      message.receiver = trip.user
      message.trip = trip
      message.body = params["stripped-text"]
      message.body_original = params["body-plain"]

      # process all attachments:
      attachment_count = params['attachment-count'].to_i
      attachment_count.times do |i|
        stream = params["attachment-#{i+1}"]
        # filename = stream.original_filename
        asset = message.assets.build
        asset.attachment = stream
      end

      unless message.save
        # notify the sender about the problem with their email reply
        Notifier.delay.email_user_message_not_valid(trip.email_reply_address, message.sender, message.receiver, message.errors.full_messages)
      end
    else
      @next.handle(params)
    end
  end

  private
  
    # check if receiver email is valid, can be link into a trip
    def retrieve_trip(receiver_email)
      # split the receiver email, must result in two parts
      receiver_email_splitted = receiver_email.split('@')
      return nil unless receiver_email_splitted.size == 2

      # parse the head and check if it's in right format and valid salt
      head = receiver_email_splitted.first
      head_splitted = head.split(Trip::EMAIL_REPLY_SEPARATOR)
      return nil unless head_splitted.size == 3

      trip_id = head_splitted[1]
      trip_salt = head_splitted[2]
      Trip.find(:first, :conditions => {:id => trip_id, :salt => trip_salt})
    end
    
end