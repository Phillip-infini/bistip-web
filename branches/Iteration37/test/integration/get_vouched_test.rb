require 'test_helper'

class GetVouchedTest < ActionDispatch::IntegrationTest
  fixtures :all

  # Get vouched.
  test "userX should get vouched by userY" do
    one = users(:one)

    full_login_as(:mactavish)

    get "/recommend/Allan"

    assert_template :handle

    assert_response :success

    get new_user_review_path(one)

    post user_reviews_path(one), :type => 'PositiveReview', :review => {
      :body => "very good service",
      :invitation => true
    }

    assert_response :redirect

  end
end
