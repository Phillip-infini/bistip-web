# City class represent a City
class City < ActiveRecord::Base

  # constants
  ALL_CITIES = "<all cities>"
  ANYWHERE = '<anywhere>'

  # scope
  default_scope :order => "cities.name ASC"
  scope :match_cities_and_countries_name, lambda { |name| {:include => :country, :conditions => ['cities.name LIKE ? OR cities.alias LIKE ? OR countries.name LIKE ? OR countries.alias LIKE ?', "%#{name}%", "%#{name}%", "%#{name}%", "%#{name}%"]}}
  scope :exclude_all_cities, :conditions => ["cities.name <> ?", "#{ALL_CITIES}"]
  scope :ids_not_in, lambda { |ids| {:conditions => ["cities.id NOT IN (?)", ids]}}
  scope :exclude_anywhere, :conditions => ["cities.name <> ?", "#{ANYWHERE}"]

  # geocoder
  reverse_geocoded_by :latitude, :longitude

  # relationships
  belongs_to :country

  # helper to check whether a city represent an all cities of a country
  def all_cities?
    self.name == ALL_CITIES
  end

  # helper to check if this city is a placeholder that represent a continent
  def is_continent_country?
    self.country.code == Country::CONTINENT_CODE
  end

  # get anywhere placeholder
  def self.get_anywhere_placeholder
    City.find_by_name(ANYWHERE)
  end

end
