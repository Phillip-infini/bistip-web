require 'paperclip'

class UsersController < ApplicationController
  ssl_required :new, :create, :edit, :update
  before_filter :authenticate, :only => [:edit, :update, :registered]

  # User profile view
  def show
    if params[:username]
      @user = User.find_by_username(params[:username])
    else
      @user = User.find(params[:id])
    end

    # user not found by id or username
    if !@user
      redirect_to error_not_found_path
    end
    
  end

  # New user form
  def new
    if logged_in?
      redirect_to root_path
    else
      @user = User.new
    end
  end

  # Edit user form
  def edit
    @user = current_user
  end

  # Update user
  def update
    @user = User.find_by_id(current_user.id)
    params[:user][:languages] = generate_user_languages(params[:user][:languages]) if params[:user][:languages]
    
    if @user.update_attributes(params[:user])
      flash[:notice] = t('user.update.message.success')
      redirect_to dashboard_profile_path
    else
      put_model_errors_to_flash(@user.errors)
      render :action => 'edit'
    end
  end

  # user not validated yet view
  def not_validated
    # do nothing just rendering view
  end

  # Create user
  def create
    @user = User.new(params[:user])

    # store new user current IP address
    @user.ip = request.ip

    if @user.valid? and User.allow_create_by_ip?(@user.ip)
      if @user.save
        if !@user.uid.nil? && !@user.provider.nil?
          flash[:longer_notice] = t('user.create.oauth.success')

          # logged in user straightaway
          create_auth_cookie(@user)
          redirect_back registered_users_path
        else
          flash[:longer_notice] = t('user.create.message.success')

          # logged in user straightaway
          create_auth_cookie(@user)
          redirect_back registered_users_path
        end
      else
        handle_invalid_user(@user)
      end
    else
      handle_invalid_user(@user)
    end
  end

  # Validate User account
  def validate
    if params[:id] && params[User::VALIDATION_KEY_PARAM]
      user = User.find(params[:id])
      if params[User::VALIDATION_KEY_PARAM] && user.email_validation_key
        user.email_validation_key = nil
        user.save(:validate => false)

        # make user logged in straight away
        create_auth_cookie(user)
        flash[:notice] = t('user.validate.message.success')
      end
    end
    redirect_back root_path
  end

  # Resend validation email of a User
  def resend_validation
    if logged_in?
      current_user.send_email_validation
      flash[:notice] = t('user.resend_validation.message.success')
    end
    redirect_to root_path
  end

  # get trips that belongs to a user
  def trips
    @user = User.find(params[:id])
    @trips = Trip.order('created_at desc').find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => Trip::SEARCH_PER_PAGE)
  end

  # get seeks that belongs to a user
  def seeks
    @user = User.find(params[:id])
    @seeks = Seek.order('created_at desc').find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => Seek::SEARCH_PER_PAGE)
  end

  def auth
    @user = User.new
  end

  # just to maintain legacy link to reputation
  def reputation
    @user = User.find(params[:id])
    redirect_to user_reviews_path(@user)
  end

  # view user reviews page
  def reviews
    @user = User.find(params[:id])
    @reviews = @user.received_reviews
  end
  
  private
    def nilly?(value)
      if value.nil? or value.empty?
        true
      else
        false
      end
    end

    # handle when invalid user
    def handle_invalid_user(user)
      put_model_errors_to_flash(user.errors)
      if nilly?(user.uid) && nilly?(user.provider)
        render :action => "new"
      else
        render :action => "auth"
      end
    end
    
    def generate_user_languages(params_language)
      languages = Array.new
      params_language.split(",").each{|lang|
        language = Language.find_by_english_name(lang)
        languages << language unless language.nil?
      }
      return languages
    end
end
