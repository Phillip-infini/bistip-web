class CreateInsuranceLogs < ActiveRecord::Migration
  def self.up
    create_table :insurance_logs, :option => 'ENGINE = MyISAM' do |t|
      t.column :insurance_id, :integer, :null => false, :references => :insurance
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :insurance_logs
  end
end
