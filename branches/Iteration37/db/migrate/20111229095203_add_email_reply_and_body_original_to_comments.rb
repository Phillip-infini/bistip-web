class AddEmailReplyAndBodyOriginalToComments < ActiveRecord::Migration
  def self.up
    add_column :comments, :email_reply, :boolean, :default => false
    add_column :comments, :body_original, :text
  end

  def self.down
    remove_column :comments, :email_reply
    remove_column :comments, :body_original
  end
end
