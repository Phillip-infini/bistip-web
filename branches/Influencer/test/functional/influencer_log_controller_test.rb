require 'test_helper'

class InfluencerLogControllerTest < ActionController::TestCase
  # Replace this with your real tests.
  test "should create one point in influencer log" do
    login_as(:dono)

    influencer = users(:dono)
    trip = trips(:jktsyd)

    assert_not_nil(influencer)
    assert_not_nil trip

    assert trip.active?
    assert_difference('InfluencerLog.count') do
      post :create, :influence=>{
        :influencer_id=>influencer.id,
        :trip_id=>trip.id,
        :ip_address=>'192.168.1.44'
      }
    end
    
    assert_redirected_to trip_path(trip)
  end

  test "should Not create one point in influencer where IP already exist" do
    login_as(:dono)

    influencer = users(:dono)
    trip = trips(:jktsyd)

    assert_not_nil(influencer)
    assert_not_nil trip

    assert trip.active?
    assert_no_difference('InfluencerLog.count') do
      post :create, :influence=>{
        :influencer_id=>influencer.id,
        :trip_id=>trip.id,
        :ip_address=>'192.168.1.44'
      }
    end

    assert_redirected_to trip_path(trip)
  end
end
