require 'test_helper'

class CitiesControllerTest < ActionController::TestCase

  test "should get city match" do
    get :index, :format => 'js', :term => 'art'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:cities)
  end
  
end
