  BistipWeb::Application.routes.draw do

  resources :forums, :only => [:index, :show] do
    resources :topics do
      resources :posts do
        member do
          get 'quote'
          post 'create_quote'
        end
      end
    end
  end

  resources :seeks, :except => [:index] do
    resources :seek_comments, :as => "comments", :path => "comments"
    member do
      get 'match'
    end
  end

  resources :trips, :except => [:index] do
    resources :trip_comments, :as => "comments", :path => "comments"
    member do
      get 'match'
    end
    collection do
      get 'new_routine'
    end
  end
  
  # user route
  resources :users, :except => [:index, :destroy] do
    resources :positive_reputations
    resources :negative_reputations
    resources :user_configuration, :as => "configuration", :path => "configuration"
    resources :messages do
      collection do
        get 'sent'
      end
    end
    member do
      get 'validate'
      get 'not_validated'
      get 'resend_validation'
      get 'trips'
      get 'seeks'
      get 'registered'
      get 'mypoint'
    end
  end

  # home route
  root :to => "home#index"

  # contact us
  resource :contact_us, :only => [:new, :create]
#  resource :influencer_logs, :only => [:create]
  # session route
  resource :session
  match '/login' => "sessions#new", :as => "login"
  match '/logout' => "sessions#destroy", :as => "logout"
  match '/users/auth' => "users#auth", :as=> "auth"

  # search trip route
  match '/search_bistiper' => 'search_trip#index', :as => "search_trip"

  # search seek route
  match '/search_wanted_bistiper' => 'search_seek#index', :as => "search_seek"

  # search user route
  match '/search_user' => 'search_user#index', :as => "search_user"
  
  # password route
  match '/passwords/reset' => "passwords#reset", :as => "reset_password"
  match '/passwords/send_email' => "passwords#send_email", :as => "send_email_password"
  match '/passwords/reset_edit' => "passwords#reset_edit", :as => "reset_edit_password"
  match '/passwords/reset_update' => "passwords#reset_update", :as => "reset_update_password"
  match '/passwords/edit' => "passwords#edit", :as => "edit_password"
  match '/passwords/update' => "passwords#update", :as => "update_password"
  
  # cities route
  match '/cities/index' => 'cities#index', :as => 'cities_index'

  # comments trip route
  match '/trips' => 'trips#show', :as => 'trips_show'
  
  # comments seek route
  match '/seeks' => 'seeks#show', :as => 'seeks_show'

  # content
  match '/terms_of_service' => 'content#terms_of_service', :as => 'terms_of_service'
  match '/privacy_policy' => 'content#privacy_policy', :as => 'privacy_policy'
  match '/guide' => 'content#guide', :as => 'guide'
  match '/about' => 'content#about', :as => 'about'
  match '/how' => 'content#how', :as => 'how'
  match '/faq' => 'content#faq', :as => 'faq'
  match '/new' => 'content#new', :as => 'new'
  match '/media' => 'content#media', :as => 'media'
  match '/registered' => 'content#registered', :as => 'registered'
  match '/rekber' => 'content#rekber', :as => 'rekber'

  # fail authentication
  match '/auth/failure', :to => 'sessions#failure'

  # match callback :provider
  match '/auth/:provider/callback', :to => 'sessions#create'

  # errors
  match '/error_not_found' => 'errors#render_error', :as => 'error_not_found'

  # profile vanity url
  match '/u/:username', :controller => 'users', :action => 'show', :as => 'profile'

  # resend email validation
  match '/resend_email_validation' => 'resend_email_validation#index', :as => 'resend_email_validation'

  match '/expire_cloud_cache' => "home#expire_cloud", :as => "expire_cloud"

  match '/i/:decodedurl', :controller => 'influencer_log', :action => 'transit', :as => 'influence'
  match '/in/:decodedurl', :controller => 'influencer_log', :action => 'create', :as => 'insert'
  match '/top_influencer' => "influencer_log#index"
  # match everything else and route to render error
  match '*a', :to => 'errors#render_error', :constraints => lambda{|request| !request.path.starts_with?("/admin")}

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
  #match '/trip_comments/destroy' => 'trip_comments#destroy', :as => 'trip_comments_destroy'
end
