require 'paperclip'

class UsersController < ApplicationController

  before_filter :authenticate, :only => [:edit, :update]

  # User profile view
  def show
    if params[:username]
      @user = User.find_by_username(params[:username])
    else
      @user = User.find(params[:id])
    end

    # user not found by id or username
    if !@user
      redirect_to error_not_found_path
    end
  end

  # New user form
  def new
    @user = User.new
  end

  # Edit user form
  def edit
    @user = current_user
  end

  # Update user
  def update
    @user = User.find_by_id(current_user.id)
    if @user.update_attributes(params[:user])
      flash[:notice] = t('user.update.message.success')
      redirect_to(profile_path(@user.username))
    else
      put_model_errors_to_flash(@user.errors)
      render :action => 'edit'
    end
  end

  # user not validated yet view
  def not_validated
    # do nothing just rendering view
  end

  # Create user
  def create
    @user = User.new(params[:user])

    captcha_valid = false

    # for social login and mobile no captcha is needed
    if !@user.uid.nil? && !@user.provider.nil? || mobile_version?
      captcha_valid = true
    else
      captcha_valid = verify_recaptcha(:model => @user,:message => t('activerecord.errors.models.user.attributes.captcha.wrong'), :attribute => t('user.field.label.captcha'))
    end

    if captcha_valid && @user.valid?
      if @user.save
        if !@user.uid.nil? && !@user.provider.nil?
          flash[:notice] = t('user.create.oauth.success')

          # logged in user in straightaway
          session[:user_id] = @user.id
          redirect_to registered_path
        else
          flash[:notice] = t('user.create.message.success')

          # logged in user in straightaway
          session[:user_id] = @user.id
          redirect_to registered_path
        end
      else
        handle_invalid_user(@user)
      end
    else
      handle_invalid_user(@user)
    end
  end

  # Validate User account
  def validate
    if params[:id] && params[User::VALIDATION_KEY_PARAM]
      user = User.find(params[:id])
      if params[User::VALIDATION_KEY_PARAM] && user.email_validation_key
        user.email_validation_key = nil
        user.save

        # create session with user_id key to login the user
        session[:user_id] = user.id
        flash[:notice] = t('user.validate.message.success')
      end
    end
    redirect_back root_path
  end

  # Resend validation email of a User
  def resend_validation
    if logged_in?
      current_user.send_email_validation
      flash[:notice] = t('user.resend_validation.message.success')
    end
    redirect_to root_path
  end

  # get trips that belongs to a user
  def trips
    @user = User.find(params[:id])
    @trips = Trip.unscoped.find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => 5)
  end

  # get seeks that belongs to a user
  def seeks
    @user = User.find(params[:id])
    @seeks = Seek.unscoped.find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => 5)
  end

  def auth
    @user = User.new
  end

  def mypoint
    @user = User.find(params[:id])
    @data = InfluencerLog.influencer_list(@user).paginate(:page=>params[:page],:per_page=>3)
    if current_user
      redirect_to profile_path(@user.username) if current_user!=@user
    else
      redirect_to profile_path(@user.username)
    end
  end
  
  def nilly?(value)
    if value.nil? or value.empty?
      true
    else
      false
    end
  end

  private
  #handle when invalid user
    def handle_invalid_user(user)
      flash.delete(:recaptcha_error)
        put_model_errors_to_flash(user.errors)
        if nilly?(user.uid) && nilly?(user.provider)
          render :action => "new"
        else
          render :action => "auth"
        end
    end
end
