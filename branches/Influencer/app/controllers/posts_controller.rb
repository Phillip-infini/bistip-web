class PostsController < ApplicationController
  
  before_filter :load_topic
  before_filter :authenticate, :only => [:new, :edit, :create, :create_quote, :update, :destroy, :quote]

  layout 'forum'

  # new post form
  def new
    @post = Post.new
    @post.topic = @topic
    @post.user = current_user
  end

  # new post form from a quote
  def quote
    @post = Post.new
    @post.topic = @topic
    @post.user = current_user
    @quoted_post = Post.find(params[:id])
  end

  # edit post form
  def edit
    @post = Post.find(params[:id])
  end

  # create post
  def create
    @post = Post.new(params[:post])
    @post.topic = @topic
    @post.user = current_user

    if @post.save
      # touch topic 'sundul'
      @topic.touch

      # send email to forum owner
      send_forum_email(@topic.forum, @topic, @post)
      flash[:notice] = t('post.create.message.success')
      redirect_to(forum_topic_path(@topic.forum, @topic, :page => @topic.posts_last_page))
    else
      put_model_errors_to_flash(@post.errors)
      render :action => "new"
    end
  end

  # create from a quote
  def create_quote
    @post = Post.new
    @post.body = params[:body]
    @quoted_post = Post.find(params[:quoted_post_id])

    # set quoted data to new post
    @post.quoted = @quoted_post.body
    @post.quoted_from = @quoted_post.user
    @post.topic = @topic
    @post.user = current_user

    if @post.save
      # send email to forum owner
      send_forum_email(@topic.forum, @topic, @post)
      flash[:notice] = t('post.create.message.success')
      redirect_to(forum_topic_path(@topic.forum, @topic, :page => @topic.posts_last_page))
    else
      put_model_errors_to_flash(@post.errors)
      render :action => "quote"
    end
  end

  # update a post
  def update
    @post = current_user.posts.find(params[:id])

    if @post.update_attributes(params[:post])
      flash[:notice] = t('post.update.message.success')
      redirect_to(forum_topic_path(@topic.forum, @topic, :page => @post.find_page))
    else
      put_model_errors_to_flash(@post.errors)
      render :action => "edit"
    end
  end

  # soft delete a post
  def destroy
    @post = current_user.posts.find(params[:id])
    page_to_go = @post.find_page
    @post.deleted = true
    @post.save
    flash[:notice] = t('post.destroy.message.success')
    redirect_to(forum_topic_path(@topic.forum, @topic, :page => page_to_go))
  end

  private

    def load_topic
      @topic = Topic.find(params[:topic_id])
    end
end
