class Post < ActiveRecord::Base
  # constants
  BODY_MAXIMUM_LENGTH = 30000

  # validation
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH }, :presence => true

  # relationship
  belongs_to :user
  belongs_to :topic
  belongs_to :quoted_from, :class_name => "User", :foreign_key => 'quoted_from_id'

  # scope
  default_scope :order => 'created_at', :conditions => 'posts.deleted = false'

  # check if given user is the owner of the post
  def owner?(user)
    self.user == user
  end

  # title of the post
  def name
    Sanitize.clean(self.title)
  end

  # find which page this post belongs to
  def find_page
    topic = self.topic
    index = topic.posts.index(self) + 1
    puts index
    puts (index / Topic::POSTS_PER_PAGE)
    ((index * 1.0)/ Topic::POSTS_PER_PAGE).ceil
  end

  # Is this post quote other post?
  def quote?
    !self.quoted.blank?
  end

  # has this post even been edited?
  def edited?
    self.created_at != self.updated_at
  end
end
