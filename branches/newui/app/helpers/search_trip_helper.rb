module SearchTripHelper

  # check if any search parameter s given
  def any_search_trip_parameter_specified?
    if !params[:origin].blank? or
       !params[:destination].blank? or
       !params[:departure_date].blank? or
       !params[:departure_date_predicate].blank? or
       !params[:notes].blank?
      true
    else
      false
    end
  end
end
