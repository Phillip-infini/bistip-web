class PositiveReputationsController < ApplicationController

  before_filter :authenticate, :not_self, :only => [:new, :create]

  # render new positive reputation form
  def new
    @user = User.find(params[:user_id])
    if PositiveReputation.last_24_hour(current_user.id, @user.id).count >= 1
      flash[:alert] = t('reputation.new.not_allowed.positive')
      redirect_to profile_path(@user.username)
    end

    if params[:invitation]
      flash.now[:notice] = t('reputation.new.invitation', :receiver => @user.username)
    end
  end

  # render positive reputations of a user
  def index
    @user = User.find(params[:user_id])
    @positives = @user.received_positive_reputations.paginate(:page => params[:page], :per_page => Reputation::DEFAULT_PER_PAGE)

    # THIS IS NO GOOD
    # THIS IS JUST FOR FALLBACK TO KEEP OLD URL AND MOBILE VERSION WORKING
    if !mobile_version?
      redirect_to reputation_user_path(@user)
    end
  end

  # create a positive reputation
  def create
    @user = User.find(params[:user_id])
    
    if PositiveReputation.last_24_hour(current_user.id, @user.id).count >= 1
      flash[:alert] = t('reputation.new.not_allowed.positive')
      redirect_to profile_path(@user.username)
    else
      @positive = @user.received_positive_reputations.new(params[:positive_reputation])
      @positive.giver = current_user

      if @positive.save
        #email notification
        Notifier.delay.email_receiver_positive_reputation(@user, current_user, @positive) if email_receive_reputation(@user)
        #live notification
        PositiveReputationNotification.create!(:receiver=>@user, :sender=>current_user, :data_id=>@user.id)
        flash[:notice] = t('reputation.create.success.positive')
        redirect_to profile_path(@user.username)
      else
        put_model_errors_to_flash(@positive.errors)
        render :action => "new"
      end
    end
  end
end
