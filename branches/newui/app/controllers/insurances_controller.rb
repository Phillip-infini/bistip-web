class InsurancesController < ApplicationController
  before_filter :authenticate, :only => [:new, :create]

  def new
    message_id = params[:message_id]
    @message = Message.find(message_id)

    # Only sender or receiver can read the conversation!
    if (@message.sender == current_user or @message.receiver == current_user)
      # OK
    else
      redirect_to root_path
    end
  end

  def create
    message_id = params[:message_id]
    @message = Message.find(message_id)
    ContactUsNotifier.delay.email_insurance(@message, current_user)
    flash[:notice] = t('insurance.create.message.success')
    redirect_to root_path
  end

end
