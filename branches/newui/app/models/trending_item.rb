# To change this template, choose Tools | Templates
# and open the template in the editor.

class TrendingItem < ActiveRecord::Base
  validates :item, :uniqueness => true

  AFTER_ANCHOR_LOOKUP = 3

  def popularity
    total_count = TrendingItem.sum(:count)
    point = ((self.count * 1.0)/total_count)*100
    (point * 10**2).round.to_f / 10**2
  end

  # get all by checking database table cache
  def self.get_all
    trending_items = TrendingItem.find(:all, :order => "count DESC", :limit => 6)
    if trending_items.size == 0
      self.generate
      trending_items = TrendingItem.find(:all, :order => "count", :limit => 6)
    end
    trending_items
  end

  # static method to generate trending item data
  def self.generate
    if ::Rails.env == 'test'
      return
    end
    
    trending_items = Hash.new
    self.get_all_match.each do |match|
      # exception catch, anything happen then ignore the text
      begin
        # get words after anchor
        words_after = self.get_words_after_anchor(match)

        # get item words
        # puts words_after.join(" ")
        item_words = find_item_words(words_after)
        item_words.strip!

        # only include non empty word and word that has length > 2
        if !item_words.blank? and item_words.length > 2
          # build hash of result
          if trending_items.has_key?(item_words)
            trending_items[item_words] = trending_items[item_words] + 1
          else
            trending_items[item_words] = 1
          end
        end
      rescue Exception => e
        # what to do?
      end
    end

    # include the item word that must be included
    IncludeItem.generate_all_in_array.each do |include_item|
      if trending_items.has_key?(include_item)
        trending_items[include_item] = trending_items[include_item] + 1
      else
        trending_items[include_item] = 1
      end
    end

    # clean and inser to database
    TrendingItem.destroy_all
    trending_items.each do |item, count|
      ti = TrendingItem.new
      ti.item = item
      ti.count = count
      ti.save
    end
  end

  private

    # get words after anchor
    def self.get_words_after_anchor(body)
      body_words = body.split(/ /)
      words_after_anchor = []

      # find earliest/lowest anchor index
      anchor_index_string = self.find_anchor_index_to_use(body_words)
      if !anchor_index_string.nil?
        anchor_index = self.find_anchor_index_to_use(body_words).to_int

        # get words after anchor then drop the anchor
        words_after_anchor = body_words.slice(anchor_index, (AFTER_ANCHOR_LOOKUP + 1)).drop(1)
      # anchor not found because it was taken from items table
      else
        words_after_anchor = body_words
      end
      words_after_anchor
    end

    # given words, find the item words
    def self.find_item_words(words_after)
      item_word_array = []
      count_item_word = 0
      anchor_words = AnchorWord.generate_all_in_array
      exclude_words = ExcludeWord.generate_all_in_array
      
      words_after.each do |word|
        word = sanitize_word(word)

        # check if it's an item word
        # the word must not be anchor and must not be excluded words
        if !anchor_words.include?(word) and !exclude_words.include?(word)
          count_item_word += 1
          item_word_array << word
        elsif count_item_word > 0
          break
        end
      end
      # puts 'item word: ' + item_word_array.join(' ')
      item_word_array.join(' ')
    end

    # clean up a string
    def self.sanitize_word(word)
      word = word.downcase.strip
      word.gsub(/[?,.:()'"$-%*&^*#]/, ' ').gsub(/\r\n/,' ')
    end

    # find the lowest anchor index to use
    def self.find_anchor_index_to_use(body_words)
      anchor_words = AnchorWord.generate_all_in_array
      index = nil
      anchor_words.each do |anchor_word|
        found_index = body_words.index anchor_word

        # only use if it's lower anchor index
        if index.nil?
          index = found_index
        elsif !found_index.nil? and found_index < index
          index = found_index
        end
      end
      index
    end

    # get all data from comments body
    def self.get_from_comments_body(anchor_words)
      scope = Comment.scoped({})
      scope = scope.body_contains(anchor_words)
      scope = scope.sixty_days_ago
      body = []
      scope.each do |comment|
        body << sanitize_word(comment.body)
      end
      body
    end

    # get all data from messages body
    def self.get_from_messages_body(anchor_words)
      scope = Message.scoped({})
      scope = scope.body_contains(anchor_words)
      scope = scope.sixty_days_ago
      body = []
      scope.each do |message|
        body << sanitize_word(message.body)
      end
      body
    end

    # get all data from seeks note
    def self.get_from_seeks_notes(anchor_words)
      scope = Seek.scoped({})
      scope = scope.notes_contains(anchor_words)
      scope = scope.sixty_days_ago
      notes = []
      scope.each do |seek|
        notes << sanitize_word(seek.notes)
      end
      notes
    end

    # get all data from trip note
    def self.get_from_trips_notes(anchor_words)
      scope = Trip.scoped({})
      scope = scope.notes_contains(anchor_words)
      scope = scope.sixty_days_ago
      notes = []
      scope.each do |trip|
        notes << sanitize_word(trip.notes)
      end
      notes
    end

    # get all data from trip note
    def self.get_from_items_name
      scope = Item.scoped({})
      scope = scope.sixty_days_ago
      names = []
      scope.each do |item|
        names << sanitize_word(item.name)
        puts item.name
      end
      names
    end

    # get all data that contains the anchor words
    def self.get_all_match
      anchor_words = AnchorWord.generate_all_in_string
      self.get_from_comments_body(anchor_words) + self.get_from_messages_body(anchor_words) + self.get_from_trips_notes(anchor_words) + self.get_from_seeks_notes(anchor_words) + self.get_from_items_name
    end
end
