require 'test_helper'

class ForumTest < ActiveSupport::TestCase
  
  test "should create forum" do
    forum = Forum.new
    forum.title = 'Forum title'
    forum.description = 'Forum description'
    assert forum.save
  end

  test "should find forum" do
    forum_id = forums(:lounge).id
    assert_nothing_raised {Forum.find(forum_id)}
  end

  test "should update forum" do
    forum = forums(:lounge)
    assert forum.update_attributes(:title => 'Lounge (updated)')
  end
  
  test "should delete forum" do
    forum_id = forums(:lounge).id
    assert_nothing_raised {Forum.find(forum_id)}
    forum = Forum.find(forum_id)
    assert forum.destroy
  end
end
