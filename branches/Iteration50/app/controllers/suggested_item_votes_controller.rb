class SuggestedItemVotesController < ApplicationController

  before_filter :authenticate, :only => [:new]

  def new
    suggested_item = SuggestedItem.find(params[:suggested_item_id])

    # owner cannot vote for himself
    if current_user == suggested_item.user
      flash[:notice] = t('suggested_item_votes.new.not_self')
    # check if vote already exist
    elsif SuggestedItemVote.where(:user_id => current_user.id, :suggested_item_id => suggested_item.id).count < 1
      vote = SuggestedItemGoodVote.new
      vote.suggested_item = suggested_item
      vote.user = current_user
      if vote.save
        flash[:notice] = t('suggested_item_votes.new.success', :item => suggested_item.name)
      else
        flash[:notice] = t('suggested_item_votes.new.failure')
      end
    else
      flash[:notice] = t('suggested_item_votes.new.already_exist', :item => suggested_item.name)
    end
    redirect_to suggested_item_path(suggested_item)
  end

end
