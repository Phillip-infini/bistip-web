# to represent message status
class MessageStatus
  
  # constants
  UNREAD = 'unread'
  NOT_HANDLED = 'not_handled'
  ESCROW = 'safepay'
  DEPARTURE_DATE_ASC = 'departure_date_asc'

  # return label of status and its value
  def self.map
    return [[I18n.t('message.filter.status.blank'), ''],[I18n.t('message.filter.status.unread'), UNREAD], [I18n.t('message.filter.status.not_handled'), NOT_HANDLED]]
  end

  # build a scope
  def self.build(status, scope)
    if status == UNREAD
      scope.unread
    elsif status == NOT_HANDLED
      scope.not_handled
    end
  end
end
