class PopulateSaltToInactiveTripsAndSeeks < ActiveRecord::Migration
  def self.up
    count_trip = 0
    Trip.unscoped.find_all_by_salt(nil).each do |trip|
      trip.generate_salt
      trip.save
      count_trip = count_trip + 1
    end
    puts 'Total inactive trips found: ' + count_trip.to_s
    count_seek = 0
    Seek.unscoped.find_all_by_salt(nil).each do |seek|
      seek.generate_salt
      seek.save
      count_seek = count_seek + 1
    end
    puts 'Total inactive seeks found: ' + count_seek.to_s
  end

  def self.down
    # do nothing
  end
end
