class AddEmailTripReminderToUserConfigurations < ActiveRecord::Migration
  def self.up
    add_column :user_configurations, :email_trip_reminder, :boolean, :default => true
  end

  def self.down
    remove_column :user_configurations, :email_trip_reminder
  end
end
