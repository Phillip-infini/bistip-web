class AddSaltToComments < ActiveRecord::Migration
  def self.up
    add_column :comments, :salt, :string
  end

  def self.down
    remove_column :comments, :salt
  end
end
