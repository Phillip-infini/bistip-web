# validator class to ensure that a date is a today's date or future date

class ValidEscrowAmountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !value.nil? and !record.payment_method.blank? and !record.payment_method.amount_allowable?(value)
      record.errors[attribute] << (options[:message] || I18n.t("escrow.errors.custom.valid_amount.#{record.payment_method.cname}"))
    end
  end
end