require 'test_helper'

class ReviewTest < ActiveSupport::TestCase

  test "user X should give a positive review to user Y" do
    userx = users(:dono)
    usery = users(:one)
    positive = usery.received_positive_reviews.new
    positive.giver = userx
    positive.body = "good quality of service"
    assert_differences([['Relation.count', 1], ['ReviewNotification.count', 1]]) do
      assert positive.save
    end
  end

  test "user X should give a negative review to user Y" do
    userx = users(:dono)
    usery = users(:one)
    negative = usery.received_negative_reviews.new
    negative.giver = userx
    negative.body = "poor quality of service"
    assert_differences([['Relation.count', 1], ['ReviewNotification.count', 1]]) do
      assert negative.save
    end
  end

  test "user X should give a recommended positive review to user Y" do
    userx = users(:dono)
    usery = users(:one)
    positive = usery.received_positive_reviews.new
    positive.giver = userx
    positive.body = "good quality of service"
    positive.invitation = true
    assert_differences([['Relation.count', 1], ['ReviewNotification.count', 1]]) do
      assert positive.save
    end
  end

  test "user X should give a recommended negative review to user Y" do
    userx = users(:dono)
    usery = users(:one)
    negative = usery.received_negative_reviews.new
    negative.giver = userx
    negative.body = "poor quality of service"
    negative.invitation = true
    assert_differences([['Relation.count', 1], ['ReviewNotification.count', 1]]) do
      assert negative.save
    end
  end

  test 'review scope method' do
    userx = users(:dono)
    usery = users(:one)
    assert Review.last_24_hour(userx.id, usery.id)
    assert Review.last_7_days(userx.id, usery.id)
    assert Review.recommendation(userx.id, usery.id)
  end
  
end
