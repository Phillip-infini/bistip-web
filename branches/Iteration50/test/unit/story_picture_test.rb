require 'test_helper'

class StoryPictureTest < ActiveSupport::TestCase
  
  test "find by story" do
    assert StoryPicture.order(:id).find_all_by_story_id(stories(:storyone)).first
  end

  test "find by id" do
    assert StoryPicture.find(story_pictures(:storypicone))
  end

end
