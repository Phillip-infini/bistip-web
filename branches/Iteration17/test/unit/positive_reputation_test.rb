require 'test_helper'

class PositiveReputationTest < ActiveSupport::TestCase

  test "user X should give a positive reputation to user Y" do
    userx = users(:dono)
    usery = users(:one)
    positive = usery.received_positive_reputations.new
    positive.giver = userx
    positive.body = "good quality of service"
    assert positive.save
  end
  
end
