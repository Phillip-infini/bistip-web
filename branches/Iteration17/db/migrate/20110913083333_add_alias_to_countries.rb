class AddAliasToCountries < ActiveRecord::Migration
  def self.up
    add_column :countries, :alias, :string

    File.open("#{Rails.root}/db/data/countries-alias-migration.txt", "r") do |infile|
      infile.read.each_line do |country_row|
        code, name, calias = country_row.chomp.split("|")
        country = Country.find_by_code(code)

        unless country.blank?
          country.alias = calias
          country.save
        end
      end
    end
  end

  def self.down
    remove_column :countries, :alias
  end
end
