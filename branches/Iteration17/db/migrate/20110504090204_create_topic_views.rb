class CreateTopicViews < ActiveRecord::Migration
  def self.up
    create_table :topic_views do |t|
      t.belongs_to :topic
      t.integer :count, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :topic_views
  end
end
