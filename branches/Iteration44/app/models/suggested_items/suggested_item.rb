class SuggestedItem < ActiveRecord::Base
  include Location

  # scope
  scope :active_only, lambda { {:conditions => ["deleted = ?", false]} }
  scope :name_match_and_not_id_and_not_origin_city_id, lambda { |name, id, origin_city_id, limit| {:conditions => ["match(name) against(?) and id <> ? and origin_city_id <> ?", name, id, origin_city_id], :limit => limit} }
  scope :name_contains, lambda { |keyword| {:conditions => ["match(suggested_items.name) against(?)", keyword]} }
  scope :origin_city_id, lambda { |origin_city_id| {:conditions => ["suggested_items.origin_city_id = ?", origin_city_id]} }
  scope :origin_city_ids, lambda { |origin_city_ids| {:conditions => ["suggested_items.origin_city_id IN (?)", origin_city_ids]} }
  scope :origin_country_id, lambda { |country_id| {:conditions => ["cities.country_id = ?", country_id], :joins => 'LEFT JOIN cities ON cities.id = suggested_items.origin_city_id'} }
  scope :origin_country_not, lambda { |country_id| {:conditions => ["cities.country_id != ?", country_id], :joins => 'LEFT JOIN cities ON cities.id = suggested_items.origin_city_id'} }
  scope :newest, :order => "suggested_items.created_at DESC"

  # restriction
  attr_readonly :name
  attr_readonly :origin_city

  # constant
  NAME_MAX_LENGTH = 50
  NAME_MIN_LENGTH = 3
  REASON_MAX_LENGTH = 400
  REASON_MIN_LENGTH = 10
  NOTES_MAX_LENGTH = 600
  PICTURES_MAXIMUM = 3
  TOTAL_SHOW_ON_SUMMARY = 10
  SEARCH_PER_PAGE = 10
  SHOW_PER_PAGE = 10

  # validation
  validates :name, :presence => true,
            :length => {:maximum => NAME_MAX_LENGTH, :minimum => NAME_MIN_LENGTH}
  validates :reason, :presence => true,
            :length => {:maximum => REASON_MAX_LENGTH, :minimum => REASON_MIN_LENGTH}
  validates :notes, :length => {:maximum => NOTES_MAX_LENGTH}
  validates :user, :presence => true
  validates :origin_city, :origin_city => true
  
  # associations
  belongs_to :user
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'
  has_many :pictures, :class_name => "SuggestedItemPicture", :foreign_key => 'suggested_item_id'
  has_many :votes, :class_name => 'SuggestedItemVote', :foreign_key => 'suggested_item_id'
  accepts_nested_attributes_for :pictures, :reject_if => lambda { |a| a[:attachment].blank? }

  # before save
  before_save :handle_point_log

  # friendly URL for SEO
  def to_param
    city = origin_city
    label_city = city.all_cities? ? city.country.name : city.name

    I18n.t('suggested_items.show.url', :id => id, :flag => TrackLink::BS_V1, :item => name, :location => label_city).parameterize
  end

  # check if item suggestion has a picture
  def has_picture?
    self.pictures.count > 0
  end

  # handler for point log before save
  def handle_point_log
    if self.deleted
      point_log = AddForSuggestedItemLog.find_by_data_id(self.id)
      point_log.destroy unless point_log.blank?
    else
      point_log = AddForSuggestedItemLog.find_by_data_id(self.id)
      if point_log.blank?
        AddForSuggestedItemLog.create!(:user_id => self.user_id, :amount => self.points, :data_id => self.id)
      end
    end
  end

  # check if the user has relation to the given user
  def voted_by?(user)
    return true if self.user == user
    self.votes.where(:user_id => user.id).count > 0
  end

  # find top voted items from a certain area
  def self.get_top_voted_from(origin_city, limit)
    all_cities = City.find_by_name_and_country_id(City::ALL_CITIES, origin_city.country_id)
    cities = Array.new
    cities << origin_city
    cities << all_cities unless all_cities.blank?

    SuggestedItem.active_only.find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
      :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id',
      :group => 'suggested_items.id',
      :order => 'counter DESC',
      :conditions => ["suggested_items.origin_city_id IN (#{cities.map{|c| c.id}.join(',')})"],
      :limit => limit)
  end

  # find random items from a certain area
  def self.get_random_from(origin_city, limit)
    all_cities = City.find_by_name_and_country_id(City::ALL_CITIES, origin_city.country_id)
    cities = Array.new
    cities << origin_city
    cities << all_cities unless all_cities.blank?

    SuggestedItem.active_only.find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id',
      :order => 'RAND()',
      :conditions => ["suggested_items.origin_city_id IN (#{cities.map{|c| c.id}.join(',')})"],
      :limit => limit)
  end

  # find similar item from other area
  def self.get_similar_from_other_city(suggested_item, limit)
    SuggestedItem.active_only.find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
      :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id',
      :group => 'suggested_items.id',
      :order => 'counter DESC',
      :conditions => ["match(name) against(?) and suggested_items.id <> ? and suggested_items.origin_city_id <> ?", suggested_item.name, suggested_item.id, suggested_item.origin_city_id],
      :limit => limit)
  end

  # find other item from the same origin city of the suggested item
  def self.get_other_suggested_item_from_same_city(suggested_item, limit)
    origin_city = suggested_item.origin_city

    if origin_city.all_cities?
      SuggestedItem.active_only.find(:all,
        :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
        :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id LEFT JOIN cities ON cities.id = suggested_items.origin_city_id',
        :group => 'suggested_items.id',
        :order => 'counter DESC',
        :conditions => ["suggested_items.id <> #{suggested_item.id} and cities.country_id = #{origin_city.country_id}"],
        :limit => limit)
    else
      all_cities = City.find_by_name_and_country_id(City::ALL_CITIES, origin_city.country_id)
      cities = Array.new
      cities << origin_city
      cities << all_cities unless all_cities.blank?

      SuggestedItem.active_only.find(:all,
        :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
        :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id',
        :group => 'suggested_items.id',
        :order => 'counter DESC',
        :conditions => ["suggested_items.id <> #{suggested_item.id} and suggested_items.origin_city_id IN (#{cities.map{|c| c.id}.join(',')})"],
        :limit => limit)
      
    end
  end

  # check if the suggested has been approved or not
  def active?
    !self.deleted?
  end

  # get the amount of bistip that a user get with this suggested item
  def points
    self.has_picture? ? PointLog::SUGGESTED_ITEM_WITH_PICTURE_POINT : PointLog::SUGGESTED_ITEM_POINT
  end

  # build scoped SuggestedItem for searching trip purpose
  def self.build_scope_search(origin = nil, keyword = nil)
    scope = SuggestedItem.scoped({})
    scope = scope.active_only
    scope = scope.newest
    
    if !origin.nil?
      if origin.all_cities?
        scope = scope.origin_country_id origin.country.id
      else
        # if i'm searching for seoul then it should find <all cities>, Korea too
        all_cities = City.find_by_name_and_country_id(City::ALL_CITIES, origin.country_id)

        ids = Array.new
        ids << all_cities.id unless all_cities.blank?
        ids << origin.id
        
        scope = scope.origin_city_ids ids
      end
    end

    # build predicate for keyword
    if !keyword.blank?
      scope = scope.name_contains keyword
    end

    return scope
  end

  # get name for display that will shorten if it's too long
  def name_short
    self.name.length > 20 ? self.name[0..20] + '...' : self.name
  end

  # get name for display that will shorten if it's too long
  def name_mini
    self.name.length > 12 ? self.name[0..12] + '...' : self.name
  end

end
