# class that wrap an activity in Bistip
class TripActivityWrap < ActivityWrap
  CNAME = 'trip_activity_wrap'

  def cname
    CNAME
  end
end
