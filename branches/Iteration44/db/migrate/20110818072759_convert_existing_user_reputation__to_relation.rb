class ConvertExistingUserReputation_toRelation < ActiveRecord::Migration
  def self.up
    count = 1
    PositiveReputation.all.each do |reputation|
      giver = reputation.giver
      receiver = reputation.receiver
      if Relation.where(:from_id => giver.id, :to_id => receiver.id).count < 1
        relation = giver.relations.new
        relation.to_id = receiver.id
        if relation.save
          puts "#{count}. now #{giver.username} know #{receiver.username}"
          count += 1
        end
      end      
    end
  end

  def self.down
    Relation.destroy_all
  end
end
