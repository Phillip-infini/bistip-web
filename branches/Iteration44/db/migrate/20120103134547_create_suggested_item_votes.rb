class CreateSuggestedItemVotes < ActiveRecord::Migration
  def self.up
    create_table :suggested_item_votes do |t|
      t.column :suggested_item_id, :integer, :null => false, :references => :suggested_items
      t.belongs_to :user
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :suggested_item_votes
  end
end
