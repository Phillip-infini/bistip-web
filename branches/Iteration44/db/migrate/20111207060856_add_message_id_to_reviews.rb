class AddMessageIdToReviews < ActiveRecord::Migration
  def self.up
    add_column :reviews, :message_id, :integer, :references => :messages
  end

  def self.down
    remove_column :reviews, :message_id
  end
end
