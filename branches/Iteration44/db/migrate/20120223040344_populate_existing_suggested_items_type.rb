class PopulateExistingSuggestedItemsType < ActiveRecord::Migration
  def self.up
    SuggestedItem.all.each do |si|
      si.update_attribute(:type, 'UserSuggestedItem')
    end
  end

  def self.down
    SuggestedItem.all.each do |si|
      si.update_attribute(:type, nil)
    end
  end
end
