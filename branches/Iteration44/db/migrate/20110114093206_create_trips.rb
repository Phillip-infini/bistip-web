class CreateTrips < ActiveRecord::Migration
  def self.up
    create_table :trips do |t|
      t.column :origin_city_id, :integer, :null => false, :references => :cities
      t.column :destination_city_id, :integer, :null => false, :references => :cities
      t.column :deleted, :boolean, :default => false
      t.belongs_to :user
      t.datetime :departure_date
      t.datetime :arrival_date
      t.text :notes

      t.timestamps
    end
    add_index(:trips, :id, :unique=>true)
  end

  def self.down
    drop_table :trips
  end
end
