require 'test_helper'

class RelationsControllerTest < ActionController::TestCase
  
  test "should create relation" do
    login_as(:one)
    assert_difference('Relation.count') do
      post :new, :user_id => users(:dono).id
    end

    assert_redirected_to profile_path(users(:dono).username)
  end

  test 'should show user social page' do
    get :index, :user_id => users(:dono).id
    assert_response :success
  end

end
