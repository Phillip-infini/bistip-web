require 'test_helper'

class MessagesControllerTest < ActionController::TestCase
  setup do
    @sender = users(:dono)
    @receiver = users(:mactavish)
    @msg = messages(:donotomac)
  end

  test "should get new" do
    login_as(:dono)
    get :new, :user_id=>@receiver.id
    assert_response :success
  end

  test "should get index of inbox messages" do
    login_as(:dono)
    get :index, :user_id=>@sender.id
    assert_response :success
    assert_not_nil(:inboxes)
  end

  test "should get sent messages" do
    login_as(:dono)
    get :sent, :user_id=>@sender.id
    assert_response :success
    assert_not_nil(:sent_messages)
  end

  test "should post a new message" do
    login_as(:dono)
    user = User.find(@receiver.id)
    assert_not_nil(user)
    assert_difference('Message.count') do
      post :create, :user_id=>@receiver.id, :receiver_id=>@receiver.id, :message => {
        :body=>"from dono to mac",
        :subject=>"dono2mac",
        :sender=>@sender
      }
    end
    assert_redirected_to user_message_path(@sender, assigns(:message))
  end

  test "should reply a message" do
    login_as(:dono)
    get :index, :user_id=>@sender.id
    assert_response :success
    get :show, :user_id=> @sender.id, :id=>@msg.id
    assert_response :success
    assert_difference('Message.count') do
      post :create, :user_id=>@receiver.id, :receiver_id=>@receiver.id, :message => {
        :body => 'reply by dono',
        :sender => @sender,
        :subject => @msg.subject,
        :reply_to_id => @msg.id
      }
    assert_redirected_to user_message_path(@sender, assigns(:message))
    end
  end
end
