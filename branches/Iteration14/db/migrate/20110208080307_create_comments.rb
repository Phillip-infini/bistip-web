class CreateComments < ActiveRecord::Migration
  def self.up
    create_table :comments do |t|
      t.belongs_to :user
      t.belongs_to :trip
      t.belongs_to :seek
      t.column :deleted, :boolean, :default => false
      t.text :body
      t.string :type
      t.timestamps

    end
  end

  def self.down
    drop_table :comments
  end
end
