module SeeksHelper

  # predefined value for destination location
  def get_predefined_destination_location
    destination_location_pre = ''
    if params[:destination_location] or !current_user.city.blank?
      destination_location_pre = params[:destination_location] if params[:destination_location]
      destination_location_pre = current_user.city_location unless current_user.city.blank?
      destination_location_pre
    else
      ''
    end
  end
end
