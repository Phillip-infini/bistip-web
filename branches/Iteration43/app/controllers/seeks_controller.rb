class SeeksController < ApplicationController
  
  before_filter :authenticate, :only => [:new, :edit, :create, :update, :destroy, :match]

  # GET /seeks/1
  def show
    @seek = Seek.find(params[:id])
    @skcomments = @seek.comments.where("reply_to_id is null").paginate(:page => params[:page], :per_page => Comment::PER_PAGE)
    if logged_in?
      @comment = Comment.new
    end
  end

  # GET /seeks/new
  def new
    @seek = Seek.new
    initialize_item(@seek)
  end
  
  # GET /seeks/1/edit
  def edit
    @seek = current_user.seeks.active_only.find(params[:id])
    initialize_item(@seek)
  end

  # POST /seeks
  def create
    @seek = current_user.seeks.new(params[:seek])

    if @seek.save
      flash[:notice] = t('seek.create.message.success')
      redirect_to(@seek)
    else
      # if there is form error, upload picture will be gone so lets clean the assets and rebuild
      flash.now[:alert] = t('general.reupload_picture') unless @seek.item.blank? or @seek.item.pictures.blank?
      put_model_errors_to_flash(@seek.errors)
      initialize_item(@seek)
      render :action => "new"
    end
  end

  # PUT /seeks/1
  def update
    @seek = current_user.seeks.active_only.find(params[:id])

    if @seek.update_attributes(params[:seek])
      flash[:notice] = t('seek.update.message.success')
      redirect_to(@seek)
    else
      initialize_item(@seek)
      put_model_errors_to_flash(@seek.errors)
      render :action => "edit"
    end
  end

  # DELETE /seeks/1
  def destroy
    # only the owner of the comment can do the deletion
    @seek = current_user.seeks.active_only.find(params[:id])
    # apply soft delete, set deleted attribute to true
    @seek.update_attribute(:deleted,true)
    flash[:notice] = t('seek.destroy.message.success')
    
    redirect_to :back
  end

  # show matching trips from a seek
  def match
    @seek = current_user.seeks.find(params[:id])
    @matches = @seek.find_matching_trips
  end

  private

    def initialize_item(seek)
      seek.item = SeekItem.new if (seek.item.blank? and seek.new_record?)
      seek.item.pictures.clear if seek.new_record?
      (3 - seek.item.pictures.size).times { seek.item.pictures.build }
      
    end
end
