class AnchorWord < ActiveRecord::Base
  def self.generate_all_in_string
    anchor_words = AnchorWord.all
    result = ""
    anchor_words.each do |anchor_word|
      result << anchor_word.word + ' '
    end
    result.strip!
  end

  def self.generate_all_in_array
    anchor_words = AnchorWord.all
    result = []
    anchor_words.each do |anchor_word|
      result << anchor_word.word
    end
    result
  end
end
