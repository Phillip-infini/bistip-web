# sub class of comment that represent Comment for Trip
class TripComment < Comment

  # create notification and send email if needed
  def broadcast
    trip = Trip.find(self.trip_id)

    # always notify trip owner
    if !trip.owner?(self.user)
      Notifier.delay.email_trip_comment(trip.id, self.user, self) if trip.user.user_configuration.email_receive_trip_comment
      CommentNotification.create!(:receiver => trip.user, :sender => self.user, :data_id => self.id)
    end
    
    # if it's a reply then notify the original comment owner
    if self.reply?
      parent = self.thread_starter
      notified = Array.new

      # let parent comment owner knows
      if !trip.owner?(parent.user) and !(parent.user == self.user)
        Notifier.delay.email_trip_comment_reply(self, parent.user) if parent.user.user_configuration.email_comment_reply
        CommentReplyNotification.create!(:receiver => parent.user, :sender => self.user, :data_id => self.id)
        notified << parent.user
      end

      # reply all the previous contributor to this comment thread, except new commentator
      replies = Comment.find_comments_in_thread_and_except(parent.id, self.id)
      replies.each do |reply|
        user = reply.user

        # sned if not trip owner, not commentator and has not been notified
        if (!trip.owner?(user)) and (user != self.user) and (!notified.include?(user))
          Notifier.delay.email_trip_comment_reply(self, user) if user.user_configuration.email_comment_reply
          CommentReplyNotification.create!(:receiver => user, :sender => self.user, :data_id => self.id)
          notified << user
        end
      end
    end
  end
  
  # find which page this comment belongs to
  def find_page
    trip = Trip.find(self.trip_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    index = trip.comments.find_all_by_reply_to_id(nil).index(comment_to_use) + 1
    ((index * 1.0)/Comment::PER_PAGE).ceil
  end

  # generate path to the comment
  def get_path
    trip = Trip.find(self.trip_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    path_helper.trip_path(trip, :page => self.find_page, :anchor => comment_to_use.internal_id)
  end

  # generate path to the comment
  def get_url
    trip = Trip.find(self.trip_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    path_helper.trip_url(trip, :page => self.find_page, :anchor => comment_to_use.internal_id)
  end

  # this method generate a link_to to this comment
  def get_path_link
    link_to(I18n.t('notification.field.trip'), get_path)
  end

end
