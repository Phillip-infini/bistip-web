require 'test_helper'

class InsuranceTest < ActiveSupport::TestCase
  
  test "should create escrow of a message" do
    insurance = Insurance.new
    insurance.buyer = users(:one)
    insurance.seller = users(:mactavish)
    insurance.message = messages(:onetomac)
    insurance.currency = Currency.first
    insurance.amount = 100000
    insurance.cover_value = 4000000
    insurance.item = 'ipad 2'
    assert insurance.save
  end

  test 'check insurance state' do
    assert insurances(:donotomac).current_state == Insurance::STATE_BUYER_APPLIED
    assert insurances(:donotomac).last_log.class == BuyerAppliedInsuranceLog
  end

  test 'insurance UI state methods' do
    insurance = insurances(:donotomac)
    assert_equal 'details', insurance.current_step
    insurance.next_step
    assert_equal 'confirmation', insurance.current_step
    insurance.previous_step
    assert_equal 'details', insurance.current_step
    insurance.next_step
    assert_equal 'confirmation', insurance.current_step

    # don;t know why below is keep failing
    assert insurance.all_valid?
  end

  test 'insurance amount calculation' do
    insurance = insurances(:donotomac)
    assert_nothing_raised {insurance.amount_with_currency}
    assert_nothing_raised {insurance.cover_value_with_currency}
  end
  
end
