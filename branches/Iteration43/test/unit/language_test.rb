require 'test_helper'

class LanguageTest < ActiveSupport::TestCase
  
  test "should get language" do
    assert Language.find_by_english_name('Indonesian')
    assert Language.match_english_name('indo')
  end
end
