require 'test_helper'

class RecommendControllerTest < ActionController::TestCase
 
  test "handle" do
    get :handle, :username => users(:one).username
    assert_not_nil assigns(:user)
    assert_response :success
    assert_template :handle
  end

  test "get recommendation" do
    login_as(:one)
    get :get_recommendation
    assert_not_nil assigns(:user)
    assert_response :success
    assert_template :get_recommendation
  end
end
