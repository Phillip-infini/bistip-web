class AddSuggestedItemNameIndexing < ActiveRecord::Migration
  def self.up
    execute('ALTER TABLE suggested_items ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_suggested_items_name ON suggested_items (name(500))')
  end

  def self.down
    execute('DROP INDEX fulltext_suggested_items_name ON suggested_items')
    execute('ALTER TABLE suggested_items ENGINE = innodb')
  end
end
