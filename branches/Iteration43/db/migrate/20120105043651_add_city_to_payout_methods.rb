class AddCityToPayoutMethods < ActiveRecord::Migration
  def self.up
    add_column :payout_methods, :city, :string
  end

  def self.down
    remove_column :payout_methods, :city
  end
end
