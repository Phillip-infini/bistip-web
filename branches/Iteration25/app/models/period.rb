# To change this template, choose Tools | Templates
# and open the template in the editor.

class Period

  ONE_WEEK = "weekly"

  TWO_WEEKS = "biweekly"

  ALL = [ONE_WEEK, TWO_WEEKS]

  def self.map
    return [[I18n.t('week_period.weekly'), ONE_WEEK],[I18n.t('week_period.biweekly'), TWO_WEEKS]]
  end
end
