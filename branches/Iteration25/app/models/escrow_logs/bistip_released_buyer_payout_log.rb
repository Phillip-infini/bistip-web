# To change this template, choose Tools | Templates
# and open the template in the editor.

class BistipReleasedBuyerPayoutLog < EscrowLog

  # broadcast the creation of this log inform of notification and email
  # override
  def broadcast
    EscrowLogNotification.create!(:data_id => self.id, :sender => escrow.buyer, :receiver => escrow.buyer)
    Notifier.delay.email_escrow_log(escrow.buyer, self)
  end
  
end
