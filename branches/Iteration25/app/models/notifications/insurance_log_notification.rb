# class to handle all escrow log notification
class InsuranceLogNotification < Notification
  def to_link(current_user)
    insurance_log = InsuranceLog.unscoped.find(data_id)
    return insurance_log.to_notification_link(current_user)
  end
end
