class RecommendController < ApplicationController

  before_filter :authenticate, :only => [:get_recommendation]
  
  def handle
    username = params[:username]
    @user = User.find_by_username(username)
  end

  def get_recommendation
    @user = current_user
  end

end
