require 'test_helper'

class EscrowStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "complete escrow state between one and dono" do
    # on eis buyer dono is seller
    one = users(:one)
    dono = users(:dono)
    message = messages(:donotoone1)

    # login
    full_login_as(:one)

    # 1. open the escrow form
    get new_escrow_path(:seller_id => dono.id, :message_id => message.id)
    assert_template :new
    assert_response :success

    # 2. post escrow form
    post escrows_path, :escrow => {:buyer_id => one.id,
      :seller_id => dono.id,
      :message_id => message.id,
      :payment_method_id => payment_methods(:paypal).id,
      :amount => 200,
      :item => 'ipad 2'
    }

    # 3. post escrow form
    assert_difference('Escrow.count') do
      post escrows_path, :escrow => {:buyer_id => one.id,
        :seller_id => dono.id,
        :message_id => message.id,
        :payment_method_id => payment_methods(:paypal).id,
        :amount => 200,
        :item => 'ipad 2'
      }
      assert_response :redirect
      follow_redirect!
    end
    
    escrow = assigns(:escrow)

    # 4. Bistip confirm payment
    assert_difference('BistipReceivedBuyerBankTransferLog.count') do
      get create_received_buyer_bank_transfer_log_path(:escrow_id => escrow.id, :password => ApplicationController::REST_PASSWORD)
      assert_redirected_to manage_escrow_path(:escrow_id => escrow.id, :password => ApplicationController::REST_PASSWORD)
    end

    # 5. Seller say he/she got the item
    get logout_path
    full_login_as(:dono)
    get user_message_path(:user_id => dono.id, :id => message.id)
    assert_difference('SellerGotItemLog.count') do
      post seller_got_item_escrows_path, :escrow_id => escrow.id
      assert_redirected_to user_message_path(:user_id => dono.id, :id => message.id)
    end

    # 6. Buyer say he/she receive the item
    get logout_path
    full_login_as(:one)
    get user_message_path(:user_id => one.id, :id => message.id)
    assert_difference('BuyerReceivedItemLog.count') do
      post buyer_received_item_escrows_path, :escrow_id => escrow.id, :review_type => 'PositiveReview', :review_body => 'body body body'
      assert_redirected_to user_message_path(:user_id => one.id, :id => message.id)
    end

    # 7. Seller request payout
    get logout_path
    full_login_as(:dono)
    get user_message_path(:user_id => dono.id, :id => message.id)
    assert_difference('SellerRequestedPayoutLog.count') do
      post seller_requested_payout_escrows_path, :escrow_id => escrow.id, :review_type => 'PositiveReview', :review_body => 'body body body'
      assert_redirected_to user_message_path(:user_id => dono.id, :id => message.id)
    end

    # 8. Bistip release payout
    assert_difference('BistipReleasedSellerPayoutLog.count') do
      get create_released_seller_payout_log_path(:escrow_id => escrow.id, :password => ApplicationController::REST_PASSWORD)
      assert_redirected_to manage_escrow_path(:escrow_id => escrow.id, :password => ApplicationController::REST_PASSWORD)
    end

    # 9. Seller say he/she received payout
    get logout_path
    full_login_as(:dono)
    get user_message_path(:user_id => dono.id, :id => message.id)
    assert_difference('SellerReceivedPayoutLog.count') do
      post seller_received_payout_escrows_path, :escrow_id => escrow.id
      assert_redirected_to user_message_path(:user_id => dono.id, :id => message.id)
    end
  end

end
