require 'test_helper'

class TripTest < ActiveSupport::TestCase

  test "should create trip" do
    trip = Trip.new
    trip.origin_location = "jakarta"
    trip.destination_location = "sydney"
    trip.departure_date = DateTime.current.advance(:days => 1)
    trip.arrival_date = DateTime.current.advance(:days => 2)
    trip.user = users(:one)
    trip.notes = 'test'
    assert trip.save
  end

  test "should create trip with item" do
    trip = Trip.new
    trip.origin_location = "jakarta"
    trip.destination_location = "sydney"
    trip.departure_date = DateTime.current.advance(:days => 1)
    trip.arrival_date = DateTime.current.advance(:days => 2)
    trip.user = users(:one)
    trip.notes = 'test'
    trip.items_attributes = [{:name => 'ipad'}]
    assert trip.save
  end

  test "should find trip" do
    trip_id = trips(:jktsyd).id
    assert_nothing_raised {Trip.find(trip_id)}
  end

  test "should update trip" do
    trip = trips(:jktsyd)
    assert trip.update_attributes(:notes => 'notes')
  end

  test "should create a routine trip" do
    trip = Trip.new
    trip.routine=true
    trip.origin_location = "jakarta"
    trip.destination_location = "sydney"
    trip.departure_date = nil
    trip.arrival_date = nil
    trip.day = ["Friday","Saturday"]
    trip.period="weekly"
    trip.user = users(:one)
    trip.notes = 'test'
    assert trip.save
  end

  test "should update a routine trip" do
    trip = trips(:bogjak)
    assert trip.update_attributes(:notes=>"liburan tiap minggu ke jakarta",:day=>["Sunday"])
  end

  test "should delete trip" do
    trip_id = trips(:jktsyd).id
    trip = Trip.find(trip_id)
    assert trip.update_attributes(:deleted=>true)
  end

  test 'trip matching' do
    assert_equal 4, trips(:jktsyd).find_matching_seeks.count
  end

  test 'trip item methods' do
    assert_nothing_raised {trips(:jktsyd).items_name_all}
    assert_nothing_raised {trips(:jktsyd).items_name_for_display}
  end

  test "trip scope and search method" do
    assert_nothing_raised {Trip.build_scope_home_index(3)}
    assert_nothing_raised {Trip.build_scope_dashboard_index(3, users(:one))}

    assert_equal 1, Trip.build_scope_search(cities(:jakarta), cities(:sydney), DateTime.current, DatePredicate::AFTER, nil, TripSort::NEWEST).count
    assert_equal 6, Trip.build_scope_search(nil, cities(:jakarta), DateTime.current, DatePredicate::AFTER, nil, TripSort::NEWEST).count
    assert_equal 3, Trip.build_scope_search(cities(:allindo), cities(:sydney), DateTime.current, DatePredicate::AFTER, nil, TripSort::NEWEST).count
  end

  test 'trip method for email thread starter' do
    trip = Trip.new
    trip.origin_location = "jakarta"
    trip.destination_location = "sydney"
    trip.departure_date = DateTime.current.advance(:days => 1)
    trip.arrival_date = DateTime.current.advance(:days => 2)
    trip.user = users(:one)
    trip.notes = 'test'
    assert trip.save
    assert !trip.salt.blank?
    assert trip.email_reply_address == ("#{Trip::EMAIL_REPLY_PREFIX}-#{trip.id}-#{trip.salt}@#{Trip::EMAIL_REPLY_DOMAIN}")
  end

  test 'trip equal same route' do
    assert trips(:banjak).same_route? trips(:banjak2)
  end

  test 'trip suggestions' do
    assert_equal 1, Trip.get_suggestions(trips(:banjak)).count
    assert_equal 3, Trip.get_suggestions(trips(:jktsyd)).count, 1
    assert_equal 0, Trip.get_suggestions_routes(cities(:bandung), cities(:jakarta)).count
    assert_equal 2, Trip.get_suggestions_routes(cities(:jakarta), cities(:sydney)).count
  end
end
