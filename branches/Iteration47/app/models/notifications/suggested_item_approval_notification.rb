class SuggestedItemApprovalNotification < Notification

  # override to_link method to perform specified action of ItemSuggestionApprovalNotification
  def to_link(current_user)
    suggested_item = SuggestedItem.find(data_id)
    link = I18n.t("notification.title.suggested_item_approval",
      :link => link_to(suggested_item.name, path_helper.suggested_item_path(suggested_item)))
    return link
  end
end
