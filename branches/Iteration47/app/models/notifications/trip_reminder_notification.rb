# To change this template, choose Tools | Templates
# and open the template in the editor.

# Notification for a reminder for a trip
class TripReminderNotification < Notification

  def to_link(current_user)
    trip = Trip.find(data_id)
    link = ""
    messages_link = StringIO.new
    messages_link_array = Array.new
    message_threads = Message.thread_starter_only.where('trip_id = ?', trip.id)
    message_threads.each do |mt|
      if Message.all_in_thread(mt.id).count > 4
        messages_link_array << link_to(mt.sender.username, path_helper.user_message_path(mt.receiver, mt))
      end
    end

    # transform message link into string sentence
    if messages_link_array.size == 1
      messages_link << messages_link_array[0]
    elsif messages_link_array.size == 2
      messages_link << messages_link_array[0]
      messages_link << ' ' + I18n.t("general.and") + " "
      messages_link << messages_link_array[1]
    else
      messages_link_array.each_with_index do |link, index|
        messages_link << link
        if (index + 1) == (messages_link_array.size - 1)
          messages_link << ' ' + I18n.t("general.and") + " "
        elsif (index + 1) <= (messages_link_array.size - 2)
          messages_link << ', '
        end
      end
    end
    
    link = I18n.t("notification.title.trip_reminder", :destination => trip.destination_location, :messages_link => messages_link.string)
    
    return link
  end
end
