class CreateReviews < ActiveRecord::Migration
  def self.up
    create_table :reviews do |t|
      t.column :giver_id, :integer, :null => false, :references => :users
      t.column :receiver_id, :integer, :null => false, :references => :users
      t.text :body
      t.string :type
      t.column :verified, :boolean, :default => false
      t.column :invitation, :boolean, :default => false
      t.timestamps
    end
  end

  def self.down
  end
end
