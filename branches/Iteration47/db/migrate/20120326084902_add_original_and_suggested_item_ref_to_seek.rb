class AddOriginalAndSuggestedItemRefToSeek < ActiveRecord::Migration
  def self.up
    add_column :seeks, :original_id, :integer, :null => true, :references => :seeks
    add_column :seeks, :suggested_item_id, :integer, :null => true, :references => :suggested_items
  end

  def self.down
    remove_column :seeks, :original_id
    remove_column :seeks, :suggested_item_id
  end
end
