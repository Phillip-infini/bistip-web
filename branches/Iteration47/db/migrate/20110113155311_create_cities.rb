class CreateCities < ActiveRecord::Migration
  def self.up
    create_table :cities do |t|
      t.string :name
      t.string :region
      t.belongs_to :country
      
      t.timestamps
    end
    add_index(:cities, :id, :unique=>true)

    File.open("#{Rails.root}/db/data/cities.txt", "r") do |infile|
      # specify id explicitly, id starts from 1
      id = 1;
      infile.read.each_line do |city_row|
        country_code, name, region = city_row.chomp.split("|")
        city = City.new
        city.id = id
        city.country = Country.find_by_code(country_code)
        city.name = name
        city.region = region
        city.save
        id = id + 1
      end
    end

    # populate <any cities> seed data into cities table using list of countries
    # singapore and hong kong is a single cities in a country, so no all_cities
    id = City.count + 1
    Country.all.each do |country|
      if country.code == 'sg'
      elsif country.code == 'hk'
      else
        city = City.new
        city.id = id
        city.country = country
        city.name = City::ALL_CITIES
        city.region = "00"
        city.save
        id = id + 1
      end
    end

    city = City.new
    city.country = Country.find_by_code('anywhere')
    city.name = '<anywhere>'
    city.region = "06"
    city.save
  end

  def self.down
    drop_table :cities
  end
end
