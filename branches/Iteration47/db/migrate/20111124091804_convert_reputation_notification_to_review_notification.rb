class ConvertReputationNotificationToReviewNotification < ActiveRecord::Migration
  
  def self.up
    PositiveReputationNotification.all.each do |prn|
      prn.update_attribute(:type, 'ReviewNotification')
    end
    NegativeReputationNotification.all.each do |nrn|
      nrn.update_attribute(:type, 'ReviewNotification')
    end
  end

  def self.down
    ReviewNotification.destroy_all
  end
end
