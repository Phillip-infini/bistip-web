require 'test_helper'

class IncludeItemTest < ActiveSupport::TestCase

  test "should get all" do
    assert_equal 2, IncludeItem.all.size
  end

  test "generate array" do
    result = IncludeItem.generate_all_in_array
    assert_equal 2, result.size
  end
end
