require 'test_helper'

class ApiControllerTest < ActionController::TestCase
  
  test "should get trip match json" do
    get :trip_by_city, :format => 'json', :from => 'bandung', :to => 'jakarta'
    json = JSON.parse(@response.body)
    assert_equal 2, json.size
    assert_response :success

    get :trip_by_city, :format => 'json', :to => 'jakarta'
    json = JSON.parse(@response.body)
    assert_equal 6, json.size
    assert_response :success

  end

  test "should get trip match xml" do
    get :trip_by_city, :format => 'xml', :from => 'bandung', :to => 'jakarta'
    assert_xml_tag @response.body, :tag => 'trips'
    assert_response :success

    get :trip_by_city, :format => 'xml', :to => 'jakarta'
    assert_xml_tag @response.body, :tag => 'trips'
    assert_response :success
  end

  test 'should not get trip' do
    get :trip_by_city, :format => 'json', :from => 'dasdas'
    json = JSON.parse(@response.body)
    assert_equal I18n.t("api.error.not_found"), json[I18n.t('api.error.label.exception')]
    assert_response :success

    get :trip_by_city, :format => 'xml', :from => 'dasdas'
    assert_xml_tag @response.body, :tag => 'hash'
    assert_response :success
  end

  test "should get seek match json" do
    get :seek_by_city, :format => 'json', :from => 'jakarta', :to => 'sydney'
    json = JSON.parse(@response.body)
    assert_equal 3, json.size
    assert_response :success

    get :seek_by_city, :format => 'json', :to => 'sydney'
    json = JSON.parse(@response.body)
    assert_equal 5, json.size
    assert_response :success
  end

  test "should get seek match xml" do
    get :seek_by_city, :format => 'xml', :from => 'jakarta', :to => 'sydney'
    assert_xml_tag @response.body, :tag => 'seeks'
    assert_response :success

    get :seek_by_city, :format => 'xml', :to => 'sydney'
    assert_xml_tag @response.body, :tag => 'seeks'
    assert_response :success
  end

  test 'should not get seek' do
    get :seek_by_city, :format => 'json', :from => 'dasdas'
    json = JSON.parse(@response.body)
    assert_equal I18n.t("api.error.not_found"), json[I18n.t('api.error.label.exception')]
    assert_response :success

    get :seek_by_city, :format => 'xml', :from => 'dasdas'
    assert_xml_tag @response.body, :tag => 'hash'
    assert_response :success
  end

  test 'should allow login' do
    get :login, :format => 'json', :email_or_username => 'allan', :password => 'secret'
    json = JSON.parse(@response.body)
    assert_equal true, json['logged_in']
  end

  test 'should not allow login' do
    get :login, :format => 'json', :email_or_username => 'allan', :password => 'password123'
    json = JSON.parse(@response.body)
    assert_equal false, json['logged_in']
  end

  test 'should get user' do
    dono = users(:dono)
    get :get_user, :format => 'json', :auth_token => dono.api_auth_key
    json = JSON.parse(@response.body)
    assert_equal 'Dono', json['fullname']
  end

  test 'should get a escrow' do
    dono = users(:dono)
    escrow = escrows(:PaypalNotified)
    get :get_escrow, :format => 'json', :auth_token => dono.api_auth_key, :escrow_id => escrow.id
    json = JSON.parse(@response.body)
    assert_equal Escrow::STATE_PAYPAL_NOTIFIED, json['current_state']
  end

  test 'should get escrows' do
    dono = users(:dono)
    get :get_escrows, :format => 'json', :auth_token => dono.api_auth_key
    json = JSON.parse(@response.body)
    assert_equal dono.escrows_as_buyer.size + dono.escrows_as_seller.size, json.size
  end

  test 'should do seller_got_item' do
    mac = users(:mactavish)
    escrow = escrows(:PaypalNotified)
    get :seller_got_item, :format => 'json', :auth_token => mac.api_auth_key, :escrow_id => escrow.id
    json = JSON.parse(@response.body)
    assert_equal true, json['escrow_success']
  end

  test 'should do buyer_received_item' do
    dono = users(:dono)
    escrow = escrows(:SellerGotItem)
    get :buyer_received_item, :format => 'json', :auth_token => dono.api_auth_key, :escrow_id => escrow.id, :review_type => 'PositiveReview', :review_body => 'good review as always'
    json = JSON.parse(@response.body)
    assert_equal true, json['escrow_success']
  end

  test 'should do seller_cancelled' do
    mac = users(:mactavish)
    escrow = escrows(:PaypalNotified)
    get :seller_cancelled, :format => 'json', :auth_token => mac.api_auth_key, :escrow_id => escrow.id
    json = JSON.parse(@response.body)
    assert_equal true, json['escrow_success']
  end

  test 'should do seller_requested_payout' do
    mac = users(:mactavish)
    escrow = escrows(:BuyerReceivedItem)
    get :seller_requested_payout, :format => 'json', :auth_token => mac.api_auth_key, :escrow_id => escrow.id, :review_type => 'PositiveReview', :review_body => 'good review as always'
    json = JSON.parse(@response.body)
    assert_equal true, json['escrow_success']
  end

  test 'should do seller_received_payout' do
    mac = users(:mactavish)
    escrow = escrows(:BistipReleasedSellerPayout)
    get :seller_received_payout, :format => 'json', :auth_token => mac.api_auth_key, :escrow_id => escrow.id
    json = JSON.parse(@response.body)
    assert_equal true, json['escrow_success']
  end

  test 'should do buyer_resumed' do
    dono = users(:dono)
    escrow = escrows(:SellerCancelled)
    get :buyer_resumed, :format => 'json', :auth_token => dono.api_auth_key, :escrow_id => escrow.id
    json = JSON.parse(@response.body)
    assert_equal true, json['escrow_success']
  end

  test 'should do buyer_requested_payout' do
    dono = users(:dono)
    escrow = escrows(:SellerCancelled)
    get :buyer_requested_payout, :format => 'json', :auth_token => dono.api_auth_key, :escrow_id => escrow.id
    json = JSON.parse(@response.body)
    assert_equal true, json['escrow_success']
  end

  test 'should do buyer_received_payout' do
    dono = users(:dono)
    escrow = escrows(:BistipReleasedBuyerPayout)
    get :buyer_received_payout, :format => 'json', :auth_token => dono.api_auth_key, :escrow_id => escrow.id
    json = JSON.parse(@response.body)
    assert_equal true, json['escrow_success']
  end

end
