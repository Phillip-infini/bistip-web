require 'test_helper'

class PostsControllerTest < ActionController::TestCase
  setup do
    @post = posts(:ipadpost)
    @topic = @post.topic
    @forum = @topic.forum
  end

  test "should get new" do
    login_as(:one)
    get :new, :topic_id => @topic.id, :forum_id => @forum.id
    assert_response :success
  end

  test "should create post" do
    login_as(:one)
    assert_difference('Post.count') do
      post :create, :post => {:body => "I agree"}, :topic_id => @topic.id, :forum_id => @forum.id
    end

    assert_redirected_to forum_topic_path(@forum, @topic, :page => @topic.posts_last_page)
  end

  test "should create quote post" do
    login_as(:one)
    assert_difference('Post.count') do
      post :create, :post => {:body => "I agree"}, :topic_id => @topic.id, :forum_id => @forum.id, :quoted_post_id => @post
    end

    assert_redirected_to forum_topic_path(@forum, @topic, :page => @topic.posts_last_page)
  end

  test "should get edit" do
    login_as(:one)
    get :edit, :id => @post.id, :topic_id => @topic.id, :forum_id => @forum.id
    assert_response :success
  end

  test "should update post" do
    login_as(:one)
    put :update, :id => @post.id, :topic_id => @topic.id, :forum_id => @forum.id

    assert_redirected_to forum_topic_path(@forum, @topic, :page => assigns(:post).find_page)
  end

  test "should destroy topic" do
    login_as(:one)
    page_to_go = @post.find_page
    assert_difference('Post.active_only.count', -1) do
      delete :destroy, :id => @post.id, :topic_id => @topic.id, :forum_id => @forum.id
    end

    assert_redirected_to forum_topic_path(@forum, @topic, :page => page_to_go)
  end
end
