require 'test_helper'

class RelationStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "user :one knows user :dono" do
    dono = users(:dono)

    #login
    full_login_as(:one)

    #view user detail
    get profile_path(dono.username)
    assert_response :success
    assert_template 'show'

    #:one indicates that he knows :dono
    get new_user_social_path(dono)

    assert_response :redirect
    assert_redirected_to profile_path(dono.username)
    follow_redirect!
  end
end
