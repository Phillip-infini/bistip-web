class LanguagesController < ApplicationController
  ssl_allowed :index
  
  # returns matching language's englishName
  def index
    term = params[:term]
    if !term.blank?
      #build scoped Language by checking parameter
      scope = Language.scoped({})
      scope = scope.match_english_name(term)
      
      @lang = Array.new
      scope.each{|language|
        @lang << language.english_name
      }
    end
  end
end
