class StoryPicturesController < ApplicationController

  def show
     @story_picture = StoryPicture.find(params[:story_picture_id])
  end

end
