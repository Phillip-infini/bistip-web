class PointLogsController < ApplicationController
  
  before_filter :authenticate, :only => [:claim_escrow_cashback]
  before_filter :check_rest_password, :only => [:add_for_boost_asia_registration]

  def claim_escrow_cashback
    if current_user.has_non_cashback_escrow?
      escrow_id = Escrow.non_cashback_escrow(current_user).first.id
      SubtractForEscrowCashbackLog.create!(:user_id => current_user.id, :amount => SubtractForEscrowCashbackLog::NEEDED_POINT, :data_id => escrow_id)
      flash[:longer_notice] = t('point_logs.escrow_cashback.success')
      ContactUsNotifier.delay.email_cashback_request(current_user, escrow_id)
    end
    redirect_to dashboard_path
  end

  def add_for_boost_asia_registration
    user_id = params[:user_id]
    point = params[:point]
    AddForBoostAsiaRegistrationLog.create!(:user_id => user_id, :amount => point)
    redirect_to root_path
  end

  def add_for_demo_asia_registration
    user_id = params[:user_id]
    point = params[:point]
    AddForDemoAsiaRegistrationLog.create!(:user_id => user_id, :amount => 100)
    redirect_to root_path
  end

end
