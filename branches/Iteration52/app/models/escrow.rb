# a class that represent a escrow transaction between 2 user
class Escrow < ActiveRecord::Base

  include ActionView::Helpers

  PAYPAL_CERT_PEM = File.read("#{Rails.root}/certs/#{APP_CONFIG[:paypal_cert_file]}")
  APP_CERT_PEM = File.read("#{Rails.root}/certs/app_cert.pem")
  APP_KEY_PEM = File.read("#{Rails.root}/certs/app_key.pem")

  belongs_to :buyer, :class_name => "User", :foreign_key => 'buyer_id'
  belongs_to :seller, :class_name => "User", :foreign_key => 'seller_id'
  belongs_to :buyer_review, :class_name => "Review", :foreign_key => 'buyer_review_id'
  belongs_to :seller_review, :class_name => "Review", :foreign_key => 'seller_review_id'
  belongs_to :message, :class_name => "Message", :foreign_key => 'message_id'
  belongs_to :currency, :class_name => "Currency", :foreign_key => 'currency_id'
  has_many :logs, :class_name => 'EscrowLog', :foreign_key => 'escrow_id'
  belongs_to :payment_method, :class_name => 'PaymentMethod', :foreign_key => 'payment_method_id'

  attr_writer :current_step

  validates :amount, :presence => true, :valid_escrow_amount => true, :if => lambda { |o| o.current_step == "details" }
  validates :item, :presence => true, :length => {:within => 5..50}, :if => lambda { |o| o.current_step == "details" }
  validates :payment_method, :presence => true

  # list of escrow transaction state
  STATE_BUYER_INITIATED = 'buyer_initiated'
  STATE_PAYPAL_NOTIFIED = 'paypal_notified'
  STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER = 'bistip_received_buyer_bank_transfer'
  STATE_SELLER_GOT_ITEM = 'seller_got_item'
  STATE_BUYER_RECEIVED_ITEM = 'buyer_received_item'
  STATE_SELLER_REQUESTED_PAYOUT = 'seller_requested_payout'
  STATE_SELLER_RECEIVED_PAYOUT = 'seller_received_payout'
  STATE_BISTIP_RELEASED_SELLER_PAYOUT = 'bistip_released_seller_payout'
  STATE_BISTIP_RELEASED_BUYER_PAYOUT = 'bistip_released_buyer_payout'
  STATE_BUYER_RESUMED = 'buyer_resumed'
  STATE_SELLER_CANCELLED = 'seller_cancelled'
  STATE_BUYER_REQUESTED_PAYOUT = 'buyer_requested_payout'
  STATE_BUYER_RECEIVED_PAYOUT = 'buyer_received_payout'
  STATE_INVALID = 'invalid'

  # flow of the state
  SUCCESS_FLOW = [BuyerInitiatedLog, 
    [PaypalNotifiedLog, BistipReceivedBuyerBankTransferLog, BuyerResumedLog],
    SellerGotItemLog,
    BuyerReceivedItemLog,
    SellerRequestedPayoutLog,
    BistipReleasedSellerPayoutLog,
    SellerReceivedPayoutLog]

  CANCEL_FLOW = [BuyerInitiatedLog,
    [PaypalNotifiedLog, BistipReceivedBuyerBankTransferLog],
    [SellerGotItemLog, SellerCancelledLog],
    BuyerRequestedPayoutLog,
    BistipReleasedBuyerPayoutLog,
    BuyerReceivedPayoutLog]

  # event
  before_create :generate_invoice_number, :set_currency
  
  # scope
  scope :user_all_escrows, lambda {|user_id| {:conditions => ["buyer_id = ? or seller_id = ?", user_id, user_id]}}
  scope :by_buyer_id_and_not_seller_id_and_not_escrow_id, lambda {|user_id, cashback_escrow_seller_id, cashback_escrow_id| {:conditions => ["buyer_id = ? AND seller_id NOT IN (?) AND id NOT IN (?) AND buyer_review_id IS NOT NULL AND seller_review_id IS NOT NULL", user_id, cashback_escrow_seller_id, cashback_escrow_id]}}
  scope :newest, :order => "escrows.created_at DESC"

  LOG_STATE_MAP = {BuyerInitiatedLog => STATE_BUYER_INITIATED,
                   PaypalNotifiedLog => STATE_PAYPAL_NOTIFIED,
                   BistipReceivedBuyerBankTransferLog => STATE_BISTIP_RECEIVED_BUYER_BANK_TRANSFER,
                   SellerGotItemLog => STATE_SELLER_GOT_ITEM,
                   BuyerReceivedItemLog => STATE_BUYER_RECEIVED_ITEM,
                   SellerRequestedPayoutLog => STATE_SELLER_REQUESTED_PAYOUT,
                   SellerReceivedPayoutLog => STATE_SELLER_RECEIVED_PAYOUT,
                   BistipReleasedSellerPayoutLog => STATE_BISTIP_RELEASED_SELLER_PAYOUT,
                   BistipReleasedBuyerPayoutLog => STATE_BISTIP_RELEASED_BUYER_PAYOUT,
                   SellerCancelledLog => STATE_SELLER_CANCELLED,
                   BuyerResumedLog => STATE_BUYER_RESUMED,
                   BuyerRequestedPayoutLog => STATE_BUYER_REQUESTED_PAYOUT,
                   BuyerReceivedPayoutLog => STATE_BUYER_RECEIVED_PAYOUT}

  # get the current state of escrow determine by latest log type
  def current_state
    unless last_log.blank?
      Escrow.get_log_state(last_log)
    end
  end

  # get last action from user on the escrow
  def last_log
    logs = self.logs(:order_by => 'created_at asc')
    unless logs.blank?
      logs.last
    end
  end

  # get encrypted paypal parameters and argument
  def paypal_encrypted(return_url, notify_url)
    values = {
      :business => APP_CONFIG[:paypal_email],
      :cmd => '_xclick',
      :upload => 1,
      :invoice => self.invoice,
      :amount => self.amount_after_fee,
      :item_name => I18n.t('escrow.paypal.description', :item => self.item, :buyer => self.buyer.username, :seller => self.seller.username),
      :notify_url => notify_url,
      :return => return_url,
      :rm => 1,
      :cbt => 'back to Bistip',
      :no_shipping => 1,
      :no_note => 1,
      :shipping => 0.00,
      :handling => 0.00,
      :cert_id => APP_CONFIG[:paypal_cert_id]
    }
    encrypt_for_paypal(values)
  end

  # form wizard: show the current step in wizard
  def current_step
    @current_step || steps.first
  end

  # form wizard: list of steps
  def steps
    %w[details confirmation]
  end

  # form wizard: get the next step
  def next_step
    self.current_step = steps[steps.index(current_step)+1]
  end

  # form wizard: get the previous step
  def previous_step
    self.current_step = steps[steps.index(current_step)-1]
  end

  # form wizard: is it currently on the first step
  def first_step?
    current_step == steps.first
  end

  # form wizard: is it currently on the last step
  def last_step?
    current_step == steps.last
  end

  # form wizard: is all step in the wizard already pass validation
  def all_valid?
    steps.all? do |step|
      self.current_step = step
      valid?
    end
  end

  # static method to get the state string based on a log
  def self.get_log_state(log)
    state = LOG_STATE_MAP[log.class]
    unless state.blank?
      state
    else
      STATE_INVALID
    end
  end

  # get ampunt with currency
  def amount_with_currency
    number_to_currency(self.amount.to_s, :unit => (self.currency.name + ' '), :precision => 0)
  end

  # get ampunt with currency
  def amount_after_fee_with_currency
    number_to_currency(amount_after_fee, :unit => (self.currency.name + ' '), :precision => 0)
  end

  # get amount after fee
  def amount_after_fee
    self.payment_method.calculate_amount_after_cost(self.amount)
  end

  # generate notes for bank transfer
  def bank_transfer_notes
    self.buyer.username + ' ESC' + self.id.to_s
  end

  # check if payment is bank transfer type
  def payment_method_bank_trasnfer?
    self.payment_method.is_a? IndonesiaBankTransfer
  end

  # check if given user is buyer or seller
  def is_buyer_or_seller?(user)
    self.buyer == user or self.seller == user
  end

  # return participant of the message which are not the given user
  def not_this_user(user)
    if is_buyer_or_seller?(user)
      user == self.buyer ? self.seller : self.buyer
    end
  end

  def self.non_cashback_escrow(user)
    # collect all cashback escrow id to an array
    cashback_escrow = SubtractForEscrowCashbackLog.find_all_by_user_id(user.id)
    cashback_escrow_id = Array.new
    cashback_escrow_seller_id = Array.new
    cashback_escrow.each do |ce|
      esc = Escrow.find(ce.data_id)
      cashback_escrow_id << ce.data_id
      cashback_escrow_seller_id << esc.seller_id
    end

    cashback_escrow_id = '' if cashback_escrow_id.size == 0
    cashback_escrow_seller_id = '' if cashback_escrow_seller_id.size == 0

    # query all escrow not in cashback escrow id
    idr = Escrow.by_buyer_id_and_not_seller_id_and_not_escrow_id(user.id, cashback_escrow_seller_id, cashback_escrow_id).where('id > 90 and currency_id = 2 and amount >= 500000')
    usd = Escrow.by_buyer_id_and_not_seller_id_and_not_escrow_id(user.id, cashback_escrow_seller_id, cashback_escrow_id).where('id > 90 and currency_id = 1 and amount >= 50')
    idr + usd
  end

  # get name for display that will shorten if it's too long
  def item_short
    self.item.length > 25 ? self.item[0..25] + '...' : self.item
  end

  # options that apply in every data in webservice that fetch trip record
  def self.data_options
    return {:only => [:id, :item], :methods => [:amount_with_currency, :current_state, :current_state_display, :awaiting_display, :buyer_username, :seller_username], :skip_types => true, :skip_instruct => true}
  end

  def buyer_username
    self.buyer.username
  end

  def seller_username
    self.seller.username
  end

  def current_state_display
    self.last_log.to_display_string
  end

  def awaiting_display
    self.last_log.get_awaiting_string
  end

  def payment_received?
    return true if (PaypalNotifiedLog.find_by_escrow_id(self.id) or BistipReceivedBuyerBankTransferLog.find_by_escrow_id(self.id))
    false
  end

  private

    # encrypt info to send to paypal
    def encrypt_for_paypal(values)
      signed = OpenSSL::PKCS7::sign(OpenSSL::X509::Certificate.new(APP_CERT_PEM), OpenSSL::PKey::RSA.new(APP_KEY_PEM, ''), values.map { |k, v| "#{k}=#{v}" }.join("\n"), [], OpenSSL::PKCS7::BINARY)
      OpenSSL::PKCS7::encrypt([OpenSSL::X509::Certificate.new(PAYPAL_CERT_PEM)], signed.to_der, OpenSSL::Cipher::Cipher::new("DES3"), OpenSSL::PKCS7::BINARY).to_s.gsub("\n", "")
    end

    # method to generate invoice number
    def generate_invoice_number
      self.invoice = UUIDTools::UUID.timestamp_create
    end

    # method to set currency before create
    def set_currency
      self.currency = self.payment_method.get_currency
    end

end
