class CreateItemPictures < ActiveRecord::Migration
  def self.up
    create_table :item_pictures do |t|
      t.column :item_id, :integer, :null => false, :references => :items
      t.string :attachment_file_name
      t.string :attachment_content_type
      t.integer :attachment_file_size
      t.datetime :attachment_updated_at
      t.timestamps
    end
  end

  def self.down
    drop_table :item_pictures
  end
end
