class AddSaltToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :salt, :string
  end

  def self.down
    remove_column :messages, :salt
  end
end
