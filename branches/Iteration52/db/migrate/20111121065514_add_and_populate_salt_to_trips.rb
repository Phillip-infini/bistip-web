class AddAndPopulateSaltToTrips < ActiveRecord::Migration
  def self.up
    add_column :trips, :salt, :string
    Trip.all.each do |trip|
      trip.generate_salt
      trip.save
    end
  end

  def self.down
    remove_column :trips, :salt
  end
end