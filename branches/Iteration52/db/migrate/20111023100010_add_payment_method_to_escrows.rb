class AddPaymentMethodToEscrows < ActiveRecord::Migration
  def self.up
    add_column :escrows, :payment_method_id, :integer, :null => false, :references => :payment_methods

    Escrow.all.each do |e|
      e.payment_method = Paypal.first
      e.save!
    end
  end

  def self.down
    remove_column :escrows, :payment_method_id
  end
end
