class AddBodyOriginalToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :body_original, :text
  end

  def self.down
    remove_column :messages, :body_original
  end
end
