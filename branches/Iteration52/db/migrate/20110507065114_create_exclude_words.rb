class CreateExcludeWords < ActiveRecord::Migration
  def self.up
    create_table :exclude_words do |t|
      t.string :word
      t.timestamps
    end
  end

  def self.down
    drop_table :exclude_words
  end
end
