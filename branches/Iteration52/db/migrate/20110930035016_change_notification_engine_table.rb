class ChangeNotificationEngineTable < ActiveRecord::Migration
  def self.up
    execute('ALTER TABLE notifications ENGINE = MyISAM')
  end

  def self.down
    execute('ALTER TABLE notifications ENGINE = innodb')
  end
end
