require 'test_helper'

class TripCommentsControllerTest < ActionController::TestCase
  setup do
    @trip = trips(:tokjak)
    @comnt = comments(:trip1)
  end

  # Replace this with your real tests.
  test "should create comment on trip" do
    login_as(:one)
    assert_difference('Comment.count') do
      post :create, :trip_id => @trip.id, :comment => {
        :body=>"trip comment test"
      }
    end
    assert_redirected_to assigns(:comment).get_path
  end

  test "should destroy comment on trip" do
    login_as(:one)
    delete :destroy, :trip_id => @trip.id, :id =>@comnt.id
    assert_equal 'comment deleted', assigns(:comment).body
    assert_redirected_to assigns(:comment).get_path
  end

  test "should create a reply comment on a trip comment" do
    login_as(:one)
    assert_difference('Comment.count') do
      post :create, :trip_id => @trip.id, :comment => {
        :body=>"trip comment reply", :reply_to_id=>@comnt.id
      }
    end
    assert_response :redirect
    assert_redirected_to trip_path(@trip, :page => @trip.last_comment_page, :anchor=>@comnt.internal_id)

  end
  
  test "should create a reply comment on a trip comment and delete it" do
    login_as(:dono)
    @repcomment = comments(:reptrip)
    delete :destroy, :trip_id => @trip.id, :id =>@repcomment.id
    assert_response :redirect
    assert_redirected_to assigns(:comment).get_path

  end
end