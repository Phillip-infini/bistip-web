require 'test_helper'

class FacebookControllerTest < ActionController::TestCase
  # initialize current user
  setup do
    login_as(:facebook)
  end

  #get /dashboard
  test "should get facebook mutual friends count" do
    get :mutual_friends_count, :user_id => '123', :access_token => '123', :target_user_id => users(:one).id
    assert_response :success
  end

  #get /dashboard
  test "should get facebook mutual friends" do
    get :mutual_friends, :user_id => '123', :access_token => '123', :target_user_id => users(:one).id
    assert_response :success
  end

end
