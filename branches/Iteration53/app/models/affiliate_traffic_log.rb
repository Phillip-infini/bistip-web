class AffiliateTrafficLog < ActiveRecord::Base
  scope :created_between, lambda {|start_period, end_period| {:conditions => ["created_at >= ? and created_at <= ?", start_period, end_period]}}
  scope :affiliate_is, lambda {|affiliate| {:conditions => ["affiliate_id = ?", affiliate.id]}}
end
