# class that wrap an activity in Bistip
class SellerEscrowActivityWrap < ActivityWrap
  CNAME = 'seller_escrow_activity_wrap'

  def cname
    CNAME
  end
end
