class SearchSeekController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    @origin = check_location(params[:origin])
    @destination = check_location(params[:destination])
    keyword = params[:keyword]
    notice = Array.new

    # check if a given location is a valid city
    if @origin.blank? and params[:origin]
      matched = City.build_match(false, false, params[:origin])
      if matched.count == 1
        @origin = matched[0]
      else
        notice << t('errors.location_not_valid', :value => params[:origin])
        if matched.count > 1
          @origin_suggestion = matched.take(3)
        end
      end
    end

    if @destination.blank? and params[:destination]
      matched = City.build_match(false, false, params[:destination])
      if matched.count == 1
        @destination = matched[0]
      else
        notice << t('errors.location_not_valid', :value => params[:destination])
        if matched.count > 1
          @destination_suggestion = matched.take(3)
        end
      end
    end

    # build scoped Seek by passing parameter to method build_scope_search
    scope = Seek.build_scope_search(@origin, @destination, keyword)
    @no_valid_parameter = (@origin.blank? and @destination.blank? and keyword.blank?)
    @total_result = scope.size
    @seeks = scope.paginate(:page=>params[:page], :per_page => Seek::SEARCH_PER_PAGE)

    flash.now[:alert] = notice unless notice.empty?
  end

end
