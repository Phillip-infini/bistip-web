module SeeksHelper

  # predefined value for destination location
  def get_predefined_destination_location
    destination_location_pre = ''
    if !@seek.destination_location.blank?
      @seek.destination_location
    elsif params[:destination_location] or !current_user.city.blank?
      destination_location_pre = current_user.city_location unless current_user.city.blank?
      destination_location_pre = params[:destination_location] if params[:destination_location]
      destination_location_pre
    else
      @seek.destination_location
    end
  end

  def get_predefined_origin_location
    origin_location_pre = ''
    if params[:origin_location]
      origin_location_pre = params[:origin_location]
      origin_location_pre
    else
      @seek.origin_location
    end
  end
end
