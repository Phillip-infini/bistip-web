class CreateRequestedItemTracks < ActiveRecord::Migration
  def self.up
    create_table :requested_item_tracks do |t|
      t.column :origin_city_id, :integer, :null => false, :references => :cities
      t.string :name
      t.integer :count
      t.timestamps
    end
  end

  def self.down
    drop_table :requested_item_tracks
  end
end
