class CreateAffiliatePayments < ActiveRecord::Migration
  def self.up
    create_table :affiliate_payments do |t|
      t.belongs_to :affiliate
      t.datetime :start
      t.datetime :end
      t.timestamps
    end
  end

  def self.down
    drop_table :affiliate_payments
  end
end
