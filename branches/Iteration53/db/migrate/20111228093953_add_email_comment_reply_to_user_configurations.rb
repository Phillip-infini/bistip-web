class AddEmailCommentReplyToUserConfigurations < ActiveRecord::Migration
  def self.up
    add_column :user_configurations, :email_comment_reply, :boolean, :default => true
  end

  def self.down
    remove_column :user_configurations, :email_comment_reply
  end
end
