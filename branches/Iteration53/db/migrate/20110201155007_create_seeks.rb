class CreateSeeks < ActiveRecord::Migration
  def self.up
    create_table :seeks do |t|
      t.column :origin_city_id, :integer, :null => false, :references => :cities
      t.column :destination_city_id, :integer, :null => false, :references => :cities
      t.column :deleted, :boolean, :default => false
      t.belongs_to :user
      t.string :departure_date_predicate
      t.datetime :departure_date
      t.text :notes

      t.timestamps
    end
    add_index(:seeks, :id, :unique=>true)
  end

  def self.down
    drop_table :seeks
  end
end
