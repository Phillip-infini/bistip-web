class Story < ActiveRecord::Base

  # scope
  scope :active_only, lambda { {:conditions => ["deleted = ?", false]} }

  # constant
  TITLE_MAX_LENGTH = 75
  TITLE_MIN_LENGTH = 10
  BODY_MAX_LENGTH = 1000
  CARRIAGE_RETURN_BUFFER = 100
  BODY_MIN_LENGTH = 30
  PER_PAGE = 5

  # validation
  validates :title, :presence => true,
            :length => {:maximum => TITLE_MAX_LENGTH, :minimum => TITLE_MIN_LENGTH}
  validates :user, :presence => true
  validates :body, :presence => true,
            :length => {:maximum => (BODY_MAX_LENGTH + CARRIAGE_RETURN_BUFFER), :minimum => BODY_MIN_LENGTH}
  
  # story belongs to user
  belongs_to :user
  has_many :story_pictures
  accepts_nested_attributes_for :story_pictures, :reject_if => lambda { |a| a[:attachment].blank? }

  # before save
  before_save :handle_point_log

  # check if the story has picture
  def has_more_than_one_picture?
    unless self.story_pictures.blank?
      self.story_pictures.count > 1
    else
      false
    end
  end

  # friendly URL for SEO
  def to_param
    "#{id} #{title}".parameterize
  end


  # check if user has picture
  def has_picture?
    self.story_pictures.count > 0
  end

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  # check if the suggested has been approved or not
  def active?
    !self.deleted?
  end

  # get the amount of bistip that a user get with this story
  def points
    self.has_picture? ? PointLog::STORY_WITH_PICTURE_POINT : PointLog::STORY_POINT
  end

  # handler for point log before save
  def handle_point_log
    if self.deleted
      point_log = AddForStoryLog.unscoped.find_by_data_id(self.id)
      point_log.update_attribute(:deleted, true) unless point_log.blank?
    else
      point_log = AddForStoryLog.unscoped.find_by_data_id(self.id)
      if point_log.blank?
        AddForStoryLog.create!(:user_id => self.user_id, :amount => self.points, :data_id => self.id)
      else
        point_log.update_attribute(:deleted, false)
      end
    end
  end

end
