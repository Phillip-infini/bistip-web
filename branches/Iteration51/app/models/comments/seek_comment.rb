# sub class of comment that represent Comment for Seek
class SeekComment < Comment

  # create notification and send email if needed
  def broadcast
    seek = Seek.find(self.seek_id)

    # always notify seek owner
    if !seek.owner?(self.user)
      Notifier.delay.email_seek_comment(seek.id, self.user, self) if seek.user.user_configuration.email_receive_seek_comment
      CommentNotification.create!(:receiver => seek.user, :sender => self.user, :data_id => self.id)
    end

    # if it's a reply then notify the original comment owner
    if self.reply?
      parent = self.thread_starter
      notified = Array.new

      # let parent comment owner knows
      if !seek.owner?(parent.user) and !(parent.user == self.user)
        Notifier.delay.email_seek_comment_reply(self, parent.user) if parent.user.user_configuration.email_comment_reply
        CommentReplyNotification.create!(:receiver => parent.user, :sender => self.user, :data_id => self.id)
        notified << parent.user
      end

      # reply all the previous contributor to this comment thread, except new commentator
      replies = Comment.find_comments_in_thread_and_except(parent.id, self.id)
      replies.each do |reply|
        user = reply.user

        # sned if not seek owner, not commentator and has not been notified
        if (!seek.owner?(user)) and (user != self.user) and (!notified.include?(user))
          Notifier.delay.email_seek_comment_reply(self, user) if user.user_configuration.email_comment_reply
          CommentReplyNotification.create!(:receiver => user, :sender => self.user, :data_id => self.id)
          notified << user
        end
      end
    end
  end

  # find which page this comment belongs to
  def find_page
    seek = Seek.find(self.seek_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    index = seek.comments.find_all_by_reply_to_id(nil).index(comment_to_use) + 1
    ((index * 1.0)/Comment::PER_PAGE).ceil
  end

  # generate path to the comment
  def get_path
    seek = Seek.find(self.seek_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    path_helper.seek_path(seek, :page => self.find_page, :anchor => comment_to_use.internal_id)
  end

  # generate path to the comment
  def get_url
    seek = Seek.find(self.seek_id)
    comment_to_use = self.reply? ? self.thread_starter : self
    path_helper.seek_url(seek, :page => self.find_page, :anchor => comment_to_use.internal_id)
  end

  # this method generate a link_to to this comment
  def get_path_link
    link_to(I18n.t('notification.field.seek'), get_path)
  end

end
