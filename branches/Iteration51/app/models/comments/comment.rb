# Comment class represent a comment from a user to a trip or seek
class Comment < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 600
  PER_PAGE = 5
  INTERNAL_ID = 'comment'
  POST_INTERNAL_ID = 'postComment'
  REPLY_INTERNAL_ID = 'postCommentReply'
  EMAIL_REPLY_DOMAIN = APP_CONFIG[:email_reply_address]
  EMAIL_REPLY_PREFIX = "cr"
  EMAIL_REPLY_SEPARATOR = '-'
  EMAIL_REPLY_FROM = 'mail@bistip.com'

  # rules to filter email reply body
  EMAIL_REPLY_RULES = [lambda { |body_line, message| body_line.include? EMAIL_REPLY_FROM},
    lambda { |body_line, message| body_line.squeeze == '>_' or body_line.squeeze == '>-' or body_line.squeeze == '_'},
    lambda { |body_line, message| body_line.include? '-----Original Message-----'}
  ]

  # include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper

  # scope
  default_scope :order => 'created_at ASC'
  scope :body_contains, lambda { |keyword| {:conditions => ["match(body) against(?)", keyword]}}
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ? and deleted = ?", 60.days.ago, false]}}
  scope :find_comments_in_thread_and_except, lambda { |thread_id, except_id| {:conditions => ["reply_to_id = ? and id <> ?", thread_id, except_id]}}

  # relationship
  belongs_to :seek
  belongs_to :trip
  belongs_to :user
  belongs_to :thread_starter, :class_name => "Comment", :foreign_key => 'reply_to_id'
  has_many :replies, :class_name => "Comment", :foreign_key => 'reply_to_id'

  # event
  before_create :generate_salt
  after_create :broadcast

  # validation
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH },
    :presence => true

  # override body
  def body
    if self.deleted?
      I18n.t('comment.deleted')
    elsif self.email_reply?
      Comment.get_main_part(self[:body], self)
    else
      self[:body]
    end
  end
  
  # cut forwarded or quoted text, if mailgun cannot detect them
  def self.get_main_part(body, comment)
    # do nothing if body is blank
    return body if body.blank?
    body_lines = body.split(/\n/)
    cut_index = body_lines.size
    body_lines.each_with_index do |body_line, index|
      unless body_line.blank?
        EMAIL_REPLY_RULES.each do |rule|
          if rule.call(body_line, comment)
            cut_index = index if index < cut_index
          end
        end
      end
    end

    main_lines = body_lines.take(cut_index)
    main_lines.join("\n")
  end

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  # this method should be call from instance of Comment subclass or will be raise NotImplemented error
  def get_path
    raise NotImplementedError
  end

  # this method should be call from instance of Comment subclass or will be raise NotImplemented error
  def get_url
    raise NotImplementedError
  end

  # this method generate a link_to to this comment
  def get_path_link
    raise NotImplementedError
  end

  # helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end

  # method to populate salt for new account
  def generate_salt
    self.salt = ActiveSupport::SecureRandom.hex(4)
  end

  # check if comment is a reply commnet
  def reply?
    !self.reply_to_id.blank?
  end

  # check if this comment is a thread starter
  def thread_starter?
    self.reply_to_id.nil?
  end

  # reply internal id to use for view anchor
  def reply_internal_id
    "#{REPLY_INTERNAL_ID}-#{self.id}"
  end

  # internal id of comment
  def internal_id
    "#{INTERNAL_ID}-#{self.id}"
  end

  # return email reply address
  def email_reply_address
    "#{EMAIL_REPLY_PREFIX}-#{self.id}-#{self.salt}@#{EMAIL_REPLY_DOMAIN}"
  end

end
