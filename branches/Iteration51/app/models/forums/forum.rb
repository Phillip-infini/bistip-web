class Forum < ActiveRecord::Base

  # constants
  TITLE_MAXIMUM_LENGTH = 50
  DESCRIPTION_MAXIMUM_LENGTH = 200

  # relationship
  belongs_to :user
  has_many :topics
  has_many :posts, :through => :topics

  # validation
  validates :title, :length => { :maximum => TITLE_MAXIMUM_LENGTH }, :presence => true
  validates :description, :length => { :minimum => 5, :maximum => DESCRIPTION_MAXIMUM_LENGTH }

  # scope
  scope :active_only, lambda { {:conditions => ["forums.deleted = ?", false]} }

  # count number of post in this forum
  def posts_count
    topics = self.topics
    count_posts = 0
    topics.each do |topic|
      count_posts += topic.posts.active_only.count
    end
    count_posts
  end

  # check if given user is the owner of the forum
  def owner?(user)
    self.user == user
  end

  # title of the forum
  def name
    self.title
  end

  # get last post on this forum
  def last_post
    self.posts.active_only.last
  end
end
