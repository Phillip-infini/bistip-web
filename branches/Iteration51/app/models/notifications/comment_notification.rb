# replacement for TripCommentNotification and SeekCommentNotification
class CommentNotification < Notification

  # override to_link method to perform specified action of TripComment notification
  def to_link(current_user)
    comment = Comment.find(data_id)
    link = I18n.t("notification.title.comment_object_owner",
      :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
      :link => comment.get_path_link)
    return link
  end
  
end
