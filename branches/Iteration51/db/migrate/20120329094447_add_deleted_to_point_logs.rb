class AddDeletedToPointLogs < ActiveRecord::Migration
  def self.up
    add_column :point_logs, :deleted, :boolean, :default => false
  end

  def self.down
    remove_column :point_logs, :deleted
  end
end
