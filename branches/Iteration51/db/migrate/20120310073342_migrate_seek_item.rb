class MigrateSeekItem < ActiveRecord::Migration
  def self.up
    added = 0
    Seek.all.each do |s|
      seek_items = Item.find_all_by_seek_id(s.id)
      if seek_items.size == 0
        new_item = SeekItem.new
        new_item.seek_id = s.id
        new_item.name = 'Contact me for info'
        new_item.save
        added += 1
      end
    end
    puts 'Seek new item added ' + added.to_s
  end

  def self.down
  end
end
