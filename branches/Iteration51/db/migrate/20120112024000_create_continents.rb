class CreateContinents < ActiveRecord::Migration
  def self.up
    create_table :continents do |t|
      t.string :code
      t.string :name
      t.timestamps
    end
    Continent.create!(:code => 'as', :name => 'Asia')
    Continent.create!(:code => 'eu', :name => 'Europe')
    Continent.create!(:code => 'na', :name => 'North America')
    Continent.create!(:code => 'sa', :name => 'South America')
    Continent.create!(:code => 'af', :name => 'Africa')
    Continent.create!(:code => 'oc', :name => 'Oceania')
    Continent.create!(:code => 'an', :name => 'Antartica')
    add_index(:continents, :id, :unique => true)
  end

  def self.down
    drop_table :continents
  end
end
