class CreateBadges < ActiveRecord::Migration
  def self.up
    create_table :badges do |t|
      t.column :user_id, :integer, :null => false, :references => :users
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :badges
  end
end
