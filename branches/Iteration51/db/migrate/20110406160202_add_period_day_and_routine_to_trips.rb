class AddPeriodDayAndRoutineToTrips < ActiveRecord::Migration
  def self.up
    add_column :trips, :period, :string
    add_column :trips, :day, :string
    add_column :trips, :routine, :boolean, :default => false
  end

  def self.down
    remove_column :trips, :routine
    remove_column :trips, :day
    remove_column :trips, :period
  end
end
