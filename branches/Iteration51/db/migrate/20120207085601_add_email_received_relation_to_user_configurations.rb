class AddEmailReceivedRelationToUserConfigurations < ActiveRecord::Migration
  def self.up
    add_column :user_configurations, :email_received_relation, :boolean, :default => true
  end

  def self.down
    remove_column :user_configurations, :email_received_relation
  end
end
