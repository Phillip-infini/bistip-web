class CreateTipUnits < ActiveRecord::Migration
  def self.up
    create_table :tip_units do |t|
      t.string :name
      t.string :symbol
      t.column :before_number, :boolean, :default => true
    end
    
    # pre populate
    tipunit = TipUnit.new
    tipunit.name = 'Percentage'
    tipunit.symbol = '%'
    tipunit.before_number = false
    tipunit.save

    tipunit = TipUnit.new
    tipunit.name = 'Indonesian Rupiah'
    tipunit.symbol = 'IDR'
    tipunit.save
  end

  def self.down
    drop_table :tip_units
  end
end
