require 'test_helper'

class ReviewStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "user :one giving a positive review to user :dono" do
    one = users(:one)
    dono = users(:dono)

    #login
    full_login_as(:one)

    #view user detail
    get profile_path(dono.username)
    assert_response :success
    assert_template 'show'

    #give one positive reputation to :dono
    post user_reviews_path(dono), :type => 'PositiveReview', :review => {
      :body => "very bad service",
      :message_id => messages(:donotoone1).id
    }

    assert_response :redirect
    assert_redirected_to profile_path(dono.username)
    follow_redirect!
  end

  test "user :one giving a negative review to user :dono" do
    one = users(:one)
    dono = users(:dono)

    #login
    full_login_as(:one)

    #view user detail
    get profile_path(dono.username)
    assert_response :success
    assert_template 'show'

    #give one positive reputation to :dono
    post user_reviews_path(dono), :type => 'NegativeReview', :review => {
      :body => "very good service",
      :message_id => messages(:donotoone1).id
    }
    
    assert_response :redirect
    assert_redirected_to profile_path(dono.username)
    follow_redirect!
  end
end
