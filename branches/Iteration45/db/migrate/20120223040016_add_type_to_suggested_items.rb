class AddTypeToSuggestedItems < ActiveRecord::Migration
  def self.up
    add_column :suggested_items, :type, :string
  end

  def self.down
    drop_column :suggested_items, :type
  end
end
