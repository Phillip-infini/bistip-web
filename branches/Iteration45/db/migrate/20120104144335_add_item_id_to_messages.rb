class AddItemIdToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :suggested_item_id, :integer, :references => :suggested_items
  end

  def self.down
    remove_column :messages, :suggested_item_id
  end
end
