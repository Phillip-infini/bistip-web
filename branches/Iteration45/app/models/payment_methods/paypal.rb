# a class to represent a payment method
class Paypal < PaymentMethod
  PAYPAL_CNAME = 'paypal'
  PAYPAL_FEE_COST = 0.00

  #include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper

  # get cname of payment method
  def cname
    PAYPAL_CNAME
  end

  # static method to calculate amount after cost according to payment method
  def calculate_amount_after_cost(amount)
    unless amount.blank?
      amount = amount.to_f
      (amount * (1 + PAYPAL_FEE_COST))
    else
      0.00
    end
  end

  # static method to calculate amount after cost according to payment method
  def amount_allowable?(amount)
    (amount >= 20 and amount <= 2000)
  end

  # static to decide what path to return according to payment method
  def get_redirect_path(escrow)
    path_helper.redirect_paypal_escrows_path(:escrow_id => escrow)
  end

  # static to decide what currency to return according to payment method
  def get_currency
    Currency.find_by_name('USD')
  end
end
