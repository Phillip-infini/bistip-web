class SearchItemController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    origin = check_location(params[:origin])
    keyword = params[:keyword]
    notice = Array.new

    # check if a given location is a valid city
    if origin.blank? and params[:origin]
      notice << t('errors.location_not_valid', :value => params[:origin])
    end
    
    # build scoped SuggestedItem by passing parameter to method build_scope_search
    scope = SuggestedItem.build_scope_search(origin, keyword)
    @no_valid_parameter = (origin.blank? and keyword.blank?)
    @total_result = scope.size
    @suggested_items = scope.paginate(:page => params[:page], :per_page => SuggestedItem::SEARCH_PER_PAGE)

    flash.now[:alert] = notice unless notice.empty?
  end
end
