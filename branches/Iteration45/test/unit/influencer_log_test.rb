require 'test_helper'

class InfluencerLogTest < ActiveSupport::TestCase
  
  test "should create one point in influencer log" do
    trip = trips(:jktsyd)
    influencer = users(:mactavish)

    if trip.user != influencer
      ip_address = '192.168.1.44'
      log = InfluencerLog.new(:ip_address => ip_address, :influencer_id => influencer.id, :trip_id => trip.id)
      assert log.save if trip.active?
    end
  end

  test "should get top influencer" do
    assert_equal 2, InfluencerLog.top_influencer.size
  end

  test "should get influencer point" do
    assert_equal 2, InfluencerLog.influencer_point(users(:one))
  end

  test "should get influencer list" do
    assert_equal 2, InfluencerLog.influencer_list(users(:one)).count
  end

end
