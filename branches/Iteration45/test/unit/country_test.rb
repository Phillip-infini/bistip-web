require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  
  test "should find country" do
    assert_nothing_raised {Country.find(countries(:indonesia).id)}
    assert_nothing_raised {Country.find_by_code(countries(:indonesia).code)}
  end
  
end
