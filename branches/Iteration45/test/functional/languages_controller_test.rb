require 'test_helper'

class LanguagesControllerTest < ActionController::TestCase
  
  test "should get language match" do
    get :index, :format => 'js', :term => 'indone'
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:lang)
    assert_equal 1, assigns(:lang).size
  end
  
end
