class ApiController < ApplicationController

  include CheckLocation
  PAGE = 1
  PER_PAGE = 10
  
  #method to produce xml/json from given data structure with some option
  def respond_with_option(object, option)
    option = {:skip_instruct => true} if object.include?(t('api.error.label.exception'))
    respond_to do |format|
      format.json { render :json => object.to_json(option)}
      format.xml  { render :xml => object.to_xml(option) }
    end
  end

  #method that call when result is empty, parameters doesn't correct
  def generate_error_not_found_report(result, *data)
    error = {}
    error[t('api.label.request')] = request.path
    case data.length
      when 0
        error[t("api.label.parameter")] = t("api.error.no_parameter")
      when 1
        error[t('api.label.keyword')] = t("api.error.name_parameter", :name => data[0]) if !data[0].nil?
      when 2
        error[t('api.label.from')] = t("api.error.from_parameter", :origin => data[0]) if check_location(data[0]).nil? and !data[0].nil?
        error[t('api.label.to')] = t("api.error.to_parameter", :destination => data[1]) if check_location(data[1]).nil? and !data[1].nil?
    end
    error[t('api.label.result')] = t("api.error.result_empty") if result.blank?
    error = {t('api.error.label.exception') => t("api.error.not_found"), t('api.error.label.message') => error}
    return error
  end

  #GET api/v1/trips/from/:origin/to/:destination(.:format)
  def trip_by_city
    origin = check_location(params[:from])
    destination = check_location(params[:to])
    
    result = nil
    if !origin.nil? or !destination.nil?
      scope = Trip.build_scope_search(origin, destination)
      
      result = scope.paginate(:page=>(params[:page].nil? ? PAGE : params[:page]),
        :per_page => (params[:per_page].nil? ? PER_PAGE : params[:per_page]))
    end

    #check if action has :from and/or :to parameters
    if !params[:from].nil? or !params[:to].nil?
      #if params[:from] couldn't produce an origin location and params[:to] not specified
      if origin.nil? and !params[:from].blank? and params[:to].blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if params[:to] couldn't produce a destination location and params[:from] not specified
      elsif destination.nil? and !params[:to].blank? and params[:from].blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if params[:from] couldn't produce an origin location also with params[:to] couldn't produce a destination location
      elsif origin.nil? and destination.nil?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if result is empty
      elsif result.blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      end
    elsif params[:from].nil? and params[:to].nil?
      result = generate_error_not_found_report(result)
    end
    
    respond_with_option(result, Trip.data_options)
  end

  #GET /api/v1/seeks/from/:origin/to/:destination(.:format)
  def seek_by_city
    origin = check_location(params[:from])
    destination = check_location(params[:to])

    result = nil
    if !origin.nil? or !destination.nil?
      scope = Seek.build_scope_search(origin, destination)
      result = scope.paginate(:page=>(params[:page].nil? ? PAGE : params[:page]),
        :per_page => (params[:per_page].nil? ? PER_PAGE : params[:per_page]))
    end

    #check if action has :from and/or :to parameters
    if !params[:from].nil? or !params[:to].nil?
      #if params[:from] couldn't produce an origin location and params[:to] not specified
      if origin.nil? and !params[:from].blank? and params[:to].blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if params[:to] couldn't produce a destination location and params[:from] not specified
      elsif destination.nil? and !params[:to].blank? and params[:from].blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if params[:from] couldn't produce an origin location also with params[:to] couldn't produce a destination location
      elsif origin.nil? and destination.nil?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      #if result is empty
      elsif result.blank?
        result = generate_error_not_found_report(result, params[:from], params[:to])
      end
    elsif params[:from].nil? and params[:to].nil?
      result = generate_error_not_found_report(result)
    end
    
    respond_with_option(result, Seek.data_options)
  end
end

