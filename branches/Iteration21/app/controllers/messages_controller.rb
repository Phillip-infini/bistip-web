class MessagesController < ApplicationController
  ssl_required :index, :show, :create, :sent, :new
  before_filter :authenticate, :only => [:index, :show, :create, :sent, :new]

  #new message
  def new
    @user = User.find(params[:user_id])
    @curr = current_user
    @message = Message.new
    3.times { @message.assets.build }

    if @user == @curr
      redirect_to root_path
    end
  end

  #get messages
  def index
    @messages = current_user.received_messages.order_by_created_at_descending.paginate(:per_page => Message::DEFAULT_PER_PAGE, :page => params[:page])
  end

  #get message
  def show
    @message = Message.find(params[:id])

    # Only sender or receiver can read the conversation!
    if (@message.sender == current_user or @message.receiver == current_user)
      # OK
    else
      redirect_to root_path
    end

    # find thread starter, if this message is a reply
    if !@message.thread_starter?
      @message = Message.find(@message.reply_to_id)
    end

    # if it's a thread starter and current user is not the sender then update the read attribute
    if current_user != @message.sender
      @message.update_attribute(:read, true)
    end

    # find all the child messages
    @messages = Message.find_all_by_reply_to_id(@message.id)
    for message in @messages do
      # only update the read attribute if current_user is the receiver
      if current_user != message.sender
        message.update_attribute(:read, true)
      end
    end
    
    # create a conversation between two users, prevent from user self messaging
    @sender = current_user
    @receiver = @message.receiver
    
    if @sender.eql?(@receiver)
      @receiver = @message.sender
    end
    
    @reply_to_id = @message.id
    
    # initialize new message for reply purpose
    @new_message = Message.new
    3.times { @new_message.assets.build }
  end

  #post message
  def create
    @receiver = User.find(params[:receiver_id])
    @message = @receiver.received_messages.new(params[:message])
    @message.sender = current_user

    # record sender IP
    @message.sender_ip = request.ip
    
    if @message.save
      #check if reply_to_id is nil, if nil there must be a new message, otherwise a reply message
      if @message.reply_to_id.nil?
        Notifier.delay.email_user_new_message(@message) if email_receive_message(@message.receiver)
      else
        Notifier.delay.email_user_reply_message(@message) if email_receive_message(@message.receiver)
      end
      flash[:notice] = t('message.create.success')
      redirect_to user_message_path(:user_id => current_user, :id => @message)
    else
      @curr = current_user
      @user = @receiver
      if @message.reply_to_id.nil?
        put_model_errors_to_flash(@message.errors)
        render :action => "new"
      else
        put_model_errors_to_flash(@message.errors, 'redirect')
        redirect_to user_message_path(:user_id => current_user, :id => @message.reply_to_id)
      end
    end
  end

  def sent
    @sents = current_user.sent_messages.order_by_created_at_descending.paginate(:per_page => 5, :page => params[:page])
  end
end
