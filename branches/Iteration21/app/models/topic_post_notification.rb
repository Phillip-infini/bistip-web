# To change this template, choose Tools | Templates
# and open the template in the editor.

class TopicPostNotification < Notification

  #overide to_link method to perform specified action of TopicPostNotification
  def to_link
    post = Post.unscoped.find(data_id)
    topic = post.topic
    forum = topic.forum

    link = I18n.t("notification.title.topic_post",
      :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
      :post_label=>link_to(I18n.t('notification.field.post'), path_helper.forum_topic_path(forum, topic, :page => post.find_page)))
    return link
  end
end
