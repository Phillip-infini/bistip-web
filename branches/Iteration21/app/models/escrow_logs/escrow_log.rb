class EscrowLog < ActiveRecord::Base
  belongs_to :escrow, :class_name => "Escrow", :foreign_key => 'escrow_id'
  after_create :broadcast

  # include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper

  # get the log as display string
  def to_display_string
    status = Escrow.get_log_state(self)
    I18n.t("escrow.state.#{status}", :seller => self.escrow.seller.username, :buyer => self.escrow.buyer.username)
  end

  # get the awaiting string of the current log
  def get_awaiting_string
    status = Escrow.get_log_state(self)
    I18n.t("escrow.awaiting.#{status}", :seller => self.escrow.seller.username ,:buyer => self.escrow.buyer.username)
  end

  # broadcast the creation of this log inform of notification and email
  def broadcast
    # sender is the actioner
    # receiver is either buyer and seller which is not the actioner
    sender = escrow.buyer
    receiver = escrow.seller

    if is_actioner_seller?
      # switch it around
      sender = escrow.seller
      receiver = escrow.buyer
    end
    EscrowLogNotification.create!(:data_id => self.id, :sender => sender, :receiver => receiver)
    Notifier.delay.email_escrow_log(receiver, self)
  end

  # convert this log into a notification form
  # this return notification text in format 'transaction: <body>'
  def to_notification_link(current_user)
    log_state = Escrow.get_log_state(self)
    escrow = self.escrow

    # generate path to the current user message. Current user must be the recipient
    transaction = link_to(I18n.t("general.transaction"),path_helper.user_message_path(current_user, escrow.message.id))
    body = I18n.t("notification.title.#{log_state}",
      :seller => link_to(escrow.seller.username, path_helper.profile_path(escrow.seller.username)),
      :buyer => link_to(escrow.buyer.username, path_helper.profile_path(escrow.buyer.username)))

    whole = I18n.t("notification.title.transaction",
      :transaction => transaction,
      :body => body)
    
    return whole
  end

  # return if the seller is actioner on this log
  # basically check if the name of state stars with 'seller'
  def is_actioner_seller?
    class_name = self.class.name
    if class_name.include?('Seller') or class_name.include?('Buyer')
      class_name.include?('Seller')
    else
      # actioner is not buyer or seller
      nil
    end
  end

  # helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end
  
end
