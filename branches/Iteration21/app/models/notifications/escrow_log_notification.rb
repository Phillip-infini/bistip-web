# class to handle all escrow log notification
class EscrowLogNotification < Notification
  def to_link(current_user)
    escrow_log = EscrowLog.unscoped.find(data_id)
    return escrow_log.to_notification_link(current_user)
  end
end
