require 'test_helper'

class SessionsControllerTest < ActionController::TestCase

  test "should render login form" do
    get :new
    assert_response :success
  end

  test "should allow user to login" do
    user_to_login = users(:one)
    post :create, :email_or_username => user_to_login.email, :password => "secret"
    assert_not_nil cookies[ApplicationController::AUTH_COOKIE]
    assert_redirected_to dashboard_path
  end

  test "should not allow user to login" do
    user_to_login = users(:one)
    post :create, :email_or_username => user_to_login.email, :password => "wrong"
    assert_not_nil flash[:alert], "Login fails notification is not reported"
    assert_nil cookies[ApplicationController::AUTH_COOKIE], "User id should not be in the session"
    assert_response :success
  end

  test "should logout user" do
    login_as(:one)
    get :destroy
    assert_redirected_to root_path
  end
end
