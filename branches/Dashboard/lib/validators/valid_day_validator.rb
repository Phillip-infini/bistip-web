# validator class to ensure that a date is a today's date or future date

class ValidDayValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !value.nil?
      value.each do |n|
        if (!Day::ALL.include?(n))
          record.errors[attribute] << (options[:message] || I18n.t('trip.errors.custom.valid_day'))
        end
      end
    end
  end
end