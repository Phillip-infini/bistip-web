require 'digest'
require 'paperclip'
require 'nilify_blanks'

# User class represent membership
class User < ActiveRecord::Base
  include CheckLocation
  
  nilify_blanks :only => [:avatar]

  # constant
  VALIDATION_KEY_PARAM = "key"
  RESET_PASSWORD_KEY_PARAM = "key"
  EMAIL_KEY_PARAM = "email"
  WEB_PREFIX = "http://"
  FACEBOOK_PREFIX = "http://www.facebook.com/"
  TWITTER_PREFIX = "http://www.twitter.com/"
  BIO_MAXIMUM_LENGTH = 500
  DAYS_AGE_TO_BE_REMINDED = 7
  NO_VALIDATION_PERIOD = 3

  # relationship
  has_one :user_configuration
  belongs_to :city, :class_name => "City", :foreign_key => 'city_id'

  has_many :trips
  has_many :seeks
  has_many :topics
  has_many :posts
  
  has_many :received_positive_reputations, :class_name => "PositiveReputation", :foreign_key => 'receiver_id'
  has_many :given_positive_reputations, :class_name => "PositiveReputation", :foreign_key => 'giver_id'

  has_many :received_negative_reputations, :class_name => "NegativeReputation", :foreign_key => 'receiver_id'
  has_many :given_negative_reputations, :class_name => "NegativeReputation", :foreign_key => 'giver_id'

  has_many :received_notifications, :class_name => "Notification", :foreign_key => 'receiver_id'
  has_many :sent_notifications, :class_name => "Notification", :foreign_key => 'sender_id'

  has_many :received_messages, :class_name => "Message", :foreign_key => 'receiver_id'
  has_many :sent_messages, :class_name => "Message", :foreign_key => 'sender_id'
  
  has_attached_file :avatar, :styles => { :original => "100x100>", :thumb => "48x48>" },
                             :default_url => '/images/default_:style_avatar.jpg',
                             :storage=> :s3,
                             :s3_credentials => "#{Rails.root}/config/s3.yml",
                             :path=> "/:attachment/:id/:style/:filename"

  validates_attachment_content_type :avatar, :content_type => ['image/gif', 'image/png', 'image/jpg', 'image/jpeg'],
                                             :message => I18n.t('user.update.message.avatar_must_be_image')

  validates_attachment_size :avatar, :less_than => 2.megabytes

  attr_accessor :password

  attr_accessor :eula

  # validation
  validates :username,
    :uniqueness => true,
    :length => { :within => 4..10 },
    :format => { :with => /^[a-zA-Z0-9]+$/i }

  validates :email,
    :uniqueness => true,
    :length => { :within => 5..100 },
    :format => { :with => /^[^@][\w.-]+@[\w.-]+[.][a-z]{2,4}$/i}

  validates :password,
    :presence => true, :if => :password_required?,
    :confirmation => true,
    :length => { :within => 4..15 }


  validates :eula, :acceptance => true, :on => :create
  validates :fullname, :presence => true, :length => { :maximum => 100 }
  validates :city_location, :presence => true
  validates :location, :length => { :maximum => 100 }
  validates :contact_number, :length => { :maximum => 100 }
  validates :extra_contact_number, :length => { :maximum => 100 }
  validates :web, :length => { :maximum => 100 }
  validates :facebook, :length => { :maximum => 100 }
  validates :twitter, :length => { :maximum => 100 }
  validates :bio, :length => { :maximum => BIO_MAXIMUM_LENGTH }

  # events
  before_save :encrypt_new_password, :generate_email_validation_key
  after_create :send_email_validation, :create_configuration

  def self.create_with_omniauth(omniauth)
    data = omniauth["extra"]["user_hash"]
    fullname = data["name"]
    location = data["location"]["name"] if !data["location"].nil?
    bio = data["bio"]

    uid = omniauth["uid"]
    provider = omniauth["provider"]
    web = omniauth["user_info"]["urls"]["Website"] if !omniauth["user_info"]["urls"].nil?
    fb = omniauth["user_info"]["urls"]["Facebook"] if !omniauth["user_info"]["urls"].nil?
    twitter = omniauth["user_info"]["urls"]["Twitter"] if !omniauth["user_info"]["urls"].nil?
    email = omniauth["user_info"]["email"]
    
    user = User.new
    user.uid = uid
    user.provider = provider
    
    user.email = email if !email.nil?
    user.fullname = fullname if !fullname.nil?
    user.facebook = fb if provider=="facebook" && !fb.nil?
    user.twitter = twitter if provider=="twitter" && !twitter.nil?
    user.web = web if !web.nil?
    user.bio = bio if !bio.nil?
    user.location = location if !location.nil?
    return user
  end

  def create_configuration
    config = UserConfiguration.new
    config.user_id = self.id
    config.save
  end
  
  # send email validation
  def send_email_validation
    Notifier.delay.email_validation(self) if !social_login?
  end

  # send email validation
  def send_email_validation_reminder
    Notifier.delay.email_validation_reminder(self) if !social_login?
  end

  # overwrite username setter to always downcase the value
  def username=(username)
    write_attribute(:username, username.downcase)
  end

  # static method to handle authentication
  def self.authenticate(email_or_username, password)
    user = find_by_email(email_or_username)
    if !user
      user = find_by_username(email_or_username)
    end
    return user if user && user.authenticated?(password)
  end

  # static method to handle OAuth authentication
  def self.oauth_authenticate(auth)
    return user = find_by_provider_and_uid(auth["provider"],auth["uid"])
  end

  # static method to update times reminded
  def self.update_times_reminded(user)
    user.times_reminded += 1
    user.save
  end

  # check method to see whether a password is a match
  def authenticated?(password)
    self.hashed_password == encrypt(password)
  end

  # check method to see whether email has been validated/account been activated
  def email_validated?
    # the first five days user can use without validation
    if self.created_at > NO_VALIDATION_PERIOD.days.ago
      true
    else
      self.email_validation_key.nil? or self.email_validation_key.empty?
    end
  end

  # equality for user
  def ==(other)
    return self.id == other.id
  end

  def social_login?
    !self.uid.nil? && !self.uid.empty? && !self.provider.nil? && !self.provider.empty?
  end

  def need_to_be_reminded_for_activation?
    if !email_validated? and self.times_reminded == 0 and self.created_at < DAYS_AGE_TO_BE_REMINDED.days.ago
      true
    else
      false
    end
  end

  def member_since
    self.created_at.strftime("%b %Y")
  end

  def need_to_complete_profile?
    count = 0
    count = count + 1 if self.fullname.blank?
    count = count + 1 if self.location.blank?
    count = count + 1 if self.contact_number.blank?
    count = count + 1 if self.twitter.blank?
    count = count + 1 if self.facebook.blank?
    count = count + 1 if self.web.blank?
    count = count + 1 if self.bio.blank?

    if count > 2
      true
    else
      false
    end
  end

  def has_trip?
    count = self.trips.size
    if count < 0
      return true
    else
      return false
    end
  end

  def has_seek?
    count = self.seeks.size
    if count < 0
      return true
    else
      return false
    end
  end

  # virtual attribute getter to get origin location
  def city_location
    city.name + ", " + city.country.name if city
  end

  # virtual attribute setter to accept origin location
  def city_location=(location)
    self.city = check_location(location)
  end

  
  protected
    # method to encrypt password before store it to database
    def encrypt_new_password
      return if password.blank?
      self.hashed_password = encrypt(password)
    end

    # method to populate email validation key for new account
    def generate_email_validation_key
      if self.new_record? and !social_login?
        self.email_validation_key = UUIDTools::UUID.timestamp_create
      end
    end

    # check method to help trigger password validation
    def password_required?
      hashed_password.blank? || password.present?
    end

    # helper to encrypt password
    def encrypt(string)
      Digest::SHA1.hexdigest(string)
    end

    def not_oauth_type?
      return uid == nil && provider == nil
    end

end
