class NotificationsController < ApplicationController
  before_filter :authenticate, :only => [:index]

  #show list of notification that receive by current_user
  def index
    data = current_user.received_notifications.find_all_by_read(false)
    data.each do |d|
      d.update_attribute(:read, true)
    end
    @notifications = current_user.received_notifications.paginate(:per_page => 10, :page => params[:page])
  end
end
