class SearchSeekController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    origin = check_location(params[:origin])
    destination = check_location(params[:destination])
    notes = params[:notes]

    # build scoped Seek by checking parameter
    scope = Seek.scoped({})

    if !origin.nil?
      if origin.all_cities?
        scope = scope.origin_country_id origin.country
      else
        scope = scope.origin_city_id origin.id
      end
    end

    if !destination.nil?
      if destination.all_cities?
        scope = scope.destination_country_id destination.country
      else
        scope = scope.destination_city_id destination.id
      end
    end

    # build predicate for notes keyword
    if !string_nil_or_empty?(notes)
      scope = scope.notes_contains notes
    end

    @seeks = scope.paginate(:page=>params[:page], :per_page => 5)
  end

end
