class DashboardController < ApplicationController
  def index
    if !current_user
      redirect_to root_path
    else
      @user = current_user
    end
  end

  def positive_reputations
    if current_user
      @user = current_user
      @positives = @user.received_positive_reputations.paginate(:page => params[:page], :per_page => 5)
    else
      redirect_to root_path
    end
  end

  def negative_reputations
    if current_user
      @user = current_user
      @negatives = @user.received_negative_reputations.paginate(:page => params[:page], :per_page => 5)
    else
      redirect_to root_path
    end
  end

  def trips
    if current_user
      @user = current_user
      @trips = Trip.unscoped.find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => 5)
    else
      redirect_to root_path
    end
  end

  def seeks
    if current_user
      @user = current_user
      @seeks = Seek.unscoped.find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => 5)
    else
      redirect_to root_path
    end
  end

  def profile
    if current_user
      @user = current_user
    else
      redirect_to root_path
    end
  end
end
