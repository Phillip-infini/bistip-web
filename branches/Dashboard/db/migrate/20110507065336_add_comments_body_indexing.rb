class AddCommentsBodyIndexing < ActiveRecord::Migration
  def self.up
    execute('ALTER TABLE comments ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_comments_body ON comments (body(1000))')
  end

  def self.down
    execute('DROP INDEX fulltext_comments_body ON comments')
    execute('ALTER TABLE comments ENGINE = innodb')
  end
end