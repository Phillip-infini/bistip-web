require 'test_helper'

class AssetTest < ActiveSupport::TestCase
  
  test "should find asset" do
    assert Asset.find(assets(:one).id)
  end

  test "should generate asset url" do
    asset = Asset.find(assets(:one).id)
    assert !asset.attachment.expiring_url(Asset::LINK_AGE).blank?
    assert !asset.attachment.expiring_url(Asset::LINK_AGE, :thumb).blank?
  end
end
