require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  
  test "tip with unit" do
    assert items(:ipad).tip_with_unit
    assert items(:galaxy).tip_with_unit
    assert items(:vitamin).tip_with_unit
  end
end
