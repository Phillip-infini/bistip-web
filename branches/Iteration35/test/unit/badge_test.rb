require 'test_helper'

class BadgeTest < ActiveSupport::TestCase
  
  test "user one should be trusted" do
    assert users(:one).has_trusted_badge?
  end

  test 'should create trusted badge for user dono' do
    assert TrustedBadge.create!(:user_id => users(:dono).id)
  end
end
