require 'test_helper'

class TopicTest < ActiveSupport::TestCase

  test "should create topic" do
    topic = Topic.new
    topic.title = 'Topic title'
    topic.user = users(:one)
    topic.forum = forums(:lounge)
    assert_difference('TopicView.count') do
      assert topic.save
    end
  end

  test "should find topic" do
    topic_id = topics(:ipad).id
    assert_nothing_raised {Topic.find(topic_id)}
  end

  test "should update topic" do
    topic = topics(:ipad)
    assert topic.update_attributes(:title => 'Ipad (updated)')
  end

  test "should delete topic" do
    topic_id = topics(:ipad).id
    assert_nothing_raised {Topic.find(topic_id)}
    topic = Topic.find(topic_id)
    assert topic.update_attributes(:deleted => true)
  end

  test 'posts last page' do
    assert topics(:ipad).posts_last_page == 2
    assert topics(:ipad).last_post.id == posts(:p9).id
  end
  
end
