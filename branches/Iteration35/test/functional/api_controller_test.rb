require 'test_helper'

class ApiControllerTest < ActionController::TestCase
  
  test "should get trip match json" do
    get :trip_by_city, :format => 'json', :from => 'bandung', :to => 'jakarta'
    json = JSON.parse(@response.body)
    assert_equal 2, json.size
    assert_response :success

    get :trip_by_city, :format => 'json', :to => 'jakarta'
    json = JSON.parse(@response.body)
    assert_equal 6, json.size
    assert_response :success

  end

  test "should get trip match xml" do
    get :trip_by_city, :format => 'xml', :from => 'bandung', :to => 'jakarta'
    assert_xml_tag @response.body, :tag => 'trips'
    assert_response :success

    get :trip_by_city, :format => 'xml', :to => 'jakarta'
    assert_xml_tag @response.body, :tag => 'trips'
    assert_response :success
  end

  test 'should not get trip' do
    get :trip_by_city, :format => 'json', :from => 'dasdas'
    json = JSON.parse(@response.body)
    assert_equal I18n.t("api.error.not_found"), json[I18n.t('api.error.label.exception')]
    assert_response :success

    get :trip_by_city, :format => 'xml', :from => 'dasdas'
    assert_xml_tag @response.body, :tag => 'hash'
    assert_response :success
  end

  test "should get seek match json" do
    get :seek_by_city, :format => 'json', :from => 'jakarta', :to => 'sydney'
    json = JSON.parse(@response.body)
    assert_equal 3, json.size
    assert_response :success

    get :seek_by_city, :format => 'json', :to => 'sydney'
    json = JSON.parse(@response.body)
    assert_equal 5, json.size
    assert_response :success
  end

  test "should get seek match xml" do
    get :seek_by_city, :format => 'xml', :from => 'jakarta', :to => 'sydney'
    assert_xml_tag @response.body, :tag => 'seeks'
    assert_response :success

    get :seek_by_city, :format => 'xml', :to => 'sydney'
    assert_xml_tag @response.body, :tag => 'seeks'
    assert_response :success
  end

  test 'should not get seek' do
    get :seek_by_city, :format => 'json', :from => 'dasdas'
    json = JSON.parse(@response.body)
    assert_equal I18n.t("api.error.not_found"), json[I18n.t('api.error.label.exception')]
    assert_response :success

    get :seek_by_city, :format => 'xml', :from => 'dasdas'
    assert_xml_tag @response.body, :tag => 'hash'
    assert_response :success
  end
end
