require 'test_helper'

class TripCommentStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "creating comment for a trip" do

    trip = trips(:tokjak)

    # login
    full_login_as(:one)

    # view trip
    get trip_path(trip)
    assert_response :success
    assert_template 'new'

    # add new comment on trip
    assert_difference ('trip.comments.count') do
      post trip_comments_path(trip), :comment => {
      :body => 'tripnya dono tokyo jakarta',
      :user_id => :one,
      :type => 'TripComment'
    }
    end
    assert trip.comments.last.thread_starter?
    assert_response :redirect
    assert_redirected_to trip_path(trip, :page => trip.last_comment_page)
    follow_redirect!

  end

  test "deleting trip comment" do
    trip = trips(:tokjak)
    comment = trip.comments.first

    # login
    full_login_as(:one)

    # view a trip
    get trip_path(trip)
    assert_response :success

    # deleting a comment
    delete trip_comment_path(trip, comment)
    assert_response :redirect
    assert_redirected_to trip
    follow_redirect!
  end

  test "replying trip comment" do
    trip = trips(:tokjak)
    comment = trip.comments.first
    #user login
    full_login_as(:dono)
    #user goes to the trip
    get trip_path(trip)
    assert_response :success
    #user post reply
    assert_difference ('trip.comments.count')do
      post trip_comments_path(trip), :comment =>{:body =>"replying",
      :user_id => :one,
      :type => 'TripComment',
      :reply_to_id => comment.id }
    end
    #user is redirected
    assert_response :redirect
    assert_redirected_to trip_path(trip, :page => trip.last_comment_page, :anchor=>comment.internal_id)
    assert trip.comments.last.reply?

  end

  test "deleting trip reply" do
    trip = trips(:tokjak)
    comment = comments(:reptrip)
    #user login
    full_login_as(:dono)
    #user goes to the trip
    get trip_path(trip)
    assert_response :success
    #user delete his comment
    delete trip_comment_path(trip,comment)
    #user is redirected
    assert_response :redirect
    assert_redirected_to trip
    follow_redirect!

  end

end
