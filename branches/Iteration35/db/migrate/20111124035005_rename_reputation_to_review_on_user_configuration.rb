class RenameReputationToReviewOnUserConfiguration < ActiveRecord::Migration
  def self.up
    rename_column :user_configurations, :email_receive_reputation, :email_receive_review
  end

  def self.down
    rename_column :user_configurations, :email_receive_review, :email_receive_reputation
  end
end
