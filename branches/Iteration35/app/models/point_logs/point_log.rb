class PointLog < ActiveRecord::Base
  belongs_to :balance
  belongs_to :currency

  INFLUENCER_LOG_POINT = 1
  STORY_POINT = 20
  STORY_WITH_PICTURE_POINT = 30
  SUGGESTED_ITEM_POINT = 5
  SUGGESTED_ITEM_WITH_PICTURE_POINT = 10

  # event
  before_create :set_currency

  # this method is to do operation on balance amount
  def do_point(amount)
    raise NotImplementedError
  end

  # generate title label
  def title_label
    raise NotImplementedError
  end

  # generate point label
  def point_label
    raise NotImplementedError
  end

  private
    def set_currency
      self.currency = Currency.find_by_name('USD')
    end
end
