class CommentReplyNotification < Notification

  # override to_link method to perform specified action of Comment Reply notification
  def to_link(current_user)
    comment = Comment.find(data_id)
    link = I18n.t("notification.title.comment_reply",
      :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
      :link => link_to(I18n.t('general.comment_thread'), comment.get_path))
    return link
  end
  
end
