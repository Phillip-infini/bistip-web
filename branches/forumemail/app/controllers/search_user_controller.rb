class SearchUserController < ApplicationController
  def index
    keyword = params[:keyword]
    if !keyword.blank? and !(keyword.length < 2)
      @users = User.name_or_username(keyword).paginate(:page=>params[:page], :per_page => 5)
    else
      @users = Array.new
    end
  end
end
