class DashboardController < ApplicationController
  before_filter :authenticate

  #GET index
  def index
    @user = current_user
    @incoming_trips = current_user.incoming_to_user_location
  end

  #GET current_user's positive reputations
  def positive_reputations
    @user = current_user
    @positives = @user.received_positive_reputations.paginate(:page => params[:page], :per_page => 5)
  end

  #GET current_user's negative reputations
  def negative_reputations
    @user = current_user
    @negatives = @user.received_negative_reputations.paginate(:page => params[:page], :per_page => 5)
  end

  #GET current_user's trips
  def trips
    @user = current_user
    @trips = Trip.unscoped.find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => 5)
  end

  #GET current_user's seeks
  def seeks
    @user = current_user
    @seeks = Seek.unscoped.find_all_by_user_id(@user.id).paginate(:page => params[:page], :per_page => 5)
  end

  #GET current_user's profile
  def profile
    @user = current_user
  end
end
