class SearchSeekController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    origin = check_location(params[:origin])
    destination = check_location(params[:destination])
    notes = params[:notes]

    # build scoped Seek by passing parameter to method build_scope_search
    scope = Seek.build_scope_search(origin, destination, notes)

    @seeks = scope.paginate(:page=>params[:page], :per_page => 5)
  end

end
