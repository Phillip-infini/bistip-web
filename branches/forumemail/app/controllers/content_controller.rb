class ContentController < ApplicationController
  before_filter :authenticate, :only => [:registered, :ppki]

  def ppki
    mod = 6
    win = 0
    @image_result = 'lanyard.png'
    @message = t('ppki.message.lanyard')
    if current_user.id % mod == win
      @image_result = 'kaos.png'
      @message = t('ppki.message.tshirt')
    end
  end
end
