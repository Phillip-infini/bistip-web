# Item class represent a item of trip or seek
class Item < ActiveRecord::Base

  # constants
  NAME_MAXIMUM_LENGTH = 50
  NAME_MINIMUM_LENGTH = 3
  
  # relationship
  belongs_to :seek
  belongs_to :trip
  belongs_to :tip_unit

  # validation
  validates :name, :presence => true, :length => { :minimum => NAME_MINIMUM_LENGTH, :maximum => NAME_MAXIMUM_LENGTH }
  validates :tip, :numericality => {:only_integer => true, :allow_nil => true, :less_than => 2000000001}

  # scope
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ?", 60.days.ago]}}

  # events
  before_save :clean_tip_unit
  
  # clean tip unit if no tip given
  def clean_tip_unit
    self.tip_unit = nil if self.tip.blank?
  end

  # tip display with unit append to it
  def tip_with_unit
    if self.tip_unit.before_number?
      self.tip_unit.symbol + ' ' + self.tip.to_s
    else
      self.tip.to_s + ' ' + self.tip_unit.symbol
    end
  end
end
