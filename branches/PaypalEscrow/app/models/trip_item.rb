# sub class of Item that represent item for Trip
class TripItem < Item

  # contants
  MAXIMUM = 8
  SUMMARY_COUNT = 2
end