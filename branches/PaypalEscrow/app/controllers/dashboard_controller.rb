class DashboardController < ApplicationController
  before_filter :authenticate
  DEFAULT_PAGING = 10
  LIMIT_INCOMING_TRIPS = 5
  #GET index
  def index
    @incoming_trips = current_user.incoming_to_user_location(LIMIT_INCOMING_TRIPS)
    @incoming_trips_count = current_user.incoming_to_user_location_count
  end

  #GET current_user's reputations
  def reputation
    @positives = current_user.received_positive_reputations
    @negatives = current_user.received_negative_reputations
  end
  
  #GET current_user's trips
  def trips
    current_user = current_user
    @trips = Trip.unscoped.order('created_at desc').find_all_by_user_id(current_user.id).paginate(:page => params[:page], :per_page => Trip::DEFAULT_PER_PAGE)
  end

  #GET current_user's seeks
  def seeks
    current_user = current_user
    @seeks = Seek.unscoped.order('created_at desc').find_all_by_user_id(current_user.id).paginate(:page => params[:page], :per_page => Seek::DEFAULT_PER_PAGE)
  end

  #GET current_user's profile
  def profile
    current_user = current_user
  end
  
  def escrows
    @escrows = Escrow.user_all_escrows(current_user.id)
  end
end
