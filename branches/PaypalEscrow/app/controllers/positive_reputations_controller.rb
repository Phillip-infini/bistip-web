class PositiveReputationsController < ApplicationController

  before_filter :authenticate, :not_self, :only => [:new, :create]

  # render new positive reputation form
  def new
    @user = User.find(params[:user_id])

    if params[:invitation]
      flash.now[:notice] = t('reputation.new.invitation', :receiver => @user.username)
    end
  end

  # render positive reputations of a user
  def index
    # deprecated, this index is just to handle legacy url
    @user = User.find(params[:user_id])
    redirect_to reputation_user_path(@user)
  end

  # create a positive reputation
  def create
    @user = User.find(params[:user_id])
    
    if PositiveReputation.last_24_hour(current_user.id, @user.id).count >= 1
      flash[:alert] = t('reputation.new.not_allowed.positive')
      redirect_to profile_path(@user.username)
    else
      @positive = @user.received_positive_reputations.new(params[:positive_reputation])
      @positive.giver = current_user

      if @positive.save
        # insert relation now that the user gave a reputation
        Relation.build_relation_from_reputation(@positive)
        #email notification
        Notifier.delay.email_receiver_positive_reputation(@user, current_user, @positive) if email_receive_reputation(@user)
        #live notification
        PositiveReputationNotification.create!(:receiver=>@user, :sender=>current_user, :data_id=>@user.id)
        flash[:notice] = t('reputation.create.success.positive')
        redirect_to reputation_user_path(@user.id)
      else
        put_model_errors_to_flash(@positive.errors)
        render :action => "new"
      end
    end
  end
end
