class ContactUsController < ApplicationController

  # send contact us details to our email
  def create
    name = params[:name]
    email = params[:email]
    subject = params[:subject]
    body = params[:body]
    validation_pass = true;

    # validation
    if !logged_in?
      if string_nil_or_empty?(name) or string_nil_or_empty?(email)
        flash.now[:alert] = t('contact_us.create.message.name_or_email_not_given')
        render :action => 'new'
        validation_pass = false;
      # if does not pass captcha and mobile does not need captcha
      elsif !verify_recaptcha && !mobile_version?
        flash.delete(:recaptcha_error)
        flash.now[:alert] = t('contact_us.create.message.captcha')
        render :action => 'new'
        validation_pass = false;
      end
    else
      name = current_user.username
      email = current_user.email
    end

    if body.blank? or body.length < 6 or body.length >= 800
      flash.now[:alert] = t('contact_us.create.message.body_length')
      if validation_pass
        render :action => 'new'
        validation_pass = false;
      end
    end

    # pass validation
    if validation_pass
      flash[:notice] = t('contact_us.create.message.success')
      ContactUsNotifier.delay.email_contact_us(name, email, subject, body)
      redirect_to root_path
    end
  end
end
