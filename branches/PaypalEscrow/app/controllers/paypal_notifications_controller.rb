class PaypalNotificationsController < ApplicationController
  protect_from_forgery :except => [:create]
  
  # receive and store paypal IPN
  def create

    if params[:payment_status] == "Completed" &&
      params[:secret] == APP_CONFIG[:paypal_secret] &&
      params[:receiver_email] == APP_CONFIG[:paypal_email]

      # create paypal notification object and PayPalNotifyLog
      PaypalNotification.create!(:params => params, :escrow_id => params[:invoice], :status => params[:payment_status], :transaction_id => params[:txn_id])
      PaypalNotifiedLog.create!(:escrow_id => params[:invoice])
    end
    render :nothing => true    
  end

  
end
