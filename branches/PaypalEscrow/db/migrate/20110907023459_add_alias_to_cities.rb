class AddAliasToCities < ActiveRecord::Migration
  def self.up
    add_column :cities, :alias, :string
  end

  def self.down
    remove_column :cities, :alias
  end
end
