# To change this template, choose Tools | Templates
# and open the template in the editor.

class MatchSeekNotification < Notification

  #overide method to provide information who create a seek that match with a trip that create by current_user
  def to_link(current_user)
    seek = Seek.unscoped.find(data_id)
    link = I18n.t("notification.title.match_seek",
      :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
      :seek_label => link_to(I18n.t("notification.field.seek"), path_helper.seek_path(seek)))
    return link
  end
end
