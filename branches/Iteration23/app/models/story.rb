class Story < ActiveRecord::Base

  # scope
  default_scope lambda {{ :conditions => ["deleted = ?", false] }}

  # constant
  TITLE_MAX_LENGTH = 75
  TITLE_MIN_LENGTH = 10
  BODY_MAX_LENGTH = 1000
  BODY_MIN_LENGTH = 30
  PER_PAGE = 5

  # validation
  validates :title, :presence => true,
            :length => {:maximum => TITLE_MAX_LENGTH, :minimum => TITLE_MIN_LENGTH}
  validates :user, :presence => true
  validates :body, :presence => true,
            :length => {:maximum => BODY_MAX_LENGTH, :minimum => BODY_MIN_LENGTH}
  
  # story belongs to user
  belongs_to :user

end
