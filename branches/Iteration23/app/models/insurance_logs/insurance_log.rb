class InsuranceLog < ActiveRecord::Base
  belongs_to :insurance, :class_name => "Insurance", :foreign_key => 'insurance_id'
  after_create :broadcast

  # include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper

  # get the log as display string
  def to_display_string
    status = Insurance.get_log_state(self)
    I18n.t("insurance.state.#{status}", :buyer => self.insurance.buyer.username, :seller => self.insurance.seller.username)
  end

  # get the awaiting string of the current log
  def get_awaiting_string
    status = Insurance.get_log_state(self)
    I18n.t("insurance.awaiting.#{status}", :buyer => self.insurance.buyer.username, :seller => self.insurance.seller.username)
  end

  # broadcast the creation of this log inform of notification and email
  def broadcast
    InsuranceLogNotification.create!(:data_id => self.id, :sender => self.insurance.buyer, :receiver => self.insurance.seller)
    Notifier.delay.email_insurance_log(self.insurance.seller, self)
  end

  # convert this log into a notification form
  # this return notification text in format 'transaction: <body>'
  def to_notification_link(current_user)
    log_state = Insurance.get_log_state(self)

    # generate path to the current user message. Current user must be the recipient
    insurance = link_to(I18n.t("general.insurance"),path_helper.user_message_path(current_user, self.insurance.message.id))
    body = I18n.t("notification.title.insurance.#{log_state}",
      :seller => link_to(self.insurance.seller.username, path_helper.profile_path(self.insurance.seller.username)),
      :buyer => link_to(self.insurance.buyer.username, path_helper.profile_path(self.insurance.buyer.username)))

    whole = I18n.t("notification.title.transaction",
      :transaction => insurance,
      :body => body)
    
    return whole
  end

  # helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end
  
end
