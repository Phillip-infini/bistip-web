class StoriesController < ApplicationController

  # must be logged in
  before_filter :authenticate, :only => [:new, :create]

  def new
    @story = Story.new
  end

  def index

    # Descending order
    @stories = Story.all(:order => "created_at DESC")

    # pagination
    @stories = @stories.paginate(:page => params[:page], :per_page => Story::PER_PAGE)
  end

  def create

    # associate story and the current user
    @story = current_user.stories.new(params[:story])
    if @story.save
      flash[:notice] = t('stories.create.story.success')
      ContactUsNotifier.delay.email_stories(@story.user.username, @story.user.email, @story.title, @story.body)
      redirect_to stories_path
    else
      put_model_errors_to_flash(@story.errors)
      render :action => "new"
    end
  end

end
