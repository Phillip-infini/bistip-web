require 'test_helper'

class TripCommentsControllerTest < ActionController::TestCase
  setup do
    @trip = trips(:tokjak)
    @comnt = comments(:trip1)
  end
  
  # Replace this with your real tests.
  test "should create comment on trip" do
    login_as(:one)
    assert_difference('Comment.count') do
      post :create, :trip_id => @trip.id, :comment => {
        :body=>"trip comment test"
      }
    end
    assert_redirected_to trip_path(@trip, :page => @trip.last_comment_page)
  end

  test "should destroy comment on trip" do
    login_as(:one)
    assert_difference('TripComment.count', -1) do
      delete :destroy, :trip_id => @trip.id, :id =>@comnt.id
    end
    assert_redirected_to trip_path(@trip)
  end
end
