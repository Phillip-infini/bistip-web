class ApplicationController < ActionController::Base
  include ::SslRequirement
  protect_from_forgery

  # force the mobile version for development:
  # @@format = :mobile
  @@format = :mobile
  cattr_accessor :format

  before_filter :prepare_for_mobile, :set_locale_by_ip, :establish_format
  
  MOBILE_BROWSERS = ["android","blackberry","palm","mobile","iphone","mobi",
                     "palm","palmos","palmsource","blackberry","nokia","phone","midp","pda",
                     "wap","java","nokia","hand","symbian","chtml","wml","ericsson","lg","audiovox","motorola",
                    "samsung","sanyo","sharp","telit","tsm","mobile","mini","windows ce","smartphone|",
                    "240x320","320x320","mobileexplorer","j2me","sgh","portable","sprint","vodafone",
                    "docomo","kddi","softbank","pdxgw","j-phone","astel","minimo","plucker","netfront",
                    "xiino","mot-v","mot-e","portalmmm","sagem","sie-s","sie-m","ipod","pre"]

  AM_I_ROBOT = ["googlebot","twitterbot",
    "facebookexternalhit", "ysearch", "yslurp", "bingbot", "feedfetcher",
    "yandex", "baidu", "voilabot", "yahoo! slurp",
    "gigabot", "crawler", "libcurl-agent",
    "libwww-perl", "slurp", "http://www.google.com/bot.html",
    "http://www.facebook.com/externalhit_uatext.php",
    "tweetmemebot", "sitebot", "msnbot", "robot", "bot"]

  AUTH_COOKIE = 'bistip_auth'
  LOCALE_COOKIE = 'bistip_locale'
  MOBILE_COOKIE = 'bistip_mobile'

  REST_PASSWORD = 'gad1ng_c00g33'

  REFERRER_PASSWORD = 'password'

  # Check if request's user agent is a robot
  def is_robot
    AM_I_ROBOT.each do |robot|
      if request.user_agent.nil?
        return true
      elsif request.user_agent.downcase.include?(robot)
        return true
      end
    end
    return false
  end
  
  # Returns the currently logged in user or nil if there isn't one
  def current_user
    return unless cookies.signed[AUTH_COOKIE]
    @current_user ||= User.authenticate_with_salt(*cookies.signed[AUTH_COOKIE])
  end

  # Make current_user available in templates as a helper
  helper_method :current_user

  # Filter method to enforce a login requirement & email validation
  # Apply as a before_filter on any controller you want to protect
  def authenticate
    if logged_in?
      if !current_user.email_validated?
        redirect_to not_validated_user_path(current_user)
      end
    else
      access_denied
    end
  end

  # filter method to be used for rest operation for bistip team
  def check_rest_password
    password = params[:password]
    if password != REST_PASSWORD
      access_denied
    end
  end

  # Predicate method to test for a logged in user
  def logged_in?
    current_user.is_a? User
  end

  # Make logged_in? available in templates as a helper
  helper_method :logged_in?

  # check if current user is the same as the user_id in params
  # if same then redirect away
  def not_self
    user = User.find(params[:user_id])
    if current_user == user
      redirect_to root_path
    end
  end

  # handle access denied
  def access_denied
    if(params[:referrer] == REFERRER_PASSWORD)
      flash[:longer_notice] = t('application.password_changed')
    else
      flash[:longer_notice] = t('application.access_denied')
    end

    redirect_target = login_path
    redirect_target = params[:denied_redirect] if params[:denied_redirect]
    redirect_away redirect_target
  end

  # redirect somewhere that will eventually return back to here
  def redirect_away(*params)
    session[:original_uri] = request.fullpath
    redirect_to(*params)
  end

  # returns the person to either the original url from a redirect_away or to a default url
  def redirect_back(*params)
    uri = session[:original_uri]
    session[:original_uri] = nil
    if uri
      redirect_to uri
    else
      redirect_to(*params)
    end
  end

  # put model error into flash in form of array
  def put_model_errors_to_flash(errors, type = 'render')
    errors_array = Array.new

    # if errors is array of OrderedHash lets iterate through
    if errors.is_a?(Array)
      errors.each do |error|
        error.full_messages.each do |msg|
          errors_array << msg
        end
      end
    else
      errors.full_messages.each do |msg|
        errors_array << msg
      end
    end
    
    if type == 'render'
      flash.now[:model_alert] = errors_array
    elsif type == 'redirect'
      flash[:model_alert] = errors_array
    end
  end

  # helper to parse date parameter
  # return nil if it's invalid date
  def parse_date_parameter(value)
    if value.nil?
      nil
    else
      begin
        Date.parse(value)
      rescue ArgumentError
        nil
      end
    end
  end
  helper_method :parse_date_parameter
  
  # boolean helper to check whether a string is nil or empty
  def string_nil_or_empty?(value)
    if value.nil? or value.empty?
      true
    else
      false
    end
  end

  def email_receive_message(receiver)
    return receiver.user_configuration.email_receive_message
  end

  def email_receive_trip_comment(receiver)
    return receiver.user_configuration.email_receive_trip_comment
  end

  def email_receive_seek_comment(receiver)
    return receiver.user_configuration.email_receive_seek_comment
  end

  def email_receive_review(receiver)
    return receiver.user_configuration.email_receive_review
  end

  def mobile_version?
    
    if get_mobile_cookie == "1"
      true
    elsif get_mobile_cookie == "0"
      false
    else
      MOBILE_BROWSERS.each do |m|
        if !request.user_agent.nil? && request.user_agent.downcase.include?(m)
          return true
        elsif !request.user_agent.nil? && request.user_agent.downcase.include?("x11")
          return false
        end
      end
      return false
    end
    
  end
  helper_method :mobile_version?

  def prepare_for_mobile
    set_mobile_cookie params[:mobile] if params[:mobile]
    if mobile_version? && !request.xhr?
      request.format = :mobile
    end
  end

  def set_mobile_cookie(value)
    cookies.permanent[MOBILE_COOKIE] = value
  end
  
  def get_mobile_cookie
    cookies[MOBILE_COOKIE]
  end
  
  def set_locale_cookie(value)
    cookies.permanent[LOCALE_COOKIE] = value
  end
  
  def get_locale_cookie
    cookies[LOCALE_COOKIE]
  end
  
  # not used now but keep it in case if in the future we need it
  def set_locale
    #if params[:locale] is nil then I18n.default_locale will be used
    if params[:locale].nil?
      I18n.locale = locale_from_accept_language_header if get_locale_cookie.nil?
    else
      set_locale_cookie params[:locale]
      I18n.locale = get_locale_cookie
    end
  end

  # set website language based on IP
  def set_locale_by_ip

    if params[:locale].nil?
      # this is the first time user access the website, no local in session
      if get_locale_cookie.nil?
        begin
          # get the country location from geocoder
          data = Geocoder.search(request.ip)
          country_code = data[0].country_code.downcase if !data.nil? and !data[0].nil?
          country_code = country_code.downcase unless country_code.blank?

          # only consider id or en value for now
          if country_code == "id" or country_code == "en"
            I18n.locale = country_code
            set_locale_cookie country_code
          else
            # cannot find country code? default to en
            I18n.locale = :en
            set_locale_cookie :en
          end
        # anything happen with geocoder then default to id
        rescue Exception => e
          username = suspicious
          # ContactUsNotifier.delay.email_exception(e.inspect, e.backtrace, request.ip, request.user_agent, request.fullpath, request.env["HTTP_REFERRER"], "LANG", username) unless is_robot
          I18n.locale = :en
        end
      else
        I18n.locale = get_locale_cookie
      end
    # user trying to switch language
    else
      # only consider id or en value for now
      if params[:locale] == 'id' or params[:locale] == 'en'
        set_locale_cookie params[:locale]
        I18n.locale = get_locale_cookie
      end
    end
  end

  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception, :with => :render_error
    rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found
    rescue_from ActionController::UnknownController, :with => :render_not_found
    rescue_from ActionController::UnknownAction, :with => :render_not_found
  end

  def send_forum_email(forum, topic, post)
    email_to_send = forum.email
    if !email_to_send.blank?
      emails = email_to_send.split(' ')
      emails.each do |email|
        ContactUsNotifier.delay.email_forum_event(email, forum, topic, post)
      end
    end
  end
  
  def comment_session(session_name)
    comment = session[session_name].nil? ? "" : session[session_name]
    session[session_name] = nil
    return comment
  end
  helper_method :comment_session

  # sort hash by keys and return the values in array
  def sort_hash_by_keys(hash)
    values = Array.new
    integer_keys = Array.new
    hash.keys.each { |key| integer_keys << key.to_i }
    integer_keys.sort.each { |key| values << hash[key.to_s] }
    values
  end

  # store user_id in cookie
  def create_auth_cookie(user)
    # create permanent cookie if remember me
    if params[:remember_me]
      cookies.permanent.signed[AUTH_COOKIE] = {:value => [user.id, user.salt]}
    else
      # create normal cookie
      cookies.signed[AUTH_COOKIE] = {:value => [user.id, user.salt]}
    end
  end
  
  private
    def render_not_found(exception)
      username = suspicious
      ContactUsNotifier.delay.email_exception(exception.inspect, exception.backtrace, request.ip, request.user_agent, request.fullpath, request.env["HTTP_REFERRER"], "404", username) unless is_robot
      render :template =>"/error/404", :status => 404
    end

    def render_error(exception)
      username = suspicious
      ContactUsNotifier.delay.email_exception(exception.inspect, exception.backtrace, request.ip, request.user_agent, request.fullpath, request.env["HTTP_REFERRER"], "500", username) unless is_robot
      render :template =>"/error/500", :status => 500
    end

    def locale_from_accept_language_header
      request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
    end

    def suspicious
      username = ""
      username = current_user.username if current_user
      return username
    end

    def establish_format
      if Rails.env == 'test'
        # If the request is from a true mobile device, don't set the format
        request.format = self.format unless (request.format == :json or request.format == :js or request.format == :xml)
      end
    end

end
