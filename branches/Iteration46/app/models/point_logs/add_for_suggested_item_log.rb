class AddForSuggestedItemLog < PointLog

  # do operation on point given
  def do_point(amount)
    amount + self.amount
  end

  # generate title label
  def title_label
    suggested_item = object
    I18n.t('dashboard.points.label.suggested_item_title', :name => link_to(suggested_item.name, path_helper.suggested_item_path(suggested_item)))
  end

  # generate point label
  def point_label
    I18n.t('general.points_plus', :points => self.amount)
  end

  def object
    SuggestedItem.find(self.data_id)
  end

end
