class AddForBoostAsiaRegistrationLog < PointLog

  # do operation on point given
  def do_point(amount)
    amount + self.amount
  end

  # generate title label
  def title_label
    I18n.t('dashboard.points.label.boost_asia_registration_title')
  end

  # generate point label
  def point_label
    I18n.t('general.points_plus', :points => self.amount)
  end

end
