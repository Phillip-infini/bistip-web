class InfluencerLog < ActiveRecord::Base

  MINIMUM_CLICK = 17
  CANDIDATE_COUNT = 30
  PAGE = 10

  # relationships
  belongs_to :user, :class_name => "User", :foreign_key => 'influencer_id'
  belongs_to :trip, :class_name => "Trip", :foreign_key => 'trip_id'

  # validation
  validates :ip_address, :uniqueness => {:scope => [:trip_id, :influencer_id]}

  def self.top_influencer
    array1 = InfluencerLog.count(:group => "influencer_id").sort {|x,y| x[1]<=>y[1]}
    count = 1
    temp = Array.new
    for data in array1.reverse do
      temp << [count, data[0], data[1]] #no, influencer_id, point
      count+=1
    end
    return temp
  end

  def self.influencer_point(user)
    data = InfluencerLog.count(:group => 'influencer_id', :conditions => "influencer_id=#{user.id}")
    return data[user.id]
  end

  def self.influencer_list(user)
    data = InfluencerLog.count(:group => 'trip_id', :conditions => "influencer_id=#{user.id}").sort {|x,y| x[1]<=>y[1]}
    count = 1
    temp = Array.new
    for item in data.reverse do
      temp << [item[0], item[1]] #trip_id, total_hit
      count += 1
    end
    return temp
  end

  # get the amount of bistip that a user get with this influencer log
  def points
    PointLog::INFLUENCER_LOG_POINT
  end

end
