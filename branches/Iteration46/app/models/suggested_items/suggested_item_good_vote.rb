class SuggestedItemGoodVote < SuggestedItemVote

  # associations
  belongs_to :user
  belongs_to :suggested_item
  
end
