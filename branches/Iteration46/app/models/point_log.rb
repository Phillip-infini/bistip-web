class PointLog < ActiveRecord::Base

  # include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper

  belongs_to :balance
  belongs_to :currency

  INFLUENCER_LOG_POINT = 1
  STORY_POINT = 20
  STORY_WITH_PICTURE_POINT = 30
  SUGGESTED_ITEM_POINT = 5
  SUGGESTED_ITEM_WITH_PICTURE_POINT = 10
  MISC_POINTS_TYPE = [SubtractForEscrowCashbackLog.name, AddForDemoAsiaRegistrationLog.name, AddForBoostAsiaRegistrationLog.name]

  default_scope lambda {{ :conditions => ["deleted = ?", false] }}
  scope :type_eq, lambda { |type| {:conditions => ["type = ?", type]}}
  scope :type_in, lambda { |types| {:conditions => ["type IN (?)", types]}}

  # event
  before_create :set_currency

  # this method is to do operation on balance amount
  def do_point(amount)
    raise NotImplementedError
  end

  # generate title label
  def title_label
    raise NotImplementedError
  end

  # generate point label
  def point_label
    raise NotImplementedError
  end

  # get object referred by data_id
  def object
    raise NotImplementedError
  end

  # helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end

  private
    def set_currency
      self.currency = Currency.find_by_name('USD')
    end
end
