# class to represent a gender
class Gender < ActiveRecord::Base

  def self.select_list
    result = Array.new
    result << ['', nil]
    Gender.all.each do |gender|
      result << [gender.name, gender.id ]
    end
    result
  end
end
