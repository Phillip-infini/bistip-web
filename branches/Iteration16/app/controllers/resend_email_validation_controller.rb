class ResendEmailValidationController < ApplicationController
  def index
    password = params[:password]
    username = params[:username]
    message = ''

    if password == 'gad1ng_c00g33'
      if !username.nil? and !username.blank?
        user = User.find_by_username(username)
        if !user.nil? and user.need_to_be_reminded_for_activation?
          user.send_email_validation_reminder
          User.update_times_reminded(user)
          message << user.username
          message << ' - '
          message << user.email
          message << ' '
        end
      else
        users = User.where('email_validation_key IS NOT NULL')
        users.each do |user|
          if user.need_to_be_reminded_for_activation?
            user.send_email_validation_reminder
            User.update_times_reminded(user)
            message << user.username
            message << ' - '
            message << user.email
            message << ' '
          end
        end
      end
    end
    flash[:notice] = message
    redirect_to root_path
  end
end
