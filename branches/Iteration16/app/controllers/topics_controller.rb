class TopicsController < ApplicationController
  before_filter :load_forum
  before_filter :authenticate, :only => [:new, :edit, :create, :update, :destroy]

  layout 'forum'

  # topic show list of children post
  def show
    @topic = Topic.find(params[:id])
    topic_view = @topic.topic_view
    topic_view.increment!(:count)
    
    @posts = @topic.posts.paginate(:page => params[:page], :per_page => Topic::POSTS_PER_PAGE)
  end

  # new topic form
  def new
    @topic = Topic.new
    @topic.forum = @forum

    # new topic need a first post
    @first_post = Post.new
  end

  # edit topic form
  def edit
    @topic = current_user.topics.find(params[:id])
  end

  # create topic
  def create
    @topic = Topic.new(params[:topic])
    @topic.forum = @forum
    @topic.user = current_user

    # set first post value
    @first_post = Post.new
    @first_post.body = params[:first_post_body]
    @first_post.user = current_user

    # if noth topic and first post valid then save both
    if @topic.valid? and @first_post.valid?
      @topic.save
      @first_post.topic = @topic
      @first_post.save

      # send email to forum owner
      send_forum_email(@forum, @topic, @first_post)
      flash[:notice] = t('topic.create.message.success')
      redirect_to(forum_topic_path(@forum, @topic))
    else
      @first_post.valid?
      put_model_errors_to_flash(@topic.errors.merge(@first_post.errors))
      render :action => "new"
    end
  end

  # update topic
  def update
    @topic = current_user.topics.find(params[:id])

    if @topic.update_attributes(params[:topic])
      flash[:notice] = t('topic.update.message.success')
      redirect_to(forum_path(@forum))
    else
      put_model_errors_to_flash(@topic.errors)
      render :action => "edit"
    end
  end

  # soft delete a topic
  def destroy
    @topic = current_user.topics.find(params[:id])
    @topic.deleted = true
    @topic.save
    flash[:notice] = t('topic.destroy.message.success')
    redirect_to forum_path(@topic.forum)
  end

  private
    def load_forum
      @forum = Forum.find(params[:forum_id])
    end

end
