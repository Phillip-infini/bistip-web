class PostsController < ApplicationController
  
  before_filter :load_topic
  before_filter :authenticate, :only => [:new, :edit, :create, :update, :destroy, :quote]

  layout 'forum'

  # 
  def new
    @post = Post.new
    @post.topic = @topic
    @post.user = current_user
  end

  #
  def quote
    @post = Post.new
    @post.topic = @topic
    @post.user = current_user
    @quoted_post = Post.find(params[:id])
  end

  # GET /posts/1/edit
  def edit
    @post = Post.find(params[:id])
  end

  # 
  def create
    @post = Post.new(params[:post])
    @post.topic = @topic
    @post.user = current_user

    if @post.save
      @topic.touch
      send_forum_email(@topic.forum, @topic, @post)
      redirect_to(forum_topic_path(@topic.forum, @topic, :page => @topic.posts_last_page), :notice => 'Post was successfully created.')
    else
      render :action => "new"
    end
  end

  def create_quote
    @post = Post.new
    @post.body = params[:body]
    @quoted_post = Post.find(params[:quoted_post_id])
    @post.quoted = @quoted_post.body
    @post.quoted_from = @quoted_post.user
    @post.topic = @topic
    @post.user = current_user

    if @post.save
      send_forum_email(@topic.forum, @topic, @post)
      redirect_to(forum_topic_path(@topic.forum, @topic, :page => @topic.posts_last_page), :notice => 'Post was successfully created.')
    else
      render :action => "quote"
    end
  end

  # 
  def update
    @post = current_user.posts.find(params[:id])

    if @post.update_attributes(params[:post])
      redirect_to(forum_topic_path(@topic.forum, @topic, :page => @post.find_page), :notice => 'Post was successfully updated.')
    end
  end

  # 
  def destroy
    @post = current_user.posts.find(params[:id])
    page_to_go = @post.find_page
    @post.deleted = true
    @post.save
    redirect_to(forum_topic_path(@topic.forum, @topic, :page => page_to_go), :notice => 'Post was successfully deleted.')
  end

  private

    def load_topic
      @topic = Topic.find(params[:topic_id])
    end
end
