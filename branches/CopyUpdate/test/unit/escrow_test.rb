require 'test_helper'

class EscrowTest < ActiveSupport::TestCase

  test "should create escrow of a message" do
    escrow = Escrow.new
    escrow.message = messages(:onetomac)
    escrow.buyer = users(:one)
    escrow.seller = users(:mactavish)
    escrow.currency = Currency.first
    escrow.payment_method = PaymentMethod.first
    escrow.amount = 30
    escrow.item = 'ipad 2'
    assert escrow.save
  end

end
