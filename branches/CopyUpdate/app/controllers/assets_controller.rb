# handle request for message attachments assets
class AssetsController < ApplicationController
  ssl_required :show
  before_filter :authenticate, :only => [:show]

  def show
    message = Message.find(params[:message_id])
    
    # the viewer must be either the message sender or receiver
    if message.is_sender_or_receiver?(current_user)
      @asset = Asset.find(params[:asset_id])
    else
      render :nothing => true
    end
  end
end
