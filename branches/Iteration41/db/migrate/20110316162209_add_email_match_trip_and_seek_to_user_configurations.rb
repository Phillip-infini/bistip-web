class AddEmailMatchTripAndSeekToUserConfigurations < ActiveRecord::Migration
  def self.up
    add_column :user_configurations, :email_match_trip, :boolean, :default => true
    add_column :user_configurations, :email_match_seek, :boolean, :default => true
  end

  def self.down
    remove_column :user_configurations, :email_match_trip
    remove_column :user_configurations, :email_match_seek
  end
end
