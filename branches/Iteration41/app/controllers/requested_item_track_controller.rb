class RequestedItemTrackController < ApplicationController

  before_filter :check_rest_password, :only => [:index, :rebuild]

  def index
    RequestedItemTrack.generate if RequestedItemTrack.count == 0
    @rit = RequestedItemTrack.order('count DESC').paginate(:page => params[:page], :per_page => 100)
    render :layout => false
  end

  def rebuild
    RequestedItemTrack.destroy_all
    RequestedItemTrack.generate
    redirect_to rit_path
  end

end
