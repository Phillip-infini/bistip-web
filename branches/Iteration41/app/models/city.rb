# City class represent a City
class City < ActiveRecord::Base

  # constants
  ALL_CITIES = "<all cities>"
  ANYWHERE = '<anywhere>'

  # scope
  default_scope :order => "cities.name ASC"
  scope :match_cities_and_countries_name, lambda { |name| {:include => :country, :conditions => ['cities.name LIKE ? OR cities.alias LIKE ? OR countries.name LIKE ? OR countries.alias LIKE ?', "%#{name}%", "%#{name}%", "%#{name}%", "%#{name}%"]}}
  scope :exclude_all_cities, :conditions => ["cities.name <> ?", "#{ALL_CITIES}"]
  scope :ids_not_in, lambda { |ids| {:conditions => ["cities.id NOT IN (?)", ids]}}
  scope :exclude_anywhere, :conditions => ["cities.name <> ?", "#{ANYWHERE}"]

  # geocoder
  reverse_geocoded_by :latitude, :longitude

  # relationships
  belongs_to :country

  # helper to check whether a city represent an all cities of a country
  def all_cities?
    self.name == ALL_CITIES
  end

  # helper to check if this city is a placeholder that represent a continent
  def is_continent_country?
    self.country.code == Country::CONTINENT_CODE
  end

  # get anywhere placeholder
  def self.get_anywhere_placeholder
    City.find_by_name(ANYWHERE)
  end

  # display name of city in form of <city>, <country>
  def display_name
    self.name + ', ' + self.country.name
  end

  # build single city match
  def self.build_match(exclude_all_cities, exclude_continents, term)
    result = Array.new

    # build scoped Trip by checking parameter
    scope = City.scoped({})
    scope = scope.exclude_anywhere

    # find matching cities and countries by name
    scope = scope.match_cities_and_countries_name term

    if exclude_all_cities then
      scope = scope.exclude_all_cities
    end

    if exclude_continents then
      continent_cities = Array.new
      Continent.all.each do |con|
        continent_cities << con.get_city_placeholder.id
      end
      scope = scope.ids_not_in continent_cities
    end

    # append <city, country> result to the array
    scope.each do |city|
      result << city
    end
    result
  end


end
