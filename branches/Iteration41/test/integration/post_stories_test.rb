require 'test_helper'

class PostStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  setup do
    @post = posts(:ipadpost)
    @topic = @post.topic
    @forum = @topic.forum
  end

  test "create new post" do

    # login
    full_login_as(:one)

    get new_forum_topic_post_path(@forum, @topic)
    assert_response :success
    assert_template 'new'

    # create trip
    post forum_topic_posts_path(@forum,  @topic), :post => {:body => "Ipad I agree"}

    assert assigns(:post).valid?
    assert_response :redirect
    follow_redirect!

  end

  test "create new quoted post" do

    # login
    full_login_as(:one)

    get quote_forum_topic_post_path(:forum_id => @forum.id, :topic_id => @topic.id, :id => @post.id)
    assert_response :success
    assert_template 'new'

    # create trip
    post create_quote_forum_topic_post_path(:forum_id => @forum.id, :topic_id => @topic.id, :id => @post.id), :body => "Ipad I agree", :quoted_post_id => @post.id

    assert assigns(:post).valid?
    assert_response :redirect
    follow_redirect!

  end

  test "edit and update a post" do

    # login
    full_login_as(:one)

    # edit
    get edit_forum_topic_post_path(:forum_id => @forum.id, :topic_id => @topic.id, :id => @post.id)

    assert_response :success
    assert_template 'edit'

    # update
    put forum_topic_post_path(:forum_id => @forum.id, :topic_id => @topic.id, :id => @post.id), :topic => {:title => "sadasdasdas das d as dase"}

    assert_response :redirect
    assert_redirected_to forum_topic_path(@forum, @topic, :page => assigns(:post).find_page)

    follow_redirect!
  end

  test "delete a post" do
     # login
    full_login_as(:one)

    page_to_go = @post.find_page

    # view the trip
    get forum_topic_path(@forum, @topic)
    assert_response :success
    assert_template 'show'

    # delete the trip
    delete forum_topic_post_path(:forum_id => @forum.id, :topic_id => @topic.id, :id => @post.id)

    assert_response :redirect
    assert_redirected_to forum_topic_path(@forum, @topic, :page => page_to_go)

    follow_redirect!
  end

end
