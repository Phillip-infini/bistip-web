require 'test_helper'

class InsuranceStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "complete insurance scenario by one" do
    # on eis buyer dono is seller
    one = users(:one)
    dono = users(:dono)
    message = messages(:donotoone1)

    # login
    full_login_as(:one)

    # 1. open the escrow form
    get new_insurance_path(:seller_id => dono.id, :message_id => message.id)
    assert_template :new
    assert_response :success

    # 2. post escrow form
    post insurances_path, :insurance => {:buyer_id => one.id,
      :seller_id => dono.id,
      :message_id => message.id,
      :cover_value => 1000000,
      :amount => 200,
      :item => 'ipad 2',
      :currency_id => currencies(:usd).id
    }

    # 3. post insurance form
    assert_difference('Insurance.count') do
      post insurances_path, :insurance => {:buyer_id => one.id,
        :seller_id => dono.id,
        :message_id => message.id,
        :cover_value => 1000000,
        :amount => 200,
        :item => 'ipad 2',
        :currency_id => currencies(:usd).id
      }
      assert_response :redirect
      follow_redirect!
    end

    insurance = Insurance.first

    # 4. Bistip approved
    assert_difference('BistipApprovedInsuranceLog.count') do
      get create_bistip_approved_insurance_log_path(:insurance_id => insurance.id, :password => ApplicationController::REST_PASSWORD)
      assert_redirected_to root_path
    end

    # 5. Bistip confirm payment
    assert_difference('BistipReceivedBuyerBankTransferInsuranceLog.count') do
      get create_bistip_received_buyer_bank_transfer_insurance_log_path(:insurance_id => insurance.id, :password => ApplicationController::REST_PASSWORD)
      assert_redirected_to root_path
    end
    
  end

  test "rejected insurance scenario by one" do
    # on eis buyer dono is seller
    one = users(:one)
    dono = users(:dono)
    message = messages(:donotoone1)

    # login
    full_login_as(:one)

    # 1. open the escrow form
    get new_insurance_path(:seller_id => dono.id, :message_id => message.id)
    assert_template :new
    assert_response :success

    # 2. post escrow form
    post insurances_path, :insurance => {:buyer_id => one.id,
      :seller_id => dono.id,
      :message_id => message.id,
      :cover_value => 1000000,
      :amount => 200,
      :item => 'ipad 2',
      :currency_id => currencies(:usd).id
    }

    # 3. post insurance form
    assert_difference('Insurance.count') do
      post insurances_path, :insurance => {:buyer_id => one.id,
        :seller_id => dono.id,
        :message_id => message.id,
        :cover_value => 1000000,
        :amount => 200,
        :item => 'ipad 2',
        :currency_id => currencies(:usd).id
      }
      assert_response :redirect
      follow_redirect!
    end

    insurance = Insurance.first

    # 4. Bistip rejected
    assert_difference('BistipRejectedInsuranceLog.count') do
      get create_bistip_rejected_insurance_log_path(:insurance_id => insurance.id, :password => ApplicationController::REST_PASSWORD)
      assert_redirected_to root_path
    end

  end

end
