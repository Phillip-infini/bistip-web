# sub class of comment that represent Comment for Seek
class SeekComment < Comment

  #this method call in comment model callback, to create email notification to other seek commentator
  def notify_other_seek_comment
    seek = Seek.unscoped.find(self.seek_id)
    comments = seek.comments
    already_sent = []
    comments.each do |comment|
      # only send comment notification if the previous comment owner
      # is not the owner of the seek and not the owner of the new comment
      commentator = comment.user
      if commentator.user_configuration.email_other_seek_comment and
          !already_sent.include?(commentator) and
          !seek.owner?(commentator) and
          !self.owner?(commentator)
        Notifier.email_other_seek_comment(self, commentator, seek).deliver
        already_sent << commentator
      end
    end
  end
  handle_asynchronously :notify_other_seek_comment

  #this method call in comment model callback, to create live notification to other seek commentator
  def live_notification_seek_comment
    seek = Seek.unscoped.find(self.seek_id)
    comments = seek.comments
    already_sent = []
    comments.each do |comment|
      # only send comment notification if the previous comment owner
      # is not the owner of the seek and not the owner of the new comment
      commentator = comment.user
      if !already_sent.include?(commentator) and
          !seek.owner?(commentator) and
          !self.owner?(commentator)
        SeekCommentNotification.create!(:receiver=>comment.user, :sender=>self.user, :data_id=>seek.id)
        already_sent << commentator
      end
    end
  end

  # find which page this comment belongs to
  def find_page
    seek = Seek.unscoped.find(self.seek_id)
    index = seek.comments.index(self) + 1
    ((index * 1.0)/Comment::PER_PAGE).ceil
  end

  # generate path to the comment
  def get_path
    seek = Seek.unscoped.find(self.seek_id)
    path_helper.seek_path(seek, :page => self.find_page)
  end
end
