class ForumsController < ApplicationController

  layout 'forum'

  TOPICS_PER_PAGE = 10

  # show all forums
  def index
    @forums = Forum.all
  end

  # Show all children topic
  def show
    @forum = Forum.find(params[:id])
    @topics = @forum.topics.paginate(:page => params[:page], :per_page => TOPICS_PER_PAGE)
  end

end
