class AddEmailTopicPostAndQuotePostToUserConfigurations < ActiveRecord::Migration
  def self.up
    add_column :user_configurations, :email_topic_post, :boolean, :default => true
    add_column :user_configurations, :email_quote_post, :boolean, :default => true
  end

  def self.down
    remove_column :user_configurations, :email_topic_post
    remove_column :user_configurations, :email_quote_post
  end
end
