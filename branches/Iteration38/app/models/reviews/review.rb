# Review class represent a review from a user to another user
class Review < ActiveRecord::Base
  BODY_MAXIMUM_LENGTH = 600;
  DEFAULT_PER_PAGE = 5

  # relationships
  belongs_to :giver, :class_name => "User", :foreign_key => 'giver_id'
  belongs_to :receiver, :class_name => "User", :foreign_key => "receiver_id"
  belongs_to :message, :class_name => "Message", :foreign_key => "message_id"

  # validations
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH }, :presence => true

  # scopes
  scope :last_24_hour, lambda {|giver_id, receiver_id| { :conditions => ["giver_id = ? and receiver_id = ? and created_at > ?", giver_id, receiver_id, 1.day.ago]}}
  scope :last_7_days, lambda{|giver_id, receiver_id|{ :conditions => ["giver_id = ? and receiver_id = ? and created_at > ?", giver_id, receiver_id, 7.days.ago]}}
  scope :last_7_days_not_invitation, lambda{|giver_id, receiver_id|{ :conditions => ["giver_id = ? and receiver_id = ? and invitation = false and created_at > ?", giver_id, receiver_id, 7.days.ago]}}
  scope :recommendation, lambda{|giver_id, receiver_id|{ :conditions => ["giver_id = ? and receiver_id = ? and invitation = true", giver_id, receiver_id]}}
  default_scope :order => 'created_at DESC'

  # events
  after_create :do_notify

  def cname
    raise NotImplementedError
  end

  # method to check whether a reply to this review already exist
  def reply_exist?
    if self.verified?
      false
    elsif self.invitation?
      Review.recommendation(self.receiver_id, self.giver_id).count > 0
    else
      Review.last_7_days_not_invitation(self.receiver.id, self.giver.id).count > 0
    end
  end

  # is the review from escrow?
  def from_escrow?
    unless self.message.blank?
      !Escrow.find_by_message_id(self.message.id).blank?
    else
      return false
    end
  end

  # get item name of its from escrow
  def item_name_from_escrow
    if from_escrow?
      Escrow.find_by_message_id(self.message.id).item
    end
  end

  # check if review receiver is buyer on escrow
  def is_receiver_as_buyer_on_escrow?
    if from_escrow?
      Escrow.find_by_message_id(self.message.id).buyer == self.receiver
    else
      return false
    end
  end

  private
    # do all the things necessary after creating a review
    def do_notify
      # insert relation now that the user gave a review
      Relation.build_relation_from_review(self)

      # no need to notify if this review is from escrow
      unless self.verified?
        # email notification
        Notifier.delay.email_review_receiver(self) if self.receiver.user_configuration.email_receive_review
        # live notification
        ReviewNotification.create!(:receiver => self.receiver, :sender => self.giver, :data_id => self.receiver.id)
      end
    end
  
end