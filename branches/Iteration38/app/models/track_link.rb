class TrackLink

  # for path_helper

  include ActionView::Helpers::UrlHelper

  ###### ref link constants begin

  # main
  DAS_REF = 'das' #dashboard
  TRIP_REF = 'tr' #trip
  TRIP_SUGGESTIONS_REF = 'tr-sg'
  TRIP_MATCH_REF = 'tr-match'
  SEEK_REF = 'sk' #seek
  SEEK_MATCH_REF = 'sk-match' #seek match
  SI_REF = 'si' #suggested_item
  SRC_RF = 'src' #search

  # Dashboard

  DAS_TRIP_REF = 'das_tr' #dashboard>trips
  DAS_SEEK_REF = 'das_sk' #dashboard>seeks
  DAS_YS_REF = 'das_ys' #dashboard>your stories
  DAS_SI_REF = 'das_si' #dashboard>suggested items
  DAS_SP_REF = 'das_sp' #dashboard>transactions
  DAS_RV_REF = 'das_rv' #dashboard>reviews
  DAS_POINTS_REF = 'das_points' #dashboard>points

  # Features
  TRIP_NEXT = 'next' #next trip
  TRIP_PREV = 'prev' #previous trip
  SI = 'si' #suggested item
  SI_SEE_ALL = 'si_see_all' # suggested item see all link
  SIMILAR = 'similar' #similar
  SIMILAR_SEE_ALL = 'similar_see_all' #similar
  MATCH = 'match' #match
  MATCH_SEE_ALL = 'match_see_all'
  NEW_SI = 'new_si' #new suggested items
  SRC_SG = 'src_sg' #search suggestion
  INCOMING_TRIP = 'incoming'
  INCOMING_TRIP_SEE_ALL = 'incoming_see_all'

  ###### ref link constants end

  # to get the path for comparison
  def self.path_helper
    Rails.application.routes.url_helpers
  end

  # track where the link comes from
  def self.generate(from_page, feature, id = 0)
    
    #main
    if from_page == path_helper.dashboard_path
      DAS_REF + '-' + feature
    elsif from_page == path_helper.dashboard_points_path
      DAS_POINTS_REF + '-' + feature
    elsif from_page == path_helper.search_trip_path
      SRC_RF + '-' + feature
    elsif from_page == path_helper.trip_path(id)
      TRIP_REF + id.to_s + '-' + feature
    elsif from_page == path_helper.seek_path(id)
      SEEK_REF + id.to_s + '-' + feature
    elsif from_page == path_helper.suggested_item_path(id)
      SI_REF + id.to_s + '-' + feature
    elsif from_page == path_helper.suggestions_trip_path(id)
      TRIP_SUGGESTIONS_REF + id.to_s + '-' + feature
    elsif from_page == path_helper.match_trip_path(id)
      TRIP_MATCH_REF + id.to_s + '-' + feature
    elsif from_page == path_helper.match_seek_path(id)
      SEEK_MATCH_REF + id.to_s + '-' + feature
    end
  end


end
