# handler for CommentReply
class CommentReplyHandler
  include ReceiveEmailChainable

  # set next in chain
  def initialize(link = nil)
    next_in_chain(link)
  end

  # try to handle the request
  def handle(params)
    if email_type(params) == Comment::EMAIL_REPLY_PREFIX
      # check if receiver email is valid referring to a message
      receiver_email = params['recipient']
      comment = retrieve_comment(receiver_email)
      return if comment.blank?

      # check if sender really exist
      sender_email = params['sender']
      sender = User.find_by_email_prefix(sender_email)
      return if (sender.blank?)

      # build a reply comment
      reply = comment.class.new
      reply.user = sender
      reply.email_reply = true
      reply.trip_id = comment.trip_id
      reply.seek_id = comment.seek_id
      reply.reply_to_id = comment.thread_starter? ? comment.id : comment.thread_starter.id
      reply.body = params["stripped-text"]
      reply.body_original = params["body-plain"]

      if reply.save
        Notifier.delay.email_user_comment_reply_delivered(reply.sender, reply.receiver) if sender.user_configuration.email_reply_email_delivered
      else
        # notify the sender about the problem with their email reply
        Notifier.delay.email_user_comment_not_valid(comment.email_reply_address, reply.user, comment.user, reply.errors.full_messages)
      end
    else
      @next.handle(params)
    end
  end

  private
  
    # check if receiver email is valid, can be link into a comment
    def retrieve_comment(receiver_email)
      # split the receiver email, must result in two parts
      receiver_email_splitted = receiver_email.split('@')
      return nil unless receiver_email_splitted.size == 2

      # parse the head and check if it's in right format and valid salt
      head = receiver_email_splitted.first
      head_splitted = head.split(Comment::EMAIL_REPLY_SEPARATOR)
      return nil unless head_splitted.size == 3

      comment_id = head_splitted[1]
      comment_salt = head_splitted[2]
      Comment.find(:first, :conditions => {:id => comment_id, :salt => comment_salt})
    end
    
end