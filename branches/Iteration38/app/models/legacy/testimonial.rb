# unused, just keep it for integrity
class Testimonial < ActiveRecord::Base
  scope :random, lambda { |*args| {:order => 'RAND()', :limit => args[0] || 1 } }
end
