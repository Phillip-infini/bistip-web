class IncludeItem < ActiveRecord::Base

  def self.generate_all_in_array
    include_items = IncludeItem.all
    result = []
    include_items.each do |include_item|
      result << include_item.word
    end
    result
  end
end
