require 'test_helper'

class ContactUsControllerTest < ActionController::TestCase

  test "should get new" do
    login_as(:one)
    get :new
    assert_response :success
  end

  test "should submit new contact us" do
    login_as(:one)
    post :create, :name => 'name', :email => 'subject', :subject => 'subject', :body => 'body 123'
    assert_redirected_to root_path
  end
end
