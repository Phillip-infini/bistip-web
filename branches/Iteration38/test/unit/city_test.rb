require 'test_helper'

class CityTest < ActiveSupport::TestCase

  test "should find city" do
    city_id = cities(:jakarta).id
    countries_id = countries(:indonesia).id
    city = City.find(city_id)
    assert_equal(countries_id, city.country.id)
  end

  test "should find city by country name and name" do
    assert City.find_by_country_id_and_name(countries(:indonesia), 'jakarta')
    assert City.find_by_country_id_and_name(countries(:indonesia), 'new york').blank?
  end

  test "should get longlat by city name" do
    city_name = cities(:jakarta).name
    city_longitude = cities(:jakarta).longitude
    city_latitude = cities(:jakarta).latitude
    name = City.find_by_name(city_name)
    long = name.longitude
    assert_equal(city_longitude, long)
    lat = name.latitude
    assert_equal(city_latitude, lat)
  end

  test "should get nearby city by city name 50 miles" do
    city = cities(:jakarta)
    city2 = Array(cities(:bogor))
    nearby = city.nearbys(50)
    assert_equal(nearby, city2)
  end

end
