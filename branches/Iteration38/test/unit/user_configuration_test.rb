require 'test_helper'

class UserConfigurationTest < ActiveSupport::TestCase

  test 'when creating a user a user configuration must be created too' do
    user = User.new
    user.username = "testcreate"
    user.fullname = 'testcreate'
    user.city_location = 'jakarta'
    user.email = "testcreate@mail.com"
    user.hashed_password = "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4"
    assert_difference('UserConfiguration.count') do
      assert user.save
    end
  end
  
  test "should_update_user_configuration" do
    config = user_configurations(:mac_config)
    assert config.update_attributes(
      :email_receive_message=>true,
      :email_receive_trip_comment=>false,
      :email_receive_seek_comment=>false,
      :email_receive_review=>false
    )
  end

  test "should_update_user_notvalid_configuration" do
    config = user_configurations(:not_validated_config)
    assert config.update_attributes(
      :email_receive_message=>true,
      :email_receive_trip_comment=>false,
      :email_receive_seek_comment=>false,
      :email_receive_review=>false
    )
  end
  
end
