require 'test_helper'

class IndonesiaBankTransferMethodTest < ActiveSupport::TestCase

  test "should create a ibt payout" do
  #creating new and valid payout
    payout=IndonesiaBankTransferPayoutMethod.new
    test_user=users(:dono)
    payout.user=test_user

    payout.bank_name='BCA'
    payout.account_name ='Dono Sudono'
    payout.branch='Kelapa Gading'
    payout.city='Jakarta'
    payout.account_number='12345678'

    #asserting save
    assert payout.save
  end

  test "should delete an ibt payout" do
  #creating new and valid payout
    payout=IndonesiaBankTransferPayoutMethod.new
    test_user=users(:dono)
    payout.user=test_user
    payout.bank_name='BCA'
    payout.account_name ='Dono Sudono'
    payout.branch='Kelapa Gading'
    payout.city='Jakarta'
    payout.account_number='12345678'

    #save the payout
    assert payout.save
    #testing delete the payout
    assert payout.update_attributes(:deleted => true)
  end

  test "should throw an exception if not valid" do
  #creating a new blank payout
    payout=IndonesiaBankTransferPayoutMethod.new
    test_user=users(:dono)
    payout.user=test_user
    #testing with no data supplied
    assert !payout.valid?
    assert payout.errors[:bank_name].any?
    assert payout.errors[:account_name].any?
    assert payout.errors[:branch].any?
    assert payout.errors[:account_number].any?
    assert payout.errors[:city].any?
    #testing with >100 chars bank name
    payout.bank_name='123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901'
    assert !payout.valid?
    assert payout.errors[:bank_name].any?
    assert payout.errors[:account_name].any?
    assert payout.errors[:branch].any?
    assert payout.errors[:account_number].any?
    assert payout.errors[:city].any?
    #testing with correct bank name only
    payout.bank_name='BCA'
    assert !payout.valid?
    assert !payout.errors[:bank_name].any?
    assert payout.errors[:account_name].any?
    assert payout.errors[:branch].any?
    assert payout.errors[:account_number].any?
    assert payout.errors[:city].any?
    #testing with invalid account name
    payout.account_name='123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901'
    assert !payout.valid?
    assert !payout.errors[:bank_name].any?
    assert payout.errors[:account_name].any?
    assert payout.errors[:branch].any?
    assert payout.errors[:account_number].any?
    assert payout.errors[:city].any?
    #testing with valid account name and bank name
    payout.account_name ='Dono Sudono'
    assert !payout.valid?
    assert payout.errors[:branch].any?
    assert payout.errors[:account_number].any?
    #testing with >100 chars branch
    payout.branch='123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901'
    assert !payout.valid?
    assert !payout.errors[:bank_name].any?
    assert !payout.errors[:account_name].any?
    assert payout.errors[:branch].any?
    assert payout.errors[:account_number].any?
    assert payout.errors[:city].any?
    #testing with valid branch
    payout.branch='Kelapa Gading'
    assert !payout.valid?

    assert payout.errors[:account_number].any?
    #testing with non numerical account_number
    payout.account_number='abcdefghij'
    assert !payout.valid?

    assert payout.errors[:account_number].any?
    #testing with >100 digits account number
    payout.account_number='123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901'
    assert !payout.valid?
    assert payout.errors[:city].any?

    assert payout.errors[:account_number].any?
    #testing with valid data
    payout.account_number='1234567890'
    assert !payout.valid?
    assert payout.errors[:city].any?

    #testing with >100 city name
    payout.city='123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901'
    assert !payout.valid?
    assert payout.errors[:city].any?

    assert payout.errors[:city].any?
    #testing with valid data
    payout.city='1234567890'
    assert payout.valid?

  end

end
