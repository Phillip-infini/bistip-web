class CreatePayoutMethods < ActiveRecord::Migration
  def self.up
    create_table :payout_methods do |t|
      t.references :user
      t.string :type
      t.string :email
      t.string :bank_name
      t.string :account_number
      t.string :account_name
      t.string :branch
      t.boolean :deleted, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :payout_methods
  end
end
