class AddPreviousTripToTrips < ActiveRecord::Migration
  def self.up
    add_column :trips, :previous_trip_id, :integer, :references => :trips
  end

  def self.down
    remove_column :trips, :previous_trip_id
  end
end
