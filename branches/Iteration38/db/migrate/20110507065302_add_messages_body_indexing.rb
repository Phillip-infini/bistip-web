class AddMessagesBodyIndexing < ActiveRecord::Migration
  def self.up
    execute('ALTER TABLE messages ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_messages_body ON messages (body(1000))')
  end

  def self.down
    execute('DROP INDEX fulltext_messages_body ON messages')
    execute('ALTER TABLE messages ENGINE = innodb')
  end
end