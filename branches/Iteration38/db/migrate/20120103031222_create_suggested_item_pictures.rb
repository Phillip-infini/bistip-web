class CreateSuggestedItemPictures < ActiveRecord::Migration
  def self.up
    create_table :suggested_item_pictures do |t|
      t.column :suggested_item_id, :integer, :null => false, :references => :suggested_items
      t.belongs_to :user
      t.string :attachment_file_name
      t.string :attachment_content_type
      t.integer :attachment_file_size
      t.datetime :attachment_updated_at
      t.timestamps
    end
  end

  def self.down
    drop_table :suggested_item_pictures
  end
end
