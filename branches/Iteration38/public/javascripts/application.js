// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

ANYWHERE = '<anywhere>';
KEYWORD = '<keyword>';

function buildSearchTripURL(mobile)
{
    var url = "/search_traveler?";
    
    var origin = $("#origin").val();
    if (origin && origin != ANYWHERE)
    {
        url += "&origin=" + encodeURIComponent(origin);
    }

    var destination = $("#destination").val();
    if (destination && destination != ANYWHERE)
    {
        url += "&destination=" + encodeURIComponent(destination);
    }

    var sort = $("#sort").val();
    if (sort)
    {
        url += "&sort=" + encodeURIComponent(sort);
    }

    if (!mobile)
    {

        var departure_date = $("#departure_date").val();
        if (departure_date)
        {
            url += "&departure_date=" + encodeURIComponent(departure_date);
        }

        var departure_date_predicate = $("input[name='departure_date_predicate']:checked").val();
        if (departure_date_predicate && departure_date_predicate != 'undefined')
        {
            url += "&departure_date_predicate=" + encodeURIComponent(departure_date_predicate);
        }

        var keyword = $("#keyword").val();
        if (keyword)
        {
            url += "&keyword=" + encodeURIComponent(keyword);
        }

    }

    window.location.replace(url);
    return false;
}

function buildSearchSeekURL(mobile)
{
    var url = "/search_request_post?";

    var origin = $("#origin").val();
    if (origin && origin != ANYWHERE)
    {
        url += "&origin=" + encodeURIComponent(origin);
    }

    var destination = $("#destination").val();
    if (destination && destination != ANYWHERE)
    {
        url += "&destination=" + encodeURIComponent(destination);
    }
    
    if (!mobile)
    {
        var keyword = encodeURIComponent($("#keyword").val());
        if (keyword)
        {
            url += "&keyword=" + keyword;
        }
    }

    window.location.replace(url);
    return false;
}

function buildSearchSuggestedItemURL(mobile)
{
    var url = "/search_item?";

    var origin = $("#origin").val();
    if (origin && origin != ANYWHERE)
    {
        url += "&origin=" + encodeURIComponent(origin);
    }

    var keyword = encodeURIComponent($("#keyword").val());
    if (keyword)
    {
        url += "&keyword=" + keyword;
    }

    window.location.replace(url);
    return false;
}

// function to remove dynamicaly an item form fields
function remove_fields(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".itemsRow").hide();
    $(".add_more_items").show();
}

// function to remove other field trip
function remove_other_trip_field(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".other_trip").remove();
    $(".add_more_trips").show();
}

// function to add dynamicaly an item form fields
function add_fields(link, association, content, maximum) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g")
    $(link).parent().before(content.replace(regexp, new_id));
    var now_fields = $(".itemsRow").length - $(".itemsRow[style='display: none;']").length
    if (now_fields >= maximum)
    {
        $(link).parent().hide();
    }
    return false;
}

// function to add dynamically an other trip form fields
function add_other_trip_fields(link, content, maximum){
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_trip", "g")
    var now_fields = $(".other_trip").length
    
    $(link).parent().before(content.replace(regexp, new_id));
    
    if (now_fields >= maximum)
    {
        $(link).parent().hide();
    }
    
    // this routine used for prepopulate current field origin location with previous field destination location
    if(now_fields==0){
        destination = $("#trip_destination_location").val();
        $("#other_trips_" + new_id + "_origin_location").val(destination);
    }else{
        destination = $(link).parent().prev().prev().find("input[id$='destination_location']").val();
        $("#other_trips_" + new_id + "_origin_location").val(destination);
    }
    
    return false;
}

// function handle from anywhere
function handle_from_anywhere(link)
{
    if (link.checked == true)
    {
        $("#seek_origin_location").val('<anywhere>');
        $("#seek_origin_location").attr('readonly', true);
        $("#seek_origin_location").attr('disabled', true);
    }
    else
    {
        $("#seek_origin_location").val('');
        $("#seek_origin_location").attr('readonly', false);
        $("#seek_origin_location").attr('disabled', false);
    }
    return true;
}

// function to handle enter key on search trip
function search_trip_field_key_up(e)
{
    if (e.keyCode == 13){
        buildSearchTripURL();
    }
}

// function to handle enter key on search trip
function search_seek_field_key_up(e)
{
    if (e.keyCode == 13){
        buildSearchSeekURL();
    }
}

// function to handle enter key on search suggested item
function search_suggested_item_field_key_up(e)
{
    if (e.keyCode == 13){
        buildSearchSuggestedItemURL();
    }
}

// redirect to url function
function redirect(url)
{
    window.location = url
}

//build search message url
function buildSearchMessageURL(url)
{
    url = url + '?'
    var sender = $("#sender").val();
    if (sender)
    {
        url += "&sender=" + encodeURIComponent(sender);
    }

    var status = $("#status").val();
    if (status)
    {
        url += "&status=" + encodeURIComponent(status);
    }

    var keyword = $("#keyword").val();
    if (keyword && keyword != KEYWORD)
    {
        url += "&keyword=" + encodeURIComponent(keyword);
    }

    window.location.replace(url);
    return false;
}

// function to handle enter key on search trip
function search_message_field_key_up(e, url)
{
    if (e.keyCode == 13){
        buildSearchMessageURL(url);
    }
}