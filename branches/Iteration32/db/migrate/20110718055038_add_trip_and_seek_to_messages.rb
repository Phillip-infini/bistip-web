class AddTripAndSeekToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :trip_id, :integer, :references => :trips
    add_column :messages, :seek_id, :integer, :references => :seeks
  end

  def self.down
    remove_column :messages, :trip_id
    remove_column :messages, :seek_id
  end
end
