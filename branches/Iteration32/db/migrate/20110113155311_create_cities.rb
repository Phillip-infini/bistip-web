class CreateCities < ActiveRecord::Migration
  def self.up
    create_table :cities do |t|
      t.string :name
      t.string :region
      t.belongs_to :country
      
      t.timestamps
    end
    add_index(:cities, :id, :unique=>true)
  end

  def self.down
    drop_table :cities
  end
end
