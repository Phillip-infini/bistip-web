require 'test_helper'

class StoriesControllerTest < ActionController::TestCase

  setup do
    @story = stories(:storyone)
  end

  test "should get new" do
    login_as(:one)
    get :new
    assert_response :success
    assert_template :new
  end

  test "should create story" do
    login_as(:one)
    assert_difference('Story.unscoped.count') do
      post :create, :story => { :title => 'title more than 10 chars', :body => 'The Body content must be more than 30 characters', :deleted => true }
      assert_redirected_to stories_path
    end
  end

  test "should get edit" do
    login_as(:one)
    get :edit, :id => @story.to_param
    assert_response :success
    assert_template :edit
  end

  test "should update story" do
    login_as(:one)
    put :update, :id => @story.to_param, :story => { :title => 'New title more than 10 chars' }
    assert_redirected_to stories_path
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_template :index
    assert_not_nil assigns(:stories)
    assert_equal 2, assigns(:stories).size
  end

end