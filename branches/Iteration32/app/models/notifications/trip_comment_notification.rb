# To change this template, choose Tools | Templates
# and open the template in the editor.

# this object is not created anymore. Keep it for legacy purposes. See CommentNotification.
class TripCommentNotification < Notification

  #overide to_link method to perform specified action of TripComment notification
  def to_link(current_user)
    trip = Trip.unscoped.find(data_id)
    link = ""

    #clasification to determine type of comment, like 'kamu', 'yang sama'
    if trip.owner?(receiver)
      link = I18n.t("notification.title.trip_comment_owner",
        :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
        :trip_label => link_to(I18n.t('notification.field.trip'), path_helper.trip_path(trip)))
    else
      link = I18n.t("notification.title.trip_comment_other",
        :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
        :trip_label=>link_to(I18n.t('notification.field.trip'), path_helper.trip_path(trip)))
    end
    return link
  end
end
