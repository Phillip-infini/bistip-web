# a class to represent a payout method
class IndonesiaBankTransferPayoutMethod < PayoutMethod
  INDONESIA_BANK_TRANSFER_CNAME = 'indonesia_bank_transfer'

  validates :bank_name, :presence => true, :length => { :maximum => 100 }
  validates :account_name, :presence => true, :length => { :maximum => 100 }
  validates :branch, :presence => true, :length => { :maximum => 100 }
  validates :account_number, :presence => true, :numericality => true, :length => { :maximum => 100 }

  # get cname of payment method
  def cname
    INDONESIA_BANK_TRANSFER_CNAME
  end
end
