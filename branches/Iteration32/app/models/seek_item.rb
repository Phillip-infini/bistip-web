# sub class of Item that represent item for Seek
class SeekItem < Item

  # contants
  MAXIMUM = 8
  SUMMARY_COUNT = 2
end