class Topic < ActiveRecord::Base
  
  # constants
  TITLE_MAXIMUM_LENGTH = 100
  POSTS_PER_PAGE = 10

  # validation
  validates :title, :length => {:minimum => 5, :maximum => TITLE_MAXIMUM_LENGTH }, :presence => true

  # relationship
  belongs_to :user
  belongs_to :forum
  has_many :posts
  has_one :topic_view

  # scope
  default_scope :order => 'rank DESC, updated_at DESC', :conditions => 'topics.deleted = false'

  # event
  after_create :create_topic_views

  # check if given user is the owner of the topic
  def owner?(user)
    self.user == user
  end

  # create topic views count
  def create_topic_views
    topic_view = TopicView.new
    topic_view.topic = self
    topic_view.save
  end

  # title of the topic
  def name
    Sanitize.clean(self.title)
  end

  # get last page of the posts
  def posts_last_page
    total_posts = self.posts.count
    [((total_posts - 1) / POSTS_PER_PAGE) + 1, 1].max
  end

  # get last post
  def last_post
    self.posts.last
  end

  # is this sticky topic?
  def sticky?
    if self.rank == 2
      true
    else
      false
    end
  end

end
