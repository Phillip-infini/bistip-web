# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.bistip.com"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #

  group(:sitemaps_path => 'sitemaps/', :filename => :statics) do
    add '/about', :changefreq => 'yearly'
    add '/terms_of_service', :changefreq => 'yearly'
    add '/privacy_policy', :changefreq => 'yearly'
    add '/guide', :changefreq => 'yearly'
    add '/guide_all', :changefreq => 'yearly'
    add '/guide_traveler', :changefreq => 'yearly'
    add '/guide_requester', :changefreq => 'yearly'
    add '/how', :changefreq => 'yearly'
    add '/faq', :changefreq => 'monthly'
    add '/new', :changefreq => 'monthly'
    add '/media', :changefreq => 'monthly'
    add '/event_bistipes', :changefreq => 'never'
    add '/event_bistipes_1', :changefreq => 'never'
    add '/event_bistipes_2', :changefreq => 'never'
    add '/event_bistipes_3', :changefreq => 'never'
    add '/event_slogan', :changefreq => 'never'
    add '/event_blog', :changefreq => 'never'
    add '/jobs', :changefreq => 'yearly'
    add '/safepay', :changefreq => 'monthly'
    add '/awards', :changefreq => 'monthly'
    add '/bistip_point', :changefreq => 'monthly'
    add '/api', :changefreq => 'yearly'
    add '/contact_us', :changefreq => 'yearly'
    add '/insurances', :changefreq => 'yearly'
    add '/search_user', :changefreq => 'yearly'
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :dinamics) do
    add '/search_traveler', :changefreq => 'always'
    add '/search_request_post', :changefreq => 'always'
    add '/stories', :changefreq => 'monthly'
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :trips) do
    add '/trips'
    Trip.unscoped.find_each do |trip|
    add trip_path(trip), :changefreq => 'always'
    end
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :seeks) do
    add '/seeks'
    Seek.unscoped.find_each do |seek|
    add seek_path(seek), :changefreq => 'always'
    end
  end

end
