require 'net/http'

class HomeController < ApplicationController

  def index
    count = 9
    if mobile_version?
      count = 2
    end

    @trips = Trip.build_scope_home_index(count).all
    @seeks = Seek.random(count)
  end

  def expire_cloud
    password = params[:password]

    if password == 'gad1ng_c00g33'

      # regenerate trending item
      TrendingItem.generate

      # expire route tag cloud cache
      expire_fragment('all_route')
      redirect_to root_path
    end
  end

  def send_statistics
    password = params[:password]

    if password == 'gad1ng_c00g33'

      # get facebook likes
      url = URI.parse('http://graph.facebook.com/194274033926856')
      req = Net::HTTP::Get.new(url.path)
      res = Net::HTTP.start(url.host, url.port) {|http|
        http.request(req)
      }
      fb_json = JSON.parse(res.body)

      # get twitter followers
      url = URI.parse('http://api.twitter.com/1/users/show.json')
      req = Net::HTTP::Get.new(url.path + '?screen_name=bistip&include_entities=true')
      res = Net::HTTP.start(url.host, url.port) {|http|
        http.request(req)
      }
      tw_json = JSON.parse(res.body)

      ContactUsNotifier.delay.email_statistics(User.count, Trip.count, Seek.count, Comment.count, Message.count, PositiveReputation.count, NegativeReputation.count, Item.count, fb_json["likes"], tw_json["followers_count"]);

      redirect_to root_path
    end
  end
end
