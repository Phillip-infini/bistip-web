# Seek class represent a search for a traveller's trip
class Seek < ActiveRecord::Base
  include Location

  # constants
  NOTES_MAXIMUM_LENGTH = 1000
  CARRIAGE_RETURN_BUFFER = 100
  MAXIMUM_AGE = 60
  SUMMARY_MATCH_COUNT = 2
  DEFAULT_PER_PAGE = 10
  SEARCH_PER_PAGE = 5

  # scope
  default_scope lambda {{ :conditions => ["seeks.created_at > ? and seeks.deleted = ?", MAXIMUM_AGE.days.ago, false], :include => [:origin_city, :destination_city] }}
  scope :origin_city_id, lambda { |origin_city_id| {:conditions => ["origin_city_id = ?", origin_city_id]}}
  scope :origin_city_or_all_cities_id, lambda { |origin_city_id, all_origin_cities_id| {:conditions => ["origin_city_id = ? OR origin_city_id = ?", origin_city_id, all_origin_cities_id]}}
  scope :destination_city_id, lambda { |destination_city_id| {:conditions => ["destination_city_id = ?", destination_city_id]}}
  scope :destination_city_or_all_cities_id, lambda { |destination_city_id, all_destination_cities_id| {:conditions => ["destination_city_id = ? OR destination_city_id = ?", destination_city_id, all_destination_cities_id]}}
  scope :origin_country_id, lambda { |country_id| {:conditions => ["cities.country_id = ?", country_id]}}
  scope :destination_country_id, lambda { |country_id| {:conditions => ["destination_cities_seeks.country_id = ?", country_id]}}
  scope :notes_contains, lambda { |keyword| {:conditions => ["match(notes) against(?)", keyword]}}
  scope :notes_and_items_contains, lambda { |keyword| {:conditions => ["match(seeks.notes) against(?) or match(items.name) against(?)", keyword, keyword], :include => [:items] }}
  scope :user_is_not, lambda { |user_id| {:conditions => ["user_id != ?", user_id]}}
  scope :user_is, lambda { |user_id| {:conditions => ["user_id = ?", user_id]}}
  scope :random, lambda { |*args| {:order => 'RAND()', :limit => args[0] || 1 } }
  scope :from_anywhere, :conditions => "from_anywhere is true"
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ?", 60.days.ago]}}

  # validation
  validates :origin_city, :origin_city => true, :if => :not_from_anywhere?

  validates :destination_city, :destination_city => true

  validates :departure_date, :future_date => true

  validates :departure_date_predicate, :presence => {:if => :departure_date_given?}, :inclusion => {:in => DatePredicate::ALL, :allow_nil => true, :allow_blank => true}

  validates :notes, :length => { :maximum => (NOTES_MAXIMUM_LENGTH + CARRIAGE_RETURN_BUFFER) }

  validates :items, :presence => true

  # relationship
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'
  belongs_to :destination_city, :class_name => "City", :foreign_key => 'destination_city_id'
  belongs_to :user
  has_many :comments, :class_name => "SeekComment"
  has_many :items, :class_name => "SeekItem", :order => "id"

  # nested
  accepts_nested_attributes_for :items, :allow_destroy => true, :reject_if => lambda { |a| a[:name].blank? }

  # event
  after_create :send_match_notification
  before_save :handle_from_anywhere

  # send email to owner of matching trip
  def send_match_notification
    matching_trips = find_matching_trips
    matching_trips.each do |trip|
      MatchSeekNotification.create!(:receiver=>trip.user,:sender=>user, :data_id=>id)
      if trip.user.user_configuration.email_match_seek and (self.origin_city.name != City::ANYWHERE)
        Notifier.email_matching_trip(self, trip).deliver
      end
    end
  end
  handle_asynchronously :send_match_notification

  # handle from anywhere seek
  def handle_from_anywhere
    if self.from_anywhere?
      self.origin_location = City::ANYWHERE
    end
  end

  def departure_date_given?
    !self.departure_date.nil?
  end

  # check if given user is the owner of the trip
  def owner?(user)
    self.user == user
  end

  # check is a seek still active
  def active?
    self.created_at > Seek::MAXIMUM_AGE.days.ago and self.deleted == false
  end

  # find match in trip table
  def find_matching_trips
    if self.active?
      scope = Trip.scoped({})
      origin = self.origin_city
      destination = self.destination_city

      if !self.from_anywhere?
        if origin.all_cities?
          scope = scope.origin_country_id origin.country
        else
          scope = scope.origin_city_id origin.id
        end
      end

      if destination.all_cities?
        scope = scope.destination_country_id destination.country
      else
        scope = scope.destination_city_id destination.id
      end

      scope = scope.user_is_not self.user_id

      # build predicate on query for departure date based on predicate
      departure_date = self.departure_date
      departure_date_predicate = self.departure_date_predicate
      if !departure_date.nil?
        case departure_date_predicate
          when DatePredicate::AFTER
            scope = scope.departure_date_after departure_date
          when DatePredicate::BEFORE
            scope = scope.departure_date_before departure_date
        end
      end

      scope
    else
      Array.new
    end
  end

  # negate of from anywhere
  def not_from_anywhere?
    !self.from_anywhere?
  end

  def items_name
    unless self.items.empty?
      result = ''
      self.items.each_with_index do |item, index|
        # append separator if not the first one
        result << ', ' if index > 0
        result << item.name
        # append ... if item is more than one, and there more items still
        if (index > 0) and (index < (self.items.size - 1))
          result << '...'
          break
        end
      end
      result
    end
  end

  def items_name_all
    unless self.items.empty?
      result = ''
      self.items.each_with_index do |item, index|
        # append separator if not the first one
        result << ', ' if index > 0
        result << item.name
      end
      return result
    end
    ''
  end

  def items_name_for_display
    items_all = items_name_all
    if !items_all.blank? and items_all.size > 30
      items_all.slice(0,30) + '...'
    else
      items_all
    end
  end

  def to_s
    origin_city_name = self.origin_city.name
    origin_city_name = I18n.t('seek.field.label.from_anywhere_short') if (self.origin_city.name == City::ANYWHERE)
    #18n.t('seek.short', :from => origin_city_name, :to => self.destination_city.name)
    result = "Pengen nitip "
    result << items_name_all
    result << " nih. Lagi cari orang dari "
    result << origin_city_name
    result << " ke "
    result << destination_city.name
    result
  end

  #options that apply in every data in webservice that fetch trip record
  def self.data_options
    return {:only => [:id, :notes, :from_anywhere, :departure_date_predicate], :skip_types=>true, :skip_instruct=>true, :methods => [:origin_location, :destination_location, :username, :departure_date_medium_format]}
  end

  # build scoped Seek for searching seek purpose
  def self.build_scope_search(origin, destination, keyword=nil)
    scope = Seek.order('seeks.created_at desc').scoped({})

    if !origin.nil?
      if origin.all_cities?
        scope = scope.origin_country_id origin.country
      else
        scope = scope.origin_city_id origin.id
      end
    end

    if !destination.nil?
      if destination.all_cities?
        scope = scope.destination_country_id destination.country
      else
        scope = scope.destination_city_id destination.id
      end
    end

    # build predicate for keyword
    if !keyword.blank?
      scope = scope.notes_and_items_contains keyword
    end
    return scope
  end

  def departure_date_medium_format
    I18n.l(departure_date, :format => :medium) if departure_date
  end

  def username
    return user.username
  end
  
  def last_comment_page
    comment_count = self.comments.count
    [((comment_count - 1) / Comment::PER_PAGE) + 1, 1].max
  end

  # equality for trip
  def same_route?(other)
    return (self.origin_city_id == other.origin_city_id) && (self.destination_city_id == other.destination_city_id)
  end
end
