require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "should create notification on trip" do
    trip_notification = notifications(:TripCommentNotification)
    assert TripCommentNotification.create!(:receiver => trip_notification.receiver,
      :sender => trip_notification.sender,
      :data_id => trip_notification.data_id,
      :type => trip_notification.type)
  end

  test "should create notification on seek" do
    seek_notification = notifications(:SeekCommentNotification)
    assert SeekCommentNotification.create!(:receiver => seek_notification.receiver,
      :sender => seek_notification.sender,
      :data_id => seek_notification.data_id,
      :type => seek_notification.type)
  end
#
  test "should create notification on positive_reputation" do
    positive_notification = notifications(:PositiveReputationNotification)
    assert PositiveReputationNotification.create!(:receiver => positive_notification.receiver,
      :sender => positive_notification.sender,
      :data_id => positive_notification.data_id,
      :type => positive_notification.type)
  end
#
  test "should create notification on negative_reputation" do
    negative_reputation = notifications(:NegativeReputationNotification)
    assert NegativeReputationNotification.create!(:receiver => negative_reputation.receiver,
      :sender => negative_reputation.sender,
      :data_id => negative_reputation.data_id,
      :type => negative_reputation.type)
  end
end
