require 'test_helper'

class InfluencerLogControllerTest < ActionController::TestCase
  # Replace this with your real tests.
  test "should create one point in influencer log" do
    login_as(:dono)

    influencer = users(:dono)
    trip = trips(:jktsyd)
    code = Base32.encode("#{trip.id} #{influencer.id}")
    assert_difference('InfluencerLog.count') do
      post :create, :code=> code
    end
    assert_redirected_to trip_path(trip)

    # should not create one point in influencer where IP already exist
    assert_no_difference('InfluencerLog.count') do
      post :create, :code => code
    end
    assert_redirected_to trip_path(trip)
  end

end
