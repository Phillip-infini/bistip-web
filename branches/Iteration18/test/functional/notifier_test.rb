require 'test_helper'

class NotifierTest < ActionMailer::TestCase
  
  test "email_validation" do
    user = users(:not_validated)
    mail = Notifier.email_validation(user)
    assert_match user.username, mail.body.encoded
    assert_match "validate?key", mail.body.encoded
  end

  test "email_password" do
    user = users(:not_validated)
    mail = Notifier.email_password(user)
    assert_match user.username, mail.body.encoded
    assert_match "passwords/reset_edit", mail.body.encoded
  end

  test "email_seek_comment" do
    seek = seeks(:banjak)
    commentator = users(:dono)
    comment = comments(:seek2)
    mail = Notifier.email_seek_comment(seek,commentator,comment)
    assert_match comment.body, mail.body.encoded
    assert_match "/seeks/", mail.body.encoded
    
  end

  test "email_trip_comment" do
    trip = trips(:tokjak)
    commentator = users(:one)
    comment = comments(:trip1)
    mail = Notifier.email_trip_comment(trip,commentator,comment)
    assert_match comment.body, mail.body.encoded
    assert_match "/trips/", mail.body.encoded
  end

  test "email_receiver_positive_reputation" do
    receiver = users(:dono)
    giver = users(:mactavish)
    positive = positive_reputations(:positive1)
    mail = Notifier.email_receiver_positive_reputation(receiver,giver,positive)
    assert_match positive.body, mail.body.encoded
  end

  test "email_receiver_negative_reputation" do
    receiver = users(:one)
    giver = users(:mactavish)
    negative = negative_reputations(:negative1)
    mail = Notifier.email_receiver_negative_reputation(receiver,giver,negative)
    assert_match negative.body, mail.body.encoded
  end

  test "email_receiver_negative_reputation_from_user_notvalid" do
    receiver = users(:dono)
    giver = users(:not_validated)
    negative = negative_reputations(:negative2)
    mail = Notifier.email_receiver_negative_reputation(receiver,giver,negative)
    assert_match negative.body, mail.body.encoded
  end

  test "email_receiver_positive_reputation_from_user_notvalid" do
    receiver = users(:mactavish)
    giver = users(:not_validated)
    positive = positive_reputations(:positive2)
    mail = Notifier.email_receiver_positive_reputation(receiver,giver,positive)
    assert_match positive.body, mail.body.encoded
  end

  test "email_seek_comment_from_user_notvalid" do
    seek = seeks(:banjak)
    commentator = users(:not_validated)
    comment = comments(:seek2)
    mail = Notifier.email_seek_comment(seek,commentator,comment)
    assert_match comment.body, mail.body.encoded
    assert_match "/seeks/", mail.body.encoded
  end

  test "email_trip_comment_from_user_notvalid" do
    trip = trips(:tokjak)
    commentator = users(:not_validated)
    comment = comments(:trip1)
    mail = Notifier.email_trip_comment(trip,commentator,comment)
    assert_match comment.body, mail.body.encoded
    assert_match "/trips/", mail.body.encoded
  end

end
