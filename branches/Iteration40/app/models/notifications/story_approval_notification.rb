class StoryApprovalNotification < Notification

  # override to_link method to perform specified action of StoryApprovalNotification
  def to_link(current_user)
    story = Story.find(data_id)
    link = I18n.t("notification.title.story_approval",
      :link => link_to(I18n.t("stories.general.singular"), path_helper.story_path(story)))
    return link
  end
end
