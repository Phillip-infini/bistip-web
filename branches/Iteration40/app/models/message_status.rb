# to represent message status
class MessageStatus
  
  # constants
  UNREAD = 'unread'
  DEPARTURE_DATE_ASC = 'departure_date_asc'

  # return label of status and its value
  def self.map
    return [[I18n.t('message.filter.status.blank'), ''],[I18n.t('message.filter.status.unread'), UNREAD]]
  end

  # build a scope
  def self.build(status, scope)
    if status == UNREAD
      scope.unread
    end
  end
end
