# Trip class represent a trip or travel plan
class RequestedItemTrack < ActiveRecord::Base

  # relationship
  belongs_to :origin_city, :class_name => "City", :foreign_key => 'origin_city_id'

  def self.generate
    anchor_words = AnchorWord.generate_all_in_string
    scope = Message.scoped({})
    scope = scope.trip_only
    scope = scope.body_contains(anchor_words)
    scope.each do |message|
      self.store_item(message.body, message.trip.origin_city)
    end

    scope = TripComment.scoped({})
    scope = scope.body_contains(anchor_words)
    scope.each do |comment|
      self.store_item(comment.body, comment.trip.origin_city)
    end

    scope = Item.scoped({})
    scope = scope.trip_only
    scope.each do |trip_item|
      self.store_item(trip_item.name, trip_item.trip.origin_city)
    end
  end

  private
    def self.store_item(text, origin_city)
      # get words after anchor
      words_after = TrendingItem.get_words_after_anchor(text)

      # get item words
      item_words = TrendingItem.find_item_words(words_after)
      item_words.strip!

      # only include non empty word and word that has length > 2
      if !item_words.blank? and item_words.length > 2
        existing = RequestedItemTrack.find_by_name_and_origin_city_id(item_words, origin_city.id)
        if existing
          existing.count = existing.count + 1
          existing.save
        else
          rit = RequestedItemTrack.new
          rit.name = item_words
          rit.origin_city = origin_city
          rit.count = 1
          rit.save
        end
      end
    end
  
end