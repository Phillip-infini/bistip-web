module ContentHelper
  def faq_slide_js()
    javascript_tag "$(document).ready(function() {
  $('h4').each(function() {
    var tis = $(this), state = false, answer = tis.next('div').hide();
    tis.click(function() {
      state = !state;
      answer.toggle(state);
      tis.toggleClass('active');
    });
  });
});"
  end
end
