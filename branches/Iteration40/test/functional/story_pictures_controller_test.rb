require 'test_helper'

class StoryPicturesControllerTest < ActionController::TestCase

  test "should show story picture" do
    get :show, :story_picture_id => story_pictures(:storypicone)
    assert_response :success
    assert_not_nil assigns(:story_picture)
  end
  
end
