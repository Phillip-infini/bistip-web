require 'test_helper'

class StoryTest < ActiveSupport::TestCase

  test "create a new story" do
    story = Story.new
    story.user=users(:one)
    story.body='The Body content must be more than 30 characters'
    story.title='This is a test story'
    assert story.save
  end

  test "new story less than minimum chars" do
    story = Story.new
    story.user=users(:one)
    story.body='Less than 30'
    story.title='less 10'
    assert !story.save
  end

  test "new story with no input" do
    story = Story.new
    story.user=users(:one)
    story.body=''
    story.title=''
    assert !story.save
  end

  test "new story with no login" do
    story = Story.new
    story.body='The Body content must be more than 30 characters'
    story.title='This is a test story'
    assert !story.save
  end

  test "find story" do
    story_id = stories(:storyone).id
    assert_nothing_raised { Story.find(story_id) }
  end

  test "update story" do
    story = stories(:storyone)
    assert story.update_attributes(:title => 'New Title more than 10 chars', :body => 'The Body content must be more than 30 characters')
  end

  test "update story less than minimum title chars" do
    story = stories(:storyone)
    assert !story.update_attributes(:title => 'New Title', :body => 'The Body content must be more than 30 characters')
  end

  test "update story less than minimum body chars" do
    story = stories(:storyone)
    assert !story.update_attributes(:title => 'New Title more than 10 chars', :body => 'The Body content')
  end

  test "story has picture" do
    story = stories(:storyone)
    assert story.has_picture?
    assert story.has_more_than_one_picture?
  end

end