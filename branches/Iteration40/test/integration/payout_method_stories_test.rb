require 'test_helper'

class PayoutMethodStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "setting payout methods" do
  #user login
    test_user=users(:dono)
    full_login_as(:dono)
    #dummy user goes to the user payout path
    get user_payout_methods_path(test_user)
    assert_response :success
    #check if template index is rendered
    assert_template 'index'
    #check if now user has a payout method
    assert !test_user.has_payout_method?

    #filling in form and submit it
    assert_difference ('PayoutMethod.count') do
      post user_payout_methods_path(test_user),:payout_method => { :type =>payment_methods(:paypal), :email => test_user.email}
    #checking if the payout is saved
    end
    #checking if user is redirected to the user payout methods path
    assert_response :redirect
    assert_redirected_to user_payout_methods_path(test_user)
    follow_redirect!

  end

  test "deleting payout methods" do
  #dummy user login
    test_user=users(:dono)
    full_login_as(:dono)
    #dummy user goes to the user payout page
    get user_payout_methods_path(test_user)
    assert_response :success
    assert_template 'new'
    #check if he doesn't have any payout method
    assert !test_user.has_payout_method?

    #filling in form
    assert_difference ('PayoutMethod.count') do
      post user_payout_methods_path(test_user), :payout_method => { :type =>payment_methods(:paypal), :email => test_user.email}
    end
    #check if a data is created

    #dummy should be redirected to the user payout method path
    assert_response :redirect
    assert_redirected_to user_payout_methods_path(test_user)
    follow_redirect!
    #dummy clicked on the delete button

    delete user_payout_methods_path(test_user)

  #requires simulation of user click in UI :selenium

  end
  test "creating multiple payout methods" do
  #user login
    test_user=users(:dono)
    full_login_as(:dono)
    #user goes to the payout method page
    get user_payout_methods_path(test_user)
    assert_response :success
    assert_template 'index'
    #payout_method/index
    #user doesn't have any payout method
    assert !test_user.has_payout_method?

    #create a new paypal method
    assert_difference ('PayoutMethod.count') do
      post user_payout_methods_path(test_user), :payout_method => { :type =>payment_methods(:paypal), :email => test_user.email}
    end
    assert_response :redirect
    assert_redirected_to user_payout_methods_path(test_user)
    follow_redirect!
    #trying to go to user payout method
    get user_payout_methods_path(test_user)
    assert_response :success
    assert_template '_paypal_payout_method'
    #no create new option will be shown
    !assert_template 'new'

  end

end
