class AddThreadStarterToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :thread_starter, :boolean, :default => false
    count = 0
    puts 'Before: Total message with nil reply to id: ' + Message.find_all_by_reply_to_id(nil).count.to_s
    Message.find_all_by_reply_to_id(nil).each do |m|
      m.thread_starter = true
      m.reply_to_id = m.id
      m.save(false)
      count = count + 1
    end
    puts 'After Total message with nil reply to id: ' + Message.find_all_by_reply_to_id(nil).count.to_s
  end

  def self.down
    remove_column :messages, :thread_starter
  end
end
