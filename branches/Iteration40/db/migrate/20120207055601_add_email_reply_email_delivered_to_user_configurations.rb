class AddEmailReplyEmailDeliveredToUserConfigurations < ActiveRecord::Migration
  def self.up
    add_column :user_configurations, :email_reply_email_delivered, :boolean, :default => true
  end

  def self.down
    remove_column :user_configurations, :email_reply_email_delivered
  end
end
