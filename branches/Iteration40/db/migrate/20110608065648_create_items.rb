class CreateItems < ActiveRecord::Migration
  def self.up
    create_table :items do |t|
      t.string :name, :null => false
      t.belongs_to :trip
      t.belongs_to :seek
      t.belongs_to :tip_unit
      t.integer :tip
      t.timestamps
    end
  end

  def self.down
    drop_table :items
  end
end
