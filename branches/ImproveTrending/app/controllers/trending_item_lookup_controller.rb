class TrendingItemLookupController < ApplicationController

  def show
    trending_item_to_lookup = params[:item]
    @trending_item = TrendingItem.find_by_item(trending_item_to_lookup)
    @trips = Trip.notes_and_items_contains(@trending_item.item)
    @seeks = Seek.notes_and_items_contains(@trending_item.item)
    @comments = Comment.body_contains(@trending_item.item)
    @items = Item.name_contains(trending_item_to_lookup)
  end
end
