class AddEmailOtherCommentToUserConfigurations < ActiveRecord::Migration
  def self.up
    add_column :user_configurations, :email_other_trip_comment, :boolean, :default => true
    add_column :user_configurations, :email_other_seek_comment, :boolean, :default => true
  end

  def self.down
    remove_column :user_configurations, :email_other_trip_comment
    remove_column :user_configurations, :email_other_seek_comment
  end
end
