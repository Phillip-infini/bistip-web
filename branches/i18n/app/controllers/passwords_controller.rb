class PasswordsController < ApplicationController
  
  before_filter :authenticate, :only => [:edit, :update]

  # Render form to send reset password link email
  def edit
    @user = current_user
  end

  # Send email to user who ask for reset password link
  def send_email
    email = params[:email]

    # if email input is empty then
    if string_nil_or_empty?(email)
      flash[:alert] = t("password.send_email.message.email_invalid")
      redirect_to :back
    else
      user = User.find_by_email(email)

      # if user is found by that email then send reset password link
      # if not then DO NOT DO ANYTHING
      if user
        user.reset_password_key = UUIDTools::UUID.timestamp_create
        user.save
        Notifier.delay.email_password(user)
      end
      flash[:notice] = t("password.send_email.message.success")
      redirect_to root_path
    end
  end

  # Commit password change
  def update
    @user = current_user

    # get all the form value
    current_password = params[:current_password]
    new_password = params[:new_password]
    new_password_confirmation = params[:new_password_confirmation]

    # validation check from the input form
    if string_nil_or_empty?(current_password)
      flash.now[:alert] = t('password.update.message.current_password_not_given')
      render :action => 'edit'
    elsif string_nil_or_empty?(new_password)
      flash.now[:alert] = t('password.update.message.new_password_not_given')
      render :action => 'edit'
    elsif !User.authenticate(current_user.email, current_password)
      flash.now[:alert] = t('password.update.message.current_password_wrong')
      render :action => 'edit'
    elsif new_password != new_password_confirmation
      flash.now[:alert] = t('password.update.message.new_password_does_not_match_confirmation')
      render :action => 'edit'
    else
      # try set the new password and commit to repository
      # if fail then go to edit view again
      
      @user.password = new_password
      if @user.valid?
        @user.save
        flash[:notice] = t("password.update.message.success")
        redirect_to profile_path(@user.username)
      else
        put_model_errors_to_flash(@user.errors)
        render :action => 'edit'
      end
    end
  end

  # commit the password change from user
  def reset_update
    # get all the form value
    email = params[:email]
    key = params[:key]
    new_password = params[:new_password]
    new_password_confirmation = params[:new_password_confirmation]
    
    @user = User.find_by_email(email)

    # validation check from the input form
    if !@user
      invalid_link_alert_message
    elsif string_nil_or_empty?(new_password)
      flash[:alert] = t('password.update.message.new_password_not_given')
      redirect_to :back
    elsif @user.reset_password_key != key
      invalid_link_alert_message
    elsif new_password != new_password_confirmation
      flash[:alert] = t('password.update.message.new_password_does_not_match_confirmation')
      redirect_to :back
    else
      # try set the new password and commit to repository
      # if fail then go to reset_edit view again
        
      @user.password = new_password
      if @user.valid?
        @user.reset_password_key = ''
        @user.save
        flash[:notice] = t("password.reset_update.message.success")
        redirect_to root_path
      else
        put_model_errors_to_flash(@user.errors)
        redirect_to :back
      end
    end
  end

  private
    def invalid_link_alert_message
      flash[:alert] = t('password.reset_update.message.invalid_link',
        :href => ActionController::Base.helpers.link_to(t('password.forgot.link'), reset_password_path))
      redirect_to :back
    end
end
