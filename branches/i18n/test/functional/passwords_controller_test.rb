require 'test_helper'

class PasswordsControllerTest < ActionController::TestCase

  test "should render edit password form" do
    login_as(:one)
    get :edit
    assert_response :success
  end

  test "should not render edit password form because not logged in" do
    get :edit
    assert_not_nil flash[:notice], "There should be notice of access denied"
    assert_redirected_to login_path
  end

  test "should send reset password email" do
    user_to_user = users(:one)
    post :send_email, :email => user_to_user.email
    assert_not_nil flash[:notice], "There should be notice of send reset password link successful"
    assert_redirected_to root_path
  end

  test "should update password" do
    login_as(:one)
    post :update, :current_password => "secret", :new_password => "changed",
      :new_password_confirmation => 'changed'
    assert_not_nil flash[:notice], "There should be notice of update password successful"
    assert_redirected_to profile_path(users(:one).username)
  end

  test "should render reset password form" do
    get :reset_edit
    assert_response :success
  end
  
  test "should update reset password" do
    user_to_use = users(:not_validated)
    post :reset_update, :email => user_to_use.email, :key => user_to_use.reset_password_key,
      :new_password => "changed", :new_password_confirmation => "changed"
    assert_not_nil flash[:notice], "There should be notice of reset password successful"
    assert_redirected_to root_path
  end

end
