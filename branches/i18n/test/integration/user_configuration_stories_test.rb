require 'test_helper'

class UserConfigurationStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  # Replace this with your real tests.
  test "edit and update a user's configuration" do
    #login
    full_login_as(:mactavish)
    mac = users(:mactavish)
    config = user_configurations(:mac_config)

    #edit
    get edit_user_configuration_path(:user_id=>mac, :id=>config)
    assert_response :success
    assert_template 'edit'

    #update
    put user_configuration_path :user_id=>mac, :id=>config, :user_configuration => {
      :email_receive_message => false,
      :email_receive_trip_comment => false,
      :email_receive_seek_comment => false,
      :email_receive_reputation => false
    }
    assert_response :redirect
    assert_redirected_to user_path(mac)
    follow_redirect!
  end
end
