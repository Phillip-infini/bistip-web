require 'test_helper'

class TripsStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "create new trip" do

    # login
    full_login_as(:one)

    get new_trip_path
    assert_response :success
    assert_template 'new'

    # create trip
    post trips_path, :trip => {:origin_location => "jakarta",
      :destination_location => "sydney",
      :departure_date => DateTime.current.advance(:day => 2),
      :arrival_date => DateTime.current.advance(:day => 3),
      :notes => "text"
      }

    assert assigns(:trip).valid?
    assert_response :redirect
    follow_redirect!
    
  end

  test "create new routine trip" do

    # login
    full_login_as(:dono)

    get new_trip_path
    assert_response :success
    assert_template 'new'

    # create trip
    post trips_path, :trip => {:origin_location => "jakarta",
      :destination_location => "bogor",
      :day => ["Sunday", "Tuesday"],
      :period => "biweekly",
      :notes => "lorem ipsum dolor sit amet",
      :routine => true
      }

    assert assigns(:trip).valid?
    assert_response :redirect
    follow_redirect!

  end

  test "edit and update a trip" do
    
    # login
    full_login_as(:one)

    trip_to_use = trips(:jktsyd)

    # edit
    get edit_trip_path(trip_to_use)

    assert_response :success
    assert_template 'edit'

    # update
    put trip_path(trip_to_use), :trip => {:notes => "new notes"}

    assert_response :redirect
    assert_redirected_to trip_path(trip_to_use)

    follow_redirect!
  end

  test "edit and update a routine trip" do

    # login
    full_login_as(:dono)

    trip_to_use = trips(:bogjak)

    # edit
    get edit_trip_path(trip_to_use)

    assert_response :success
    assert_template 'edit'

    # update
    put trip_path(trip_to_use), :trip => {:notes => "new notes",:day=>["Friday"],:period=> "biweekly"}

    assert_response :redirect
    assert_redirected_to trip_path(trip_to_use)

    follow_redirect!
  end

  test "delete a trip" do
     # login
    full_login_as(:one)

    trip_to_use = trips(:jktsyd)

    # view the trip
    get trip_path(trip_to_use)
    assert_response :success
    assert_template 'show'

    # delete the trip
    delete trip_path(trip_to_use)
    
    assert_response :redirect
    assert_redirected_to user_path(users(:one))

    follow_redirect!
  end

end
