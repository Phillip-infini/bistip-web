require 'test_helper'

class TripCommentTest < ActiveSupport::TestCase
  test "should create comment on trip" do
    comment = TripComment.new
    comment.body = 'body 123'
    comment.trip = trips(:jktsyd)
    comment.user = users(:one)
    assert comment.save
  end

  test "should delete comment on trip" do
    cmt = comments(:trip1)
    comment = TripComment.find(cmt.id) #comment.find(comments(:cmt))
    assert comment.destroy
  end
end
