require 'test_helper'

class TripTest < ActiveSupport::TestCase

  test "should create trip" do
    trip = Trip.new
    trip.origin_location = "jakarta"
    trip.destination_location = "sydney"
    trip.departure_date = DateTime.current.advance(:days => 1)
    trip.arrival_date = DateTime.current.advance(:days => 2)
    trip.user = users(:one)
    trip.notes = 'test'
    assert trip.save
  end

  test "should find trip" do
    trip_id = trips(:jktsyd).id
    assert_nothing_raised {Trip.find(trip_id)}
  end

  test "should update trip" do
    trip = trips(:jktsyd)
    assert trip.update_attributes(:notes => 'notes')
  end

  test "should create a routine trip" do
    trip = Trip.new
    trip.routine=true
    trip.origin_location = "jakarta"
    trip.destination_location = "sydney"
    trip.departure_date = nil
    trip.arrival_date = nil
    trip.day = ["Friday","Saturday"]
    trip.period="weekly"
    trip.user = users(:one)
    trip.notes = 'test'
    assert trip.save
  end

  test "should update a routine trip" do
    trip = trips(:bogjak)
    assert trip.update_attributes(:notes=>"liburan tiap minggu ke jakarta",:day=>["Sunday"])
  end

  test "should delete trip" do
    trip_id = trips(:jktsyd).id
    assert_nothing_raised {Trip.find(trip_id)}
    trip = Trip.find(trip_id)
    trip.update_attributes(:deleted=>true)
  end
end
