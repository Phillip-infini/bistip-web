class CreateNotifications < ActiveRecord::Migration
  def self.up
    create_table :notifications do |t|
      t.column :receiver_id, :integer, :null => false, :references => :users
      t.column :sender_id, :integer, :null => false, :references => :users
      t.integer :data_id
      t.string :type
      t.column :read, :boolean, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :notifications
  end
end
