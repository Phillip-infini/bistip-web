class AddDeletedToForums < ActiveRecord::Migration
  def self.up
    add_column :forums, :deleted, :boolean, :default => false
  end

  def self.down
    remove_column :forums, :deleted
  end
end
