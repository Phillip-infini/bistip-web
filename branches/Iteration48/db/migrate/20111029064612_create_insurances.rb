class CreateInsurances < ActiveRecord::Migration
  def self.up
    create_table :insurances do |t|
      t.column :buyer_id, :integer, :null => false, :references => :users
      t.column :seller_id, :integer, :null => false, :references => :users
      t.column :message_id, :integer, :null => false, :references => :messages
      t.column :amount, :decimal, :precision => 15, :scale => 2
      t.column :cover_value, :decimal, :precision => 15, :scale => 2
      t.string :item
      t.belongs_to :currency
      t.timestamps
    end
  end

  def self.down
    drop_table :insurances
  end
end
