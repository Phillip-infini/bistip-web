class ItemPicture < ActiveRecord::Base
  belongs_to :item
  
  has_attached_file :attachment, :styles => lambda{ |a| ['image/gif', 'image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg', 'application/octet-stream'].include?( a.content_type ) ? { :original => "600x600>", :thumb => "48x48>" } : {}},
                             :storage => :s3,
                             :s3_credentials => "#{Rails.root}/config/s3.yml",
                             :path => "/item_pictures/:id/:style/:filename"

  validates_attachment_content_type :attachment, :content_type => ['image/gif', 'image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg', 'application/octet-stream'],
                                             :message => 'must be image'

  validates_attachment_size :attachment, :less_than => 2.megabytes
end
