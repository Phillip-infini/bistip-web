# To change this template, choose Tools | Templates
# and open the template in the editor.

class MatchTripNotification < Notification

  #overide method to provide information who create a trip that match with a seek that create by current_user
  def to_link(current_user)
    trip = Trip.find(data_id)
    link = I18n.t("notification.title.match_trip",
      :sender => link_to(sender.username, path_helper.profile_path(sender.username)),
      :trip_label=>link_to(I18n.t('notification.field.trip') , path_helper.trip_path(trip)))
    return link
  end
end
