class SeekCommentsController < ApplicationController
  before_filter :authenticate, :only => [:destroy,:create]

  # POST new comment
  def create
    @seek = Seek.find(params[:seek_id])
    @comment = @seek.comments.new(params[:comment])
    @comment.user = current_user

    # set comment body to session to repopulate form if fail validation
    store_body_in_session
    
    if @comment.save
      clean_data_in_session
      flash[:notice] = t('comment.message.create.success')
      handle_redirect
    else
      put_model_errors_to_flash(@comment.errors, 'redirect')
      handle_error_redirect
    end
    
  end

  # DELETE Comment only for comment owner
  def destroy
    @seek = Seek.find(params[:seek_id])
    @comment = @seek.comments.find(params[:id])

    # only the owner of the comment can do the deletion
    if @comment.owner?(current_user)
      # apply soft delete, set deleted attribute to true
      @comment.update_attribute(:deleted,true)
      flash[:notice] = t('comment.message.destroy.success')
    else
      flash[:alert] = t('comment.message.destroy.fails')
    end
    redirect_to @comment.get_path
  end

  private

    # set comment body to session to repopulate form if fail validation
    def store_body_in_session
      if @comment.reply?
        session[:seek_reply_comment] = @comment.body
        session[:seek_reply_comment_id] = @comment.reply_to_id
      else
        session[:seek_comment] = @comment.body
      end
    end

    # clean comment session
    def clean_data_in_session
      session[:seek_comment] = nil
      session[:seek_reply_comment] = nil
      session[:seek_reply_comment_id] = nil
    end

    # handle redirect
    def handle_error_redirect
      if @comment.reply?
        redirect_to seek_path(@seek, :page => @comment.thread_starter.find_page, :anchor => @comment.thread_starter.internal_id)
      else
        redirect_to seek_path(@seek, :anchor => Comment::POST_INTERNAL_ID)
      end
    end

    # handle redirect
    def handle_redirect
      redirect_to @comment.get_path
    end

end
