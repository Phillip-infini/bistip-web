namespace :populate  do
  desc "create alias for countries"
  task :countries_alias => :environment do
    File.open("#{Rails.root}/db/data/countries-alias-migration.txt", "r") do |infile|
      infile.read.each_line do |country_row|
        code, name, calias = country_row.chomp.split("|")
        country = Country.find_by_code(code)
        unless country.blank?
          puts 'going to save alias for: ' + country.name
          country.alias = calias
          country.save
        end
      end
    end
  end
end
