require 'test_helper'

class PostTest < ActiveSupport::TestCase

  test "should create post" do
    post = Post.new
    post.body = 'Ipad Ipad'
    post.user = users(:one)
    post.topic = topics(:ipad)
    assert post.save
  end

  test "should find post" do
    post_id = posts(:ipadpost).id
    assert_nothing_raised {Post.find(post_id)}
  end

  test "should update post" do
    post = posts(:ipadpost)
    assert post.update_attributes(:body => 'Ipad Ipad')
  end

  test "should delete post" do
    post_id = posts(:ipadpost).id
    assert_nothing_raised {Post.find(post_id)}
    post = Post.find(post_id)
    assert post.update_attributes(:deleted=>true)
  end

end