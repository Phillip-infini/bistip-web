# To change this template, choose Tools | Templates
# and open the template in the editor.

class PositiveReputationNotification < Notification

  #overide method, to provide info who gave positive reputation to current_user
  def to_link
    positive_reputation = User.find(data_id)
    link = I18n.t("notification.title.positive_reputation",
      :sender => link_to(sender.username, path_helper.user_path(sender)),
      :reputation_label=>link_to(I18n.t('notification.field.positive_reputation'), path_helper.user_positive_reputations_path(positive_reputation)))
    return link
  end
end
