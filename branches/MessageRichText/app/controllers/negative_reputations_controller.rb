class NegativeReputationsController < ApplicationController

  before_filter :authenticate, :not_self, :only => [:new, :create]
  #new user
  def new
    @user = User.find(params[:user_id])
    if NegativeReputation.last_24_hour(current_user.id, @user.id).count >= 1
      flash[:alert] = t('reputation.new.not_allowed.negative')
      redirect_to profile_path(@user.username)
    end
  end

  #list of user's positive reputation
  def index
    @user = User.find(params[:user_id])
    @negatives = @user.received_negative_reputations.paginate(:page => params[:page], :per_page => 5)
  end

  # post
  def create
    @user = User.find(params[:user_id])
    
    if NegativeReputation.last_24_hour(current_user.id, @user.id).count >= 1
      flash[:alert] = t('reputation.new.not_allowed.negative')
      redirect_to profile_path(@user.username)
    else
      @negative = @user.received_negative_reputations.new(params[:negative_reputation])
      @negative.giver = current_user

      if @negative.save
        #email notification
        Notifier.delay.email_receiver_negative_reputation(@user, current_user, @negative) if email_receive_reputation(@user)
        #live notification
        NegativeReputationNotification.create!(:receiver=>@user, :sender=>current_user, :data_id=>@user.id)
        flash[:notice] = t('reputation.create.success.negative')
        redirect_to @user
      else
        put_model_errors_to_flash(@negative.errors)
        render :action => "new"
      end
    end
  end
end
