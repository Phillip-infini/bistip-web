require 'test_helper'

class SeekCommentStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  # Replace this with your real tests.
  test "creating comment for a seek" do
    seek = seeks(:tokjak)

    # login
    full_login_as(:one)

    # view seek
    get seek_path(seek)
    assert_response :success
    assert_template 'new'

    # add new comment on seek
    post seek_comments_path(seek), :comment => {
      :body => 'seeknya dono tokyo jakarta',
      :user_id => :one,
      :type => 'SeekComment'
    }

    assert_response :redirect
    assert_redirected_to seek
    follow_redirect!

  end

  test "deleting seek comment" do
    seek = seeks(:banjak)
    comment = seek.comments.first

    # login
    full_login_as(:one)

    # view a seek
    get seek_path(seek)
    assert_response :success

    # deleting a comment
    delete seek_comment_path(seek, comment)

    assert_response :redirect
    assert_redirected_to seek
    follow_redirect!
  end

end
