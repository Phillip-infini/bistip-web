class HomeController < ApplicationController

  def index
    count = 9
    if mobile_version?
      count = 2
    end
    
    @trips = Trip.random(count)
    @seeks = Seek.random(count)
  end

  def expire_cloud
    expire_fragment('all_route')
    redirect_to root_path
  end
end
