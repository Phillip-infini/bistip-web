class RecommendController < ApplicationController

  before_filter :authenticate, :not_self => [:get_recommendation]
  
  def handle
    username = params[:username]
    user = User.find_by_username(username)
    redirect_to new_user_positive_reputation_path(:user_id => user, :invitation => true)
  end

  def get_recommendation
    @user = current_user
  end

end
