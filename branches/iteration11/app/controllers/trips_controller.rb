class TripsController < ApplicationController

  before_filter :authenticate, :only => [:new, :edit, :create, :update, :destroy, :match, :new_routine]

  # GET /trips/1
  def show
    @trip = Trip.unscoped.find(params[:id])
    @similiar_trips = Trip.similiar_trips(@trip.origin_city, @trip.destination_city, @trip.user)
    @trcomments = @trip.comments.paginate(:page => params[:page], :per_page => 5)
    if current_user
      @visible = !@trip.owner?(current_user)
    else
      @visible = true
    end
    if logged_in?
      @comment = Comment.new
    end
  end

  # GET /trips/new
  def new
    @trip = Trip.new
  end

  # GET /trips/new_routine
  def new_routine
    @trip = Trip.new
  end
  
  # GET /trips/1/edit
  def edit
    @trip = current_user.trips.find(params[:id])
  end

  # POST /trips
  def create
    @trip = current_user.trips.new(params[:trip])

    if @trip.save
      flash[:notice] = t('trip.create.message.success')
      redirect_to(@trip)
    else
      put_model_errors_to_flash(@trip.errors)
      if !@trip.routine?
        render :action => "new"
      else
        render :action => "new_routine"
      end
    end
  end

  # PUT /trips/1
  def update
    @trip = current_user.trips.find(params[:id])
    
    # if params is nil, then trip's day should be nil too
    if params[:trip][:day].nil?
      @trip.day = nil
    end
    if @trip.update_attributes(params[:trip])
      flash[:notice] = t('trip.update.message.success')
      redirect_to(@trip)
    else
      put_model_errors_to_flash(@trip.errors)
      render :action => "edit"
    end
  end

  # DELETE /trips/1
  def destroy
    # only the owner of the comment can do the deletion
    @trip = current_user.trips.find(params[:id])
    # apply soft delete, set deleted attribute to true
    @trip.update_attribute(:deleted, true)
    flash[:notice] = t('trip.destroy.message.success')
    
    redirect_to(profile_path(current_user.username))
  end

  # show matching seeks from a trip
  def match
    @trip = current_user.trips.find(params[:id])
    @matches = @trip.find_matching_seeks
  end
end
