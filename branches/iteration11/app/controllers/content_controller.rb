class ContentController < ApplicationController
  before_filter :authenticate, :only => [:registered]
end
