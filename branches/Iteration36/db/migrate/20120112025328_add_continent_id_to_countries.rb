class AddContinentIdToCountries < ActiveRecord::Migration
  def self.up
    add_column :countries, :continent_id, :integer, :null => true, :references => :continents
    
    # create and populate continent id for countries
    File.open("#{Rails.root}/db/data/countries-continents.txt", "r") do |infile|
      infile.read.each_line do |country_row|
        country_code, country_name, continent = country_row.chomp.split("|")
        if Country.find_by_code(country_code.downcase)
          country = Country.find_by_code(country_code.downcase)
          continent = Continent.find_by_code(continent.downcase)
          country.continent_id = continent.id
          country.save
          puts 'setting continent id for: ' + country_code + ' with value ' + continent.name
        else
          puts 'not found: ' + country_code + ' ' + country_name + " " + continent + ' in the database'
        end
      end
    end

        # europe country already created, lets handle migration for this
    euro = Country.find_by_code('eu')
    euro.code = Country::CONTINENT_CODE
    euro.continent = Continent.find_by_code('eu')
    euro.save

    # create all 5 continent placeholder in country database

    # Asia
    country = Country.new
    country.name = 'Asia'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('as')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # North America
    country = Country.new
    country.name = 'North America'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('na')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # South America
    country = Country.new
    country.name = 'South America'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('sa')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # Africa
    country = Country.new
    country.name = 'Africa'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('af')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # Africa
    country = Country.new
    country.name = 'Oceania'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('oc')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

    # Antartica
    country = Country.new
    country.name = 'Antartica'
    country.code = Country::CONTINENT_CODE
    country.continent = Continent.find_by_code('an')
    country.save

    city = City.new
    city.name = City::ALL_CITIES
    city.country = country
    city.save

  end

  def self.down
    remove_column :countries, :continent_id
  end
end
