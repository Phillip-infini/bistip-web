class ConvertTestimonialsToStories < ActiveRecord::Migration
  def self.up
    testimonials = Testimonial.all

    testimonials.each do |t|
      user = User.find_by_username(t.from)
      story = Story.new
      story.user = user

      story.body = t.body
      story.created_at = t.created_at
      story.deleted = false
      story.save(:validate => false)
    end
  end

  def self.down
    Story.destroy_all
  end
end
