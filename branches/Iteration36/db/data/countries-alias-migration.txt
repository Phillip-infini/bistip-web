ae|United Arab Emirates|Uni Emirat Arab
an|Netherlands Antilles|Antillen
aq|Antarctica|Antartika
be|Belgium|Belgia
br|Brazil|Brasil
ca|Canada|Kanada
cd|Congo, The Democratic Republic of the|Kongo
cf|Central African Republic|Afrika Tengah
cg|Congo|Kongo
ch|Switzerland|Swiss
ci|Cote d'Ivoire|Pantai Gading
ck|Cook Islands|Kepulauan Cook
cl|Chile|Chili
cm|Cameroon|Kamerun
cn|China|Cina
co|Colombia|Kolumbia
cr|Costa Rica|Kosta Rika
cu|Cuba|Kuba
cy|Cyprus|Siprus
cz|Czech Republic|Ceko
de|Germany|Jerman Deutschland
do|Dominican Republic|Dominika
dz|Algeria|Aljazair
ec|Ecuador|Ekuador
eg|Egypt|Mesir
es|Spain|Spanyol
et|Ethiopia|Etiopia
fr|France|Prancis
gb|United Kingdom|Inggris skotlandia wales irlandia England scotland nothern ireland
gf|French Guiana|Guyana
gr|Greece|Yunani
hk|Hong Kong|HongKong
hr|Croatia|Kroasia
ie|Ireland|Irlandia
iq|Iraq|Irak
is|Iceland|Islandia
it|Italy|Italia
jo|Jordan|Yordania
jp|Japan|Jepang
kh|Cambodia|Kamboja
la|Lao People's Democratic Republic|Laos
lb|Lebanon|Libanon
lu|Luxembourg|Luksemburg
ly|Libyan Arab Jamahiriya|Libya
ma|Morocco|Moroko
mc|Monaco|Monako
md|Moldova, Republic of|Moldova
mk|Macedonia|Makedonia
mo|Macao|Makao
mv|Maldives|Maladewa
my|Malay|Melayu
mz|Mozambique|Mozambik
nc|New Caledonia|Kaledonia
ni|Nicaragua|Nikaragua
nl|Netherlands|Belanda Dutch
no|Norway|Norwegia
nz|New Zealand|Selandia
pg|Papua New Guinea|Papua Nugini
ph|Philippines|Filipina
pl|Poland|Polandia
re|Reunion|Reuni
ro|Romania|Rumania
se|Sweden|Swedia
sg|Singapore|Singapura
sk|Slovakia|Slowakia
tr|Turkey|Turki
tw|Taiwan|Taipei
ua|Ukraine|Ukraina
us|United States of America (USA)|Amerika Serikat
va|Holy See (Vatican City State)|Vatikan
ye|Yemen|Yaman
za|South Africa|Afrika Selatan
