require 'test_helper'

class AnchorWordTest < ActiveSupport::TestCase

  test "generate array" do
    result = AnchorWord.generate_all_in_array
    assert_equal 2, result.size
  end

  test "generate string" do
    result = AnchorWord.generate_all_in_string
    assert_equal 'nitip bawain', result
  end
end
