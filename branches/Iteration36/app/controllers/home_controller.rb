require 'net/http'

class HomeController < ApplicationController

  before_filter :check_rest_password, :only => [:expire_cloud,
    :create_released_seller_payout_log,
    :create_released_buyer_payout_log,
    :create_received_buyer_bank_transfer_log,
    :create_bistip_approved_insurance_log,
    :create_bistip_rejected_insurance_log,
    :create_bistip_received_buyer_bank_transfer_insurance_log,
    :grant_trusted_badge,
    :send_statistics]

  def index
    count = 9
    if mobile_version?
      count = 2
    end

    @trips = Trip.build_scope_home_index(count).all
  end

  def expire_cloud

    # regenerate trending item
    TrendingItem.generate

    # expire route tag cloud cache
    expire_fragment('all_route')
    redirect_to root_path
  end

  # utility to create bistip payout log.
  def create_released_seller_payout_log
    escrow_id = params[:escrow_id]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_SELLER_REQUESTED_PAYOUT
      BistipReleasedSellerPayoutLog.create!(:escrow_id => escrow_id)
      redirect_to root_path
    end
  end

  # utility to create bistip payout log.
  def create_released_buyer_payout_log
    escrow_id = params[:escrow_id]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_BUYER_REQUESTED_PAYOUT
      BistipReleasedBuyerPayoutLog.create!(:escrow_id => escrow_id)
      redirect_to root_path
    end
  end

  # utility to create bistip received transfer log
  def create_received_buyer_bank_transfer_log
    escrow_id = params[:escrow_id]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_BUYER_INITIATED
      BistipReceivedBuyerBankTransferLog.create!(:escrow_id => escrow_id)
      redirect_to root_path
    end
  end

  # utility to create bistip approved insurance log
  def create_bistip_approved_insurance_log
    insurance_id = params[:insurance_id]
    insurance = Insurance.find_by_id(insurance_id)

    if !insurance_id.blank? and insurance.current_state.eql? Insurance::STATE_BUYER_APPLIED
      BistipApprovedInsuranceLog.create!(:insurance_id => insurance_id)
      redirect_to root_path
    end
  end

  # utility to create bistip rejected insurance log
  def create_bistip_rejected_insurance_log
    insurance_id = params[:insurance_id]
    insurance = Insurance.find_by_id(insurance_id)

    if !insurance_id.blank? and insurance.current_state.eql? Insurance::STATE_BUYER_APPLIED
      BistipRejectedInsuranceLog.create!(:insurance_id => insurance_id)
      redirect_to root_path
    end
  end

  # utility to create bistip received transfer log
  def create_bistip_received_buyer_bank_transfer_insurance_log
    insurance_id = params[:insurance_id]
    insurance = Insurance.find_by_id(insurance_id)

    if !insurance_id.blank? and insurance.current_state.eql? Insurance::STATE_BISTIP_APPROVED
      BistipReceivedBuyerBankTransferInsuranceLog.create!(:insurance_id => insurance_id)
      redirect_to root_path
    end
  end

  # give trusted badge to a user
  def grant_trusted_badge
    user_id = params[:user_id]
    user = User.find_by_id(user_id)

    if !user.blank? and !user.has_trusted_badge?
      TrustedBadge.create!(:user_id => user_id)
      Notifier.delay.email_trusted_badge(user)
      redirect_to root_path
    end
  end

  # send statistics to our email
  def send_statistics

    # get facebook likes
    url = URI.parse('http://graph.facebook.com/194274033926856')
    req = Net::HTTP::Get.new(url.path)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    fb_json = JSON.parse(res.body)

    # get twitter followers
    url = URI.parse('http://api.twitter.com/1/users/show.json')
    req = Net::HTTP::Get.new(url.path + '?screen_name=bistip&include_entities=true')
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    tw_json = JSON.parse(res.body)

    #ContactUsNotifier.delay.email_statistics(User.count, Trip.count, Seek.count, Comment.count, Message.count, PositiveReputation.count, NegativeReputation.count, Item.count, fb_json["likes"], tw_json["followers_count"]);

    redirect_to root_path
  end

  def stats
    password = params[:password]

    if password == 'ayoayo_kitabisa'
      # continue to render html page
      render :layout => false
    else
      redirect_to root_path
    end
  end
  
end
