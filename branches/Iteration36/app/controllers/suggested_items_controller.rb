class SuggestedItemsController < ApplicationController
  
  # must be logged in
  before_filter :authenticate, :only => [:new]

  # using password
  before_filter :check_rest_password, :only => [:approve]

  # display a suggested item
  def show
    @suggested_item = SuggestedItem.find(params[:id])
    @suggested_item_pictures = SuggestedItemPicture.find_all_by_suggested_item_id(params[:id])
    @main_picture = @suggested_item_pictures.first
    @results = Trip.build_scope_search(@suggested_item.origin_city, nil, nil, nil, nil, nil).paginate(:per_page => SuggestedItem::SHOW_PER_PAGE, :page => params[:page])
    @total_result = Trip.build_scope_search(@suggested_item.origin_city, nil, nil, nil, nil, nil).count
  end

  # display a suggested item new form
  def new
    @suggested_item = SuggestedItem.new
    SuggestedItem::PICTURES_MAXIMUM.times { @suggested_item.pictures.build }
  end

  # accept form submission to create suggested item
  def create
    # associate suggested item with the current user
    @suggested_item = current_user.suggested_items.new(params[:suggested_item])

    # set each pictures owner to be current user
    @suggested_item.pictures.each do |picture|
      picture.user = current_user
    end
    
    if @suggested_item.save
      flash[:notice] = t('suggested_items.create.message.success')
      ContactUsNotifier.delay.email_suggested_item(@suggested_item)
      redirect_to suggested_item_path(@suggested_item)
    else
      # if there is form error, upload picture will be gone so lets clean the assets and rebuild
      flash.now[:alert] = t('general.reupload_picture') unless @suggested_item.pictures.blank?
      @suggested_item.pictures.clear
      SuggestedItem::PICTURES_MAXIMUM.times { @suggested_item.pictures.build }
      put_model_errors_to_flash(@suggested_item.errors)
      render :action => "new"
    end
  end

  # display a suggested item edit form
  def edit
    @suggested_item = current_user.suggested_items.find(params[:id])
    (SuggestedItem::PICTURES_MAXIMUM - @suggested_item.pictures.size).times { @suggested_item.pictures.build }
  end

  # update suggested item
  def update
    @suggested_item = current_user.suggested_items.find(params[:id])
    
    # set each pictures owner to be current user
    @suggested_item.pictures.each do |picture|
      picture.user = current_user
    end

    if @suggested_item.update_attributes(params[:suggested_item])
      flash[:notice] = t('suggested_items.update.message.success')
      redirect_to suggested_item_path(@suggested_item)
    else
      put_model_errors_to_flash(@suggested_item.errors)
      
      # if there is form error, upload picture will be gone so lets clean the assets and rebuild
      rejected = @suggested_item.pictures.reject {|picture| !picture.new_record?}
      puts rejected
      flash.now[:alert] = t('general.reupload_picture') unless rejected.blank?
      (SuggestedItem::PICTURES_MAXIMUM - @suggested_item.pictures.size).times { @suggested_item.pictures.build }
      
      render :action => "edit"
    end
  end

  # utility to approve suggested item
  def approve
    suggested_item = SuggestedItem.find(params[:id])
    suggested_item.deleted = false
    suggested_item.save
    SuggestedItemApprovalNotification.create!(:sender_id => suggested_item.user.id, :receiver_id => suggested_item.user.id, :data_id => suggested_item.id)
    Notifier.delay.email_suggested_item_approval(suggested_item)
    redirect_to root_path
  end

end
