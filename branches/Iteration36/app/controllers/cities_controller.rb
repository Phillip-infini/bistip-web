class CitiesController < ApplicationController
  ssl_allowed :index, :search

  TOTAL_SUGGESTIONS = 8
  ALL_CITIES_SUGGESTIONS = 3
  CITY_COUNT_TO_ENABLE_TRIP_MATCH = 3

  # returns matching location(city, countries) + trip match
  def search
    term = params[:term]
    exclude_all_cities = params[:exclude_all_cities]
    origin = params[:origin]

    # only process if term is specified and at least 3 characters
    if !term.blank? and term.length > 2

      @result = Array.new
      city_result = Array.new

      # build city match
      @result = build_city_match(city_result, exclude_all_cities, term)

      # append route time of search result if there's a match
      @result = build_trip_match(@result, city_result, origin)

      # return result
      @result
    end
  end

  # returns matching location(city, countries) only
  def index
    term = params[:term]
    exclude_all_cities = params[:exclude_all_cities]

    # only process if term is specified and at least 3 characters
    if !term.blank? and term.length > 2

      @result = Array.new
      city_result = Array.new

      # build city match
      @result = build_city_match(city_result, exclude_all_cities, term)
      
      # return result
      @result
    end
  end

  private
  
  # build single city match
  def build_city_match(city_result, exclude_all_cities, term)
    result = Array.new

    # build scoped Trip by checking parameter
    scope = City.scoped({})
    scope = scope.exclude_anywhere

    # find matching cities and countries by name
    scope = scope.match_cities_and_countries_name term

    if exclude_all_cities then
      scope = scope.exclude_all_cities
    end

    # append <city, country> result to the array
    scope.each do |city|
      city_result << city
      result << city.name + ", " + city.country.name
    end
    result
  end

  # build suggestions of trip match
  def build_trip_match(previous_result, city_result, origin)
    trip_result = Array.new
    # only do this is city match is not empty and city result is not too many
    if city_result.size > 0 and city_result.size <= CITY_COUNT_TO_ENABLE_TRIP_MATCH

      # control how many trip match to take for each city result
      route_to_take = ((TOTAL_SUGGESTIONS * 1.0) / city_result.size).ceil

      # loop through each city result and find available trip
      city_result.each do |city|

        # match the trip on the origin or destination
        if origin then
          routes = Trip.active_only.count(:conditions => "origin_city_id = #{city.id}", :group => ['origin_city_id', 'destination_city_id'], :order => 'count_all DESC')
        else
          routes = Trip.active_only.count(:conditions => "destination_city_id = #{city.id}", :group => ['origin_city_id', 'destination_city_id'], :order => 'count_all DESC')
        end

        # go through each result and build the trip match string
        routes.take(route_to_take).each do |route|
          trip_result << route_to_result(route)
        end
      end

      # append to previous result
      previous_result + trip_result

      # city match is still a lot, perhaps user was typing country name
      # if there's all cities, it means user is typing country name
      # lets collect suggestion for all cities
    elsif city_result.size > CITY_COUNT_TO_ENABLE_TRIP_MATCH
      # collect all cities
      all_cities = collect_all_cities(city_result)
      all_cities.each do |city|
        # match the trip on the origin or destination
        if origin then
          routes = Trip.active_only.count(:conditions => "cities.country_id = #{city.country.id}", :include => [:origin_city, :destination_city], :group => ['origin_city_id', 'destination_city_id'], :order => 'count_id DESC')
        else
          routes = Trip.active_only.count(:conditions => "destination_cities_trips.country_id = #{city.country.id}", :include => [:origin_city, :destination_city], :group => ['origin_city_id', 'destination_city_id'], :order => 'count_id DESC')
        end

        # go through each result and build the trip match string
        routes.take(ALL_CITIES_SUGGESTIONS).each do |route|
          trip_result << route_to_result(route)
        end

      end

      # append to previous result
      previous_result.insert(all_cities.size, *trip_result)
    end
  end

  # convert route to trip result
  def route_to_result(route)
    origin = City.find(route[0][0])
    dest = City.find(route[0][1])
    count = route[1]
    (origin.name + ', ' + origin.country.name + ' >> ' + dest.name + ', ' + dest.country.name + " (#{count})")
  end

  # collect <all cities> from the given list
  def collect_all_cities(city_result)
    all_cities_match = Array.new
    city_result.each do |city|
      all_cities_match << city if city.all_cities?
    end
    all_cities_match
  end
end
