class NegativeReputationsController < ApplicationController

  before_filter :authenticate, :not_self, :only => [:new, :create]
  
  # render new positive reputation form
  # deprecated - just placeholder to handle legacy url
  def new
    @user = User.find(params[:user_id])
    redirect_to new_user_review_path(@user)
  end

  # render positive reputations of a user
  # deprecated - just placeholder to handle legacy url
  def index
    @user = User.find(params[:user_id])
    redirect_to reviews_user_path(@user)
  end

  # create a positive reputation
  # deprecated - just placeholder to handle legacy url
  def create
    @user = User.find(params[:user_id])
    redirect_to new_user_review_path(@user)
  end
end
