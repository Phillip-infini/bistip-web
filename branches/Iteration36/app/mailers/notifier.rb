# Class to send email notification
require 'net/http'
require 'net/https' # You can remove this if you don't need HTTPS
require 'uri'

class Notifier < ActionMailer::Base
  default :from => "Bistip.com <no-reply@bistip.com>"

  # Send validation email to user
  def email_validation(user)
    @user = user
    @email_validation_url = validate_user_url(@user) + "?" + User::VALIDATION_KEY_PARAM + "=" + user.email_validation_key

    mail :to => user.email
    mail :subject => t('notifier.email_validation.subject')
  end

  # Send validation email reminder to user
  def email_validation_reminder(user)
    @user = user
    @email_validation_url = validate_user_url(@user) + "?" + User::VALIDATION_KEY_PARAM + "=" + user.email_validation_key

    mail :to => user.email
    mail :subject => t('notifier.email_validation_reminder.subject')
  end

  # Send reset password link to user
  def email_password(user)
    @user = user
    @reset_password_url = reset_edit_password_url + "?" + User::EMAIL_KEY_PARAM + "=" + user.email + "&" + User::RESET_PASSWORD_KEY_PARAM + "=" + user.reset_password_key

    mail :to => user.email
    mail :subject => t('notifier.email_password.subject')
  end

  # Send trip comment to the Trip owner
  def email_trip_comment(trip_id, commentator, comment)
    trip = Trip.find(trip_id)
    @trip = trip
    @commentator = commentator
    @user = trip.user
    @content = comment.body
    @created = comment.created_at
    
    @trips_show_url = trip_url(trip, :page => comment.find_page, :anchor => comment.internal_id)
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{comment.email_reply_address}>", :to => @user.email, :subject => t('notifier.comment.subject.trip', :user => commentator.username)
  end

  # Send seek comment to the Seek owner
  def email_seek_comment(seek_id, commentator, comment)
    seek = Seek.find(seek_id)
    @seek = seek
    @commentator = commentator
    @user = seek.user
    @content = comment.body
    @created = comment.created_at

    @seeks_show_url = seek_url(seek, :page => comment.find_page, :anchor => comment.internal_id)
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{comment.email_reply_address}>", :to => @user.email, :subject => t('notifier.comment.subject.seek', :user => commentator.username)
  end

  # send reply comment to previous replied comment owner
  def email_trip_comment_reply(comment, recipient)
    @comment = comment
    @user = recipient
    trip = Trip.find(@comment.trip_id)
    @url = trip_url(trip, :page => comment.find_page)
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{comment.email_reply_address}>", :to => @user.email, :subject => t('notifier.comment.subject.reply', :user => comment.user.username)
  end

  # send reply comment to previous replied comment owner
  def email_seek_comment_reply(comment, recipient)
    @comment = comment
    @user = recipient
    seek = Seek.find(@comment.seek_id)
    @url = seek_url(seek, :page => comment.find_page)
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{comment.email_reply_address}>", :to => @user.email, :subject => t('notifier.comment.subject.reply', :user => comment.user.username)
  end

  def email_review_receiver(review)
    @review = review
    @user = review.receiver
    @receiver = review.receiver
    @giver = review.giver
    @content = review.body
    @created = review.created_at
    mail :to => @receiver.email, :subject => t('notifier.review.subject', :user => @giver.username)
  end

  def email_user_new_message(message)
    @message = message
    @user = message.receiver
    @sender = message.sender
    add_message_attachments(message)
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{@message.email_reply_address}>", :to => @user.email, :subject => t('notifier.message.subject.new', :sender => @message.sender.username)
  end

  def email_user_message_not_valid(email_reply_address, sender, receiver, errors)
    @errors = errors
    @sender = sender
    @receiver = receiver
    @user = sender
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{email_reply_address}>", :to => @user.email, :subject => t('notifier.message.subject.not_valid', :receiver => @receiver.username)
  end

  def email_user_comment_not_valid(email_reply_address, sender, receiver, errors)
    @errors = errors
    @sender = sender
    @receiver = receiver
    @user = sender
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{email_reply_address}>", :to => @user.email, :subject => t('notifier.comment.subject.not_valid', :receiver => @receiver.username)
  end

  # add message attachments to email
  def add_message_attachments(message)
    begin
      if message.assets.count > 0
        message.assets.each do |a|
          attachment = a.attachment
          attachments[a.attachment_file_name] = open(attachment.expiring_url(Asset::LINK_AGE)){ |f| f.read }
        end
      end
    rescue => e
      # problem with attaching message?, ignore and log warning
      puts 'Message ID ' + message.id + ': failed to add attachments to email'
    end
  end
  
  def email_matching_seek(trip, seek)
    @user = seek.user
    @seek = seek
    @trip = trip
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{@trip.email_reply_address}>", :to => @user.email, :subject => t('notifier.match.subject.seek')
  end

  def email_matching_trip(seek, trip)
    @user = trip.user
    @seek = seek
    @trip = trip
    mail :from => "Bistip.com <mail@bistip.com>", :reply_to => "Bistip.com <#{@seek.email_reply_address}>", :to => @user.email, :subject => t('notifier.match.subject.trip')
  end

  def email_topic_post(topic, post)
    @topic = topic
    @post = post
    @user = topic.user
    @created = post.created_at
    mail :to => @user.email, :subject => t('notifier.forum.topic_post', :user => @post.user.username)
  end

  def email_quote_post(topic, post, quoted_post)
    @topic = topic
    @post = post
    @quoted_post = quoted_post
    @user = @quoted_post.user
    @created = post.created_at
    mail :to => @user.email, :subject => t('notifier.forum.quote_post', :user => @post.user.username)
  end

  def email_relation(from_user, to_user, back_relation_exist)
    @user = to_user
    @from_user = from_user
    @back_relation_exist = back_relation_exist
    mail :to => @user.email, :subject => t('notifier.relation.new', :user => @from_user.username)
  end
  
  def email_escrow_log(receiver, log)
    @user = receiver
    @log_state = Escrow.get_log_state(log)
    @escrow = log.escrow
    mail :to => @user.email, :subject => t("notifier.escrow.#{@log_state}", :buyer => @escrow.buyer.username, :seller => @escrow.seller.username, :item => @escrow.item, :amount => @escrow.amount_with_currency)
  end

  def email_insurance_log(receiver, log)
    @user = receiver
    @log_state = Insurance.get_log_state(log)
    @insurance = log.insurance
    mail :to => @user.email, :subject => t("notifier.insurance.#{@log_state}", :buyer => @insurance.buyer.username, :seller => @insurance.seller.username)
  end

  def email_trusted_badge(user)
    @user = user
    mail :to => @user.email, :subject => t("notifier.badge.trusted")
  end

  def email_suggested_item_approval(suggested_item)
    @user = suggested_item.user
    @suggested_item = suggested_item
    mail :to => @user.email, :subject => t("notifier.suggested_item.approval", :item => suggested_item.name)
  end

  def email_story_approval(story)
    @user = story.user
    @story = story
    mail :to => @user.email, :subject => t("notifier.story.approval")
  end

end
