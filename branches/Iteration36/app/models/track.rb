class TrackLink

  # for path_helper

  include ActionView::Helpers::UrlHelper

  ###### ref link constants begin

  # main
  DAS_REF = 'das' #dashboard
  TRIP_REF = 'tr' #trip
  SEEK_REF = 'sk' #seek
  SI_REF = 'si' #suggested_item
  SRC_RF = 'src' #search

  # Dashboard

  DAS_TRIP_REF = 'das_tr' #dashboard>trips
  DAS_SEEK_REF = 'das_sk' #dashboard>seeks
  DAS_YS_REF = 'das_ys' #dashboard>your stories
  DAS_SI_REF = 'das_si' #dashboard>suggested items
  DAS_SP_REF = 'das_sp' #dashboard>transactions
  DAS_RV_REF = 'das_rv' #dashboard>reviews

  # Features
  TRIP_NEXT = 'next' #next trip
  TRIP_PREV = 'prev' #previous trip
  SI = 'si' #suggested item
  SI_SEE_ALL = 'si_see_all' # suggested item see all link
  SIMILAR = 'sim' #similar
  MATCH = 'match' #match
  NEW_SI = 'nwsi' #new suggested items
  SRC_SG = 'src_sg' #search suggestion

  ###### ref link constants end

  # to get the path for comparison
  def self.path_helper
    Rails.application.routes.url_helpers
  end

  # track where the link comes from
  def self.generate(from_page, feature, id = 0)
    
    #main
    if from_page == path_helper.dashboard_path
      DAS_REF + '-' + feature
    elsif from_page == path_helper.trip_path(id)
      TRIP_REF + id.to_s + '-' + feature
    elsif from_page == path_helper.seek_path(id)
      SEEK_REF + id.to_s + '-' + feature
    elsif from_page == path_helper.suggested_item_path(id)
      SI_REF + id.to_s + '-' + feature
    end
  end


end
