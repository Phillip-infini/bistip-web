# handler for MessageReply
class MessageReplyHandler
  include ReceiveEmailChainable

  # set next in chain
  def initialize(link = nil)
    next_in_chain(link)
  end

  # try to handle the request
  def handle(params)
    if email_type(params) == Message::EMAIL_REPLY_PREFIX
      # check if receiver email is valid referring to a message
      receiver_email = params['recipient']
      message = retrieve_message(receiver_email)
      return if message.blank?

      # check if sender really exist
      sender_email = params['sender']
      sender = User.find_by_email(sender_email)
      return if (sender.blank? or !message.is_sender_or_receiver?(sender))

      # build a reply message
      reply = Message.new
      reply.sender = sender
      reply.email_reply = true
      reply.receiver = message.not_this_user(sender)
      reply.reply_to_id = message.thread_starter? ? message.id : message.parent.id
      reply.body = params["stripped-text"]
      reply.body_original = params["body-plain"]

      # process all attachments:
      attachment_count = params['attachment-count'].to_i
      attachment_count.times do |i|
        stream = params["attachment-#{i+1}"]
        # filename = stream.original_filename
        asset = reply.assets.build
        asset.attachment = stream
      end

      if reply.save
        # mark the replied message as read
        message.update_attribute(:read, true)
      else
        # notify the sender about the problem with their email reply
        Notifier.delay.email_user_message_not_valid(message.email_reply_address, reply.sender, reply.receiver, reply.errors.full_messages)
      end
    else
      @next.handle(params)
    end
  end

  private
  
    # check if receiver email is valid, can be link into a conversation
    def retrieve_message(receiver_email)
      # split the receiver email, must result in two parts
      receiver_email_splitted = receiver_email.split('@')
      return nil unless receiver_email_splitted.size == 2

      # parse the head and check if it's in right format and valid salt
      head = receiver_email_splitted.first
      head_splitted = head.split(Message::EMAIL_REPLY_SEPARATOR)
      return nil unless head_splitted.size == 3

      message_id = head_splitted[1]
      message_salt = head_splitted[2]
      Message.find(:first, :conditions => {:id => message_id, :salt => message_salt})
    end
    
end