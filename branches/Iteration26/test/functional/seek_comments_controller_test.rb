require 'test_helper'

class SeekCommentsControllerTest < ActionController::TestCase
  setup do
    @seek = seeks(:jktsyd)
    @comnt = comments(:seek1)
  end
  # Replace this with your real tests.
  test "should create comment on seek" do
    login_as(:one)
    assert_difference('Comment.count') do
      post :create, :seek_id => @seek.id, :comment => {
        :body=>"seek comment test"
      }
    end
    assert_redirected_to seek_path(@seek, :page => @seek.last_comment_page)
  end

  test "should destroy comment on seek" do
    login_as(:dono)
    assert_difference('SeekComment.count', -1) do
      delete :destroy, :seek_id => @seek.id, :id =>@comnt.id
    end
    assert_redirected_to seek_path(@seek)
  end
end
