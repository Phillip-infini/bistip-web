# a class to represent a payment method
class PaymentMethod < ActiveRecord::Base

  #include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper

  # get cname of payment method
  def cname
    raise NotImplementedError
  end

  # helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end

  # static method to calculate amount after cost according to payment method
  def calculate_amount_after_cost(amount)
    raise NotImplementedError
  end

  # static method to calculate amount after cost according to payment method
  def amount_allowable?(amount)
    raise NotImplementedError
  end

  # static to decide what path to return according to payment method
  def get_redirect_path(escrow)
    raise NotImplementedError
  end

  # static to decide what currency to return according to payment method
  def get_currency
    raise NotImplementedError
  end
end
