# a class to represent a payment method
class IndonesiaBankTransfer < PaymentMethod
  INDONESIA_BANK_TRANSFER_CNAME = 'indonesia_bank_transfer'
  INDONESIA_BANK_TRANSFER_FEE_COST = 0.02

  # get cname of payment method
  def cname
    INDONESIA_BANK_TRANSFER_CNAME
  end

    # static method to calculate amount after cost according to payment method
  def calculate_amount_after_cost(amount)
    unless amount.blank?
      amount = amount.to_f
      (amount * (1 + INDONESIA_BANK_TRANSFER_FEE_COST))
    else
      0.00
    end
  end

  # static method to calculate amount after cost according to payment method
  def amount_allowable?(amount)
    (amount >= 250000 and amount <= 8000000)
  end

  # static to decide what path to return according to payment method
  def get_redirect_path(escrow)
    path_helper.redirect_indonesia_bank_transfer_escrows_path(:escrow_id => escrow)
  end

  # static to decide what currency to return according to payment method
  def get_currency
    Currency.find_by_name('IDR')
  end
end
