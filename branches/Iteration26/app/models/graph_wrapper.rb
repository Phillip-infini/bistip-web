require 'fb_graph'

class GraphWrapper < ActiveRecord::Base
  
  def self.get_mutual_friends(uid1, uid2, access_token)
    sql = "SELECT name, pic_square, profile_url FROM user WHERE uid IN (SELECT uid1 FROM friend WHERE uid1 IN (SELECT uid2 FROM friend WHERE uid1=#{uid1}) AND uid2=#{uid2})"
    begin
      array = FbGraph::Query.new(sql).fetch(access_token)
    rescue Exception
      array = Array.new
    end
    return array
  end

  def self.get_app_id
    if Rails.env == 'production'
      return '161019173951344'
    elsif Rails.env == 'staging' or Rails.env == 'ci'
      return '189138291128089'
    elsif Rails.env == 'development'
      return '118162271593723'
    end
  end
end
