class CreateTrendingItems < ActiveRecord::Migration
  def self.up
    create_table :trending_items do |t|
      t.string :item
      t.integer :count
      t.timestamps
    end
  end

  def self.down
    drop_table :trending_items
  end
end
