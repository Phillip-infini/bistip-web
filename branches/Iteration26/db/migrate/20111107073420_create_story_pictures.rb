class CreateStoryPictures < ActiveRecord::Migration
  def self.up
    create_table :story_pictures do |t|
      t.column :story_id, :integer, :null => false, :references => :stories
      t.string :attachment_file_name
      t.string :attachment_content_type
      t.integer :attachment_file_size
      t.datetime :attachment_updated_at
      t.timestamps
    end
  end

  def self.down
    drop_table :story_pictures
  end
end
