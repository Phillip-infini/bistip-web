class BistipReceivedBuyerBankTransferLog < EscrowLog

  # broadcast the creation of this log inform of notification and email
  def broadcast
    EscrowLogNotification.create!(:data_id => self.id, :sender => escrow.buyer, :receiver => escrow.buyer)
    EscrowLogNotification.create!(:data_id => self.id, :sender => escrow.seller, :receiver => escrow.seller)
    Notifier.delay.email_escrow_log(escrow.buyer, self)
    Notifier.delay.email_escrow_log(escrow.seller, self)
  end
end
