# Model consist of constant relate to date predicate
class DatePredicate
  # constants

  BEFORE = 'before'

  AFTER = 'after'
  
  ALL = ['before', 'after']

end
