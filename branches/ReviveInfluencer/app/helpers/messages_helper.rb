module MessagesHelper

  # check if given parameter indicates that a message is related
  def message_related_to_trip_or_seek?(trip_id, seek_id)
    if !trip_id.blank? or !seek_id.blank?
      true
    else
      false
    end
  end

  # generate text that indicates what trip or seek a message relate to
  def generate_label_for_related_trip_or_seek(trip_id, seek_id)
    if message_related_to_trip_or_seek?(trip_id, seek_id)
      if !trip_id.blank?
        trip = Trip.unscoped.find(trip_id)
        render 'message_trip_route', :locals => {:trip => trip}
      elsif !seek_id.blank?
        seek = Seek.unscoped.find(seek_id)
        render 'message_seek_route', :locals => {:seek => seek}
      end
    end
  end

end
