require 'test_helper'

class NegativeReputationStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  test "user :one giving a negative reputation to user :dono" do
    one = users(:one)
    dono = users(:dono)

    #login
    full_login_as(:one)

    #view user detail
    get profile_path(dono.username)
    assert_response :success
    assert_template 'show'

    #give one positive reputation to :dono
    post user_negative_reputations_path(dono),:negative_reputation => {
      :body=>"very good service",
      :giver_id=> one
    }

    assert_response :redirect
    assert_redirected_to reputation_user_path(dono.id)
    follow_redirect!
  end
end
