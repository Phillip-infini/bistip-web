require 'test_helper'

class MessageStoriesTest < ActionDispatch::IntegrationTest
  fixtures :all

  # Replace this with your real tests.
  test "mac send a new message to the dono" do
    dono = users(:dono)
    mac = users(:mactavish)

    #login
    full_login_as(:mactavish)
    get new_user_message_path(dono)
    assert_response :success
    assert_template 'new'

    #post
    post user_messages_path :user_id=>dono, :receiver_id=>dono, :message => {
      :body=>"new message from mac to dono",
      :subject=>"Mac2Dono",
      :sender_id=>mac,
      :receiver_id=>dono
    }

    assert_response :redirect
    assert_redirected_to user_message_path(:user_id => mac, :id => assigns(:message), :anchor => assigns(:message).internal_link_id)
    follow_redirect!
  end

  test "one reply a mac's message" do
    one = users(:one)
    mac = users(:mactavish)
    msg = messages(:onetomac)
    #login
    full_login_as(:one)
    get user_messages_path(mac)
    assert_response :success

    #get message
    get user_message_path :user_id=>one.id, :id=>msg.id
    assert_response :success
    assert_template 'show'
    assert_difference('Message.count') do
      post user_messages_path :user_id=>mac, :receiver_id=>mac, :message => {
        :body => "reply from dono to mac",
        :subject => "RE:Mac2One",
        :reply_to_id => msg,
        :receiver_id => mac
      }
      assert_response :redirect
      assert_redirected_to user_message_path(:user_id => one, :id => assigns(:message), :anchor => assigns(:message).internal_link_id)
    end

  end


end
