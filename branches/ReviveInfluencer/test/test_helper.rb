ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def login_as(user)
    user_to_use = users(user)
    cookies = ActionDispatch::Cookies::CookieJar.build(@request)
    cookies.signed[ApplicationController::AUTH_COOKIE] = {:value => [user_to_use.id, user_to_use.salt]}
    @request.cookies.merge!(cookies)
  end

  # helper method to login
  def full_login_as(user)
    get login_path

    assert_response :success
    assert_template 'new'

    user_to_use = users(user)
    post session_path, :email_or_username => user_to_use.email, :password => 'secret'

    assert_response :redirect
    assert_redirected_to dashboard_path

    follow_redirect!

    assert_response :success
    assert_template 'index'
    assert_not_nil cookies[ApplicationController::AUTH_COOKIE]
  end

  # helper to create a trip
  def create_trip(origin, destination, departure_date, notes)
    get new_trip_path

    assert_response :success
    assert_template 'new'

    post trips_path, :trip => {:origin_location => origin,
      :destination_location => destination,
      :departure_date => departure_date,
      :notes => notes
      }

    assert_response :redirect
    assert assigns(:trip).valid?
    follow_redirect!
  end

  # helper to create a seek
  def create_seek(origin, destination, departure_date, departure_date_predicate, notes)
    get new_seek_path

    assert_response :success
    assert_template 'new'

    post seeks_path, :seek => {:origin_location => origin,
      :destination_location => destination,
      :notes => notes,
      :departure_date => departure_date,
      :departure_date_predicate => departure_date_predicate, :items_attributes => {:item => {:name => 'ipad 2'}}
      }

    assert_response :redirect
    assert assigns(:seek).valid?
    follow_redirect!
  end

  #helper to encrypt password
  def encrypt(string)
      Digest::SHA1.hexdigest(string)
  end
  
end
