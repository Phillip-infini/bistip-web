require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  # initialize current user
  setup do
    login_as(:mactavish)
  end

  #get /dashboard
  test "should get dashboard index" do
    get :index
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:user)
    assert_not_nil assigns(:trips)
    assert_not_nil assigns(:incoming_trips)
  end

  #get /review
  test "should get current_user's review" do
    get :reviews
    assert_response :success
    assert_template 'reviews'
    assert_not_nil assigns(:user)
    assert_not_nil assigns(:reviews)
  end

  #get /trips
  test "should get current_user's trips" do
    get :trips
    assert_response :success
    assert_template 'trips'
    assert_not_nil assigns(:user)
    assert_not_nil assigns(:trips)
  end

  #get /seeks
  test "should get current_user's seeks" do
    get :seeks
    assert_response :success
    assert_template 'seeks'
    assert_not_nil assigns(:user)
    assert_not_nil assigns(:seeks)
  end

  #get /profile
  test "should get current_user's profile" do
    get :profile
    assert_response :success
    assert_template 'profile'
    assert_not_nil assigns(:user)
  end

  #get /profile
  test "should get current_user's escrows" do
    get :escrows
    assert_response :success
    assert_template 'escrows'
    assert_not_nil assigns(:user)
    assert_not_nil assigns(:escrows)
  end
end
