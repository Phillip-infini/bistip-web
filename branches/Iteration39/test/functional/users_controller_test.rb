require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, :user => {:username => "barry",
        :fullname => 'barry',
        :city_location => 'jakarta',
        :email => "barry@mail.com",
        :hashed_password => "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4"
        }
    end

    assert_redirected_to registered_users_path
  end

  test "should show user" do
    get :show, :id => @user.to_param
    assert_response :success
  end

  test "should get edit" do
    login_as(:one)
    get :edit, :id => @user.to_param
    assert_response :success
  end

  test "should update user" do
    login_as(:one)
    put :update, :id => @user.to_param, :user => {:bio => "one's bio"}
    assert_redirected_to dashboard_profile_path
  end

  test "should validate user's email" do
    # create a new user (not validated yet)
    username_to_use = "validate"
    post :create, :user => {:username => username_to_use,
        :fullname => 'validate',
        :city_location => 'jakarta',
        :email => "validate@mail.com",
        :hashed_password => "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4"
        }
    user_not_validated = User.find_by_username(username_to_use)
    get :validate, :id => user_not_validated.id, :key => user_not_validated.email_validation_key
    
    # refetch the user, it must be validated now
    user_validated = User.find_by_username(username_to_use)
    assert user_validated.email_validated?
    assert_redirected_to root_path
  end

  test 'should render not validated' do
    get :not_validated, :id => users(:one).id
    assert_response :success
    assert_template :not_validated
  end

  test 'should resend validation' do
    login_as(:not_validated)
    get :resend_validation, :id => users(:one).id
    assert_response :redirect
  end

  test 'should registered' do
    login_as(:one)
    get :registered
    assert_response :success
    assert_template :registered
  end

  test 'should list trips' do
    get :trips, :id => users(:one).id
    assert_response :success
    assert_template :trips
    assert_not_nil assigns(:trips)
    assert_not_nil assigns(:user)
  end

  test 'should list seeks' do
    get :seeks, :id => users(:one).id
    assert_response :success
    assert_template :seeks
    assert_not_nil assigns(:seeks)
    assert_not_nil assigns(:user)
  end

  test 'should render auth' do
    get :auth
    assert_response :success
    assert_template :auth
  end

  test 'reputation legacy link' do
    get :reputation, :id => users(:one).id
    assert_response :redirect
  end

  test 'reviews' do
    get :reviews, :id => users(:one).id
    assert_response :success
    assert_template :reviews
    assert_not_nil assigns(:reviews)
    assert_not_nil assigns(:user)
  end

end
