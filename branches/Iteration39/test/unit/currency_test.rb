require 'test_helper'

class CurrencyTest < ActiveSupport::TestCase
  
  test "should find currency" do
    assert Currency.find_by_name(currencies(:usd).name)
  end
end
