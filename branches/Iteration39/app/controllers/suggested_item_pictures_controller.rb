class SuggestedItemPicturesController < ApplicationController
  def show
     @suggested_item_picture = SuggestedItemPicture.find(params[:suggested_item_picture_id])
  end
end
