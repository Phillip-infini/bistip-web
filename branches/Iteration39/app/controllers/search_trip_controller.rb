class SearchTripController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    @origin = check_location(params[:origin])

    # input from search from can be: 'Jakarta, Indonesia (2)' due to inconsistencies of browser
    # lets take this extranous string out
    destination_param = params[:destination].gsub(/\(\d+\)/,'') if params[:destination]
    @destination = check_location(destination_param)
    
    sort = params[:sort]
    departure_date = parse_date_parameter(params[:departure_date])
    departure_date_predicate = params[:departure_date_predicate]
    keyword = params[:keyword]
    notice = Array.new

    # check if a given location is a valid city
    if @origin.blank? and params[:origin]
      matched = City.build_match(false, false, params[:origin])
      if matched.count == 1
        @origin = matched[0]
      else
        notice << t('errors.location_not_valid', :value => params[:origin])
        if matched.count > 1
          @origin_suggestion = matched.take(3)
        end
      end
    end

    if @destination.blank? and destination_param
      matched = City.build_match(false, false, destination_param)
      if matched.count == 1
        @destination = matched[0]
      else
        notice << t('errors.location_not_valid', :value => params[:destination])
        if matched.count > 1
          @destination_suggestion = matched.take(3)
        end
      end
    end

    # build scoped Trip by passing parameter to method build_scope_search
    scope = Trip.build_scope_search(@origin, @destination, departure_date, departure_date_predicate, keyword, sort)
    @no_valid_parameter = (@origin.blank? and @destination.blank? and departure_date.blank? and departure_date_predicate.blank? and keyword.blank?)
    @total_result = scope.size
    @trips = scope.paginate(:page => params[:page], :per_page => Trip::SEARCH_PER_PAGE)

    # build suggestion list for the right panel
    if !@origin.blank? and !@origin.all_cities? and !@destination.blank?
      @suggested_routes = Trip.get_suggestions_routes(@origin, @destination)
    end

    flash.now[:alert] = notice unless notice.empty?
  end
end
