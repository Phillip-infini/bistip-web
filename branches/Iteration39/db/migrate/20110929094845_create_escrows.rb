class CreateEscrows < ActiveRecord::Migration
  def self.up
    create_table :escrows do |t|
      t.column :buyer_id, :integer, :null => false, :references => :users
      t.column :seller_id, :integer, :null => false, :references => :users
      t.column :message_id, :integer, :null => false, :references => :messages
      t.column :amount, :decimal, :precision => 8, :scale => 2
      t.string :invoice
      t.belongs_to :currency
      t.string :item

      t.timestamps
    end
  end

  def self.down
    drop_table :escrows
  end
end
