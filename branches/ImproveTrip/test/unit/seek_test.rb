require 'test_helper'

class SeekTest < ActiveSupport::TestCase

  test "should create seek" do
    seek = Seek.new
    seek.origin_location = "jakarta"
    seek.destination_location = "sydney"
    seek.departure_date = DateTime.current.advance(:days => 1)
    seek.departure_date_predicate = 'after'
    seek.user = users(:one)
    seek.notes = 'test'
    assert seek.save
  end

  test "should find seek" do
    seek_id = seeks(:jktsyd).id
    assert_nothing_raised {Seek.find(seek_id)}
  end

  test "should update seek" do
    seek = seeks(:jktsyd)
    assert seek.update_attributes(:notes => 'notes')
  end

  test "should delete seek" do
    sedel = seeks(:banjak)
    seeks = Seek.find(sedel.id) #comment.find(comments(:cmt))
    assert seeks.destroy
  end
end
