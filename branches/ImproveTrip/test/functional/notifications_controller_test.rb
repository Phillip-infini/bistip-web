require 'test_helper'

class NotificationsControllerTest < ActionController::TestCase

  test "should get index of notifition" do
    login_as(:mactavish)
    get :index, :user_id => users(:mactavish).id
    assert_response :success
    assert_not_nil assigns(:notifications)
  end
  
end
