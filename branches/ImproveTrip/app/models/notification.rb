class Notification < ActiveRecord::Base
  #include this module to provide calling link_to method inside model
  include ActionView::Helpers::UrlHelper
  
  belongs_to :receiver, :class_name => "User", :foreign_key => 'receiver_id'
  belongs_to :sender, :class_name => "User", :foreign_key => 'sender_id'
  
  # scope
  default_scope :order=>"created_at desc"

  #this method should be call from instance of Notification subclass or will be raise NotImplemented error
  def to_link
    raise NotImplementedError
  end

  #helper method to enable routing path calling inside model that subclass of this class
  def path_helper
    Rails.application.routes.url_helpers
  end
end