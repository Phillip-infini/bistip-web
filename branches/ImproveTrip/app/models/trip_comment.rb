# sub class of comment that represent Comment for Trip
class TripComment < Comment

  ##this method call in comment model callback, to create live notification to other trip commentator
  def notify_other_trip_comment
    trip = Trip.unscoped.find(self.trip_id)
    comments = trip.comments
    already_sent = []
    comments.each do |comment|
      # only send comment notification if the previous comment owner
      # is not the owner of the trip and not the owner of the new comment
      commentator = comment.user
      if commentator.user_configuration.email_other_trip_comment and
          !already_sent.include?(commentator) and
          !trip.owner?(commentator) and
          !self.owner?(commentator)
        Notifier.email_other_trip_comment(self, commentator, trip).deliver
        already_sent << commentator
      end
    end
  end
  handle_asynchronously :notify_other_trip_comment

  #this method call in comment model callback, to create live notification to other trip commentator
  def live_notification_trip_comment
    trip = Trip.unscoped.find(self.trip_id)
    comments = trip.comments
    already_sent = []
    comments.each do |comment|
      # only send comment notification if the previous comment owner
      # is not the owner of the trip and not the owner of the new comment
      commentator = comment.user
      if !already_sent.include?(commentator) and
          !trip.owner?(commentator) and
          !self.owner?(commentator)
        TripCommentNotification.create!(:receiver=>comment.user, :sender=>self.user, :data_id=>trip.id)
        already_sent << commentator
      end
    end
  end
end
