# City class represent a City
class City < ActiveRecord::Base

  # constants
  ALL_CITIES = "<all cities>"

  # scope
  default_scope :order => "cities.name ASC"
  scope :match_cities_and_countries_name, lambda { |name| {:include => :country, :conditions => ['cities.name LIKE ? OR countries.name LIKE ?', "%#{name}%", "%#{name}%"]}}
  scope :exclude_all_cities, :conditions => ["cities.name <> ?", "#{ALL_CITIES}"]

  # relationships
  belongs_to :country

  # helper to check whether a city represent an all cities of a country
  def all_cities?
    self.name == ALL_CITIES
  end
end
