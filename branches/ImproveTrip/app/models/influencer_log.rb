class InfluencerLog < ActiveRecord::Base

  MINIMUM_CLICK = 17
  CANDIDATE_COUNT = 30
  PAGE = 10
  #validation
  validates :ip_address, :uniqueness => {:scope => [:trip_id, :influencer_id]}

  def self.top_influencer
    array1 = InfluencerLog.count(:group => "influencer_id").sort {|x,y| x[1]<=>y[1]}
    count = 1
    temp = Array.new
    for data in array1.reverse do
      temp << [count, data[0], data[1]] #no, influencer_id, point
      count+=1
    end
    return temp
  end

  def self.influencer_point(user)
    data = InfluencerLog.count(:group=>'influencer_id', :conditions=>"influencer_id=#{user.id}")
    return data[user.id]
  end

  def self.influencer_list(user)
    data = InfluencerLog.count(:group=>'trip_id', :conditions=>"influencer_id=#{user.id}").sort {|x,y| x[1]<=>y[1]}
    count = 1
    temp = Array.new
    for item in data.reverse do
      temp << [count, item[0], item[1]] #no, trip_id, total_hit
      count += 1
    end
    return temp
  end

end
