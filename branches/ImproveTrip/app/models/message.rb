class Message < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 1000
  SUBJECT_MAXIMUM_LENGTH = 100

  # validation
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH },
            :presence => true
          
  validates :subject, :length => {:minimum => 5, :maximum => SUBJECT_MAXIMUM_LENGTH }
  
  # relationships
  belongs_to :receiver, :class_name => "User", :foreign_key => 'receiver_id'
  belongs_to :sender, :class_name => "User", :foreign_key => 'sender_id'
  belongs_to :thread_starter, :class_name => "Message", :foreign_key => 'reply_to_id'

  # scope
  scope :body_contains, lambda { |keyword| {:conditions => ["match(body) against(?)", keyword]}}
  scope :order_by_created_at_descending, order("created_at desc")
  scope :ninety_days_ago, lambda {{ :conditions => ["created_at > ?", 90.days.ago]}}

  def subject
    if self[:subject].nil? or self[:subject].empty?
      I18n.t('message.label.no_subject')
    else
      self[:subject]
    end
  end

  def subject_display
    if self.thread_starter?
      self.subject
    else
      I18n.t('message.label.reply') + self.thread_starter.subject
    end
  end

  def thread_starter?
    self.reply_to_id.nil?
  end
end
