class AddTimesRemindedToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :times_reminded, :integer, :default => 0
  end

  def self.down
    remove_column :users, :times_reminded
  end
end
