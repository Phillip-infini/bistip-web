class AddInvitationToPositiveReputations < ActiveRecord::Migration
  def self.up
    add_column :positive_reputations, :invitation, :boolean, :default => false
  end

  def self.down
    remove_column :positive_reputations, :invitation
  end
end
