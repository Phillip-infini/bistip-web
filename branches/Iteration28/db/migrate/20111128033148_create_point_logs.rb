class CreatePointLogs < ActiveRecord::Migration
  def self.up
    create_table :point_logs do |t|
      t.belongs_to :user
      t.string :type
      t.integer :amount
      t.integer :data_id
      t.timestamps
    end
  end

  def self.down
    drop_table :point_logs
  end
end
