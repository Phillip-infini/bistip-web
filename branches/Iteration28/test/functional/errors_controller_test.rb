require 'test_helper'

class ErrorsControllerTest < ActionController::TestCase
  # Replace this with your real tests.
  test "should_raise_404_error" do
    login_as(:dono)
    assert_raise(ActionController::RoutingError){
      get users_path
      assert_response :render_error
    }
    
  end
end
