require 'test_helper'

class UserConfigurationTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "should_update_user_configuration" do
    config = user_configurations(:mac_config)
    assert config.update_attributes(
      :email_receive_message=>true,
      :email_receive_trip_comment=>false,
      :email_receive_seek_comment=>false,
      :email_receive_review=>false
    )
  end

  test "should_update_user_notvalid_configuration" do
    config = user_configurations(:not_validated_config)
    assert config.update_attributes(
      :email_receive_message=>true,
      :email_receive_trip_comment=>false,
      :email_receive_seek_comment=>false,
      :email_receive_review=>false
    )
  end
  
end
