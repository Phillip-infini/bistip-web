require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "should create notification on trip" do
    trip_notification = notifications(:TripCommentNotification)
    assert TripCommentNotification.create!(:receiver => trip_notification.receiver,
      :sender => trip_notification.sender,
      :data_id => trip_notification.data_id,
      :type => trip_notification.type)
  end

  test "should create notification on seek" do
    seek_notification = notifications(:SeekCommentNotification)
    assert SeekCommentNotification.create!(:receiver => seek_notification.receiver,
      :sender => seek_notification.sender,
      :data_id => seek_notification.data_id,
      :type => seek_notification.type)
  end

end
