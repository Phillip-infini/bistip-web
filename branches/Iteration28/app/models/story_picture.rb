class StoryPicture < ActiveRecord::Base
  belongs_to :story
  
  has_attached_file :attachment, :styles => lambda{ |a| ['image/gif', 'image/png', 'image/jpg', 'image/jpeg'].include?( a.content_type ) ? { :original => "600x600>", :thumb => "48x48>" } : {}},
                             :storage => :s3,
                             :s3_credentials => "#{Rails.root}/config/s3.yml",
                             :path => "/story_pictures/:id/:style/:filename"

  validates_attachment_content_type :attachment, :content_type => ['image/gif', 'image/png', 'image/jpg', 'image/jpeg'],
                                             :message => I18n.t('general.must_be_image')

  validates_attachment_size :attachment, :less_than => 2.megabytes
end
