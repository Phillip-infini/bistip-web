module TrendingItemLookupHelper

  # render trips that contains the item
  def render_trending_item_trips
    render 'trips' unless @trips.empty?
  end

  # render seeks that contains the item
  def render_trending_item_seeks
    render 'seeks' unless @seeks.empty?
  end

  # get unique tips of a item from trips collection
  def get_unique_same_route_item_tips(trending_item, trips)
    items = Array.new
    tips = Array.new

    trips.each do |trip|
      items = items + trip.items.name_contains(trending_item)
    end

    items.each do |item|
      unless item.tip_with_unit.blank?
        tips << item.tip_with_unit
      end
    end
    tips.uniq
  end

  # get unique tips of a item from seeks collection
  def get_unique_same_route_item_seeks(trending_item, seeks)
    items = Array.new
    tips = Array.new

    seeks.each do |seek|
      items = items + seek.items.name_contains(trending_item)
    end

    items.each do |item|
      unless item.tip_with_unit.blank?
        tips << item.tip_with_unit
      end
    end
    tips.uniq
  end

  # helper to check whether a trip route already exist in a collection
  def trip_route_exists(trip, trips)
    unless trips.blank?
      trips.each do |t|
        if trip.same_route?(t)
          return true
        end
      end
    end

    # no same route found or trips is empty? return false
    false
  end

  # helper to check whether a seek route already exist in a collection
  def seek_route_exists(seek, seeks)
    unless seeks.blank?
      seeks.each do |s|
        if seek.same_route?(s)
          return true
        end
      end
    end

    # no same route found or seeks is empty? return false
    false
  end

  # helper to collect all trips with the same route from a collection
  def get_same_route_trips(trip, trips)
    same_route_trips = Array.new
    same_route_trips << trip
    trips.each do |t|
      if trip.same_route?(t)
        same_route_trips << t
      end
    end
    same_route_trips
  end

  # helper to collect all seeks with the same route from a collection
  def get_same_route_seeks(seek, seeks)
    same_route_seeks = Array.new
    same_route_seeks << seek
    seeks.each do |s|
      if seek.same_route?(s)
        same_route_seeks << s
      end
    end
    same_route_seeks
  end

  # render by grouping trip route and item tips
  def render_by_trip_and_tip_group
    result = ''
    unless @trips.blank?
      trip_already_processed = Array.new
      @trips.each_with_index do |trip, index|
        unless trip_route_exists(trip, trip_already_processed)
          same_route_trips = get_same_route_trips(trip, @trips)
          tips = get_unique_same_route_item_tips(@trending_item.item, same_route_trips)
          trip_already_processed << trip
          result << (render 'trip', :locals => {:trip => trip, :tips => tips, :index => index})
        end
      end
    end
    raw result
  end

  # render by grouping seek route and item tips
  def render_by_seek_and_tip_group
    result = ''
    unless @seeks.blank?
      seek_already_processed = Array.new
      @seeks.each_with_index do |seek, index|
        unless seek_route_exists(seek, seek_already_processed)
          same_route_seeks = get_same_route_seeks(seek, @seeks)
          tips = get_unique_same_route_item_tips(@trending_item.item, same_route_seeks)
          seek_already_processed << seek
          result << (render 'seek', :locals => {:seek => seek, :tips => tips, :index => index})
        end
      end
    end
    raw result
  end
end
