class SellerRequestedPayoutLog < EscrowLog

  def broadcast
    super
    ContactUsNotifier.delay.email_escrow_log(escrow)
  end
end

