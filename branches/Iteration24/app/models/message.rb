class Message < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 1000
  CARRIAGE_RETURN_BUFFER = 100
  SUBJECT_MAXIMUM_LENGTH = 100
  DEFAULT_PER_PAGE = 5
  REPLY_INTERNAL_ID = 'message_reply'
  KEYWORD = '<keyword>'

  # validation
  validates :body, :length => { :minimum => 5, :maximum => (BODY_MAXIMUM_LENGTH + CARRIAGE_RETURN_BUFFER) },
            :presence => true
          
  validates :subject, :length => {:minimum => 5, :maximum => SUBJECT_MAXIMUM_LENGTH }
  
  # relationships
  belongs_to :seek
  belongs_to :trip
  belongs_to :receiver, :class_name => "User", :foreign_key => 'receiver_id'
  belongs_to :sender, :class_name => "User", :foreign_key => 'sender_id'
  belongs_to :thread_starter, :class_name => "Message", :foreign_key => 'reply_to_id'
  has_one :escrow, :class_name => 'Escrow', :foreign_key => 'message_id'
  has_one :insurance, :class_name => 'Insurance', :foreign_key => 'message_id'
  has_many :assets
  accepts_nested_attributes_for :assets, :reject_if => lambda { |a| a[:attachment].blank? }

  # scope
  scope :body_contains, lambda { |keyword| {:conditions => ["match(body) against(?)", keyword]}}
  scope :body_or_subject_contains, lambda {|keyword| {:conditions => ["match(body) against(?) OR subject like ?", keyword, "%#{keyword}%"]}}
  scope :order_by_created_at_descending, order("created_at desc")
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ?", 60.days.ago]}}
  scope :replied, lambda{ |sender_id, reply_to_id, created_at| {:conditions => ["sender_id = ? AND reply_to_id = ? AND created_at > ?", sender_id, reply_to_id, created_at]}}
  scope :sender_list, lambda{ |receiver_id| { :conditions => ["receiver_id = ?", receiver_id], :select => ['sender_id']}}
  scope :sender, lambda{ |sender_id| {:conditions => ["sender_id = ?", sender_id]}}
  scope :receiver, lambda{ |receiver_id| {:conditions => ["receiver_id = ?", receiver_id]}}
  scope :unread, :conditions => "`read` is false"

  def subject
    if self[:subject].nil? or self[:subject].empty?
      I18n.t('message.label.no_subject')
    else
      self[:subject]
    end
  end

  def subject_display
    if self.thread_starter?
      self.subject
    else
      I18n.t('message.label.reply') + self.thread_starter.subject
    end
  end

  def thread_starter?
    self.reply_to_id.nil?
  end

  # check if given user is sender or receiver
  def is_sender_or_receiver?(user)
    self.sender == user or self.receiver == user
  end

  # check if the message has an attachment/asset
  def has_asset?
    unless self.assets.blank?
      self.assets.count > 0
    else
      false
    end
  end

  # check if a message belongs to an escrow
  def has_escrow?
    if thread_starter?
      if self.escrow.blank?
        false
      else
        true
      end
    else
      thread_starter = self.thread_starter
      if thread_starter.escrow.blank?
        false
      else
        true
      end
    end
  end

  # check if a message is replied my current user
  def replied?
    reply_to_id = self.thread_starter? ? self.id : self.reply_to_id
    Message.replied(self.receiver_id, reply_to_id, self.created_at).count > 0
  end

  # return participant of the message which are not the given user
  def not_this_user(user)
    user == self.sender ? self.receiver : self.sender
  end

  # get id for internal link
  def internal_link_id
    "msg_" + self.id.to_s
  end

  # find list of users who has send at least a message to given user
  def self.all_sender_list(user)
    senders_id = Message.sender_list(user.id)
    User.find(senders_id.collect{ |s| s.sender_id }, :order => :username)
  end

  # build message search scope
  def self.build_scope_search(current_user, sender, status, keyword)
    
    # no current user, this search scope cannot be performed
    if current_user.blank?
      return nil
    end

    scope = Message.scoped({})
    scope = scope.receiver(current_user.id)
    scope = scope.order_by_created_at_descending

    unless sender.blank?
      scope = scope.sender(sender)
    end

    unless status.blank?
      scope = MessageStatus.build(status, scope)
    end

    unless keyword.blank?
      scope = scope.body_or_subject_contains(keyword)
    end

    return scope
  end

end
