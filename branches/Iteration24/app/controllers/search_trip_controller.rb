class SearchTripController < ApplicationController
  include CheckLocation

  # show render search form and the result of search
  def index
    # set all parameter to be variable for easy access
    origin = check_location(params[:origin])
    destination = check_location(params[:destination])
    sort = params[:sort]
    departure_date = parse_date_parameter(params[:departure_date])
    departure_date_predicate = params[:departure_date_predicate]
    keyword = params[:keyword]
    notice = Array.new

    # check if a given location is a valid city
    if origin.blank? and params[:origin]
      notice << t('errors.location_not_valid', :value => params[:origin])
    end

    if destination.blank? and params[:destination]
      notice << t('errors.location_not_valid', :value => params[:destination])
    end
    
    # build scoped Trip by passing parameter to method build_scope_search
    scope = Trip.build_scope_search(origin, destination, departure_date, departure_date_predicate, keyword, sort)
    @no_valid_parameter = (origin.blank? and destination.blank? and departure_date.blank? and departure_date_predicate.blank? and keyword.blank?)
    @total_result = scope.size
    @trips = scope.paginate(:page => params[:page], :per_page => Trip::SEARCH_PER_PAGE)

    flash.now[:alert] = notice unless notice.empty?
  end
end
