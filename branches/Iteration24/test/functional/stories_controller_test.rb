require 'test_helper'

class StoriesControllerTest < ActionController::TestCase

  setup do
    @story = stories(:storyone)
    request.env["HTTP_REFERER"] = stories_path
  end

  test "should get new" do
    login_as(:one)
    get :new
    assert_response :success
  end

  test "should create story" do
    login_as(:one)
    post :create, :story => { :title => 'title more than 10 chars', :body => 'The Body content must be more than 30 characters', :deleted => true }
    assert_redirected_to stories_path
  end

  test "should get edit" do
    login_as(:one)
    get :edit, :id => @story.to_param
    assert_response :success
  end

  test "should update story" do
    login_as(:one)
    put :update, :id => @story.to_param, :story => { :title => 'New title more than 10 chars' }
    assert_redirected_to stories_path
  end

  
end