class AddSeeksNotesIndexing < ActiveRecord::Migration
  def self.up
    execute('ALTER TABLE seeks ENGINE = MyISAM')
    execute('CREATE FULLTEXT INDEX fulltext_seeks_notes ON seeks (notes(500))')
  end

  def self.down
    execute('DROP INDEX fulltext_seeks_notes ON seeks')
    execute('ALTER TABLE seeks ENGINE = innodb')
  end
end
