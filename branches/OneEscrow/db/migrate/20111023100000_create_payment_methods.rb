class CreatePaymentMethods < ActiveRecord::Migration
  def self.up
    create_table :payment_methods do |t|
      t.string :type

      t.timestamps
    end
    Paypal.create!
    IndonesiaBankTransfer.create!
    
    # all previous escrow is paypal payment method
    Escrow.all.each do |e|
      e.payment_method = PaymentMethod.find_by_name('Paypal')
      e.save!
    end

    # create additional currency
    Currency.create!(:name => 'IDR')
  end

  def self.down
    drop_table :payment_methods
  end
end
