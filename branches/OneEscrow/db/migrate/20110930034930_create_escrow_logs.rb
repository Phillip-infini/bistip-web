class CreateEscrowLogs < ActiveRecord::Migration
  def self.up
    create_table :escrow_logs, :option => 'ENGINE = MyISAM' do |t|
      t.column :escrow_id, :integer, :null => false, :references => :escrow
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :escrow_logs
  end
end
