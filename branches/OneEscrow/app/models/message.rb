class Message < ActiveRecord::Base

  # constants
  BODY_MAXIMUM_LENGTH = 1000
  SUBJECT_MAXIMUM_LENGTH = 100
  DEFAULT_PER_PAGE = 5

  # validation
  validates :body, :length => { :minimum => 5, :maximum => BODY_MAXIMUM_LENGTH },
            :presence => true
          
  validates :subject, :length => {:minimum => 5, :maximum => SUBJECT_MAXIMUM_LENGTH }
  
  # relationships
  belongs_to :seek
  belongs_to :trip
  belongs_to :receiver, :class_name => "User", :foreign_key => 'receiver_id'
  belongs_to :sender, :class_name => "User", :foreign_key => 'sender_id'
  belongs_to :thread_starter, :class_name => "Message", :foreign_key => 'reply_to_id'
  has_one :escrow, :class_name => 'Escrow', :foreign_key => 'message_id'
  has_many :assets
  accepts_nested_attributes_for :assets, :reject_if => lambda { |a| a[:attachment].blank? }

  # scope
  scope :body_contains, lambda { |keyword| {:conditions => ["match(body) against(?)", keyword]}}
  scope :order_by_created_at_descending, order("created_at desc")
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ?", 60.days.ago]}}

  def subject
    if self[:subject].nil? or self[:subject].empty?
      I18n.t('message.label.no_subject')
    else
      self[:subject]
    end
  end

  def subject_display
    if self.thread_starter?
      self.subject
    else
      I18n.t('message.label.reply') + self.thread_starter.subject
    end
  end

  def thread_starter?
    self.reply_to_id.nil?
  end

  # check if given user is sender or receiver
  def is_sender_or_receiver?(user)
    self.sender == user or self.receiver == user
  end

  # check if the message has an attachment/asset
  def has_asset?
    unless self.assets.blank?
      self.assets.count > 0
    else
      false
    end
  end

  # check if a message belongs to an escrow
  def has_escrow?
    if thread_starter?
      if self.escrow.blank?
        false
      else
        true
      end
    else
      thread_starter = self.thread_starter
      if thread_starter.escrow.blank?
        false
      else
        true
      end
    end
  end

end
