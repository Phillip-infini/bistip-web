# clean sessions that has not been active in the last 7 days
namespace :utility  do
  desc "This task will delete sessions that have not been updated in 7 days."
  task :clean_sessions => :environment do
    ActiveRecord::Base.connection.execute("DELETE FROM sessions WHERE updated_at < DATE_SUB(CURDATE(), INTERVAL 7 DAY);")
  end
end