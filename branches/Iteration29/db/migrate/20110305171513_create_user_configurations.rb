class CreateUserConfigurations < ActiveRecord::Migration
  def self.up
    create_table :user_configurations do |t|
      t.column :email_receive_message, :boolean, :default => true
      t.column :email_receive_trip_comment, :boolean, :default => true
      t.column :email_receive_seek_comment, :boolean, :default => true
      t.column :email_receive_reputation, :boolean, :default => true
      t.belongs_to :user
      t.timestamps
    end
  end

  def self.down
    drop_table :user_configurations
  end
end
