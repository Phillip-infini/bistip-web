class ConvertLocationToCityForUsers < ActiveRecord::Migration
  include CheckLocation

  def self.up
    count = 0
    count_location = 0
    User.all.each do |user|
      location = user.location
      unless location.blank?
        #puts 'user: ' + user.username
        city = self.check_location(location)
        count_location = count_location + 1
        if !city.nil?
          #puts 'user: ' + user.username + ' location: ' + location + ' city: ' + city.name
          user.city = city
          user.location = nil
          user.save(false)
          count = count + 1
        else
          puts 'could not find city: ' + location
        end
      end
    end
    puts 'total converted: ' + count.to_s + ' out of ' + count_location.to_s
  end

  def self.down
    User.all.each do |user|
      city_location = user.city_location
      unless city_location.blank?
        puts 'user: ' + user.username + ' location: ' + user.city_location
        user.location = city_location
        user.city = nil
        user.save(false)
      end
    end
  end

  def self.check_location(location)
    # return if nothing given
    if location.nil? or location.empty?
      return
    end

    # remove all whitespace
    location.strip!

    # check if the string value is in <city>, <country> format
    if location =~ /^.+,.+$/
      splitted = location.split(',')
      city = splitted.shift
      city.strip!

      country = splitted.join(',')
      country.strip!

      # do a look up for the city
      city_match_results = City.find_all_by_name(city)
      if city_match_results.size > 0
        city_match_results.each do |city_match_result|
          if city_match_result.country.name.casecmp(country) == 0
            return city_match_result
          end
        end
        return nil
      else
        return nil
      end

    # just <city> input
    else
      city = location

      # do a look up for the city
      city_match_results = City.find_all_by_name(city)

      # must only match one city, if not then it's ambiguous without country
      if city_match_results.size == 1
        return city_match_results[0]
      else
        return nil
      end
    end
  end
end
