class PopulateMessagesSubject < ActiveRecord::Migration
  def self.up
    Message.find(:all, :conditions => 'reply_to_id IS NOT NULL').each do |message|
      message.subject = message.thread_starter.subject
      message.save
    end
  end

  def self.down
    Message.find(:all, :conditions => 'reply_to_id IS NOT NULL').each do |message|
      message.subject = nil
      message.save
    end
  end
end
