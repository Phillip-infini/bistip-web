class TrendingItemLookupController < ApplicationController

  def show
    trending_item_to_lookup = params[:item]
    @trending_item = TrendingItem.find_by_item(trending_item_to_lookup)

    if (@trending_item.blank?)
      redirect_to root_path
    else
      @trips = Trip.notes_and_items_contains(@trending_item.item)
      @seeks = Seek.notes_and_items_contains(@trending_item.item)
      @comments = Comment.body_contains(@trending_item.item)
      @items = Item.name_contains(trending_item_to_lookup)
      @all_private_information = @trips.blank? && @seeks.blank? && @comments.blank? && @items.blank?
    end
  end
end
