# controller to handle reviews related option
class ReviewsController < ApplicationController
  before_filter :authenticate, :not_self, :only => [:new, :create]

  # render new review
  def new
    @user = User.find(params[:user_id])
    @review = review_type.new
    @review.invitation = params[:invitation] ? true : false
    @review.message_id = params[:message_id]

    # see if its allowed
    if validate
      if params[:invitation]
        flash.now[:longer_notice] = t('review.new.invitation', :receiver => @user.username)
      end
    else
      redirect_to profile_path(@user.username)
    end
  end

  # create a review
  def create
    @user = User.find(params[:user_id])
    @review = review_type.new(params[:review])
    @review.giver = current_user
    @review.receiver = @user

    # see if its allowed
    if validate
      if @review.save
        flash[:notice] = t('review.create.success')
        handle_redirect
      else
        put_model_errors_to_flash(@review.errors)
        render :action => "new"
      end
    else
      redirect_to profile_path(@user.username)
    end
  end

  private
  
    # handle redirect after storing the review
    def handle_redirect
      if @review.invitation
        redirect_to reviews_user_path(@user.id)
      elsif !@review.message_id.blank?
        redirect_to user_message_path(current_user, @review.message_id)
      else
        redirect_to reviews_user_path(@user.id)
      end
    end

    # return the review type in form of class
    def review_type
      params[:type].blank? ? PositiveReview : params[:type].constantize
    end

    # validate new review
    def validate
      invitation = @review.invitation
      message_id = @review.message_id

      # currently through controller only recommendation and message review is allowed
      if !invitation and message_id.blank?
        return false
      end

      # only one recommendation is allowed
      if invitation and Review.recommendation(current_user.id, @user.id).count >= 1
        flash[:alert] = t('review.new.recommendation_exist', :receiver => @user.username)
        return false
      end

      # review based on message
      if !message_id.blank?
        # message type of review validation
        message = Message.find(message_id)

        # not allowed if:
        # - if message is blank
        # - not review-able by current user
        # - receiver is not participant in the message
        if message.blank? or
            !message.thread_starter? or
            !message.reviewable_by(current_user) or
            !message.is_sender_or_receiver?(@user)
          flash[:alert] = t('review.new.not_allowed', :receiver => @user.username)
          return false
        end
      end
      true
    end

end
