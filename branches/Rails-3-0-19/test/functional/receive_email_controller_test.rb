require 'test_helper'

class ReceiveEmailControllerTest < ActionController::TestCase

  test "post message reply from mailgun" do
    one = users(:one)
    message = messages(:donotoone1)
    assert_difference('Message.count') do
      post :post, {"Subject"=>"hahahah", "body-plain"=>"hahahahha\n", "timestamp"=>"1321007205",
        "X-Originating-Ip"=>"[202.152.194.153]", "token"=>"8nz-iswigifia7d-luj8ojqg97a50y1lxvzevldjbd0g93h556",
        "From"=>"Willy Ekasalim <Allan@email.com>", "Message-Id"=>"<CAAJdsWzTfes=LFHL9D4xGtMDJF8nLFSnqH9T5OF+E5W4Si30-Q@mail.gmail.com>",
        "Received"=>"by 10.68.60.103 with HTTP; Fri, 11 Nov 2011 02:26:44 -0800 (PST)", "from"=>"Willy Ekasalim <allan@email.com>", "sender"=>"#{one.email}",
        "signature"=>"ca93f75ccc346c0b37e4e2e3be95ecd6b94d403f3b90196260fcbc5be4454632", "stripped-text"=>"hahahahha", "To"=>"#{message.email_reply_address}",
        "Date"=>"Fri, 11 Nov 2011 17:26:44 +0700", "subject"=>"hahahah", "recipient"=>"#{message.email_reply_address}"}
      assert_response :success
    end
  end

  test "post trip new message from mailgun" do
    one = users(:one)
    trip = trips(:tokjak)
    assert_difference('Message.count') do
      post :post, {"Subject"=>"hahahah", "body-plain"=>"hahahahha\n", "timestamp"=>"1321007205",
        "X-Originating-Ip"=>"[202.152.194.153]", "token"=>"8nz-iswigifia7d-luj8ojqg97a50y1lxvzevldjbd0g93h556",
        "From"=>"Willy Ekasalim <Allan@email.com>", "Message-Id"=>"<CAAJdsWzTfes=LFHL9D4xGtMDJF8nLFSnqH9T5OF+E5W4Si30-Q@mail.gmail.com>",
        "Received"=>"by 10.68.60.103 with HTTP; Fri, 11 Nov 2011 02:26:44 -0800 (PST)", "from"=>"Willy Ekasalim <allan@email.com>", "sender"=>"#{one.email}",
        "signature"=>"ca93f75ccc346c0b37e4e2e3be95ecd6b94d403f3b90196260fcbc5be4454632", "stripped-text"=>"hahahahha", "To"=>"#{trip.email_reply_address}",
        "Date"=>"Fri, 11 Nov 2011 17:26:44 +0700", "subject"=>"hahahah", "recipient"=>"#{trip.email_reply_address}"}
      assert_response :success
    end
  end

  test "post seek new message from mailgun" do
    one = users(:one)
    seek = seeks(:tokjak)
    assert_difference('Message.count') do
      post :post, {"Subject"=>"hahahah", "body-plain"=>"hahahahha\n", "timestamp"=>"1321007205",
        "X-Originating-Ip"=>"[202.152.194.153]", "token"=>"8nz-iswigifia7d-luj8ojqg97a50y1lxvzevldjbd0g93h556",
        "From"=>"Willy Ekasalim <Allan@email.com>", "Message-Id"=>"<CAAJdsWzTfes=LFHL9D4xGtMDJF8nLFSnqH9T5OF+E5W4Si30-Q@mail.gmail.com>",
        "Received"=>"by 10.68.60.103 with HTTP; Fri, 11 Nov 2011 02:26:44 -0800 (PST)", "from"=>"Willy Ekasalim <allan@email.com>", "sender"=>"#{one.email}",
        "signature"=>"ca93f75ccc346c0b37e4e2e3be95ecd6b94d403f3b90196260fcbc5be4454632", "stripped-text"=>"hahahahha", "To"=>"#{seek.email_reply_address}",
        "Date"=>"Fri, 11 Nov 2011 17:26:44 +0700", "subject"=>"hahahah", "recipient"=>"#{seek.email_reply_address}"}
      assert_response :success
    end
  end
end
