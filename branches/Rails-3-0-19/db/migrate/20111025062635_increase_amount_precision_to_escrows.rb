class IncreaseAmountPrecisionToEscrows < ActiveRecord::Migration
  def self.up
    change_column :escrows, :amount, :decimal, :precision => 15, :scale => 2
  end

  def self.down
    change_column :escrows, :amount, :decimal, :precision => 8, :scale => 2
  end
end
