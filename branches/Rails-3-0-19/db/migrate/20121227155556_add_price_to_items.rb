class AddPriceToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :price, :decimal, :precision => 20, :scale => 2
  end

  def self.down
    remove_column :items, :price
  end
end
