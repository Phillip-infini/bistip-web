class ConvertStoriesAndSuggestedItemsToPointLogs < ActiveRecord::Migration
  def self.up
    sim = 0
    SuggestedItem.all.each do |si|
      AddForSuggestedItemLog.create!(:user_id => si.user_id, :amount => si.points, :data_id => si.id)
      sim += 1
    end
    puts 'Total suggested item migrated ' + sim.to_s

    sm = 0
    Story.all.each do |s|
      AddForStoryLog.create!(:user_id => s.user_id, :amount => s.points, :data_id => s.id)
      sm += 1
    end
    puts 'Total story migrated ' + sm.to_s
  end

  def self.down
  end
end
