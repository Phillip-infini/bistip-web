class AddRoleIdToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :role_id, :integer, :references => :user_roles
  end

  def self.down
    remove_column :users, :role_id
  end
end
