class CreatePriceUnits < ActiveRecord::Migration
  def self.up
    create_table :price_units do |t|
      t.string :name
      t.string :symbol
    end

    # pre populate
    price_unit = PriceUnit.new
    price_unit.name = 'Indonesian Rupiah'
    price_unit.symbol = 'IDR'
    price_unit.save

    price_unit = PriceUnit.new
    price_unit.name = 'US Dollar'
    price_unit.symbol = 'USD'
    price_unit.save
  end

  def self.down
    drop_table :price_units
  end
end
