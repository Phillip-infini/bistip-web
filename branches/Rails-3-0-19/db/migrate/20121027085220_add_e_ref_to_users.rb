class AddERefToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :eref, :string
  end

  def self.down
    remove_column :users, :eref
  end
end
