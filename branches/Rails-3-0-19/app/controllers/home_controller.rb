require 'net/http'
require 'csv'

class HomeController < ApplicationController

  before_filter :authenticate, :only => [:subscribe_wego_deals]

  before_filter :check_rest_password, :only => [:expire_cloud,
    :generate_trip_reminder,
    :create_bistip_approved_insurance_log,
    :create_bistip_rejected_insurance_log,
    :create_bistip_received_buyer_bank_transfer_insurance_log,
    :grant_trusted_badge,
    :send_statistics,
    :send_weekly_email_to_wego,
    :disable_user,
    :enable_user,
    :batch_disable_users,
    :batch_disable_users_upload,
    :geronimo]

  def expire_cloud
    # regenerate trending item
    TrendingItem.generate

    # expire route tag cloud cache
    expire_fragment('all_route')
    redirect_to root_path
  end

  def generate_trip_reminder
    # find all trip that gonna depart exactly 3 days from now
    Trip.depart_exactly_3_days_from_now.each do |trip|
      break unless TripReminderNotification.find_by_data_id(trip.id).blank?
      message_threads = Message.thread_starter_only.where('trip_id = ?', trip.id)
      message_threads.each do |message_thread|
        if Message.all_in_thread(message_thread.id).count > 4
          Notifier.delay.email_trip_reminder(trip, message_threads) if trip.user.user_configuration.email_trip_reminder
          TripReminderNotification.create!(:receiver => trip.user, :sender => trip.user, :data_id => trip.id)
          break
        end
      end
    end
    render :nothing => true
  end

  # utility to create bistip approved insurance log
  def create_bistip_approved_insurance_log
    insurance_id = params[:insurance_id]
    insurance = Insurance.find_by_id(insurance_id)

    if !insurance_id.blank? and insurance.current_state.eql? Insurance::STATE_BUYER_APPLIED
      BistipApprovedInsuranceLog.create!(:insurance_id => insurance_id)
      redirect_to root_path
    end
  end

  # utility to create bistip rejected insurance log
  def create_bistip_rejected_insurance_log
    insurance_id = params[:insurance_id]
    insurance = Insurance.find_by_id(insurance_id)

    if !insurance_id.blank? and insurance.current_state.eql? Insurance::STATE_BUYER_APPLIED
      BistipRejectedInsuranceLog.create!(:insurance_id => insurance_id)
      redirect_to root_path
    end
  end

  # utility to create bistip received transfer log
  def create_bistip_received_buyer_bank_transfer_insurance_log
    insurance_id = params[:insurance_id]
    insurance = Insurance.find_by_id(insurance_id)

    if !insurance_id.blank? and insurance.current_state.eql? Insurance::STATE_BISTIP_APPROVED
      BistipReceivedBuyerBankTransferInsuranceLog.create!(:insurance_id => insurance_id)
      redirect_to root_path
    end
  end

  # give trusted badge to a user
  def grant_trusted_badge
    user_id = params[:user_id]
    user = User.find_by_id(user_id)

    if !user.blank? and !user.has_trusted_badge?
      TrustedBadge.create!(:user_id => user_id)
      Notifier.delay.email_trusted_badge(user)
      redirect_to root_path
    end
  end

  # send statistics to our email
  def send_statistics

    # get facebook likes
    url = URI.parse('http://graph.facebook.com/194274033926856')
    req = Net::HTTP::Get.new(url.path)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    fb_json = JSON.parse(res.body)

    # get twitter followers
    url = URI.parse('http://api.twitter.com/1/users/show.json')
    req = Net::HTTP::Get.new(url.path + '?screen_name=bistip&include_entities=true')
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    tw_json = JSON.parse(res.body)

    #ContactUsNotifier.delay.email_statistics(User.count, Trip.count, Seek.count, Comment.count, Message.count, PositiveReputation.count, NegativeReputation.count, Item.count, fb_json["likes"], tw_json["followers_count"]);

    redirect_to root_path
  end

  def stats
    password = params[:password]

    if password == 'ayoayo_kitabisa'
      # continue to render html page
      render :layout => false
    else
      redirect_to root_path
    end
  end

  def stats_safepay
    password = params[:password]

    if password == 'ayoayo_kitabisa'
      # continue to render html page
      render :layout => false
    else
      redirect_to root_path
    end
  end

  def stats_users
    password = params[:password]

    if password == 'ayoayo_kitabisa'
      # continue to render html page
      render :layout => false
    else
      redirect_to root_path
    end
  end


  # action to subscribe to wego deals
  def subscribe_wego_deals
    if current_user.id == params[:user_id].to_i
      WegoTravelDealsSubscription.create(:user_id => params[:user_id])
      flash[:notice] = 'Thank you :)'
    end
    redirect_to dashboard_path
  end

  # send weekl email to wego about deals subscriber
  def send_weekly_email_to_wego
    ContactUsNotifier.delay.email_deals_subscriber_to_wego if WegoTravelDealsSubscription.find_all_by_email_sent(false).count > 0
    render :nothing => true
  end

  # enable user account
  def enable_user
    user = User.find_by_username(params[:username])
    user.update_attribute(:disabled, false)
    render :nothing => true
  end

  # disable user account
  def disable_user
    user = User.find_by_username(params[:username])
    user.update_attribute(:disabled, true)
    render :nothing => true
  end

  # disable user account
  def batch_disable_users
    render :layout => false
  end

  def batch_disable_users_upload
    disabled_count = 0
    all_count = 0
    file = params[:file]
    if file
      file.read.each do |line|
        email = line.chomp
        user = User.find_by_email(email)
        if !user.blank? and user.disabled == false and !user.email_validation_key.nil?
          user.update_attribute(:disabled, true)
          disabled_count = disabled_count + 1
        end
        all_count = all_count + 1
      end
    end
    flash[:alert] = "all count = #{all_count} - disabled_count = #{disabled_count}"
    redirect_to batch_disable_users_path(:password => REST_PASSWORD)
  end

  # all users username and email
  def geronimo
    csv_string = FasterCSV.generate do |csv|
      User.all.each do |user|
        csv << [user.username, user.email]
      end
    end

    send_data csv_string,
            :type => 'text/csv; charset=iso-8859-1; header=present',
            :disposition => "attachment; filename=users.csv" 
  end
  
end
