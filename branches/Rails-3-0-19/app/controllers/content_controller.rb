class ContentController < ApplicationController

  def event_item_suggestion_voting
    @suggested_items = SuggestedItem.active_only.created_between(SuggestedItem::COMP_START, SuggestedItem::COMP_END).find(:all,
      :select => 'suggested_items.id, suggested_items.name, suggested_items.origin_city_id, count(suggested_item_votes.id) as counter',
      :joins => 'LEFT OUTER JOIN suggested_item_votes ON suggested_item_votes.suggested_item_id = suggested_items.id LEFT OUTER JOIN users ON users.id = suggested_item_votes.user_id',
      :group => 'suggested_items.id',
      :conditions => ["users.disabled = false"],
      :order => 'counter DESC').paginate(:page => params[:page], :per_page => SuggestedItem::VOTING_PER_PAGE)
  end
end
