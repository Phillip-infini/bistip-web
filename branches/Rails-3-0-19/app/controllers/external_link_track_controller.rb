class ExternalLinkTrackController < ApplicationController

  def forward
    username = params[:username]
    url = params[:url]
    ref = params[:ref]

    user_id = nil

    if logged_in?
      user_id = current_user.id
    else
      user = User.find_by_username(username)
      user_id = user.id unless user.blank?
    end

    ExternalLinkTrack.create(:user_id => user_id, :url => url, :ref => ref)

    redirect_to url
  end

end
