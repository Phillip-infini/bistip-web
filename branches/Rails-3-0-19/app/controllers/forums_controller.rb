class ForumsController < ApplicationController

  layout 'forum'

  TOPICS_PER_PAGE = 10

  # show all forums
  def index
    @forums = Forum.active_only.order(:created_at)
  end

  # Show all children topic
  def show
    @forum = Forum.active_only.find(params[:id])
    @topics = @forum.topics.active_only.paginate(:page => params[:page], :per_page => TOPICS_PER_PAGE)
  end

end
