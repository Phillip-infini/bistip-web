class ConsoleController < ApplicationController

  before_filter :authenticate
  before_filter :has_admin_role

  layout 'console'

  ESCROWS_PER_PAGE = 10

  def index
    redirect_to console_escrow_path
  end

  def escrow
    @escrows = Escrow.order("id DESC").paginate(:per_page => ESCROWS_PER_PAGE, :page => params[:page])
  end

  def escrow_edit
    @escrow = Escrow.find(params[:id])
  end

  def escrow_update
    @escrow = Escrow.find(params[:id])
    @escrow.internal = params[:do_internal] == 'on' ? true : false

    if @escrow.update_attributes(params[:escrow])
      flash[:notice] = 'Escrow Updated'
      redirect_to console_escrow_path(:page => find_escrow_page(@escrow))
    else
      put_model_errors_to_flash(@escrow.errors)
      render :action => 'escrow_edit'
    end
  end

    # utility to create bistip payout log.
  def escrow_create_released_seller_payout_log
    escrow_id = params[:escrow_id]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_SELLER_REQUESTED_PAYOUT
      BistipReleasedSellerPayoutLog.create!(:escrow_id => escrow_id)
      flash[:notice] = 'Done :)'
      redirect_to console_escrow_path(:page => find_escrow_page(escrow))
    end
  end

  # utility to create bistip payout log.
  def escrow_create_released_buyer_payout_log
    escrow_id = params[:escrow_id]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_BUYER_REQUESTED_PAYOUT
      BistipReleasedBuyerPayoutLog.create!(:escrow_id => escrow_id)
      flash[:notice] = 'Done :)'
      redirect_to console_escrow_path(:page => find_escrow_page(escrow))
    end
  end

  # utility to create bistip received transfer log
  def escrow_create_received_buyer_bank_transfer_log
    escrow_id = params[:escrow_id]
    escrow = Escrow.find_by_id(escrow_id)

    if !escrow_id.blank? and escrow.current_state.eql? Escrow::STATE_BUYER_INITIATED
      BistipReceivedBuyerBankTransferLog.create!(:escrow_id => escrow_id)
      flash[:notice] = 'Done :)'
      redirect_to console_escrow_path(:page => find_escrow_page(escrow))
    end
  end

  # find payout method to a user
  def payout_method
    username = params[:username]
    user = User.find_by_username(username)
    @payout_methods = user.payout_methods unless user.blank?
  end

  def has_admin_role
    # only apply admin role for production environment
    if ::Rails.env == 'production'
      unless current_user.admin?
        redirect_to root_path
      end
    end
  end

  # find which page an escrow belongs to
  def find_escrow_page(escrow)
    escrow_page = Escrow.order("id DESC").index(escrow)
    index = escrow_page.blank? ? 1 : escrow_page + 1 #find array index of current_post and +1 because page start from 1
    ((index * 1.0)/ ESCROWS_PER_PAGE).ceil #make index float and divide by PER_PAGE and ceil the result
  end

  # suggested item index
  def suggested_item
    @suggested_items = SuggestedItem.order("id DESC").where('deleted = true').paginate(:per_page => 10, :page => params[:page])
  end

  # utility to approve suggested item
  def suggested_item_approve
    suggested_item = SuggestedItem.find(params[:id])
    suggested_item.update_attribute(:deleted, false)
    SuggestedItemApprovalNotification.create!(:sender_id => suggested_item.user.id, :receiver_id => suggested_item.user.id, :data_id => suggested_item.id)
    Notifier.delay.email_suggested_item_approval(suggested_item)
    redirect_to console_suggested_item_path
  end

  # story index
  def story
    @stories = Story.order("id DESC").where('deleted = true').paginate(:per_page => 10, :page => params[:page])
  end

  # utility to create bistip approved story
  def story_approve
    story = Story.find(params[:id])
    story.deleted = false
    story.save
    StoryApprovalNotification.create!(:sender_id => story.user.id, :receiver_id => story.user.id, :data_id => story.id)
    Notifier.delay.email_story_approval(story)
    redirect_to console_story_path
  end

end
