class StoriesController < ApplicationController

  # must be logged in
  before_filter :authenticate, :only => [:new, :create, :edit]

  def new
    @story = Story.new
    1.times { @story.story_pictures.build }
  end

  def show
    @story = Story.find(params[:id])
    @story_picture = StoryPicture.find_by_story_id(params[:id])
  end

  def index
    # Descending order
    @stories = Story.active_only.order("created_at DESC").paginate(:page => params[:page], :per_page => Story::PER_PAGE)
  end

  def create

    # associate story and the current user
    @story = current_user.stories.new(params[:story])
    if @story.save
      flash[:notice] = t('stories.create.story.success')
      ContactUsNotifier.delay.email_stories(@story.id, @story.user.username, @story.user.email, @story.title, @story.body)
      redirect_to dashboard_stories_path
    else
      # user must reupload picture if error happen
      flash.now[:alert] = t('general.reupload_picture') unless @story.story_pictures.blank?
      @story.story_pictures.clear
      1.times { @story.story_pictures.build }
      put_model_errors_to_flash(@story.errors)
      render :action => "new"
    end
  end

  def edit
    # associate story and the current user
    @story = current_user.stories.find(params[:id])

    # build if no picture before
    if (@story.story_pictures.size == 0)
      1.times { @story.story_pictures.build }
    end
  end

  def update
    @story = current_user.stories.find(params[:id])

    # awaiting for confirmation
    @story.deleted = true

    if @story.update_attributes(params[:story])

      # delete previous picture after update
      if @story.has_more_than_one_picture?
        story_picture = StoryPicture.order(:id).find_all_by_story_id(@story.id).first
        story_picture.destroy
      end
      flash[:notice] = t('stories.create.story.success')
      ContactUsNotifier.delay.email_stories(@story.id, @story.user.username, @story.user.email, @story.title, @story.body)
      redirect_to dashboard_stories_path
    else
      put_model_errors_to_flash(@story.errors)
      @story.story_pictures.clear
      1.times { @story.story_pictures.build }
      render :action => "edit"
    end
  end

end
