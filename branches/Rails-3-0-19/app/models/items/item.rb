# Item class represent a item of trip or seek
class Item < ActiveRecord::Base

  include ActionView::Helpers

  # constants
  NAME_MAXIMUM_LENGTH = 50
  NAME_MINIMUM_LENGTH = 3
  PICTURES_MAXIMUM = 3
  
  # relationship
  belongs_to :seek
  belongs_to :trip
  belongs_to :tip_unit
  belongs_to :price_unit
  has_many :pictures, :class_name => "ItemPicture", :foreign_key => 'item_id'
  accepts_nested_attributes_for :pictures, :allow_destroy => true, :reject_if => lambda { |a| a[:attachment].blank? }

  # validation
  validates :name, :presence => true, :length => { :minimum => NAME_MINIMUM_LENGTH, :maximum => NAME_MAXIMUM_LENGTH }
  validates :tip, :numericality => {:only_integer => true, :allow_nil => true, :less_than => 2000000001}
  validates :price, :numericality => {:allow_nil => true}

  # scope
  scope :sixty_days_ago, lambda {{ :conditions => ["created_at > ?", 60.days.ago]}}
  scope :name_contains, lambda { |keyword| {:conditions => ["match(name) against(?)", keyword]}}
  scope :trip_only, :conditions => 'trip_id IS NOT NULL'
  scope :seek_only, :conditions => 'seek_id IS NOT NULL'

  # events
  before_validation :clean_unused_data

  # other
  attr_accessor :offer_method
  
  # clean tip unit if no tip given
  def clean_unused_data
    if self.offer_method == ItemOfferMethod::SELL_PRICE
      self.tip_unit = nil
      self.tip = nil
    elsif
      self.offer_method == ItemOfferMethod::TIP
      self.price_unit = nil
      self.price = nil
    else
      self.tip_unit = nil
      self.tip = nil
      self.price_unit = nil
      self.price = nil
    end
  end

  # tip display with unit append to it
  def tip_with_unit
    unless self.tip.blank?
      if self.tip_unit.before_number?
        number_to_currency(self.tip.to_s, :unit => (self.tip_unit.symbol + ' '), :precision => 0)
      else
        self.tip.to_s + ' ' + self.tip_unit.symbol
      end
    end
  end

  # price display with unit append to it
  def price_with_unit
    unless self.price.blank?
      number_to_currency(self.price.to_s, :unit => (self.price_unit.symbol + ' '), :precision => 0)
    end
  end


  # do soft delete on item
  def destroy
    update_attribute(:deleted, true)
  end

  # get first picture
  def first_picture(type)
    self.pictures.blank? ? '/images/default_thumb_item.png' : self.pictures.first.attachment.url(type)
  end

  # override offer method accessor
  def offer_method
    if @offer_method.blank?
      if !self.tip.blank? and !self.tip_unit.blank?
        ItemOfferMethod::TIP
      elsif !self.price.blank? and !self.price_unit.blank?
        ItemOfferMethod::SELL_PRICE
      end
    else
      @offer_method
    end
  end
end
