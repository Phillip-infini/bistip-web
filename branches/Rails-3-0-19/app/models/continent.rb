# Continent class represent a Continent
# every continent has a placeholder or proxy in Country and City
class Continent < ActiveRecord::Base

  has_many :countries

  def ==(other)
    return self.id == other.id
  end

  # get list of countries id under this continent
  def get_country_ids
    countries_id = Array.new
    self.countries.each do |c|
      countries_id << c.id
    end
    countries_id
  end

  # get country placeholder for this continent
  def get_country_placeholder
    Country.find_by_code_and_name(Country::CONTINENT_CODE, self.name)
  end

  # get city placeholder for this continent
  def get_city_placeholder
    City.find_by_country_id(get_country_placeholder.id)
  end
end
