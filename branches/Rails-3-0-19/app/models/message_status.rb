# to represent message status
class MessageStatus
  
  # constants
  UNREAD = 'unread'
  NOT_HANDLED = 'not_handled'
  HAS_ESCROW = 'has_escrow'
  DEPARTURE_DATE_ASC = 'departure_date_asc'

  # return label of status and its value
  def self.map
    return [[I18n.t('message.filter.status.blank'), ''],[I18n.t('message.filter.status.has_escrow'), HAS_ESCROW], [I18n.t('message.filter.status.unread'), UNREAD], [I18n.t('message.filter.status.not_handled'), NOT_HANDLED]]
  end

  # build a scope
  def self.build(status, scope)
    if status == UNREAD
      scope.unread
    elsif status == NOT_HANDLED
      scope.not_handled
    elsif status == HAS_ESCROW
      scope.has_escrow
    end
  end
end
