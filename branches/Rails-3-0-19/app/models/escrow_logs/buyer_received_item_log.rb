class BuyerReceivedItemLog < EscrowLog

  # override
  def to_notification_link(current_user)
    log_state = Escrow.get_log_state(self)
    escrow = self.escrow

    # generate path to the current user message. Current user must be the recipient
    transaction = link_to(I18n.t("general.transaction"), path_helper.message_path(escrow.message.id))
    body = I18n.t("notification.title.escrow.#{log_state}",
      :seller => link_to(escrow.seller.username, path_helper.profile_path(escrow.seller.username)),
      :buyer => link_to(escrow.buyer.username, path_helper.profile_path(escrow.buyer.username)),
      :review => link_to(I18n.t('notification.field.review'), path_helper.reviews_user_path(:username => escrow.seller.username, :anchor => escrow.buyer_review.internal_link_id)))

    whole = I18n.t("notification.title.transaction",
      :transaction => transaction,
      :body => body)

    return whole
  end

  # generate string for email notifier
  def to_notifier_string
    log_state = Escrow.get_log_state(self)
    escrow = self.escrow

    I18n.t("notifier.escrow.buyer_received_item_with_review", :buyer => escrow.buyer.username, :seller => escrow.seller.username, :item => escrow.item, :amount => escrow.amount_with_currency, :review => escrow.buyer_review.body)
  end

  # override broadcast
  def broadcast
    super
    Notifier.delay.email_story_invite(self.escrow.buyer)
  end
  
end
