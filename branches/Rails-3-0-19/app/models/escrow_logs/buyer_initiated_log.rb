class BuyerInitiatedLog < EscrowLog

  def broadcast
    super
    Notifier.delay.email_escrow_indonesia_bank_account(self) if self.escrow.payment_method.class == IndonesiaBankTransfer
    ContactUsNotifier.delay.email_escrow_log(escrow)
  end
  
end
