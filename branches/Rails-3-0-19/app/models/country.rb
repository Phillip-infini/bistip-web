# Country class represent a Country
class Country < ActiveRecord::Base

  CONTINENT_CODE = '<continent>'

  # relationships
  belongs_to :continent
  
  def ==(other)
    return self.id == other.id
  end
end
