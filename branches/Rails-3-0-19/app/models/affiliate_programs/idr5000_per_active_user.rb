class IDR5000PerActiveUser < AffiliateProgram

  PRICE_PER_UNIT = 5000
  
  # get unit count of the program during the period
  def unit_count(start_period, end_period)
    affiliate = self.affiliate
    User.created_between(start_period, end_period).active_only.affiliate_is(affiliate).count
  end

  # get value count of the program during the period
  def value_count(start_period, end_period)
    unit_count(start_period, end_period) * PRICE_PER_UNIT
  end

  # generate title label
  def title_label
    I18n.t('affiliate.programs.idr5000_per_active_user')
  end
  
end
