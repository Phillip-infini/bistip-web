class AffiliateProgram < ActiveRecord::Base

  belongs_to :affiliate

  # the start of affiliate program is July 2012
  REPORT_START_MONTH = DateTime.new(2012, 4, 1)

  # get unit count of the program during the period
  def unit_count(start_period, end_period)
    raise NotImplementedError
  end

  # get value count of the program during the period
  def value_count(start_period, end_period)
    raise NotImplementedError
  end

  # generate title label
  def title_label
    raise NotImplementedError
  end
  
end
