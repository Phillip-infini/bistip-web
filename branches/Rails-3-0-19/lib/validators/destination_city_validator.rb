# validator class to handle city location

class DestinationCityValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.blank?
      if record.destination_location.blank?
        record.errors[:destination_location] << (options[:message] || I18n.t('errors.location_not_given'))
      else
        record.errors[:destination_location] << (options[:message] || I18n.t('errors.location_not_valid', :value => record.destination_location))
      end
    end
  end
end
