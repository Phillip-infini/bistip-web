# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.bistip.com"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #

  group(:sitemaps_path => 'sitemaps/', :filename => :statics) do
    add '/about', :changefreq => 'weekly'
    add '/terms_of_service', :changefreq => 'weekly'
    add '/privacy_policy', :changefreq => 'weekly'
    add '/guide', :changefreq => 'weekly'
    add '/guide_all', :changefreq => 'weekly'
    add '/guide_traveler', :changefreq => 'weekly'
    add '/guide_requester', :changefreq => 'weekly'
    add '/how', :changefreq => 'weekly'
    add '/faq', :changefreq => 'weekly'
    add '/new', :changefreq => 'weekly'
    add '/media', :changefreq => 'monthly'
    add '/event_bistipes', :changefreq => 'never'
    add '/event_bistipes_1', :changefreq => 'never'
    add '/event_bistipes_2', :changefreq => 'never'
    add '/event_bistipes_3', :changefreq => 'never'
    add '/event_bistipes_4', :changefreq => 'never'
    add '/event_slogan', :changefreq => 'never'
    add '/event_blog', :changefreq => 'never'
    add '/jobs', :changefreq => 'monthly'
    add '/safepay', :changefreq => 'weekly'
    add '/awards', :changefreq => 'monthly'
    add '/bistip_point', :changefreq => 'monthly'
    add '/api', :changefreq => 'monthly'
    add '/contact_us', :changefreq => 'yearly'
    add '/insurances', :changefreq => 'weekly'
    add '/search_user', :changefreq => 'yearly'
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :dinamics) do
    add '/search_traveler', :changefreq => 'always'
    add '/search_request_post', :changefreq => 'always'
    add '/stories', :changefreq => 'monthly'
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :trips) do
    def to_param
      ori_city = Trip.origin_city.all_cities? ? Trip.origin_city.country.name : Trip.origin_city.name
      dest_city = Trip.destination_city.all_cities? ? Trip.destination_city.country.name : Trip.destination_city.name

      I18n.t('trip.index.url', :id => id, :flag => TrackLink::BS_V1,:ori => ori_city, :dest => dest_city).parameterize
    end

    Trip.find_each do |trip|
    add trip_path(trip), :changefreq => 'weekly'
    end
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :seeks) do

    def to_param
      origin_city_name = Seek.origin_city.name
      origin_city_name = Seek.origin_city.country.name if Seek.origin_city.all_cities?
      origin_city_name = I18n.t('seek.field.label.from_anywhere_short') if (Seek.origin_city.name == City::ANYWHERE)

      I18n.t('seek.url', :id => id, :flag => TrackLink::BS_V1, :ori => origin_city_name, :dest => Seek.destination_city.name).parameterize
    end

    Seek.find_each do |seek|
    add seek_path(seek), :changefreq => 'weekly'
    end
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :users) do
    User.find_each do |user|
    add user_path(user), :changefreq => 'weekly'
    end
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :suggested_items) do


    def to_param
      city = SuggestedItem.origin_city
      label_city = city.all_cities? ? city.country.name : city.name

      I18n.t('suggested_items.show.url', :id => id, :flag => TrackLink::BS_V1, :item => SuggestedItem.name, :location => label_city).parameterize
    end

    SuggestedItem.find_each do |suggested_item|
    add suggested_item_path(suggested_item), :changefreq => 'weekly'
    end
  end

  group(:sitemaps_path => 'sitemaps/', :filename => :stories) do
    def to_param
      "#{id} #{title}".parameterize
    end

    Story.find_each do |story|
      add story_path(story), :changefreq => 'weekly'
    end
  end

end
